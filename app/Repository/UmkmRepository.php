<?php

namespace App\Repository;

use App\Models\TbKbli;
use App\Models\TbPekerjaan;
use App\Models\TbPerusahaan;
use App\Models\TbPerusahaanCabang;
use App\Models\TbPerusahaanCabangTr;
use App\Models\TbPerusahaanDetil;
use App\Models\TbPerusahaanKbli;
use App\Models\TbPerusahaanKebutuhanKbli;
use App\Models\TbPerusahaanKebutuhanProduk;
use App\Models\TbPerusahaanKebutuhanProdukTr;
use App\Models\TbPerusahaanLokasi;
use App\Models\TbPerusahaanProduk;
use App\Models\TbPerusahaanProdukTr;
use App\Models\TbPerusahaanStatus;
use App\Models\TbPerusahaanTr;
use App\Models\TbPrsKlbi;
use App\Models\TbUmkm;
use App\Models\TbUmkmKbli;
use App\Models\TbUmkmPekerjaan;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;
use File;
use PhpParser\Node\Stmt\TryCatch;

class UmkmRepository implements IUmkmRepository{

    public function GetUmkmPerusahaans($type)
    {
        try {
            //code...
            $sessionUser = Auth::user();

            $perusahaans = TbPerusahaan::join('sde.tb_adm_kabkot','sde.tb_perusahaan.id_adm_kabkot','sde.tb_adm_kabkot.id_adm_kabkot')
                                        ->join('sde.tb_adm_provinsi','sde.tb_adm_kabkot.id_adm_provinsi','sde.tb_adm_provinsi.id_adm_provinsi')
                                        ->join('sde.tb_perusahaan_status','sde.tb_perusahaan.id_perusahaan','sde.tb_perusahaan_status.id_perusahaan')
                                        ->join('sde.tb_perusahaan_lokasi','sde.tb_perusahaan.id_perusahaan','sde.tb_perusahaan_lokasi.id_perusahaan')
                                        ->join('sde.tb_perusahaan_tipe','tb_perusahaan.id_tipe','tb_perusahaan_tipe.id_perusahaan_tipe')
                                        ->join('public.tb_user','sde.tb_perusahaan_status.created_by','public.tb_user.id_user');

            if($type == 'perusahaan') {  // chek condition perusahan and UMKM
                $perusahaans = $perusahaans->whereIn('sde.tb_perusahaan.id_tipe',array(1,2)); // 1 == PMA , 2 == PMN

            }else{
                $perusahaans = $perusahaans->where('sde.tb_perusahaan.id_tipe','=',3);
            }

            $perusahaans = $perusahaans->get(['sde.tb_perusahaan.id_perusahaan','sde.tb_perusahaan.nib','sde.tb_perusahaan.nama','tb_perusahaan_lokasi.alamat','sde.tb_perusahaan_lokasi.no_telp'
                                                ,'sde.tb_perusahaan.status as status_perusahaan','sde.tb_perusahaan.status_verifikasi'
                                                ,'sde.tb_adm_kabkot.nama as nama_kab','sde.tb_adm_provinsi.nama as nama_provinsi','public.tb_user.*','tb_perusahaan_tipe.kode as kode_tipe_perusahaan']);

            $dtPerusahaans = array();
            if(count($perusahaans) > 0){
                foreach ($perusahaans as $perusahaan) {
                    $tmpArr = array(
                        'IdPerusahaan' => $perusahaan->id_perusahaan,
                        'Tipe'  => $perusahaan->kode_tipe_perusahaan,
                        'Nib' => $perusahaan->nib,
                        'NamaPerusahaan'    => $perusahaan->nama,
                        'Alamat'    => $perusahaan->alamat,
                        'Phone' => $perusahaan->no_telp,
                        'Cp'    => $perusahaan->cp,
                        'Status'    => $perusahaan->status_perusahaan,
                        'StatusVerifikasi'    => $perusahaan->status_verifikasi,
                        'FullName'  => $perusahaan->first_name.' '.$perusahaan->middle_name.' '.$perusahaan->last_name,
                        'KabKot'    => $perusahaan->nama_kab,
                        'Provinsi'  => $perusahaan->nama_provinsi
                    );

                    $dtPerusahaans[] = $tmpArr;
                }
            }
            return $dtPerusahaans;

        } catch (QueryException $qe) {
            //throw $th;
            return  $qe->getMessage();
        }
    }

    public function AddUmkmPerusahaan($request)
    {
       // echo json_encode($request);


        DB::beginTransaction();
        try {
            //code...
            $idUser = Auth::user()->id_user;

            $maxIdPerusahaan = TbPerusahaan::max('id_perusahaan') + 1;

            // Begin Table Perusahaan
            $Perusahaan = new TbPerusahaan();
            $Perusahaan->id_perusahaan = $maxIdPerusahaan;
            $Perusahaan->id_tipe =  $request->IdTipe;
            $Perusahaan->id_adm_kabkot =  $request->IdAdmKabKot;
            $Perusahaan->nib =  $request->Nib;
            $Perusahaan->kd_pa =  "-";
            $Perusahaan->kd_kmk =  "-";
            $Perusahaan->nama =  $request->NamaPerusahaan;
            $Perusahaan->status =  DOCUMENT_CREATE;
            $Perusahaan->status_verifikasi =  DOCUMENT_NO_VERIFIKASI;

            $Perusahaan->save();

            // Begin Table Perusahaan Lokasi
            $maxIdPerusahaanLokasi = TbPerusahaanLokasi::max('id_perusahaan_lokasi') + 1;
            $PerusahaanLokasi = new TbPerusahaanLokasi();
            $PerusahaanLokasi->id_perusahaan = $Perusahaan->id_perusahaan;
            $PerusahaanLokasi->id_perusahaan_lokasi = $maxIdPerusahaanLokasi;
            $PerusahaanLokasi->id_tipe = 1; // Hardcode Perusahaan Pusat, cabang == 2
            $PerusahaanLokasi->id_adm_kabkot =  $request->IdAdmKabKot;
            $PerusahaanLokasi->alamat =  $request->Alamat;
            $PerusahaanLokasi->no_telp =  $request->NoTlp;
            $PerusahaanLokasi->no_fax =  $request->NoFax;
            $PerusahaanLokasi->url_web =  $request->UrlWeb;
            $PerusahaanLokasi->email =  $request->Email;
            $PerusahaanLokasi->cp =  $request->Cp;
            $PerusahaanLokasi->lat =  $request->Lat;
            $PerusahaanLokasi->lon =  $request->Lon;
            $PerusahaanLokasi->created_by =  $idUser;
            $PerusahaanLokasi->created_date =  date('Y-m-d H:i:s');

            $PerusahaanLokasi->save();

            $PerusahaanStatus = new TbPerusahaanStatus();
            $PerusahaanStatus->id_perusahaan = $maxIdPerusahaan;
            $PerusahaanStatus->status = DOCUMENT_CREATE;
            $PerusahaanStatus->status_proses = DOCUMENT_CREATE;
            $PerusahaanStatus->keterangan = DOCUMENT_CREATE_NOTE;
            $PerusahaanStatus->created_by = $idUser;
            $PerusahaanStatus->created_date = Carbon::now()->toDateTimeString();
            $PerusahaanStatus->save();

            /* insert perusahaan kbli */
            if($request['ArrKbliSelected'] ?? false){
                $idKbliKlmpToArr = explode(",",$request['ArrKbliSelected']);
                $countIdKbliArr = count($idKbliKlmpToArr);
                $TbPerusahaanKbli = new TbPerusahaanKbli();
                $maxIdPerusahaanKbli = TbPerusahaanKbli::max('id_perusahaan_kbli') + 1;
                if($countIdKbliArr == 1){

                    $TbPerusahaanKbli->id_perusahaan_kbli = $maxIdPerusahaanKbli;
                    $TbPerusahaanKbli->id_perusahaan = $Perusahaan->id_perusahaan;
                    $TbPerusahaanKbli->id_kbli = $request['ArrKbliSelected'];

                    $TbPerusahaanKbli->save();

                }else{

                    $allItemPerusahaanKbli = []; // temporary array for insert bulk
                    foreach($idKbliKlmpToArr as $item){
                        $TbPerusahaanKbliArr = new TbPerusahaanKbli();
                        $TbPerusahaanKbliArr->id_perusahaan_kbli = $maxIdPerusahaanKbli;
                        $TbPerusahaanKbliArr->id_perusahaan = $Perusahaan->id_perusahaan;
                        $TbPerusahaanKbliArr->id_kbli = $item;
                        $maxIdPerusahaanKbli++;
                        $allItemPerusahaanKbli[] = $TbPerusahaanKbliArr->attributesToArray();
                    }
                    $TbPerusahaanKbli::insert($allItemPerusahaanKbli);
                }

            }


            /*
            if($request->hasFile('FileName')){

                $folder = public_path('upload/Perusahaan');
                if (!Storage::exists($folder)) {
                    //Storage::makeDirectory($folder, 0777, true, true);
                }
                 $file =  $request->file('FileName');
                 $filename  = $file->getClientOriginalName();
                 $rename = time()."_".$filename;
                 $file->move(public_path('upload/Perusahaan'), $rename);
                 $Perusahaan->file_name = $rename;

            }else{
                $Perusahaan->file_name = '-';
            } */


            /* $maxIdPerusahaanTr = TbPerusahaanTr::max("id_perusahaan_tr")+1;

            $PerusahaanTr = new TbPerusahaanTr();
            $PerusahaanTr->id_perusahaan_tr = $maxIdPerusahaanTr;
            $PerusahaanTr->id_perusahaan = $Perusahaan->id_perusahaan;
            $PerusahaanTr->kd_bahasa = "en";
            if($request->NmPerusahaanEn != "" || $request->NmPerusahaanEn != null){

                $PerusahaanTr->nama = $request->NmPerusahaanEn;
            }else{
                $PerusahaanTr->nama = $request->NamaPerusahaan;
            }

            $PerusahaanTr->save(); */

            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Disimpan"
            );
            return $responses;
        } catch (QueryException $ex) {
            //throw $th;
            $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    public function GetUmkmPerusahaan($request)
    {
        try {
            //code...
            $perusahaan = TbPerusahaan::leftJoin("sde.tb_perusahaan_tr","sde.tb_perusahaan.id_perusahaan","sde.tb_perusahaan_tr.id_perusahaan")
                                        -> join("tb_user","sde.tb_perusahaan.created_by","tb_user.id_user")
                                        -> join("sde.tb_adm_kabkot","sde.tb_perusahaan.id_adm_kabkot","sde.tb_adm_kabkot.id_adm_kabkot")
                                        -> join("sde.tb_adm_provinsi","sde.tb_adm_kabkot.id_adm_provinsi","sde.tb_adm_provinsi.id_adm_provinsi")
                                        -> where("sde.tb_perusahaan.id_perusahaan","=",$request['IdPerusahaan'])
                                        -> first(["sde.tb_perusahaan.id_perusahaan","sde.tb_perusahaan.nama","sde.tb_perusahaan.id_tipe","sde.tb_perusahaan.id_adm_kabkot","sde.tb_perusahaan.nib"
                                                   ,"sde.tb_perusahaan.nama","sde.tb_perusahaan.alamat","sde.tb_perusahaan.no_telp","sde.tb_perusahaan.no_fax","sde.tb_perusahaan.url_web"
                                                   ,"sde.tb_perusahaan.email","sde.tb_perusahaan.file_name","sde.tb_perusahaan.cp","sde.tb_perusahaan.lon","sde.tb_perusahaan.lat"
                                                   ,"sde.tb_perusahaan.status_verifikasi","sde.tb_perusahaan.status"
                                                   ,"sde.tb_perusahaan_tr.id_perusahaan_tr","sde.tb_perusahaan_tr.kd_bahasa","sde.tb_perusahaan_tr.nama as nama_translate","sde.tb_adm_kabkot.nama as nama_kabkot"
                                                   ,"sde.tb_adm_kabkot.id_adm_provinsi","sde.tb_adm_provinsi.nama as nama_provinsi"

                                                ]);
            $dtPerusahaan = array();
            if($perusahaan !== null){
                $nameOfFile = "";
                if($perusahaan->file_name == null or $perusahaan->file_name == '-'){
                    $nameOfFile = url('admins/assets/img/no_image.jpg');
                }else{
                    if(file_exists(public_path('upload/Perusahaan/'.$perusahaan->file_name))){
                        $nameOfFile = url('upload/Perusahaan/'.$perusahaan->file_name);
                    }else{
                        $nameOfFile = url('admins/assets/img/no_image.jpg');
                    }
                }
                $tmpArr = array(
                    'IdPerusahaan'  => $perusahaan->id_perusahaan,
                    'NamaPerusahaan'  => $perusahaan->nama,
                    'Nib'  => $perusahaan->nib,
                    'IdTipe'  => $perusahaan->id_tipe,
                    'IdAdmKabkot'  => $perusahaan->id_adm_kabkot,
                    'Alamat'  => $perusahaan->alamat,
                    'Cp'  => $perusahaan->cp,
                    'NoTelp'  => $perusahaan->no_telp,
                    'NoFax'  => $perusahaan->no_fax,
                    'UrlWeb'  => $perusahaan->url_web,
                    'Email'  => $perusahaan->email,
                    'FileName'  => $nameOfFile,
                    'Lat'  => $perusahaan->lat,
                    'Lon'  => $perusahaan->lon,
                    'StatusVerifikasi'  => $perusahaan->status_verifikasi,
                    'Status'  => $perusahaan->status,
                    'IdPerusahaanTr'  => $perusahaan->id_perusahaan_tr,
                    'KodeBahasa'  => $perusahaan->kd_bahasa,
                    'NameTranslate'  => $perusahaan->nama_translate,
                    'NamaKabKot'  => $perusahaan->nama_kabkot,
                    'IdAdmProvinsi'  => $perusahaan->id_adm_provinsi,
                    'NamaProvinsi'  => $perusahaan->nama_provinsi,
                );

                $dtPerusahaan = $tmpArr;
            }

            return $dtPerusahaan;


        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function EditPerusahaanUmkm($request)
    {
        DB::beginTransaction();
        try {
            //code...
            $idUser = Auth::user()->id_user;
            $Perusahaan = TbPerusahaan::where('id_perusahaan','=',$request['IdPerusahaan'])->first();

            $Perusahaan->id_tipe =  $request->IdTipe;
            $Perusahaan->id_adm_kabkot =  $request->IdAdmKabKot;
            $Perusahaan->nib =  $request->Nib;
            $Perusahaan->kd_pa =  "-";
            $Perusahaan->kd_kmk =  "-";
            $Perusahaan->nama =  $request->NamaPerusahaan;
            $Perusahaan->alamat =  $request->Alamat;
            $Perusahaan->no_telp =  $request->NoTlp;
            $Perusahaan->no_fax =  $request->NoFax;
            $Perusahaan->url_web =  $request->UrlWeb;
            $Perusahaan->email =  $request->Email;
            $Perusahaan->cp =  $request->Cp;
            $Perusahaan->status =  1;
            $Perusahaan->status_verifikasi =  1;
            $Perusahaan->lat =  $request->Lat;
            $Perusahaan->lon =  $request->Lon;
            $Perusahaan->updated_by =  $idUser;
            $Perusahaan->updated_date =  date('Y-m-d H:i:s');

            if($request->hasFile('FileName')){

                $folder = public_path('upload/Perusahaan');
                if (!Storage::exists($folder)) {
                    Storage::makeDirectory($folder, 0777, true, true);
                }
                 $file =  $request->file('FileName');
                 $filename  = $file->getClientOriginalName();
                 $rename = time()."_".$filename;
                 $file->move(public_path('upload/Perusahaan'), $rename);
                 $Perusahaan->file_name = $rename;

            }else{
                if($Perusahaan->file_name == "" || $Perusahaan->file_name == "-"){
                    $Perusahaan->file_name = "-";
                }else{
                    $Perusahaan->file_name = $Perusahaan->file_name;
                }
            }

            $Perusahaan->save();

            $PerusahaanTr = TbPerusahaanTr::where('id_perusahaan','=',$request['IdPerusahaan'])->first();
            if($PerusahaanTr == null){
                 $maxIdPerusahaanTr = TbPerusahaanTr::max("id_perusahaan_tr")+1;

                $PerusahaanTrInsert = new TbPerusahaanTr();
                $PerusahaanTrInsert->id_perusahaan_tr = $maxIdPerusahaanTr;
                $PerusahaanTrInsert->id_perusahaan = $Perusahaan->id_perusahaan;
                $PerusahaanTrInsert->kd_bahasa = "en";
                if($request->NmPerusahaanEn != "" || $request->NmPerusahaanEn != null){

                    $PerusahaanTrInsert->nama = $request->NmPerusahaanEn;
                }else{
                    $PerusahaanTrInsert->nama = $request->NamaPerusahaan;
                }

                $PerusahaanTrInsert->save();

            }else{
                //$PerusahaanTr->id_perusahaan = $Perusahaan->id_perusahaan;
                $PerusahaanTr->kd_bahasa = "en";
                if($request->NmPerusahaanEn != "" || $request->NmPerusahaanEn != null){

                    $PerusahaanTr->nama = $request->NmPerusahaanEn;
                }else{
                    $PerusahaanTr->nama = $request->NamaPerusahaan;
                }

                $Perusahaan->save();
            }


            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Dirubah"
            );
            return $responses;

        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
             $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    public function DeletePerusahaanUmkm($request)
    {
        DB::beginTransaction();
        try {
            //code...
            $cekTablePerusahaanCabang = TbPerusahaanCabang::where('id_perusahaan','=',$request['IdPerusahaan']);
            if($cekTablePerusahaanCabang->count() > 0){
                 $arr = array(
                    'Status'    => false,
                    'Message'   => 'Tidak dapat Menghapus Perusahaan Terdapat Data Cabang'
                );

                return $arr;
            }

            $cekTablePerusahaanKbli = TbPerusahaanKbli::where('id_perusahaan','=',$request['IdPerusahaan']);
            if($cekTablePerusahaanKbli->count() > 0){
                 $arr = array(
                    'Status'    => false,
                    'Message'   => 'Tidak dapat Menghapus Perusahaan Terdapat Data Kbli'
                );

                return $arr;
            }




            $PerusahaanTr = TbPerusahaanTr::where('id_perusahaan',$request['IdPerusahaan']);
            if($PerusahaanTr->count() > 0){
                $PerusahaanTrDel = TbPerusahaanTr::where('id_perusahaan',$request['IdPerusahaan'])->delete();
            }

            $deletePerusahaan = TbPerusahaan::where('id_perusahaan',$request['IdPerusahaan'])->delete();

            if($deletePerusahaan){

                $arr = array(
                    'Status'    => true,
                    'Message'   => 'Data Berhasil Dihapus'
                );
                DB::commit();
                return $arr;
            }else{
                $arr = array(
                    'Status'    => false,
                    'Message'   => 'Data Gagal dihapus'
                );
                    DB::rollback();
                return $arr;
            }
        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
             $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    /* Cabang Perusahaan */
    public function GetUmkmCabangPerusahaans($request)
    {
        try {
            //code...


            $CabangPerusahaans = TbPerusahaanLokasi::join("sde.tb_perusahaan","sde.tb_perusahaan_lokasi.id_perusahaan","sde.tb_perusahaan.id_perusahaan")
                                                    -> join ("tb_user","tb_perusahaan_lokasi.created_by","tb_user.id_user")
                                                    -> where ("sde.tb_perusahaan_lokasi.id_perusahaan","=",$request["IdPerusahaan"])
                                                    -> get(["sde.tb_perusahaan_lokasi.id_perusahaan_lokasi","sde.tb_perusahaan_lokasi.id_perusahaan"
                                                            , "sde.tb_perusahaan_lokasi.alamat","sde.tb_perusahaan_lokasi.no_telp","sde.tb_perusahaan_lokasi.no_fax","sde.tb_perusahaan_lokasi.url_web"
                                                            , "sde.tb_perusahaan_lokasi.email","sde.tb_perusahaan_lokasi.cp","sde.tb_perusahaan_lokasi.lat","sde.tb_perusahaan_lokasi.lon"
                                                            , "tb_user.first_name", "tb_user.middle_name","tb_user.last_name"]);
            $dtCabangPerusahaans = array();
            if($CabangPerusahaans->count() > 0){
                foreach($CabangPerusahaans as $CabangPerusahaan){

                    $tmpArr = array(
                        "IdPerusahaanCabang"    => $CabangPerusahaan->id_perusahaan_lokasi,
                        "IdPerusahaan"    => $CabangPerusahaan->id_perusahaan,
                        /* "Nama"    => $CabangPerusahaan->nama, */
                        "Alamat"    => $CabangPerusahaan->alamat,
                        "NoTelp"    => $CabangPerusahaan->no_telp,
                        "NoFax"    => $CabangPerusahaan->no_fax,
                        "UrlWeb"    => $CabangPerusahaan->url_web,
                        "Email"    => $CabangPerusahaan->email,
                        "Cp"    => $CabangPerusahaan->cp,
                        "Lon"    => $CabangPerusahaan->lon,
                        "Lat"    => $CabangPerusahaan->lat,
                        "CreatedBy"    => $CabangPerusahaan->first_name." ".$CabangPerusahaan->middle_name." ".$CabangPerusahaan->last_name,
                    );

                    $dtCabangPerusahaans[] = $tmpArr;
                }
            }

            return $dtCabangPerusahaans;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }

    }

    public function AddUmkmCabangPerusahaan($request)
    {
        DB::beginTransaction();
        try {
            $idUser = Auth::user()->id_user;

            $maxIdPerusahaanCabang = TbPerusahaanCabang::max('id_perusahaan_cabang') + 1;

            $Perusahaan = new TbPerusahaanCabang();
            $Perusahaan->id_perusahaan_cabang = $maxIdPerusahaanCabang;
            $Perusahaan->id_perusahaan = $request->IdPerusahaan;
            //$Perusahaan->id_tipe =  $request->IdTipe;
            $Perusahaan->id_adm_kabkot =  $request->IdAdmKabKot;
            //$Perusahaan->nib =  $request->Nib;
            /* $Perusahaan->kd_pa =  "-";
            $Perusahaan->kd_kmk =  "-"; */
            $Perusahaan->nama =  $request->NamaPerusahaan;
            $Perusahaan->alamat =  $request->Alamat;
            $Perusahaan->no_telp =  $request->NoTlp;
            $Perusahaan->no_fax =  $request->NoFax;
            $Perusahaan->url_web =  $request->UrlWeb;
            $Perusahaan->email =  $request->Email;
            $Perusahaan->cp =  $request->Cp;
            /* $Perusahaan->status =  1;
            $Perusahaan->status_verifikasi =  1; */
            $Perusahaan->lat =  $request->Lat;
            $Perusahaan->lon =  $request->Lon;
            $Perusahaan->created_by =  $idUser;
            $Perusahaan->created_date =  date('Y-m-d H:i:s');


            if($request->hasFile('FileName')){

                $folder = public_path('upload/PerusahaanCabang');
                if (!Storage::exists($folder)) {
                    Storage::makeDirectory($folder, 0777, true, true);
                }
                 $file =  $request->file('FileName');
                 $filename  = $file->getClientOriginalName();
                 $rename = time()."_".$filename;
                 $file->move(public_path('upload/Perusahaan'), $rename);
                 $Perusahaan->file_name = $rename;

            }else{
                $Perusahaan->file_name = '-';
            }
            $Perusahaan->save();
            $maxIdPerusahaanTr = TbPerusahaanCabangTr::max("id_perusahaan_cabang_tr") + 1;

            $PerusahaanTr = new TbPerusahaanCabangTr();
            $PerusahaanTr->id_perusahaan_cabang_tr = $maxIdPerusahaanTr;
            $PerusahaanTr->id_perusahaan_cabang = $Perusahaan->id_perusahaan_cabang;
            $PerusahaanTr->kd_bahasa = "en";
            if($request->NmPerusahaanEn != "" || $request->NmPerusahaanEn != null){

                $PerusahaanTr->nama = $request->NmPerusahaanEn;
            }else{
                $PerusahaanTr->nama = $request->NamaPerusahaan;
            }

            $PerusahaanTr->save();

            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Disimpan"
            );
            return $responses;
        } catch (QueryException $ex) {
            //throw $th;
            $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    /* Perusahaan KBLI */
    public function GetUmkmPerusahaanKblis($request)
    {
        try {
            //code...

            $PerusahaanKblis = TbPerusahaanKbli::join("sde.tb_perusahaan","sde.tb_perusahaan_kbli.id_perusahaan","sde.tb_perusahaan.id_perusahaan")
                                                -> join ("sde.tb_kbli","sde.tb_perusahaan_kbli.id_kbli","sde.tb_kbli.id_kbli")
                                                -> where ("sde.tb_perusahaan_kbli.id_perusahaan","=",$request['IdPerusahaan'])
                                                -> get(["sde.tb_perusahaan_kbli.id_perusahaan_kbli","sde.tb_perusahaan_kbli.id_perusahaan"
                                                        ,"sde.tb_kbli.id_kbli","sde.tb_kbli.id_kbli_parent","sde.tb_kbli.tahun","sde.tb_kbli.kode","sde.tb_kbli.nama"
                                                    ]);
            $dtPerusahaanKblis = array();
            if($PerusahaanKblis->count() > 0){
                foreach($PerusahaanKblis as $PerusahaanKbli){
                    $tmpArr = array(
                        'IdPerusahaanKbli'  => $PerusahaanKbli->id_perusahaan_kbli,
                        'IdPerusahaan'  => $PerusahaanKbli->id_perusahaan,
                        'IdKbli'  => $PerusahaanKbli->id_kbli,
                        'IdKbliParent'  => $PerusahaanKbli->id_kbli_parent,
                        'Tahun'  => $PerusahaanKbli->tahun,
                        'Kode'  => $PerusahaanKbli->kode,
                        'Nama'  => $PerusahaanKbli->nama,
                    );

                    $dtPerusahaanKblis[] = $tmpArr;

                }

            }

            return $dtPerusahaanKblis;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function AddPerusahaanKbli($request)
    {
        DB::beginTransaction();
        try {
            //code...
            $maxIdPerusahaanKbli = TbPerusahaanKbli::max("id_perusahaan_kbli") + 1;
            $TbPerusahaanKbli = new TbPerusahaanKbli();
            $TbPerusahaanKbli->id_perusahaan_kbli = $maxIdPerusahaanKbli;
            $TbPerusahaanKbli->id_perusahaan = $request->IdPerusahaan;
            $TbPerusahaanKbli->id_kbli = $request->IdKbli;

            $TbPerusahaanKbli->save();
            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Disimpan"
            );
            return $responses;

        } catch (QueryException $ex) {
            //throw $th;
             $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;

        }
    }

    public function GetPerusahaanKbliByIdPerusahaan($request)
    {
        try {
            //code...
            $tbPerusahaanKblis = TbPerusahaanKbli::join("sde.tb_kbli","sde.tb_perusahaan_kbli.id_kbli","sde.tb_kbli.id_kbli")
                                                -> where("sde.tb_perusahaan_kbli.id_perusahaan","=",$request['IdPerusahaan'])
                                                -> get (["sde.tb_kbli.*","sde.tb_perusahaan_kbli.id_perusahaan_kbli"]);
            $dtPerusaahnKbliArr = array();
            if($tbPerusahaanKblis->count() > 0){
                foreach($tbPerusahaanKblis as $tbPerusahaanKbli ){
                    $tmpArr = array(
                        'IdPerusahaanKbli'  => $tbPerusahaanKbli->id_perusahaan_kbli,
                        'Kode'  => $tbPerusahaanKbli->kode,
                        'Nama'  => $tbPerusahaanKbli->nama,
                    );
                    $dtPerusaahnKbliArr[] = $tmpArr;
                }

            }
            return $dtPerusaahnKbliArr;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    /* Perusahaan Produk */
    public function GetPerusahaanProduks($request)
    {
        try {
            //code...
            $TbPerusahaanProduks = TbPerusahaanProduk::join("sde.tb_perusahaan_kbli","sde.tb_perusahaan_produk.id_perusahaan_kbli","sde.tb_perusahaan_kbli.id_perusahaan_kbli")
                                                       -> where("sde.tb_perusahaan_produk.id_perusahaan_kbli","=",$request['IdPerusahaanKbli'])
                                                       -> get(["sde.tb_perusahaan_produk.*"]);
            $dtPerusahaanProduks = array();
            if($TbPerusahaanProduks->count() > 0){
                foreach($TbPerusahaanProduks as $TbPerusahaanProduk){
                    $tmpArr = array(
                        'IdPerusahaanProduk' => $TbPerusahaanProduk->id_perusahaan_produk,
                        'IdPerusahaanKbli' => $TbPerusahaanProduk->id_perusahaan_kbli,
                        'Nama' => $TbPerusahaanProduk->nama,
                        'Keterangan' => $TbPerusahaanProduk->keterangan,
                        'FileName' => $TbPerusahaanProduk->file_name,
                    );
                    $dtPerusahaanProduks [] = $tmpArr;
                }
            }
            return $dtPerusahaanProduks;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function AddPerusahaanProduk($request)
    {
        DB::beginTransaction();
        try {
            //code...
            $idUser = Auth::user()->id_user;
            $maxIdPerusahaanProduk = TbPerusahaanProduk::max('id_perusahaan_produk') + 1;

            $TbPerusahaanProduk = new TbPerusahaanProduk();
            $TbPerusahaanProduk->id_perusahaan_produk = $maxIdPerusahaanProduk;
            $TbPerusahaanProduk->id_perusahaan_kbli = $request->IdPerusahaanKbli;
            $TbPerusahaanProduk->nama = $request->NamaPerusahaanProduk;
            $TbPerusahaanProduk->keterangan = $request->KeteranganPerusahaanProduk;
            $TbPerusahaanProduk->created_by = $idUser;
            $TbPerusahaanProduk->created_date = date('Y-m-d H:i:s');

            if($request->hasFile('FileNamePerusahaanProduk')){

                $folder = public_path('upload/PerusahaanProduk');
                if (!Storage::exists($folder)) {
                    Storage::makeDirectory($folder, 0777, true, true);
                }
                 $file =  $request->file('FileNamePerusahaanProduk');
                 $filename  = $file->getClientOriginalName();
                 $rename = time()."_".$filename;
                 $file->move(public_path('upload/PerusahaanProduk'), $rename);
                 $TbPerusahaanProduk->file_name = $rename;

            }else{
                $TbPerusahaanProduk->file_name = '-';
            }
            $TbPerusahaanProduk->save();

            $maxIdPerusahaanProdukTr = TbPerusahaanProdukTr::max('id_perusahaan_produk_tr') + 1;

            $TbPerusahaanProdukTr = new TbPerusahaanProdukTr();
            $TbPerusahaanProdukTr->id_perusahaan_produk_tr = $maxIdPerusahaanProdukTr;
            $TbPerusahaanProdukTr->id_perusahaan_produk = $TbPerusahaanProduk->id_perusahaan_produk;
            $TbPerusahaanProdukTr->kd_bahasa = "en";
            if($request->NmPerusahaanProdukEn != "" || $request->NmPerusahaanProdukEn != null){

                $TbPerusahaanProdukTr->nama = $request->NmPerusahaanProdukEn;
            }else{
                $TbPerusahaanProdukTr->nama = $request->NamaPerusahaan;
            }

            if($request->KeteranganPerusahaanProdukEn != "" || $request->KeteranganPerusahaanProdukEn != null){

                $TbPerusahaanProdukTr->keterangan = $request->KeteranganPerusahaanProdukEn;
            }else{
                $TbPerusahaanProdukTr->keterangan = $request->KeteranganPerusahaanProduk;
            }

            $TbPerusahaanProdukTr->save();

            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Disimpan"
            );
            return $responses;



        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
            $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    /* Perusahaan Kebutuhan Produk KBLI */
    public function GetPerusahaanKebutuhanProdukKblis($request)
    {
        try {
            //code...
            $PerusahaanKebutuhanProdukKblis = TbPerusahaanKebutuhanProduk::join("sde.tb_perusahaan_lokasi","sde.tb_perusahaan_lokasi.id_perusahaan_lokasi","sde.tb_perusahaan_kebutuhan_produk.id_perusahaan_lokasi")
                                                                        ->where("sde.tb_perusahaan_kebutuhan_produk.id_perusahaan_lokasi","=",$request['IdPerusahaan'])
                                                                        -> get(["sde.tb_perusahaan_kebutuhan_produk.id_perusahaan_kebutuhan_produk","sde.tb_perusahaan_kebutuhan_produk.id_perusahaan_lokasi","sde.tb_perusahaan_kebutuhan_produk.id_perusahaan_kebutuhan_produk"
                                                                                ,"sde.tb_perusahaan_kebutuhan_produk.nama as nama_kebutuhan_produk","sde.tb_perusahaan_kebutuhan_produk.keterangan as keterangan_kebutuhan_produk"]);
            $dtPerusahaanKebutuhanProdKbliArr = array();
            if($PerusahaanKebutuhanProdukKblis->count() > 0){
                foreach($PerusahaanKebutuhanProdukKblis as $PerusahaanKebutuhanProdukKbli){
                    $tmpArr = array(
                        'IdPerusahaanKebutuhanKbli' => $PerusahaanKebutuhanProdukKbli->id_perusahaan_kebutuhan_kbli,
                        'IdPerusahaan' => $PerusahaanKebutuhanProdukKbli->id_perusahaan,
                        'IdKbli' => $PerusahaanKebutuhanProdukKbli->id_kbli,
                        'IdPerusahaanKebutuhanProduk' => $PerusahaanKebutuhanProdukKbli->id_perusahaan_kebutuhan_produk,
                        'NamaKebutuhanProduk' => $PerusahaanKebutuhanProdukKbli->nama_kebutuhan_produk,
                        'KeteranganKebutuhanProduk' => $PerusahaanKebutuhanProdukKbli->keterangan_kebutuhan_produk,
                        'Kode' => $PerusahaanKebutuhanProdukKbli->kode,
                        'Tahun' => $PerusahaanKebutuhanProdukKbli->tahun,
                        'NamaKbli' => $PerusahaanKebutuhanProdukKbli->nama,
                    );

                    $dtPerusahaanKebutuhanProdKbliArr [] = $tmpArr;
                }
            }
            return $dtPerusahaanKebutuhanProdKbliArr;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function AddPerusahaanKebutuhanProdukKbli($request)
    {
        DB::beginTransaction();
        try {
            //code...
            $idUser = Auth::user()->id_user;
            /* $maxIdPerusahaanKebutuhanProduk = TbPerusahaanKebutuhanProduk::max("id_perusahaan_kebutuhan_produk") +1 ;
            $TbPerusahaanKebutuhanKbli = new TbPerusahaanKebutuhanKbli();
            $TbPerusahaanKebutuhanKbli->id_perusahaan_kebutuhan_kbli = $maxIdPerusahaanKebutuhanProduk;
            $TbPerusahaanKebutuhanKbli->id_perusahaan = $request->IdPerusahaan; */
            //$TbPerusahaanKebutuhanKbli->id_kbli = $request->IdKbli;

            //$TbPerusahaanKebutuhanKbli->save();


             $maxIdPerusahaanKebutuhanProduk = TbPerusahaanKebutuhanProduk::max("id_perusahaan_kebutuhan_produk") +1 ;
            $TbPerusahaanKebutuhanProduk = new TbPerusahaanKebutuhanProduk();
            $TbPerusahaanKebutuhanProduk->id_perusahaan_kebutuhan_produk = $maxIdPerusahaanKebutuhanProduk;
            $TbPerusahaanKebutuhanProduk->id_perusahaan_lokasi = $request->IdPerusahaanLokasi;
            $TbPerusahaanKebutuhanProduk->nama = $request->NamaPerusahaanKebutuhanProduk;
            $TbPerusahaanKebutuhanProduk->keterangan = $request->KeteranganPerusahaanKebutuhanProduk;
            $TbPerusahaanKebutuhanProduk->created_by = $idUser;
            $TbPerusahaanKebutuhanProduk->created_date = date('Y-m-d H:i:s');

            $TbPerusahaanKebutuhanProduk->save();

            $maxIdPerusahaanKebutuhanProdukTr =  TbPerusahaanKebutuhanProdukTr::max("id_perusahaan_kebutuhan_produk_tr") + 1;

            $TbPerusahaanKebutuhanProdukTr = new TbPerusahaanKebutuhanProdukTr();
            $TbPerusahaanKebutuhanProdukTr->id_perusahaan_kebutuhan_produk_tr = $maxIdPerusahaanKebutuhanProdukTr;
            $TbPerusahaanKebutuhanProdukTr->id_perusahaan_kebutuhan_produk = $TbPerusahaanKebutuhanProduk->id_perusahaan_kebutuhan_produk;
            $TbPerusahaanKebutuhanProdukTr->kd_bahasa = 'en';

            if($request->NamaPerusahaanKebutuhanProdukEn ?? false){
                $TbPerusahaanKebutuhanProdukTr->nama = $request->NamaPerusahaanKebutuhanProdukEn;
            }else{
                $TbPerusahaanKebutuhanProdukTr->nama = $request->NamaPerusahaanKebutuhanProduk;
            }

            if($request->KeteranganPerusahaanKebutuhanProdukEn ?? false){
                $TbPerusahaanKebutuhanProdukTr->keterangan = $request->KeteranganPerusahaanKebutuhanProdukEn;
            }else{
                $TbPerusahaanKebutuhanProdukTr->keterangan = $request->NamaPerusahaanKebutuhanProduk;
            }


            $TbPerusahaanKebutuhanProdukTr->save();

            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Disimpan"
            );
            return $responses;




        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
             $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }


    /* Master KBLI */
    public function GetKategoriKbli($request)
    {
        try {
            //code...
            $KategoriKblis = TbKbli::where('tahun','=',$request['Tahun'])
                                    -> whereRaw('LENGTH(kode) = 1')
                                    -> get();
            return $KategoriKblis;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetKbliByIdParents($request)
    {
        try {
            //code...
            $Kblis = TbKbli::where('tahun','=',$request['Tahun'])
                                    -> where('id_kbli_parent',"=",$request['IdParent'])
                                    -> get();
            return $Kblis;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetKelompokKblis($request)
    {
        try {
            //code...
            $KategoriKblis = TbKbli::where('tahun','=',$request['Tahun'])
                                    -> whereRaw('LENGTH(kode) = 5')
                                    -> get();
            return $KategoriKblis;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetKelompokKbliParents($request)

    {

        try {
            //code...
            $kelompoks = DB::table('sde.tb_kbli as kelompok')
                        -> join('sde.tb_kbli as subgolongan','kelompok.id_kbli_parent','=','subgolongan.id_kbli')
                        -> join('sde.tb_kbli as golongan','subgolongan.id_kbli_parent','=','golongan.id_kbli')
                        -> join('sde.tb_kbli as gol_pok','golongan.id_kbli_parent','=','gol_pok.id_kbli')
                        -> join('sde.tb_kbli as kategori','gol_pok.id_kbli_parent','=','kategori.id_kbli')
                        -> where ('kelompok.tahun',$request->Tahun)
                        -> whereRaw('LENGTH(kelompok.kode) = 5')
                        -> get([
                                'kategori.kode as kode_kategori',
                                'kategori.nama as nama_kategori',
                                'gol_pok.kode as kode_golpok',
                                'gol_pok.nama as nama_golpok',
                                'golongan.kode as kode_golongan',
                                'golongan.nama as nama_golongan',
                                'subgolongan.kode as kode_subgolongan',
                                'subgolongan.nama as nama_subgolongan',
                                'kelompok.id_kbli as id_kbli_kelompok',
                                'kelompok.kode as kode_kelompok',
                                'kelompok.nama as nama_kelompok',
                                'kelompok.tahun as tahun_kelompok',
                            ]);
            return $kelompoks;
        } catch (QueryException $ex) {
            //throw $th;
        }
    }

}
