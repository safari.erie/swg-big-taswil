<?php

namespace App\Repository;

    /**
     * createdby : eri.safari
     * createdt : 23 June 2021
     * interface repository for module ApplTask
     *
     */

    interface IApplTaskRepository
    {
        public function GetMenus($idAppl, $idUser);
        public function GetRootTaskParent($idParent, $idApplTask);
        public function GetRootChild($idApplTask, $idUser);
        public function GetApplTasks($idAppl);
        public function GetApplTaskById($idApplTask);
        public function GetApplTaskByRole($idRole);
        public function AddTasksRole($idRole, $idApplTask);
        public function GetAppls();
        public function AddAppl($request);
        public function GetApplSettings();
        public function GetPermissions($idRole);
        public function ChangePermission($request);
        public function CheckPermission($request);
    }

?>
