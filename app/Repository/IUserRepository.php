<?php

namespace App\Repository;

    /**
     * createdby : eri.safari
     * createdt : 25 June 2021
     * interface repository for module Users
     *
     */

    interface IUserRepository
    {
        public function AddUser($request);
        public function GetUsers();
    }

?>
