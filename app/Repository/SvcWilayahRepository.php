<?php
namespace App\Repository;
use App\Models\Provinsi;
use App\Models\TbAdmKabkot;
use App\Models\TbAdmProvinsi;
use Illuminate\Database\QueryException;


class SvcWilayahRepository implements IWilayahRepository{

    /**
     * createdby : eri.safari
     * createdt :
     * Implement interface IWilayahRepository
     *
     */

    public function findAllProvinsis(){
        $provinsis = TbAdmProvinsi::all();
        return $provinsis;
    }

    public function GetKabkotByIdProv($request)
    {
        try {
            //code...
            $Kabkots = TbAdmKabkot::where('id_adm_provinsi','=',$request['IdAdmProvinsi'])
                        ->get();

            $dtKabKotsArr = array();
            if(count($Kabkots) > 0 ){
                foreach($Kabkots as $Kabkot){
                     $tmpArr = array(
                         'IdAdmKabKot' => $Kabkot->id_adm_kabkot,
                         'IdAdmProv' => $Kabkot->id_adm_provinsi,
                         'Nama' => $Kabkot->nama,

                        );
                    $dtKabKotsArr[] = $tmpArr;
                }
            }

            return $dtKabKotsArr;

        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }
}
?>
