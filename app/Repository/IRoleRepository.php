<?php

namespace App\Repository;

    /**
     * createdby : eri.safari
     * createdt : 25 June 2021
     * interface repository for module Roles
     *
     */

    interface IRoleRepository
    {
        public function GetRoles();
        public function GetRole($idRole);
        public function AddRole($request);
        public function DeleteRole($request);
        public function UpdateRole($request);
    }

?>
