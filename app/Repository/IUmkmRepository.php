<?php

namespace App\Repository;

/**
     * createdby : eri.safari
     * createdt : 7 Juli 2021
     * interface repository for module Admin Umkm
     *
     */
    interface IUmkmRepository {

        public function GetUmkmPerusahaans($type);
        public function AddUmkmPerusahaan($request);
        public function GetUmkmPerusahaan($request);
        public function EditPerusahaanUmkm($request);
        public function DeletePerusahaanUmkm($request);

        /* Cabang Perusahaan */
        public function GetUmkmCabangPerusahaans($request);
        public function AddUmkmCabangPerusahaan($request);

        /* Perusahaan KBLI */
        public function GetUmkmPerusahaanKblis($request);
        public function AddPerusahaanKbli($request);
        public function GetPerusahaanKbliByIdPerusahaan($request);

        /* Perusahaan Produk */
        public function GetPerusahaanProduks($request);
        public function AddPerusahaanProduk($request);

        /* Perusahaan Kebutuhan Produk KBLI */
        public function GetPerusahaanKebutuhanProdukKblis($request);
        public function AddPerusahaanKebutuhanProdukKbli($request);

        /* Get Master KBLI */
        public function GetKategoriKbli($request);
        public function GetKbliByIdParents($request);
        public function GetKelompokKblis($request);
        public function GetKelompokKbliParents($request);



    }
