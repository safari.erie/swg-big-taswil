<?php
namespace App\Repository;

use App\Models\TbRole;

class RoleRepository implements IRoleRepository{

    public function GetRoles()
    {
        try {
            //code...
            $roles = TbRole::orderBy('id_role')->get();

            return $roles;

        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetRole($idRole)
    {
        try {
            //code...
            $role = TbRole::where('id_role',$idRole)->first();
            return $role;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function AddRole($request)
    {

        try {
            //code...
            $max_id = TbRole::max('id_role');

            $objTbRole = new TbRole();
            $objTbRole['id_role'] = $max_id + 1;
            $objTbRole->role_name = $request->role_name;

            $objTbRole->save();
            if($objTbRole){
                return '';
            }else{
                return 'Failed';
            }
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function DeleteRole($request)
    {
        try {
            //code...
            $deleteRole = TbRole::where('id_role',$request)->delete();
            return $deleteRole;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function UpdateRole($request)
    {

        try {
            //code...
             $role = TbRole::where('id_role',$request['IdRole'])->first();
             $role->role_name = $request['RoleName'];
             $role->save();
             if($role){
                return '';
            }else{
                return 'Failed';
            }
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }
}

