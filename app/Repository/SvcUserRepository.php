<?php
namespace App\Repository;

use App\Models\TbAdmUser;
use App\Models\TbRole;
use App\Models\TbUserProfile;
use App\Models\TbUserRole;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SvcUserRepository implements IUserRepository{

    /**
     * createdby : eri.safari
     * createdt :
     * Implement interface IUserRepository
     *
     */
    public function AddUser($request){

        DB::beginTransaction();
        try {
            //code...
            $idUser = Auth::user()->id_user;
            // cek off input username or email in table tb_user
            $TbUser = User::where('username',$request->Username)
                        -> orwhere('email',$request->Email)
                        -> get();

            if( $TbUser->count() > 0 ){
                 $responses = array(
                    "Status"    => false,
                    "Message"   => "Username / Email sudah terdaftar"
                    );
                return $responses;

            }else{
                $max_id_user = User::max('id_user') + 1;
                $password = Hash::make($request->Password);
                $objTbUser = new User();
                $objTbUser->id_user = $max_id_user;
                $objTbUser->id_role = $request->Role;
                $objTbUser->username = $request->Username;
                $objTbUser->password = $password;
                $objTbUser->email = $request->Email;
                $objTbUser->first_name = $request->Firstname;
                $objTbUser->middle_name = $request->MiddleName;
                $objTbUser->last_name = $request->LastName;
                $objTbUser->status = 1;
                //$objTbUser->created_at = $idUser;
                $objTbUser->created_date = date('Y-m-d H:i:s');

                $objTbUser->save();

                $objTbAdmUser = new TbAdmUser();
                if($request->Role == 3){

                    $objTbAdmUser->id_user = $objTbUser->id_user;
                    $objTbAdmUser->id_adm_provinsi = $request->Provinsi;
                    $objTbAdmUser->save();
                }else if($request->Role == 4){
                    $objTbAdmUser->id_user = $objTbUser->id_user;
                        //$objTbAdmUser->id_adm_provinsi = $request->Provinsi;
                        $objTbAdmUser->id_adm_kabkot = $request->KabKot;
                        $objTbAdmUser->save();
                }
                DB::commit();
                $responses = array(
                    "Status"    => true,
                    "Message"   => "Data Berhasil Disimpan"
                );
                return $responses;
            }


        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
             $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;
        }
    }

    public function GetUsers(){

        try {
            $users = User::all();
            $userArray = array();
            $no_inc = 1;
            foreach ($users as  $user) {
                # code...
                $roles = TbRole::join('tb_user','tb_role.id_role','=','tb_user.id_role')
                            ->where('tb_role.id_role',$user->id_role)
                            ->get(['tb_role.id_role','tb_role.role_name']);

                $userTmpArr = [
                    'No'   => $no_inc,
                    'Address'   => $user->address,
                    'Username'  => $user->username,
                    'Email'  => $user->email,
                    'FirstName' => $user->first_name,
                    'MiddleName'   => $user->middle_name,
                    'LastName'   => $user->last_name,
                    'Status'   => $user->status,
                    'Roles'      => $roles

                ];
                $userArray[] = $userTmpArr;
                $no_inc++;
            }

            return $userArray;
        } catch (QueryException $ex) {
            //throw $th;

            return $ex->getMessage();
        }
    }

}
?>
