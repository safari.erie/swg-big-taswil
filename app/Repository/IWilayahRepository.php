<?php

namespace App\Repository;

    /**
     * createdby : eri.safari
     * createdt : 10 June 2021
     * interface repository
     *
     */

    interface IWilayahRepository{
        public function findAllProvinsis();
        public function GetKabkotByIdProv($request);
    }

?>
