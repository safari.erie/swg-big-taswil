<?php
namespace App\Repository;

use App\Models\TbAppl;
use App\Models\TbApplTask;
use App\Models\TbAppSetting;
use App\Models\TbRole;
use App\Models\TbRoleApplTask;
use App\Models\TbUserRole;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;


class ApplTaskRepository implements IApplTaskRepository{

    /**
     * createdby : eri.safari
     * createdt :
     * Implement interface IApplTaskRepository
     *
     */

    public function GetMenus($idAppl, $idRole){

        try {
            //code...
            $dataMenus = TbRole::join('tb_role_appl_task','tb_role.id_role','=','tb_role_appl_task.id_role')
                                -> join('tb_appl_task','tb_role_appl_task.id_appl_task','=','tb_appl_task.id_appl_task')
                                -> where('tb_appl_task.id_appl','=',$idAppl)
                                -> where('tb_role.id_role','=',$idRole)
                                -> orderBy('tb_appl_task.id_appl_task', 'asc')
                                -> get(['tb_appl_task.*']);
            return $dataMenus;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetRootTaskParent($idParent, $idApplTask){
       // echo $idParent;

        try {
            //code...
             $dataChilds = TbUserRole::join('tb_role','tb_role.id_role','=','tb_user_role.id_role')
                         -> join('tb_role_appl_task','tb_user_role.id_role','=','tb_role_appl_task.id_role')
                         -> join('tb_appl_task','tb_role_appl_task.id_appl_task','=','tb_appl_task.id_appl_task')
                         -> join('tb_appl','tb_appl_task.id_appl','=','tb_appl.id_appl')
                         -> where('tb_appl_task.id_appl_task_parent','=',$idParent)
                         -> where('tb_appl_task.id_appl_task','=',$idApplTask)
                         -> orderBy('tb_appl_task.id_appl_task', 'asc')
                         -> get(['tb_appl_task.*']);

            return $dataChilds;

        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetRootChild($idApplTask,$idRole){
        try {
            //code...


            $dataChilds = TbRole::join('tb_role_appl_task','tb_role.id_role','=','tb_role_appl_task.id_role')
                                -> join('tb_appl_task','tb_role_appl_task.id_appl_task','=','tb_appl_task.id_appl_task')
                                -> where('tb_appl_task.id_appl_task_parent','=',$idApplTask)
                                -> where('tb_role.id_role','=',$idRole)
                                -> orderBy('tb_appl_task.id_appl_task', 'asc')
                                -> get(['tb_appl_task.*']);

            return $dataChilds;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetApplTasks($idAppl)
    {
        try {
            //code...
            $applTasks = TbApplTask::join('tb_appl','tb_appl.id_appl','=','tb_appl_task.id_appl')
                         -> where('tb_appl.id_appl','=',$idAppl)
                         -> orderBy('tb_appl_task.id_appl_task','asc')
                         -> get(['tb_appl_task.*','tb_appl.*']);

            return $applTasks;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function GetApplTaskById($idApplTask)
    {
        try{
             $applTask = TbApplTask::join('tb_appl','tb_appl.id_appl','=','tb_appl_task.id_appl')
                         -> where('tb_appl_task.id_appl_task','=',$idApplTask)
                         -> orderBy('tb_appl_task.id_appl_task','asc')
                         -> get(['tb_appl_task.*','tb_appl.*']);


            return $applTask;

        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function GetApplTaskByRole($idRole)
    {
        try {
            //code...
            $assignTasks = array();
            $noAssignTasks = array();

            $retArr = array();
            //check for id Role
            $role = TbRole::where('id_role','=',$idRole)->first();
            if($role === null){
                $errorMessage = array(
                    'Status' => false,
                    'Message' => 'Data Tidak Ada'
                );
                return $retArr = $errorMessage;
            }
            $tempArr = array();
            $tempArr['Status'] = true;
            $tempArr['Message'] = '';
            $tempArr['IdRole'] = $idRole;
            $tempArr['RoleName'] = $role->role_name;



            $queryAssign = TbApplTask::join("tb_role_appl_task","tb_appl_task.id_appl_task","tb_role_appl_task.id_appl_task")
                                        -> where("tb_role_appl_task.id_role","=",$idRole)
                                        -> orderBy("tb_appl_task.id_appl_task","asc")
                                        -> get(["tb_appl_task.id_appl_task","tb_appl_task.id_appl_task_parent"
                                                ,"tb_appl_task.appl_task_name","tb_appl_task.controller_name"
                                                ,"tb_role_appl_task.can_read","tb_role_appl_task.can_edit"
                                                ,"tb_role_appl_task.can_approve"
                                                ,"tb_role_appl_task.can_delete"
                                                ,"tb_role_appl_task.can_add"
                                    ]);




            if(count($queryAssign) == 0){
                $tempArr['AssignTasks'] = [];
            }

            //$tmpArrIdApplTask = [];

            $datasobjAssignTasks = $this->treeRecursive($queryAssign,$pid=null,$level=0,$html='&nbsp;&nbsp;');

            foreach($datasobjAssignTasks as $item){

                $tmpArrIdApplTask[] = $item->id_appl_task;
                $assignTasks['IdApplTask'] = $item->id_appl_task;
                //$assignTasks['ApplName'] = $item->appl_name;
                $assignTasks['IdApplTaskParent'] = $item->id_appl_task_parent;
                $assignTasks['ApplTaskName'] = $item->appl_task_name;
                $assignTasks['ApplTaskName'] = $item->appl_task_name;
                $assignTasks['IdAppl'] = $item->id_appl;
                $assignTasks['ControllerName'] = $item->controller_name;
                $assignTasks['ActionName'] = $item->action_name;
                $assignTasks['Description'] = $item->description;
                $assignTasks['IconName'] = $item->icon_name;
                $assignTasks['Level'] = $item->level;
                $assignTasks['Html'] = $item->html;
                $tempArr['AssignTasks'][] = $assignTasks;
            }

            //dd($objNoAssignTasks);

            /* $queryNoAssign = "SELECT "
                              ."     * "
                              ." FROM ( "
                              ." SELECT "
                              ."     * "
                              ." FROM tb_appl_task "
                              ." WHERE id_appl_task IN (SELECT "
                              ."     id_appl_task_parent "
                              ." FROM tb_appl_task "
                              ." WHERE controller_name IS NOT NULL "
                              ." AND id_appl_task NOT IN (SELECT "
                              ."     a.id_appl_task "
                              ." FROM tb_appl_task a "
                              ." JOIN tb_role_appl_task b "
                              ."     ON a.id_appl_task = b.id_appl_task "
                              ." WHERE a.controller_name IS NOT NULL "
                              ." AND b.id_role = $idRole)) "
                              ." UNION ALL "
                              ." SELECT "
                              ."     * "
                              ." FROM tb_appl_task "
                              ." WHERE controller_name IS NOT NULL "
                              ." AND id_appl_task NOT IN (SELECT "
                              ."     a.id_appl_task "
                              ." FROM tb_appl_task a "
                              ." JOIN tb_role_appl_task b "
                              ."     ON a.id_appl_task = b.id_appl_task "
                              ." WHERE a.controller_name IS NOT NULL "
                              ." AND b.id_role = $idRole) "
                              ." ) x "
                              ." ORDER BY id_appl_task";

            $objNoAssignTasks = DB::select(DB::raw($queryNoAssign)); */
            $objNoAssignTasks =[];
             $queryTmpAssign = TbApplTask::join("tb_role_appl_task","tb_appl_task.id_appl_task","tb_role_appl_task.id_appl_task")
                                        -> where("tb_role_appl_task.id_role","=",$idRole)
                                        -> orderBy("tb_appl_task.id_appl_task","asc")
                                        -> get(["tb_appl_task.id_appl_task",])
                                        -> toArray();

            $queryNoAssign = TbApplTask::whereNotIn("tb_appl_task.id_appl_task",$queryTmpAssign)->orderBy("tb_appl_task.id_appl_task","asc")->get();

            if(count($queryNoAssign) == 0){

                $tempArr['NoAssignTasks']= [];
            }else{
                $dataQueryNoAssign = $this->treeRecursiveMenu($queryNoAssign,$pid=null,$level=0,$html='&nbsp;&nbsp;');

                foreach ($dataQueryNoAssign as $items) {
                    # code...
                    $noAssignTasks['IdApplTask'] = $items->id_appl_task;
                //  $noAssignTasks['ApplName'] = $items->appl_name;
                    $noAssignTasks['IdApplTaskParent'] = $items->id_appl_task_parent;
                    $noAssignTasks['ApplTaskName'] = $items->appl_task_name;
                    $noAssignTasks['ApplTaskName'] = $items->appl_task_name;
                    $noAssignTasks['IdAppl'] = $items->id_appl;
                    $noAssignTasks['ControllerName'] = $items->controller_name;
                    $noAssignTasks['ActionName'] = $items->action_name;
                    $noAssignTasks['Description'] = $items->description;
                    $noAssignTasks['IconName'] = $items->icon_name;
                    $noAssignTasks['Level'] = $items->level;
                    $noAssignTasks['Html'] = $items->html;

                    $tempArr['NoAssignTasks'][] = $noAssignTasks;
                }
            }


            $retArr = $tempArr;
            return $retArr;

        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    private function treeRecursiveMenu(&$list,$pid=null,$level=0,$html='&nbsp;&nbsp;')
    {
        static $tree=array();
        foreach($list as $v){
            if($v->id_appl_task_parent == $pid){
                $v->level=$level;
                $v->html=str_repeat($html,$level);
                $tree[]=$v;
                $this->treeRecursiveMenu($list,$v->id_appl_task,$level+1,$html);
            }
        }
        return $tree;
    }

    public function GetPermissions($idRole)
    {
        $getPermissions = TbApplTask::join("tb_role_appl_task","tb_appl_task.id_appl_task","tb_role_appl_task.id_appl_task")
                                        -> where("tb_role_appl_task.id_role","=",$idRole)
                                        -> orderBy("tb_appl_task.id_appl_task","asc")
                                        -> get(["tb_appl_task.id_appl_task","tb_appl_task.id_appl_task_parent"
                                                ,"tb_appl_task.appl_task_name","tb_appl_task.controller_name"
                                                ,"tb_role_appl_task.can_read","tb_role_appl_task.can_edit"
                                                ,"tb_role_appl_task.can_approve"
                                                ,"tb_role_appl_task.can_delete"
                                                ,"tb_role_appl_task.can_add"
                                    ]);

        $dataPermissions = $this->treeRecursive($getPermissions,$pid=null,$level=0,$html='&nbsp;&nbsp;');
        return $dataPermissions;

    }

    private function treeRecursive(&$list,$pid=null,$level=0,$html='&nbsp;&nbsp;')
    {
        static $tree=array();
        foreach($list as $v){
            if($v['id_appl_task_parent'] == $pid){
                $v['level']=$level;
                $v['html']=str_repeat($html,$level);
                $tree[]=$v;
                $this->treeRecursive($list,$v['id_appl_task'],$level+1,$html);
            }
        }
        return $tree;
    }



    public function ChangePermission($request)
    {

        try {
            //code...
            $affected = DB::table('public.tb_role_appl_task')
              ->where('id_role', $request['IdRole'])
              ->where('id_appl_task',$request['IdApplTask'])
              ->update(['can_'.$request['ColumnName'].'' => $request->Value]);


            DB::commit();
            $responses = array(
                "Status"    => true,
                "Message"   => "Data Berhasil Dirubah"
            );
            return $responses;

        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
            $responses = array(
                "Status"    => false,
                "Message"   => $ex->getMessage()
            );
            return $responses;

        }
    }



    public function GetAppls()
    {
        try {
            //code...
            $appls = TbAppl::orderBy('id_appl')->get();
            return $appls;
        } catch (\Exception $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }

    public function AddAppl($request)
    {
         DB::beginTransaction();
        try {
            //code...
            $objApplTask = new TbApplTask();
            if($request['IdApplTaskParent'] == null){

                $objApplTask->id_appl_task = TbApplTask::whereNull('id_appl_task_parent')->count() + 1;
                $objApplTask->id_appl_task *= 1000000;

            }
            else{
                $objApplTaskByIdParent = TbApplTask::where('id_appl_task_parent','=',$request['IdApplTaskParent'])->first();

                $idParent = strval($request['IdApplTaskParent']);

                $lengthOfIdParent = strlen($idParent);
                if($objApplTaskByIdParent == null){
                    $objTbApplTaskParent = TbApplTask::where('id_appl_task_parent','=',$request['IdApplTaskParent'])->first();
                    $objApplTask->id_appl_task = 1;
                }else{
                     $objApplTask->id_appl_task = $objApplTaskByIdParent->count() + 1;
                }
                if($lengthOfIdParent >= 8){
                    $tmpIdApplTask = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 1000;
                    $test = (int)substr(strval($tmpIdApplTask),7,1);
                    if((int)substr(strval($request['IdApplTaskParent']),6,1) > 0){
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task;
                    }
                    else if((int)substr(strval($request['IdApplTaskParent']),5,1) > 0){
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 10;
                    }else if((int)substr(strval($request['IdApplTaskParent']),4,1) > 0){
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 100;
                    }else if((int)substr(strval($request['IdApplTaskParent']),3,1) > 0){
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 1000;
                    }else if((int)substr(strval($request['IdApplTaskParent']),2,1) > 0){
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 1000;
                    }
                    else{
                        if($objApplTask->id_appl_task > 10){
                            $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 1000;
                        }else{
                            $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 10000;
                        }
                    }


                }else{
                    //$test = (int)substr(strval($request['IdApplTaskParent']),5,1);
                    if((int)substr(strval($request['IdApplTaskParent']),5,1) > 0){
                         $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task;

                    }else if((int)substr(strval($request['IdApplTaskParent']),4,1) > 0){
                         $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 10;

                    }else if((int)substr(strval($request['IdApplTaskParent']),2,1) > 0){
                         $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 100;

                    }
                    else{
                        $objApplTask->id_appl_task = $request['IdApplTaskParent'] + $objApplTask->id_appl_task * 10000;
                    }
                }

            }

            $objApplTask->id_appl_task_parent = $request['IdApplTaskParent'];
            $objApplTask->id_appl = $request['IdAppl'];
            $objApplTask->appl_task_name = $request['ApplTaskName'];
            $objApplTask->controller_name = $request['ControllerName'];
            $objApplTask->action_name = $request['ActionName'];
            $objApplTask->description = $request['Description'];
            $objApplTask->icon_name = $request['IconName'];
            $objApplTask->save();
            DB::commit();
            return true;
        } catch (\Throwable $ex) {
            //throw $th;
            DB::rollback();
            return $ex->getMessage();
        }
    }

    public function GetApplSettings()
    {
        try {
            //code...
            $tbApplSettings = TbAppSetting::get();

            return $tbApplSettings;
        } catch (\Throwable $th) {
            //throw $th;
            return $th->getMessage();
        }
    }

    private function SetTaskParentRoleTask($idRole, $idApplTask){

        $objTbApplTask = TbApplTask::where('tb_appl_task.id_appl_task','=',$idApplTask)
                        -> first();
        if($objTbApplTask->id_app_task_parent != null){
            $objTbRoleApplTask = TbRoleApplTask::where('tb_role_appl_task.id_role','=',$idRole)
                                -> where ('tb_role_appl_task.id_appl_task','=',$objTbApplTask->id_appl_task_parent)
                                -> first();
            if($objTbRoleApplTask == null){
                $tbRoleApplTask = new TbRoleApplTask();
                $tbRoleApplTask->id_role = $idRole;
                $tbRoleApplTask->id_appl_task  = $objTbApplTask->id_appl_task_parent;
                $tbRoleApplTask->save();
            }
            $this->SetTaskParentRoleTask($idRole, $objTbApplTask->id_appl_task_parent);
        }
    }

    public function AddTasksRole($idRole, $idApplTask)
    {
        DB::beginTransaction();
        try {
            //code...
            $objOldTaskRoles = TbRoleApplTask::where('tb_role_appl_task.id_role','=',$idRole)
                                ->get();

            foreach($objOldTaskRoles as $objOldTaskRole){
                 TbRoleApplTask::where('id_role', $idRole)->delete();
            }

            if($idApplTask !== null){
                $idApplTaskToArr = explode(";",$idApplTask);
                foreach($idApplTaskToArr as $item){
                    $this->SetTaskParentRoleTask($idRole,$item);
                    $objInsertRoleApplTask = new TbRoleApplTask();
                    $objInsertRoleApplTask->id_role = $idRole;
                    $objInsertRoleApplTask->id_appl_task = $item;
                    $objInsertRoleApplTask->save();
                }
            }
            DB::commit();
            return null;
        } catch (QueryException $ex) {
            //throw $th;
            DB::rollback();
            return $ex->getMessage();
        }
    }

    public function CheckPermission($request)
    {

        try {
            //code...

            $permission = TbApplTask::join('tb_role_appl_task',function($join) use ($request){
                $join->on('tb_appl_task.id_appl_task','tb_role_appl_task.id_appl_task')
                ->where('tb_role_appl_task.id_role',$request['role_id']);
            })
            -> where('tb_appl_task.action_name','=',$request['action_name'])
            ->first();
            return $permission;
        } catch (QueryException $ex) {
            //throw $th;
            return $ex->getMessage();
        }
    }
}
?>
