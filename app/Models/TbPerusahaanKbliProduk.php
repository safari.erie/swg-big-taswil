<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanKbliProduk extends Model
{
    use HasFactory;
     use HasFactory;
    protected $table = 'sde.tb_perusahaan_kbli_produk';
    protected $primaryKey = 'id_perusahaan_kbli_produk';
    public $timestamps = false;
}
