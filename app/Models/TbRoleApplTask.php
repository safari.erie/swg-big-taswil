<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbRoleApplTask extends Model
{
    use HasFactory;
    protected $table = 'public.tb_role_appl_task';
    protected $primaryKey  = 'id_role';
    public $timestamps = false;
}
