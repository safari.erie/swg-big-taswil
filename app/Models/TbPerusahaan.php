<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaan extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan';
    protected $primaryKey = 'id_perusahaan';
    public $timestamps = false;
}
