<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanProdukTr extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_produk_tr';
    protected $primaryKey = 'id_perusahaan_produk_tr';
    public $timestamps = false;
}
