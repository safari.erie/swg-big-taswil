<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanKbli extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_kbli';
    protected $primaryKey = 'id_perusahaan_kbli';
    public $timestamps = false;
}
