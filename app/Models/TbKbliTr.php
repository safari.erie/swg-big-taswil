<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbKbliTr extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_kbli_tr';
    protected $primaryKey = 'id_kbli_tr';
    public $timestamps = false;
}
