<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbAdmUser extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_adm_user';
    protected $primaryKey = 'id_user'; // or null
    public $timestamps = false;
}
