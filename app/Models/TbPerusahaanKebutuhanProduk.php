<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanKebutuhanProduk extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_kebutuhan_produk';
    protected $primaryKey = 'id_perusahaan_kebutuhan_produk';
    public $timestamps = false;
}
