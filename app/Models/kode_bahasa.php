<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;



class kode_bahasa extends Authenticatable
{
    protected $table = 'sde.tb_kode_bahasa';
    protected $primaryKey = 'kd_bahasa'; // or null
    public $timestamps = false;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tahun',
        'nilai_investasi',
    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
  


}




