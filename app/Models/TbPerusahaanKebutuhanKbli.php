<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanKebutuhanKbli extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_kebutuhan_kbli';
    protected $primaryKey = 'id_perusahaan_kebutuhan_kbli';
    public $timestamps = false;
}
