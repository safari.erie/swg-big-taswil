<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'sde.provinsi';
    public $timestamps = false;
    protected $primaryKey = 'objectid';
    protected $hidden = ['shape'];
}
