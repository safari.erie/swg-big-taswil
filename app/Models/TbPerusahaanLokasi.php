<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanLokasi extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_lokasi';
    protected $primaryKey = 'id_perusahaan_lokasi';
    public $timestamps = false;
}
