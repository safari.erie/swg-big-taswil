<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanCabang extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_cabang';
    protected $primaryKey = 'id_perusahaan_cabang';
    public $timestamps = false;
}
