<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbUserRole extends Model
{
    use HasFactory;
    protected $table = 'public.tb_user_role';
    protected $primaryKey = 'id_user'; // or null
    public $timestamps = false;

}
