<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbAppSetting extends Model
{
    use HasFactory;
    protected $table = 'public.tb_appl_setting';
     public $timestamps = false;
    protected $primaryKey  = 'code';
    public $incrementing = false;

    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';
}
