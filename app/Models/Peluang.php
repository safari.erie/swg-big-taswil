<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Peluang extends Authenticatable
{
    protected $table = 'sde.tb_peluang_kabkot';
    protected $primaryKey = 'id_peluang_kabkot';
    public $timestamps = false;
    use HasFactory, Notifiable;
   
 
  

}


class Peluang_tr extends Authenticatable
{
    protected $table = 'sde.tb_peluang_kabkot_tr';
    protected $primaryKey = 'id_peluang_kabkot_tr'; // or null
    public $timestamps = false;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tahun',
        'nilai_investasi',
    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
  


}





