<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbAppl extends Model
{
    use HasFactory;
    protected $table = 'public.tb_appl';
}
