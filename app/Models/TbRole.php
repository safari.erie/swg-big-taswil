<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbRole extends Model
{
    use HasFactory;
    protected $table = 'public.tb_role';
    protected $primaryKey  = 'id_role';
    public $timestamps = false;
}
