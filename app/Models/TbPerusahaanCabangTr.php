<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanCabangTr extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_cabang_tr';
    protected $primaryKey = 'id_perusahaan_cabang_tr';
    public $timestamps = false;
}
