<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanProduk extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_produk';
    protected $primaryKey = 'id_perusahaan_produk';
    public $timestamps = false;
}
