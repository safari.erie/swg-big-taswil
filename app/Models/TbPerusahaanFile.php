<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanFile extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_file';
    protected $primaryKey = 'id_perusahaan_file';
    public $timestamps = false;
}
