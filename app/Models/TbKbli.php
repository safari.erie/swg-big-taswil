<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbKbli extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_kbli';
    public $timestamps = false;
    protected $primaryKey = 'id_kbli';
}
