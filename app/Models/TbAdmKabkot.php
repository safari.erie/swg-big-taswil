<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbAdmKabkot extends Model
{
    use HasFactory;
     protected $table = 'sde.tb_adm_kabkot';
    public $timestamps = false;
    protected $primaryKey = 'id_adm_kabkot';
    protected $hidden = ['shape'];
}
