<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanTr extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_tr';
    protected $primaryKey = 'id_perusahaan_tr';
    public $timestamps = false;
}
