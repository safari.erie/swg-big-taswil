<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbAdmProvinsi extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_adm_provinsi';
    public $timestamps = false;
    protected $primaryKey = 'id_adm_provinsi';
    protected $hidden = ['shape'];
}
