<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbApplTask extends Model
{
    use HasFactory;
    protected $table = 'public.tb_appl_task';
     public $timestamps = false;
    protected $primaryKey  = 'id_appl_task';
}
