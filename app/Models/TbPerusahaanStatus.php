<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanStatus extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_status';
    protected $primaryKey = 'id_perusahaan';
    public $timestamps = false;
}
