<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanTipeTr extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_tipe_tr';
    protected $primaryKey = 'id_perusahaan_tipe_tr';
    public $timestamps = false;
}
