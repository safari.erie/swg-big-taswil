<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbPerusahaanTipe extends Model
{
    use HasFactory;
    protected $table = 'sde.tb_perusahaan_tipe';
    protected $primaryKey = 'id_perusahaan_tipe';
    public $timestamps = false;
}
