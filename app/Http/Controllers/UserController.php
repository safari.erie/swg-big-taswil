<?php

namespace App\Http\Controllers;

use App\Repository\SvcUserRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class UserController extends Controller
{
    //
    protected $svcUserRepository;
    public function __construct(SvcUserRepository $svcUserRepository)
    {
        $this->svcUserRepository = $svcUserRepository;
    }

    public function userPengguna(){
        // sample get uri from controller
        //echo request()->segment(2);

        return view('users.user_management');

    }

    public function AddUser(Request $request){
/*
        $param = $request->only([
              'Username','Password','Email','Firstname','MiddleName','LastName','Address','PhoneNumber','MobileNumber','Role'
            ]);

        $validator = Validator::make($request->all(),[
            'Username' => ['required'],
            'Password' => ['required'],
        ]); */

        $addUser = $this->svcUserRepository->AddUser($request);
         $responses = array(
            'Status'    => $addUser['Status'],
            'Data'     => '',
            'Message'   => $addUser['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function GetUsers(){
        $users = $this->svcUserRepository->GetUsers();

        $responses =  array(
            'Status'    => count($users) > 0 ? true : false,
            'Data'     => $users,
            'Message'   => count($users) > 0 ? "Data Tersedia" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

}
