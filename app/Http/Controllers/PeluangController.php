<?php

namespace App\Http\Controllers;
use File;
use App\Repository\SvcPeluangRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class PeluangController extends Controller
{
    //
    protected $svcPeluangRepository;
    public function __construct(SvcPeluangRepository $svcPeluangRepository)
    {
        $this->svcPeluangRepository = $svcPeluangRepository;
     
    }


    public function peluang(){
        
        return view('admins/peluang.view_peluang');
    }
    /**
     * @OA\Get(
     *     path="/api/Users/GetUsers",
     *     tags={"Appl_user"},
     *     summary="Return All User",
     *     description="Retrive data User all",
     *

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function GetPeluang(Request $request){

        $Peluang = $this->svcPeluangRepository->GetPeluang($request);
    
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $Peluang,
            'Message'   => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function Getdaerah(Request $request){

        $daerah = $this->svcPeluangRepository->Getdaerah($request);
    
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $daerah,
            'Message'   => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function Getsumber(Request $request){

        $sumber = $this->svcPeluangRepository->Getsumber($request);
    
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $sumber,
            'Message'   => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function upload_summernote(Request $request){


        
        //Loop for getting files with index like image0, image1
     
            if ($request->hasFile('image')) {
             
            $file      = $request->file('image');
            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture   =  $filename;
      
      //Save files in below folder path, that will make in public folder
            $file->move(public_path('upload/'), $picture);
            echo url('/upload/'. $filename.'');
        
         
    }
    
    }
    public function remove_summernote(Request $request){


        
        //Loop for getting files with index like image0, image1
     
        $file_name = str_replace(url('/'), '', $request['url']);
        $nama = str_replace('upload', '',  $file_name);
        $last = str_replace('//', '',  $nama);
        $image_path = (public_path('upload/'). $last);
           
               $file = File::delete($image_path);
               echo $file;
             
      
    
    }
    
    public function Getsektor(Request $request){

        $sektor = $this->svcPeluangRepository->Getsektor($request);
    
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $sektor,
            'Message'   => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }
    
    public function AddPeluang(Request $request){

      

        $addPeluang = $this->svcPeluangRepository->AddPeluang($request);
         $responses =  array(
            'Status'    => $addPeluang == true ? true:false,
            'Data'     => ''.$addPeluang.'',
            'Message'   => $addPeluang == true ? 'Data Berhasil Disimpan':'Data Gagal Disimpan'
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

     public function DeletePeluang(Request $request){
     
        $deletePeluang = $this->svcPeluangRepository->DeletePeluang($request);
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => null,
            'Message'   =>  $deletePeluang
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }


    public function Updatepeluang(Request $request){
     

        $updatepeluang = $this->svcPeluangRepository->Updatepeluang($request);
         $responses =  array(
            'Status'    => $updatepeluang == '' ? true:false ,
            'Data'     => $updatepeluang,
            'Message'   => $updatepeluang == '' ? 'Data Berhasil Dirubah':'Data Gagal dirubah'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

}
