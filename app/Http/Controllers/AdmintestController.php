<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdmintestController extends Controller
{
    //

    public function formListUser(){
        return view('users.user_management');
    }

    public function formListRole(){
        return view('users.role_setting');
    }

    public function formListMenu(){
        return view('peluang.menu_peluang');
    }

    public function formApplSetting(){
        return view('users.appl_setting');

    }
}
