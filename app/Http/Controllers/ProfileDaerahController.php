<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileDaerahController extends Controller
{
    //

    public function DemografiProvinsi(){
        return view('admins.profil_daerah.provinsi.view_demografi_provinsi');
    }
    public function UmrProvinsi(){
        return view('admins.profil_daerah.provinsi.view_umr_provinsi');
    }
    public function PdrbProvinsi(){
        return view('admins.profil_daerah.provinsi.view_pdrb_provinsi');
    }
    public function DemografiKabKot(){
        return view('admins.profil_daerah.kabkota.view_demografi_kabupaten');
    }
    public function UmrKabkot(){
        return view('admins.profil_daerah.kabkota.view_umr_kabkot');
    }
    public function PdrbKabkot(){
        return view('admins.profil_daerah.kabkota.view_pdrb_kabkot');
    }
}
