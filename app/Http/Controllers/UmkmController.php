<?php

namespace App\Http\Controllers;

use App\Repository\ApplTaskRepository;
use App\Repository\UmkmRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class UmkmController extends Controller
{
    /*
        | -----------------------------------------------------
        | PRODUCT NAME: 	Admin PIR
        | -----------------------------------------------------
        | AUTHOR:			safari
        | -----------------------------------------------------
        | EMAIL:			safari.erie@gmail.com
        | -----------------------------------------------------
        | CREATED_DT:   20210714
        | -----------------------------------------------------
        | Module: UMKM
        | -----------------------------------------------------
	*/

    /* show form Umkm Perusahaan */

    protected $applTaskRepository;
    public function __construct(ApplTaskRepository $applTaskRepository)
    {
        $this->applTaskRepository = $applTaskRepository;

    }
    public function UmkmPerusahaan(){
        $action_uri_name = request()->segment(2);

        $id_role = Auth::user()->id_role;
        $param = ['action_name'=>$action_uri_name,'role_id'=>$id_role];
        $permission = $this->applTaskRepository->CheckPermission($param);

        return view('admins.umkm.umkm_perusahaan',compact('permission'));
    }

    public function Umkm(){
        return view('admins.umkm.umkm_list');
    }

    public function LaporanUmkm(){
        return view('admins.umkm.laporan_umkm');
    }


}
