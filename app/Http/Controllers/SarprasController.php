<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SarprasController extends Controller
{
    //
    public function Bandara(){
        return view('admins.sarpras.view_bandara');
    }

    public function Pelabuhan(){
        return view('admins.sarpras.view_pelabuhan');
    }
    public function Pendidikan(){
        return view('admins.sarpras.view_Pendidikan');
    }
    public function RumahSakit(){
        return view('admins.sarpras.view_rumah_sakit');
    }
    public function Hotel(){
        return view('admins.sarpras.view_hotel');
    }
    public function Blk(){
        return view('admins.sarpras.view_blk');
    }
}
