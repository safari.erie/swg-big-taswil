<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WilayahAdmController extends Controller
{
    //
    /* show view provinsi */
    public function formListProvinsi(){
        return view('admins.wilayah_adm.provinsi_list');
    }

    /* show view kabupaten */
    public function formListKabupaten(){
         return view('admins.wilayah_adm.kabupaten_list');
    }
    /* show view kecamatan */
    public function formListKecamatan(){
         return view('admins.wilayah_adm.kecamatan_list');
    }

}
