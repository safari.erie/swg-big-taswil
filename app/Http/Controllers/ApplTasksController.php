<?php

namespace App\Http\Controllers;

use App\Repository\ApplTaskRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class ApplTasksController extends Controller
{
    //
    protected $applTaskRepository;
    public function __construct(ApplTaskRepository $applTaskRepository)
    {
        $this->applTaskRepository = $applTaskRepository;
    }

    public function GetApplTasks(Request $request){

        $param = $request->only([
              'IdAppl'
            ]);
        $validator = Validator::make($request->all(),[
                'IdAppl' => ['required']
            ]);
        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);

        }

        $applTasks = $this->applTaskRepository->GetApplTasks($param['IdAppl']);
        $responses =  array(
            'Status'    => $applTasks->count() > 0 ? true:false,
            'Data'     => $applTasks,
            'Message'   => $applTasks->count() > 0 ? 'Data Tersedia':'Data Belum Tersedia'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetApplTaskById(Request $request){
         $param = $request->only([
              'IdApplTask'
            ]);
        $validator = Validator::make($request->all(),[
                'IdApplTask' => ['required']
            ]);
        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);

        }

        $applTaskById = $this->applTaskRepository->GetApplTaskById($param['IdApplTask']);
        $responses =  array(
            'Status'    => $applTaskById->count() > 0 ? true:false,
            'Data'     => $applTaskById,
            'Message'   => $applTaskById->count() > 0 ? 'Data Tersedia':'Data Belum Tersedia'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetAppls(){
        $appls = $this->applTaskRepository->GetAppls();
        $responses =  array(
            'Status'    => $appls->count() > 0 ? true : false,
            'Data'     => $appls,
            'Message'   => ''
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function AddApplTask(Request $request){
         $param = $request->only([
              'IdAppl','IdApplTaskParent','ApplTaskName','ControllerName','ActionName','Description','IconName'
            ]);

        $validator = Validator::make($request->all(),[
            'IdAppl' => ['required'],
            'ApplTaskName' => ['required'],
        ]);

          if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);

        }

        $storeApplTask = $this->applTaskRepository->AddAppl($param);
        $responses = array(
            'Status'    => $storeApplTask = true ? true : false,
            'Data'     => '',
            'Message'   => $storeApplTask = true ? 'Data Berhasil Disimpan':'Data Gagal Disimpan'.$storeApplTask
        );

        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function GetApplTaskByRole(Request $request){
         $param = $request->only([
              'IdRole'
            ]);

        $validator = Validator::make($request->all(),[
            'IdRole' => ['required']
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);

        }

        $GetApplTasks = $this->applTaskRepository->GetApplTaskByRole($param['IdRole']);
        $responses = array(
            'Status'    => true,
            'Data'     => $GetApplTasks,
            'Message'   => ''
        );
       /*  $responses = array(
            'Status'    => $GetApplTasks['Status'] ? true:false,
            'Data'     => $GetApplTasks['Status'] ? $GetApplTasks['Data'] : null,
            'Message'   => $GetApplTasks['Message']
        ); */

        return response()->json($responses,HttpFoundationResponse::HTTP_OK);


    }

    public function AddTasksRole(Request $request){
        $param = $request->only([
            'IdRole','IdApplTasks'
        ]);

        $validator = Validator::make($request->all(),[
            'IdRole' => ['required']
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);

        }



        $addTaskRoles = $this->applTaskRepository->AddTasksRole($param['IdRole'],$param['IdApplTasks']);

        $responses = array(
            'Status'    => true,
            'Data'     => $addTaskRoles,
            'Message'   => ''
        );


        return response()->json($responses,HttpFoundationResponse::HTTP_OK);



    }

    public function GetApplSettings(){
        $tbApplSettings = $this->applTaskRepository->GetApplSettings();
        $responses = array(
            'Status'    => $tbApplSettings != null ? true : false,
            'Data'     => $tbApplSettings,
            'Message'   => $tbApplSettings = true ? 'Data Tersedia':'Data Belum Tersedia'
        );

        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }


}
