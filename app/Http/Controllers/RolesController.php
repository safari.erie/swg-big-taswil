<?php

namespace App\Http\Controllers;

use App\Repository\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class RolesController extends Controller
{
    //
    protected $roleRepository;
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/Roles/GetRoles",
     *     tags={"Appl_Role"},
     *     summary="Return All Role MAster",
     *     description="Retrive data Role all",
     *

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

    public function GetRoles(){
        $roles = $this->roleRepository->GetRoles();
        $responses =  array(
            'Status'    => $roles !== null ? true:false,
            'Data'     => $roles,
            'Message'   => $roles !== null ? 'Data Tersedia':'Data Tidak Admin'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

     /**
     * @OA\Get(
     *     path="/api/Roles/GetRole",
     *     tags={"Appl_Role"},
     *     summary="Return Role By Id Role",
     *     description="Retrive data Role By IdRole",
     *     @OA\Parameter(
     *          name="idRole",
     *          description="IdRole",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

    public function GetRole(Request $request){
        $param = $request->only([
              'idRole'
            ]);
        $role = $this->roleRepository->GetRole($param['idRole']);
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $role,
            'Message'   => ''
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

     /**
     * @OA\Delete(
     *     path="/api/Roles/DeleteRole",
     *     tags={"Appl_Role"},
     *     summary="Return Delete By Id Role",
     *     description="Delete data Role By IdRole",
     *     @OA\Parameter(
     *          name="idRole",
     *          description="IdRole",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */

    public function DeleteRole(Request $request){
        $param = $request->only([
              'idRole'
            ]);
        $validator = Validator::make($request->all(),[
                'idRole' => ['required']
            ]);
        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        $role = $this->roleRepository->DeleteRole($param['idRole']);
        $responses =  array(
            'Status'    => 'OK',
            'Data'     => null,
            'Message'   => $role
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function UpdateRole(Request $request){
        $param = $request->only([
              'IdRole','RoleName'

            ]);
        $validator = Validator::make($request->all(),[
                'IdRole' => ['required'],
                'RoleName' => ['required']
            ]);
        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $updateRole = $this->roleRepository->UpdateRole($param);
         $responses =  array(
            'Status'    => $updateRole == '' ? true:false ,
            'Data'     => null,
            'Message'   => $updateRole == '' ? 'Data Berhasil Dirubah':'Data Gagal dirubah'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function AddRole(Request $request){

        $param = $request->only([
            'role_name'
        ]);
        $validator = Validator::make($request->all(),[
                'role_name' => ['required']
            ]);

        if($validator->fails()){
            return response()->json([
                'status' => HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY,
                'data'  => null,
                'returnMessage' => $validator->errors()
            ],HttpFoundationResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $saveRole = $this->roleRepository->AddRole($request);
        $responses =  array(
            'Status'    => $saveRole == '' ? true:false ,
            'Data'     => null,
            'Message'   => $saveRole == '' ? 'Data Berhasil Disimpan':'Data Gagal disimpan'
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
}
