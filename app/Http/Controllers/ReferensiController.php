<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReferensiController extends Controller
{
    //
    public function Provinsi(){
        return view('admins.referensi.provinsi.provinsi_index');
    }
    public function Kabupaten(){
        return view('admins.referensi.kabupaten.kabupaten_index');
    }

    public function KategoriKawasan(){
        return view('admins.referensi.kategori_kawasan_industri.kategori_kawasan_industi_index');
    }
}
