<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KawasanIndustriController extends Controller
{
    //
    public function index(){
        return view('admins.master_kawasan_industri.view_kawasan_industri');
    }
    public function InisialKawasanIndustri(){
        return view('admins.master_kawasan_industri.view_inisialisasi_kawasan_industri');
    }
}
