<?php

namespace App\Http\Controllers;

use App\Repository\ApplTaskRepository;
use App\Repository\RoleRepository;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class AdminApplController extends Controller
{
    //
    protected $applTaskRepository;
    protected $roleRepository;
    public function __construct(ApplTaskRepository $applTaskRepository,RoleRepository $roleRepository)
    {
        $this->applTaskRepository = $applTaskRepository;
        $this->roleRepository = $roleRepository;
    }

    public function formListUser(){
        return view('users.user_management');
    }

    public function formListRole(){
        return view('users.role_setting');
    }

    public function formListMenu(){
        return view('users.menu_setting');
    }

    public function formApplSetting(){
        return view('users.appl_setting');

    }

    public function formPermission(Request $request){

        $IdRole = $request->IdRole;
        $GetRoleById = $this->roleRepository->GetRole($IdRole);

        $GetPermissions = $this->applTaskRepository->GetPermissions($IdRole);

        return view('users.role_permission',compact('GetRoleById','GetPermissions'));


    }

    public function GetPermission(Request $request){

        $permissions = $this->applTaskRepository->GetPermissions($request->IdRole);

        $responses = array(
            'Status'    => $permissions != null ? true : false,
            'Data'     => $permissions,
            'Message'   => $permissions != null ? 'Data Tersedia':'Data Belum Tersedia'
        );

        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function ChangePermission(Request $request){
        $changePermission = $this->applTaskRepository->ChangePermission($request);

        $responses = array(
            'Status'    => $changePermission['Status'],
            'Data'     => '',
            'Message'   => $changePermission['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
}
