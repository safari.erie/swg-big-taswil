<?php

namespace App\Http\Controllers;

use App\Repository\SvcUserRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class UsersController extends Controller
{
    //
    protected $svcUserRepository;
    public function __construct(SvcUserRepository $svcUserRepository)
    {
        $this->svcUserRepository = $svcUserRepository;
    }

    /**
     * @OA\Get(
     *     path="/api/Users/GetUsers",
     *     tags={"Appl_user"},
     *     summary="Return All User",
     *     description="Retrive data User all",
     *

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function GetUsers(){

        $users = $this->svcUserRepository->GetUsers();

        $responses =  array(
            'Status'    => 'OK',
            'Data'     => $users,
            'Message'   => ''
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function AddUser(Request $request){

        $param = $request->only([
              'Username','Password','Email','Firstname','MiddleName','LastName','Address','PhoneNumber','MobileNumber','Role'
            ]);

        $validator = Validator::make($request->all(),[
            'Username' => ['required'],
            'Password' => ['required'],
        ]);

        $addUser = $this->svcUserRepository->AddUser($param);
         $responses =  array(
            'Status'    => $addUser == true ? true:false,
            'Data'     => '',
            'Message'   => $addUser == true ? 'Data Berhasil Disimpan':'Data Gagal Disimpan'
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

}
