<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Repository\UmkmRepository;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
class UmkmsController extends Controller
{
    //
    protected $umkmRepository;
    public function __construct(UmkmRepository $umkmRepository)
    {
        $this->umkmRepository = $umkmRepository;

    }

    public function GetPerusahaans(Request $request){

        $param = $request->only([
            'Type',
        ]);

        $perusahaans = $this->umkmRepository->GetUmkmPerusahaans($param['Type']);
        $responses = array(
            'Status'    => count($perusahaans) > 0 ? true :false,
            'Data'     => $perusahaans,
            'Message'   => ''
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function AddPerusahaanUmkm(Request $request){



        $addPerusahaanUmkm = $this->umkmRepository->AddUmkmPerusahaan($request);
        $responses = array(
            'Status'    => $addPerusahaanUmkm['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanUmkm['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
    public function AddPerusahaanCabangUmkm(Request $request){



        $addPerusahaanUmkm = $this->umkmRepository->AddUmkmCabangPerusahaan($request);
        $responses = array(
            'Status'    => $addPerusahaanUmkm['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanUmkm['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
    public function EditPerusahaanUmkm(Request $request){



        $addPerusahaanUmkm = $this->umkmRepository->EditPerusahaanUmkm($request);
        $responses = array(
            'Status'    => $addPerusahaanUmkm['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanUmkm['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

     public function DeletePerusahaan(Request $request){


        $deletePerusahaan = $this->umkmRepository->DeletePerusahaanUmkm($request);
        $responses =  array(
            'Status'    => $deletePerusahaan['Status'],
            'Data'     => '',
            'Message'   => $deletePerusahaan['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetPerusahaan(Request $request){
        $param = $request->only([
            'IdPerusahaan',
        ]);

        $perusahaan = $this->umkmRepository->GetUmkmPerusahaan($param);
         $responses = array(
            'Status'    => count($perusahaan) > 0 ? true : false,
            'Data'     => $perusahaan,
            'Message'   => count($perusahaan) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetPerusahaanCabangs(Request $request){


        $perusahaan = $this->umkmRepository->GetUmkmCabangPerusahaans($request);

         $responses = array(
            'Status'    => count($perusahaan) > 0 ? true : false,
            'Data'     => $perusahaan,
            'Message'   => count($perusahaan) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetPerusahaanKblis(Request $request){


        $perusahaanKblis = $this->umkmRepository->GetUmkmPerusahaanKblis($request);

         $responses = array(
            'Status'    => count($perusahaanKblis) > 0 ? true : false,
            'Data'     => $perusahaanKblis,
            'Message'   => count($perusahaanKblis) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function AddPerusahaanKbli(Request $request){

        $addPerusahaanKbli = $this->umkmRepository->AddPerusahaanKbli($request);
        $responses = array(
            'Status'    => $addPerusahaanKbli['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanKbli['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function GetPerusahaanProduk(Request $request){

        $PerusahaanProduks = $this->umkmRepository->GetPerusahaanProduks($request);
        $responses = array(
            'Status'    => count($PerusahaanProduks) > 0 ? true : false,
            'Data'     => $PerusahaanProduks,
            'Message'   => count($PerusahaanProduks) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function AddPerusahaanProduk(Request $request){
        $addPerusahaanProduk = $this->umkmRepository->AddPerusahaanProduk($request);
        $responses = array(
            'Status'    => $addPerusahaanProduk['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanProduk['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetPerusahaanKbliByIdPerusahaans(Request $request ){

        $perusahaanKblis = $this->umkmRepository->GetPerusahaanKbliByIdPerusahaan($request);
         $responses = array(
            'Status'    => count($perusahaanKblis) > 0 ? true : false,
            'Data'     => $perusahaanKblis,
            'Message'   => count($perusahaanKblis) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetKategoriKblis(Request $request){


        $KategoriKblis = $this->umkmRepository->GetKategoriKbli($request);

         $responses = array(
            'Status'    => count($KategoriKblis) > 0 ? true : false,
            'Data'     => $KategoriKblis,
            'Message'   => count($KategoriKblis) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

     public function GetKelompokKblis(Request $request){


        $KelompokKblis = $this->umkmRepository->GetKelompokKblis($request);

         $responses = array(
            'Status'    => count($KelompokKblis) > 0 ? true : false,
            'Data'     => $KelompokKblis,
            'Message'   => count($KelompokKblis) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetKbliByIdParents(Request $request){


        $Kblis = $this->umkmRepository->GetKbliByIdParents($request);

         $responses = array(
            'Status'    => count($Kblis) > 0 ? true : false,
            'Data'     => $Kblis,
            'Message'   => count($Kblis) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetKelompokKbliParents(Request $request){


        $Kblis = $this->umkmRepository->GetKelompokKbliParents($request);

         $responses = array(
            'Status'    => '',
            'Data'     => $Kblis,
            'Message'   => ''
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }


    public function GetPerusahaanKebutuhanProdukKblis(Request $request){

        $PerusahaanKebutuhanProduk = $this->umkmRepository->GetPerusahaanKebutuhanProdukKblis($request);
         $responses = array(
            'Status'    => count($PerusahaanKebutuhanProduk) > 0 ? true : false,
            'Data'     => $PerusahaanKebutuhanProduk,
            'Message'   => count($PerusahaanKebutuhanProduk) > 0 ? "Data Ada" : "Data tidak ada"
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);

    }

    public function AddPerusahaanKebutuhanProdukKbli(Request $request){
         $addPerusahaanKebutuhanProduk = $this->umkmRepository->AddPerusahaanKebutuhanProdukKbli($request);
        $responses = array(
            'Status'    => $addPerusahaanKebutuhanProduk['Status'],
            'Data'     => '',
            'Message'   => $addPerusahaanKebutuhanProduk['Message']
        );
        return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
}
