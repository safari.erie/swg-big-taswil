<?php

namespace App\Http\Controllers;

use App\Repository\SvcWilayahRepository;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class WilayahAdministrasiController extends Controller
{
    protected $svcWilayahRepository;
    public function __construct(SvcWilayahRepository $svcWilayahRepository)
    {
        $this->svcWilayahRepository = $svcWilayahRepository;
    }
    /**
     * @OA\Get(
     *     path="/api/WilayahAdm/provinsis",
     *     tags={"WA_Provinsis"},
     *     summary="Return All Provinsi",
     *     description="Retrive data Provinsi all",
     *     operationId="greet",

     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function provinsis()
    {
        $provinsis = $this->svcWilayahRepository->findAllProvinsis();
        $responses = array(
            'Status' => $provinsis->count() > 0 ? true : false,
            'Data'  => $provinsis,
            'returnMessage' => ''
        );
         return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }

    public function GetKabKots(Request $request){
         $param = $request->only([
            'IdAdmProvinsi',
        ]);
        $KabKots = $this->svcWilayahRepository->GetKabkotByIdProv($param);
        $responses = array(
            'Status' => count($KabKots) > 0 ? true : false,
            'Data'  => $KabKots,
            'returnMessage' => ''
        );
          return response()->json($responses, HttpFoundationResponse::HTTP_OK);
    }
}
