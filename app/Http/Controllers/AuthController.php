<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Models\User;

/* Created by : Safari.erie
   Description
   To Do List
 */

class AuthController extends Controller
{
    //
    public function showFormLogin(){
        if(Auth::check()){
            return redirect()->route('admin');
        }
        return view('users.login');
    }

    public function login(Request $request){


        $rules = [
            'username' =>   'required',
            'password'  =>  'required|string',
        ];

        $messages = [
            'username.required'        => 'Email wajib diisi',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = [
            'username'     => $request->input('username'),
            'password'  => $request->input('password'),
        ];


        Auth::attempt($data);
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success

            return redirect('/admin');
            //echo 'masuk';

        } else { // false

            //Login Fail
            Session::flash('error', 'Email atau password salah');
            return redirect()->route('login');
        }


    }

    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login');
    }
}
