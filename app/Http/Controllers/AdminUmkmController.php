<?php

namespace App\Http\Controllers;

use App\Repository\UmkmRepository;
use Illuminate\Http\Request;

class AdminUmkmController extends Controller
{
    //
    protected $umkmRepository;
    public function __construct(UmkmRepository $umkmRepository)
    {
        $this->umkmRepository = $umkmRepository;
    }
    public function perusahaan(){
        return view('admins.umkm.perusahaan');
    }

    /* created safari.eri
       method for show master umkm

    */
    public function formMasterUmkm(){
        return view('admins.umkm.umkm_list');
    }

    public function formUmkmPekerjaan(){
        return view('admins.umkm.umkm_pekerjaan_list');
    }


}
