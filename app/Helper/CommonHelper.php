<?php

use App\Repository\ApplTaskRepository;
use Illuminate\Support\Facades\Auth;
class CommonHelper {

    protected $applTaskRepository;

    public function __construct(ApplTaskRepository $applTaskRepository)
    {
        $this->applTaskRepository = $applTaskRepository;
    }
    static function get_query_menu($param){

        $objMenus = new ApplTaskRepository();
        $idUser = Auth::User()->id_role;

        $datas = $objMenus->GetMenus(1,$idUser);

        $counts = $datas->count();

        $OPEN_LIST_ITEM_TAG_CLASS = "<li class='nav-item dropdown'>";
        $OPEN_LIST_UL_CLASS_DROPDOWN = "<ul class='dropdown-menu'>";
        $CLOSE_LIST_ITEM_TAG = "</li>";
        $html = "";
        $url = url('/')."/";
        if($counts == 0){
            $html .= " <ul> {$url}</ul>";
        }else{

             $filterObjTaskParents = $datas->filter(function ($value, $key) {
                 return $value['id_appl_task_parent'] === null;
            });
            foreach($filterObjTaskParents as $filterObjTaskParent){
                $html .= $OPEN_LIST_ITEM_TAG_CLASS;
                // check of each obj all data same parentid
                $id_appl_task = $filterObjTaskParent['id_appl_task'];

                $dataChilds = $objMenus->GetRootChild($id_appl_task,$idUser);

                if($dataChilds->count() > 0){

                    $html .= "<a href='javascript:void(0)' data-toggle='dropdown' class='nav-link has-dropdown'> <i class='fa fa-bar'></i> <span>" .$filterObjTaskParent['appl_task_name']. "</span> </a>";
                    $html .= self::buildOfTree($filterObjTaskParent, $idUser, $param);
                }else{

                    $html .= "<a href='javascript:void(0)' data-toggle='dropdown' class='nav-link has-dropdown'> <i class='fa fa-bar'></i> <span>" .$filterObjTaskParent['appl_task_name']. "</span> </a>";
                }

                $html .= $CLOSE_LIST_ITEM_TAG;



            }

             return $html;
        }
    }

    static function buildOfTree($datas, $idUser, $param){

        $objMenus = new ApplTaskRepository();
        $html = "<ul class='dropdown-menu'>";
        $childItems = $objMenus->GetRootChild($datas['id_appl_task'],$idUser);
        foreach($childItems as $childItem){

            if($childItem['controller_name'] != null){
                $url = $_ENV['APP_URL']."/" ;
                if($param == $childItem['action_name']){
                    $html .= '<li class="active">';
                }else{
                    $html .= '<li>';
                }

                $html .= "<a href='${url}".$childItem['controller_name']."/".$childItem['action_name']."' class='nav-link'> <i class='fa fa-bar'></i> <span>" .$childItem['appl_task_name']. "</span> </a>";
                $subChilds = $objMenus->GetRootChild($childItem['id_appl_task'],$idUser);
                if($subChilds->count() > 0){
                     $html .= self::buildOfTree($childItem, $idUser,$param);

                }
                $html .= '</li>';
            }else{
                $html .= "<li class='nav-item dropdown'>";
                $html .= "<a href='javascript:void(0)' data-toggle='dropdown' class='nav-link has-dropdown'> <i class='fa fa-bar'></i> <span>" .$childItem['appl_task_name']. "</span> </a>";
                //$html .= "<a href='javascript:void(0)' data-toggle='dropdown' class='nav-link has-dropdown'>  " .$childItem['appl_task_name']. "</a>";
                $subChilds = $objMenus->GetRootChild($childItem['id_appl_task'], $idUser);
                if($subChilds->count() > 0){
                     $html .= self::buildOfTree($childItem, $idUser, $param);

                }
                $html .= "</li>";
            }


        }
        $html .= "</ul>";
        return $html;
    }

    static function GetUserName(){
        $username = Auth::User()->username;

        return $username;
    }


}
?>
