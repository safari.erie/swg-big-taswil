<?php
    define('DOCUMENT_CREATE',0);
    define('DOCUMENT_APPROVE',99);
    define('DOCUMENT_REJECT',-1);
    define('DOCUMENT_PUBLISHED',1);
    define('DOCUMENT_VERIFIKASI',1);
    define('DOCUMENT_NO_VERIFIKASI',0);
    define('PERUSAHAAN',1);
    define('LOKASI_KERJA_PERUSAHAAN',2);
    define('NO_ADD_PERMISSION','Tidak Mempunyai Akses Tambah');
    define('NO_READ_PERMISSION','Tidak Mempunyai Akses Baca');
    define('DOCUMENT_CREATE_NOTE','Dokumen Baru');

?>
