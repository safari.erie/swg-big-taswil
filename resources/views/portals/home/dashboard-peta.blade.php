<div class="container-fluid m-0 p-0" style="background: rgba(100,200,150,0.3)">
  <div class="row m-0 p-0" style="background: url('{{asset('portal/img/batik.png')}}') repeat ">
    <div class="col-lg-12 m-0 p-2 text-right">
      <h3 class="font-weight-bolder text-light text-monospace">&nbsp;</h3>
    </div>
  </div>
</div>
<div class="container-fluid p-0 m-0" style="background: rgba(50,50,50,0.7)">
  <div class="row p-2 m-0">
    <div class="col-lg-4">
      <div class="card shadow rounded bg-transparent border-success">
        <div class="card-body text-success">
          <h5 class="card-title text-center"><span class="fa fa-map-pin">&nbsp;Provinsi</span></h5>
          <h4 class="card-text text-center font-weight-bolder">Aceh</h4>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card shadow rounded bg-transparent border-warning">
        <div class="card-body text-warning">
          <h5 class="card-title text-center"><i class="fa fa-money"></i>&nbsp;Total Nilai Proyek</h5>
          <h4 class="card-text text-center font-weight-bolder">Rp.1,234 M</h4>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card shadow rounded bg-transparent border-info">
        <div class="card-body text-info">
          <h5 class="card-title text-center"><i class="fa fa-truck"></i>&nbsp;Jumlah Proyek</h5>
          <h4 class="card-text text-lg-center font-weight-bolder">3</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="row m-0 p-0">
    <div class="col m-0 p-0 align-middle">
        {{-- <img class="text-center d-none d-lg-block" src="{{asset('portal/img/id.png')}}" alt="..."> --}}
         <div id="map">

    </div>
     </div>
  </div>
</div>
<div class="container-fluid m-0 p-0" style="background: rgba(100,200,150,0.3)">
  <div class="row m-0 p-0" style="background: url('{{asset('portal/img/batik.png')}}') repeat ">
    <div class="col-lg-12 m-0 p-2 text-right">
      <h3 class="font-weight-bolder text-light text-monospace">&nbsp;</h3>
    </div>
  </div>
</div>

