<div class="container-fluid pb-5 shadow topo align-content-center">
  <div class="row mb-5 pt-5"></div>
  <div class="row overflow-hidden" style="background: rgba(115,197,51,0.3);border-radius: 7em 0 0 7em;">
    <div class="col-lg-4 col-md-12 p-0" style=" z-index: 2;" >
      <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
          <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
          <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" class="active"></li>
        </ol>
        <div class="carousel-inner" >
          <div class="carousel-item active"> <img src="{{asset('portal/img/Jalan_menuju_pelabuhan_Pertambangan_PT__ARA.jpg')}}" class="d-block img-fluid" alt="..."
            style="object-fit: contain">
            <div class="carousel-caption d-block d-md-block p-1" style="background: rgba(100,100,100,0.60);">
              <h5 class=" p-1 m-0 font-weight-bold border-bottom">Proyek 1</h5>
              <p class="p-0 mb-3">deskripsi......</p>
            </div>
          </div>
          <div class="carousel-item"> <img src="{{asset('portal/img/Jalan_menuju_pelabuhan_Pertambangan_PT__ARA.jpg')}}" class="d-block img-fluid" alt="..."
            style="object-fit: contain">
            <div class="carousel-caption d-block d-md-block p-1" style="background: rgba(100,100,100,0.60);">
              <h5 class=" p-1 m-0 font-weight-bold border-bottom">Proyek 2</h5>
              <p class="p-0 mb-3">deskripsi......</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev"> <span class="carousel-control-prev-icon bg-dark"></span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next"> <span class="carousel-control-next-icon bg-dark"></span> </a> </div>
      </div>
      <div class="col-lg text-center align-self-center p-2 m-2 rounded shadow" style="background: rgba(255,255,255,0.1)">
        <p> <strong class="text-light">R<span class="pir-green">i</span>iS</strong> (Regional Investment Information System) is a geospatial-based information system for investment potential and opportunities in 34 Provinces and 514 Regencies/Cities in Indonesia.<br><br>
        <strong class="text-light">R<span class="pir-green">i</span>iS</strong> contains regional profile information including demographic data, commodities, income and the Regional Minimum Wage (UMR), as well as supporting infrastructure.</p>
      </div>
      <div class="col-lg align-middle">
        <div class="row h-50 align-middle">
          <div class="col-md align-middle" style="z-index: 2;background-color: rgba(50,50,50,0.60);">
            <div class="row h-100 text-monospace text-light align-items-center">
              <div class="col-md text-center align-middle border-end border-5">
                <h6>Total Nilai Proyek</h6>
                <h5>Rp. 103 Triliun</h5>
              </div>
              <div class="col-md text-center align-middle border-start border-5">
                <h6>Jumlah Proyek</h6>
                <h5>30</h5>
              </div>
            </div>
          </div>
        </div>
        <div class="row h-50 align-middle">
          <div class="col-md align-middle" style="opacity: 0.8; z-index: 2;">
            <div class="row h-100 align-items-center" >
              <div class="col align-middle">
                <div class="row me-1 my-1">
                  <div class="col rounded-start border text-light bg-info">Industri</div>
                  <div class="col bg-dark fw-bold rounded-end text-info text-end border border-info">54321</div>
                </div>
                <div class="row me-1 my-1">
                  <div class="col rounded-start border text-light bg-warning">Pariwisata</div>
                  <div class="col bg-dark fw-bold rounded-end text-warning text-end border border-warning">54321</div>
                </div>
                <div class="row me-1 my-1">
                  <div class="col rounded-start border text-light bg-success">Pangan dan Pertanian</div>
                  <div class="col bg-dark fw-bold rounded-end text-success text-end border border-success">54321</div>
                </div>
                <div class="row me-1 my-1">
                  <div class="col rounded-start border text-light bg-primary">Infrastruktur</div>
                  <div class="col bg-dark fw-bold rounded-end text-primary text-end border border-primary">54321</div>
                </div>
                <div class="row me-1 my-1">
                  <div class="col rounded-start border text-light bg-danger">Jasa</div>
                  <div class="col bg-dark fw-bold rounded-end text-danger text-end border border-danger">54321</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>