
@extends('portal_layouts.main')
@push('styles')
{{-- <link rel="stylesheet" href="https://js.arcgis.com/3.36/dijit/themes/claro/claro.css">
  <link rel="stylesheet" href="https://js.arcgis.com/3.36/esri/css/esri.css"> --}}
  <link rel="stylesheet" href={{asset('/arcgis_js_api/library/3.36/3.36/esri/css/esri.css')}}>
  <style>
    html, body { height: 400px; width: 100%; margin: 0; padding: 0; }
    #map{
      padding:0;
    }
        p {
            line-height: 1.5rem;
            color: rgb(0 0 0 / 76%);
        }
  </style>
@endpush

@section('content')
    @include('portals.home.dashboard-utama')
    @include('portals.home.dashboard-peta')
@stop
@push('scripts')
    <script src={{asset('/arcgis_js_api/library/3.36/3.36/init.js')}}></script>
    {{-- <script src="https://js.arcgis.com/3.36/"></script> --}}
    <script src={{asset('/module-js/portals/beranda.js')}}></script>
@endpush
