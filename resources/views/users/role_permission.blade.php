@extends('admin_layouts.main')
@section('title','Hak Akses')
@push('style-admin')
    <style type="text/css">
        .fix-height-card {
            height: 560px;
            max-height: 560px;
        }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Role</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Manajemen Aplikasi</a></div>
              <div class="breadcrumb-item"><a href="#">Role </a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Hak Akses Role : {{$GetRoleById->role_name}} <strong> </strong></h2>
             <input type="hidden" name="IdRole" id="IdRole" value={{$GetRoleById->id_role}}>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a href={{$_ENV['APP_URL'].'/appl/role_setting'}} class="btn btn-dark"> <i class="fa fa-backward"></i> Back</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="role-permissions" style="width:100%;">
                                    <thead>
                                        <th>#</th>
                                        <th>Modul</th>
                                        <th>Read</th>
                                        <th>Edit</th>
                                        <th>Approve</th>
                                        <th><?php echo '&nbsp;&nbsp;'?>Delete</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($GetPermissions as $item)
                                        @php
                                            $taskName = str_repeat(' <pre>  ',$item->level).$item->appl_task_name;
                                            $can_read = $item->can_read == 1 ? 'checked':'';
                                            $can_edit = $item->can_edit == 1 ? 'checked':'';
                                            $can_approve = $item->can_approve == 1 ? 'checked':'';
                                            $can_delete = $item->can_delete == 1 ? 'checked':'';
                                            //$output = nl2br(str_repeat('&nbsp;$nbsp', $item->level. $item->appl_task_name));

                                        @endphp
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{!!$taskName!!}</td>
                                                <td class="text-center">
                                                    @if ($item->controller_name != null)
                                                        <label class="fancy-checkbox">
                                                            <input type="checkbox" name="read_{{$item->id_appl_task}}" id="read_{{$item->id_appl_task}}" {{$can_read}} onClick="changeStatus(this);" />
                                                            <span></span>
                                                        </label>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if ($item->controller_name != null)
                                                        <label class="fancy-checkbox">
                                                            <input type="checkbox" name="edit_{{$item->id_appl_task}}" id="edit_{{$item->id_appl_task}}" {{$can_edit}} onClick="changeStatus(this);" />
                                                            <span></span>
                                                        </label>
                                                    @endif

                                                </td>
                                                <td class="text-center">
                                                    @if ($item->controller_name != null)
                                                        <label class="fancy-checkbox">
                                                            <input type="checkbox" name="approve_{{$item->id_appl_task}}" id="approve_{{$item->id_appl_task}}" {{$can_approve}} onClick="changeStatus(this);" />
                                                            <span></span>
                                                        </label>
                                                    @endif

                                                </td>
                                                <td class="text-center">
                                                    @if ($item->controller_name != null)
                                                        <label class="fancy-checkbox">
                                                            <input type="checkbox" name="delete_{{$item->id_appl_task}}" id="delete{{$item->id_appl_task}}" {{$can_delete}} onClick="changeStatus(this);" />
                                                            <span></span>
                                                        </label>
                                                    @endif

                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
@push('script-adm')
    <script src={{ asset('/module-js/admins/appl/role_permission.js') }}></script>
@endpush
