@extends('admin_layouts.main')
@section('title','Dashaboard')
@push('style-admin')
    <style>
        .modal-full {
                max-width: 98%;
            }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>User Pengguna</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Utility</a></div>
              <div class="breadcrumb-item"><a href="#">Pengguna</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master User Pengguna</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="userPenggunaModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="user-management" style="width:100%;">
                                    <thead>
                                        <th >No</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Nama</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- all define modal --}}
    <div class="modal fade" role="dialog" id="UserManagementModal">
         <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data">
                        <input type="hidden" class="form-control" name="IdUser" placeholder="Ketikkan Id User..." />
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Username:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Username" placeholder="Ketikkan Username..." />
                            </div>
                        </div>
                        <div class="form-group form-row" id="DivPassword">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Password:</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="Password" placeholder="Ketikkan Password..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Daftar Sebagai:</label>
                             <div class="col-sm-9">

                                 <select class="form-control select2 " name="Roles"></select>
                             </div>
                        </div>
                        <div class="form-group form-row" id="DivIdProv" style="display: none">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Provinsi</label>
                             <div class="col-sm-9">
                                    <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                            </select>
                             </div>
                        </div>

                        <div class="form-group form-row" id="DivIdKabkot" style="display: none">
                                <label for="kabupaten" class="col-sm-3  col-form-label">Kabupaten</label>
                            <div class="col-sm-8" id="cmbKabkot">
                                <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">

                                </select>
                            </div>
                            <div class="col-sm-8" id="divCmbKabkot" style="display: none">
                                <label for="setNameKab" class="col-sm col-form-label" id="setNameKab"></label>
                            </div>
                        </div>
                         <div class="form-group form-row" id="DivKawasanIndustri" style="display: none">
                                <label for="kabupaten" class="col-sm-3  col-form-label">Kawasan Industri</label>
                            <div class="col-sm-8" id="cmbKabkot">
                                <select class="form-control select2" name="IdAdmKawasan" id="IdAdmKawasan">
                                    <option value="0"> -- Pilih Salah Satu --</option>
                                    <option value="1"> Kawasan 1</option>
                                    <option value="2"> Kawasan 2</option>
                                </select>
                            </div>
                            <div class="col-sm-8" id="divCmbKabkot" style="display: none">
                                <label for="setNameKab" class="col-sm col-form-label" id="setNameKab"></label>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Email:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="Email" placeholder="Ketikkan Email..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nama Lengkap:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="FirstName" placeholder="Nama Depan..." />
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="MiddleName" placeholder="Nama Tengah..." />
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="LastName" placeholder="Nama Belakang..." />
                            </div>
                        </div>
                         <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Alamat Lengkap:</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Address" placeholder="Ketikkan Alamat Lengkap..." rows="5" style="height:100px"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nomor Telpon:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="PhoneNumber" placeholder="Ketikkan Nomor Telpon..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nomor HP:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="MobileNumber" placeholder="Ketikkan Nomor HP..." />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
                    <button type="button" class="btn btn-primary" id="BtnSaveUser"><i class="fa fa-save"></i> Simpan Data</button>
                </div>
            </div>
         </div>
    </div>
@endsection
@push('script-adm')
    <script src={{ asset('/modulejs/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/modulejs/admins/appl/user_managements.js') }}></script>
@endpush
