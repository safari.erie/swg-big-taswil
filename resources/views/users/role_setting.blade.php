@extends('admin_layouts.main')
@section('title','Role')
@push('style-admin')
    <style type="text/css">
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Role</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Utility</a></div>
              <div class="breadcrumb-item"><a href="#">Role</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master Role</h2>
            <div class="row">
                <div class="col-md-7">
                    <div class="card flex-fill h-100 ">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="roleFormShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body flex-fill">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="roles" style="width:100%;">
                                    <thead>
                                        <th>Id Role</th>
                                        <th>Nama Role</th>
                                        <th>Aksi</th>

                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card flex-fill h-100">
                        <div class="card-header">
                            <h4 id="HeaderTitle">Manajemen Role</h4>
                        </div>
                        <div class="card-body flex-fill">
                            <center id="img-add-role" class="pt-2" >
                                <img src={{asset('/admins/assets/img/ilustration/ad_role.svg')}} class="col-md-12" style="padding-top:100px" width="350px" alt="">
                            </center>
                            <form id="FormSetData" action="#" method="post" style="display:none;">
                                <input type="hidden" name="IdRole" />
                                <div class="bootstrap-duallistbox-container form-row moveonselect moveondoubleclick">
                                    <div class="box1 col-md-6">
                                        <label class="col-form-label font-weight-bold" id="LabelNoAssign"></label>
                                        <div class="form-group" style="padding-bottom:0;margin-bottom:7px;"><button onClick="PilihSemua();" type="button" class="btn btn-info btn-block"><i class="fa fa-check-double"></i> Pilih Semua</button></div>
                                        <select multiple="multiple" class="form-control ComboSumber" style="height: 222px;">
                                        </select>
                                    </div>
                                    <div class="box2 col-md-6">
                                        <label class="col-form-label font-weight-bold" id="LabelAssign"></label>
                                        <div class="form-group" style="padding-bottom:0;margin-bottom:7px;"><button onClick="BatalPilihSemua();" type="button" class="btn btn-danger btn-block"><i class="fa fa-times"></i> Batal Pilih Semua</button></div>
                                        <select multiple="multiple" class="form-control ComboTerpilih" style="height: 222px;"></select>
                                    </div>
                                </div>
                                <br />
                                <button type="button" id="BtnSave" class="btn btn-success btn-block"><i class="fa fa-save"></i> Simpan Data</button>
                            </form>
                            <form id="FormRole" style="display: none;">

                                <input type="hidden" name="id_role"/>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4">Nama Role</label>
                                    <input type="text" class="form-control col-md-8" name="RoleName" placeholder="Ketikkan Nama Role...">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success btn-block" onclick="save_role();"><i class="fa fa-save"></i> Simpan Data</button>
                                </div>
                             </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="DivIdPermission" style="display: none">
         <div class="section-body">
             <h2 class="section-title" id="title-permision" >Data Master Role</h2>
             <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body">
                            <div class="table-responsive tableFixHead">
                                <table class="table table-bordered table-hover nowrap"  id="role-permission" style="width:100%;">
                                    <thead>
                                        <th>#</th>
                                        <th>Modul</th>
                                        <th>Add</th>
                                        <th>Read</th>
                                        <th>Edit</th>
                                        <th>Approve</th>
                                        <th>Delete</th>
                                    </thead>
                                    <tbody ></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
    </section>
@endsection
@push('script-adm')
    <script src={{ asset('/modulejs/admins/appl/role_setting.js') }}></script>
@endpush
