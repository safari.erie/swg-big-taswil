@extends('admin_layouts.main')
@section('title','App Seting')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pengaturan Aplikasi</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Utility</a></div>
              <div class="breadcrumb-item"><a href="#">App Setting</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master Pengaturan Aplikasi</h2>
            <div class="row">
                <div class="col-7">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="userPenggunaModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="appl_setting" style="width:100%;">
                                    <thead>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Isi</th>
                                        <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script-adm')
    <script src={{ asset('/module-js/admins/appl/appl_setting.js') }}></script>
@endpush
