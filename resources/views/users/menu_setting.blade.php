@extends('admin_layouts.main')
@section('title','Menu')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pengaturan Menu</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Utility</a></div>
              <div class="breadcrumb-item"><a href="#">Menu</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master Menu</h2>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">

                        <form id="formTabelMenuAplikasi" class="form-inline">
                        <div class="card-header form-row">
                            <div class="col-sm-9 d-flex flex-row text-right float-right">
                                <div class="form-group text-right p-1">
                                    <div class="form-group text-right">
                                        <label class="col-form-label font-weight-bold">Jenis Aplikasi</label>
                                    </div> &nbsp;
                                    <div class="form-group text-right">
                                        <select name="IdAppl" class="form-control">
                                        </select>
                                    </div>&nbsp;
                                </div>
                                <div class="form-group text-right p-1" id="DivBtnTambah" style="display: none">
                                    <button type="button" class="btn btn-success btn-flat" onclick="TambahMenu();"><i class="fa fa-plus"></i> Tambah</button>
                                </div>
                            </div>

                        </div>
                    </form>
                        <div class="card-body">
                            <center id="ImgSearch">
                                <img src={{asset('/admins/assets/img/ilustration/house_searching.svg')}} width="325px" alt="">
                            </center>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="menus" style="width:100%;display:none;">
                                    <thead>
                                        <th>Jenis Aplikasi</th>
                                        <th>Nama Menu</th>
                                        <th>Controller & Action</th>
                                        <th>Aksi</th>

                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header">
                            <h4 id="HeaderTitle"></h4>
                        </div>
                        <div class="card-body">
                            <center id="img-add-menu">
                                <img src={{asset('/admins/assets/img/ilustration/ad_role.svg')}} width="350px" alt="">
                            </center>
                            <form id="FormMenu" style="display: none;">

                                <input type="hidden" name="id_appl_task"/>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4">Jenis Aplikasi</label>
                                    <input type="text" class="form-control col-md-8" name="ApplName" placeholder="Ketikkan Nama Aplikasi...">
                                </div>
                                 <div class="form-group row">
                                    <label class="col-form-label font-weight-bold col-md-4">Parent Menu</label>
                                    <select class="col-md-8 form-control" name="IdApplTaskParent">
                                    </select>
                                </div>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4">Nama Aplikasi</label>
                                    <input type="text" class="form-control col-md-8" name="ApplTaskName" placeholder="Ketikkan Nama Menu" />
                                </div>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4"> Controller</label>
                                    <input type="text" class="form-control col-md-8" name="ControllerName" placeholder="Ketikkan Nama Controller" />
                                </div>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4"> Action</label>
                                    <input type="text" class="form-control col-md-8" name="ActionName" placeholder="Ketikkan Nama Aksi" />
                                </div>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4"> Icon </label>
                                    <input type="text" class="form-control col-md-8" name="IconName" placeholder="Ketikkan Nama Icon" />

                                </div>
                                <div class="form-group form-row">
                                    <label class="col-form-label font-weight-bold col-md-4"> Deskripsi </label>
                                    <textarea class="form-control col-md-8" placeholder="Ketikkan Deskripsi" name="Description"></textarea>

                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success btn-block" onclick="save_menu();"><i class="fa fa-save"></i> Simpan Data</button>
                                </div>

                             </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('script-adm')
    <script src={{ asset('/module-js/admins/appl/menu_setting.js') }}></script>
@endpush
