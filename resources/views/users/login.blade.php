<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="theme-color" content="#fe773f" />
    <meta name="msapplication-navbutton-color" content="#fe773f" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#fe773f" />
  <title>Pir - Login</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href={{asset('/admins/modules/bootstrap/css/bootstrap.css')}} >
  <link rel="stylesheet" href={{asset('/admins/modules/fontawesome/css/all.min.css')}}>

  <!-- plugin datatables -->
  <link rel="stylesheet" href={{asset('/admins/plugin/datatable/1.10.19/css/dataTables.bootstrap4.min.css')}}>
  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href={{asset('/admins/assets/css/style.css')}}>
  <link rel="stylesheet" href={{asset('/admins/assets/css/components.css')}}>

</head>

<body>

    <div id="app">

    <section class="section">
      <div class="d-flex flex-wrap align-items-stretch">
        <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
          <div class="p-4 m-3">
            <center><img src={{asset('/admins/assets/img/logo-bkpm.png')}} alt="logo" width="150" class="shadow-light  mb-5 mt-2 "></center>
            <h4 class="text-dark font-weight-normal">Welcome to <span class="font-weight-bold">Admin Pir</span></h4>
            <form action="{{ route('login') }}" method="post">
                @csrf
                @if(session('errors'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Something it's wrong:
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    @endif

                     <div class="form-group">
                        <label for=""><strong>Username</strong></label>
                        <input type="text" name="username" class="form-control" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for=""><strong>Password</strong></label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>



              <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary btn-block">Log In</button>
              </div>


            </form>

            <div class="text-center mt-5 text-small">
              Copyright &copy; BKPM2021
              <div class="mt-2">
                <a href="#">Privacy Policy</a>
                <div class="bullet"></div>
                <a href="#">Terms of Service</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background={{asset('/admins/assets/img/unsplash/login-bg.jpg')}}>
          <div class="absolute-bottom-left index-2">
            <div class="text-light p-5 pb-2">
              <div class="mb-5 pb-3">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>



  <!-- General JS Scripts -->
  <script src={{asset('/admins/modules/jquery.min.js')}}></script>
  <script src={{asset('/admins/modules/popper.js')}}></script>
  <script src={{asset('/admins/modules/bootstrap/js/bootstrap.js')}}></script>
  <script src={{asset('/admins/modules/nicescroll/jquery.nicescroll.js')}}></script>
  <script src={{asset('/admins/modules/moment.min.js')}}></script>
  <script src={{asset('/admins/assets/js/stisla.js')}}></script>
  <script src={{asset('/admins/plugin/datatable/1.10.19/js/dataTables.bootstrap4.min.js')}}></script>
  <script src={{asset('/admins/plugin/datatable/1.10.19/js/jquery.dataTables.js')}}></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src={{asset('/admins/assets/js/scripts.js')}}></script>
  <script src={{asset('/admins/assets/js/custom.js')}}></script>

  <!-- Page Specific JS File -->
</body>
</html>
