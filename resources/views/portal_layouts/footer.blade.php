<div id="footer p-0 m-0">
  <div class="container-fluid bg-pir-blue p-4">
    <div class="row text-light">
      <div class="col-md-5 col-sm-6"> <img class="footer-logo" src="https://www.bkpm.go.id/assets/icon/Logo_BKPM_IND.svg" alt=""> <br>
        <br>
        <p> PIR merupakan sistem informasi berbasis geospasial untuk potensi dan peluang investasi di 34 Provinsi dan 514 Kabupaten / Kota di Indonesia dan merupakan bagian dari situs Badan Koordinasi Penanaman Modal (BKPM). PIR memuat informasi profil daerah antara lain data demografi, komoditas, pendapatan dan Upah Minimum Regional (UMR), serta infrastruktur pendukung. <!-- PIR is a geospatial based information system for investment potential and opportunities in 34 Provinces and 514 Regencies / Cities in Indonesia and is part of the website of the Investment Coordinating Board (BKPM). PIR contains information on regional profiles including demographic data, commodities, income and Regional Minimum Wages (UMR), as well as supporting infrastructure-->
      </p>
    </div>
    <div class="col-md-4 col-sm-6 ">
      <h4>Tautan Cepat</h4>
      <ul class="footer-links">
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//infranasional/id">Infrastruktur</a></li>
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//industrinasional/id">Kawasan Industri dan KEK</a></li>
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//holiday/id">Tax Holiday</a></li>
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//allow/id">Tax Allowance</a></li>
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//master/id">Masterlist</a></li>
        <li><a href="https://regionalinvestment.bkpm.go.id/portal//super/id">Super Deduction</a></li>
      </ul>
      <ul class="footer-links">
        <li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpma">PMA</a></li>
        <li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fpmdn">PMDN</a></li>
        <li><a href="http://kemitraan.bkpm.go.id/web/index.php?r=site%2Fumkm">UMKM</a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="col-md-3  col-sm-12">
      <h4>Hubungi Kami</h4>
      <div class="text-widget"> <span>Jl. Jend. Gatot Subroto No. 44, Jakarta 12190</span> <br>
      <span>P.O. Box 3186, Indonesia</span> <br>
      Telepon : <span>+6221 525 2008 </span><br>
      Contact Center: <span>0807 100 2576 </span><br>
      Fax: <span>+6221 525 4945 </span><br>
      E-Mail:<span> <a href="#">office@bkpm.com</a> </span><br>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="text-light">© 2021 BKPM. All Rights Reserved.</div>
  </div>
</div>
</div>
</div>
