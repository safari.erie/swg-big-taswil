<nav class="navbar navbar-expand-md navbar-dark  fixed-top grad shadow"
	style="opacity: 0.9; border: solid thin;" >
	<div class="container-fluid">
		<button class="navbar-toggler mb-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"> <span class="navbar-toggler-icon m-0 p-0"></span> </button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<a class="navbar-brand font-weight-bold p-1 rounded-circle" style="border:solid 1px rgba(115,197,51,1)" href="#"><strong class="text-light">R<span class="pir-green">i</span>iS</strong></a>
			<ul class="navbar-nav">
				<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle text-light" href="#" role="button" data-bs-toggle="dropdown"> Region </a>
				<div class="dropdown-menu bg-pir-blue"> <a class="dropdown-item text-light" href="#">Sumatera</a> <a class="dropdown-item text-light" href="#">Jawa</a> <a class="dropdown-item text-light" href="#">Kep.Nusa Tenggara</a> <a class="dropdown-item text-light" href="#">Kalimantan</a> <a class="dropdown-item text-light" href="#">Sulawesi</a> <a class="dropdown-item text-light" href="#">Kep.Maluku</a> <a class="dropdown-item text-light" href="#">Papua</a> </div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-light" href="#" role="button" data-bs-toggle="dropdown"> Sector </a>
				<div class="dropdown-menu bg-pir-blue ">
					<a class="dropdown-item text-light" href="#">Industri</a>
					<a class="dropdown-item text-light" href="#">Pariwisata</a>
					<a class="dropdown-item text-light" href="#">Smelter</a>
					<a class="dropdown-item text-light" href="#">Pangan dan Pertanian</a>
					<a class="dropdown-item text-light" href="#">Jasa</a>
					<a class="dropdown-item text-light" href="#">Infrastruktur</a> </div>
				</li>
				<li class="nav-item border border-warning rounded"> <a class="nav-link text-warning " href="#"><i>Interactive Map</i></a> </li>
			</ul>
		</div>
		<div class="position-relative">
            <a href={{url('/login')}} class="btn btn-outline-dark "> Login</a>
			<img src="{{asset('portal/img/LogoBKPM-v.png')}}" alt="" width="50px" class="img-fluid"/>
		</div>
	</div>
</nav>
