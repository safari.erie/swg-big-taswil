<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>RiiS-BKPM</title>
		<link href="{{asset('portal/css/bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{asset('portal/css/font-awesome4.min.css')}}" rel="stylesheet">
		<link href="{{asset('portal/css/my.css')}}" rel="stylesheet">
        @stack('styles')
	</head>
	<body>
		@section('header')
		@include('portal_layouts.header')
		@show
		<div class="container-fluid p-0 grad" >
			@yield('content')
		</div>
		@section('footer')
		@include('portal_layouts.footer')
		@show
		<script src="{{asset('portal/js/bootstrap.bundle.min.js')}}"></script>
        @stack('scripts')
	</body>
</html>
