@extends('admin_layouts.main')
@section('title','Dashaboard')
@push('style-admin')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
        }
        .bg-aqua, .callout.callout-info, .alert-info, .label-info,
         .modal-info .modal-body {
            background-color: #00c0ef !important;
        }

        .bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body {
            background-color: #00a65a !important;
        }

        .bg-yellow, .callout.callout-warning, .alert-warning, .label-warning, .modal-warning .modal-body {
                background-color: #f39c12 !important;
        }

        .bg-red, .callout.callout-danger, .alert-danger, .alert-error, .label-danger, .modal-danger .modal-body {
            background-color: #dd4b39 !important;
        }

        .small-box>.inner {
            padding: 10px;
        }

        .small-box h3 {
            font-size: 38px;
            font-weight: bold;
            margin: 0 0 10px 0;
            white-space: nowrap;
            padding: 0;
        }

        .small-box .icon {
            color: rgba(0,0,0,.15);
            z-index: 0;
        }

        .small-box .icon > i {
            font-size: 90px;
            position: absolute;
            right: 15px;
            top: 15px;
            transition: -webkit-transform .3s linear;
            transition: transform .3s linear;
            transition: transform .3s linear,-webkit-transform .3s linear;
        }

        .small-box .icon > i.fa, .small-box .icon > i.fab, .small-box .icon > i.fad, .small-box .icon > i.fal, .small-box .icon > i.far, .small-box .icon > i.fas, .small-box .icon > i.ion {
            font-size: 70px;
            top: 20px;
        }

        .bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green{
            color: #fff;
        }
        .bg-info, .bg-info > a {
                color: #fff !important;
        }

        .small-box > .small-box-footer {
            background-color: rgba(0,0,0,.1);
            color: rgba(255,255,255,.8);
            display: block;
            padding: 3px 0;
            position: relative;
            text-align: center;
            text-decoration: none;
            z-index: 10;
        }
        .small-box > .small-box-footer:hover {
            background-color: rgba(0,0,0,.15);
            color: #fff;
        }
        .small-box:hover {
            text-decoration: none;
        }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>

            <div class="section-body">
                <div class="card card-info">

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-has-icon ">

                                <div class="alert-body">
                                    <div class="alert-title">Demografi</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Jumlah Penduduk&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Pencari Kerja&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-friends"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3 align="left">44</h3>

                                    <p align="left">PDRB&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money-bill-wave"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">UMR&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>




    <section class="section">


            <div class="section-body">
                <div class="card card-warning">

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-has-icon ">

                                <div class="alert-body">
                                    <div class="alert-title">Sarana Dan Prasarana</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Hotel&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-hotel"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Pelabuhan&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-ship"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Rumah Sakit&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-hospital"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Pendidikan&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-book"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section">


            <div class="section-body">
                <div class="card card-success" style="background-color: #e8e6e0">

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">0</h3>

                                    <p align="left">Profil Komuditi&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-user-plus"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">108</h3>

                                    <p align="left">Kantor Pemerintahan&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-home"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">53</h3>

                                    <p align="left">Jarak&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-pause"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 align="left">101</h3>

                                    <p align="left">Info Peluang&nbsp;&nbsp;</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-search"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
