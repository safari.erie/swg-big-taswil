@extends('admin_layouts.main')
@section('title','Dashaboard')
@push('style-admin')
    <style>
        .modal-full {
                max-width: 98%;
            }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Master Sumber Data</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Sumebr data</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master Sumber Data</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="KomoditiModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="Komoditi" style="width:100%;">
                                    <thead>
                                        <th >No</th>
                                        <th>Nama</th>
                                        <th>Website</th>
                                        <th>Instansi</th>
                                        <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- all define modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="KomoditiModal">
         <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data">


                        <input type="hidden" class="form-control" name="IdUser" placeholder="Ketikkan Id User..." />
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nama:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Nama" placeholder="Ketikkan Nama..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Website:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Website" placeholder="Ketikkan Website..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Contact Person:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="contact" placeholder="Ketikkan Kontak..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Email:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="email" placeholder="Ketikkan Email..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Telp:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="telp" placeholder="Ketikkan No Telepon..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Fax:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="fax" placeholder="Ketikkan Fax..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Alamat:</label>
                            <div class="col-sm-9">
                                <textarea  class="form-control" name="Komoditi"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Instansi:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="instansi" placeholder="Ketikkan Instansi..." />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
                    <button type="button" class="btn btn-primary" id="BtnSaveKomoditi"><i class="fa fa-save"></i> Simpan Data</button>
                </div>
            </div>
         </div>
    </div>
@endsection
@push('script-adm')
    <script src={{ asset('/modulejs/admins/Master_sumber_data/sumber_data.js') }}></script>
@endpush
