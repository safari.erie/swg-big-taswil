@extends('admin_layouts.main')
@section('title','Dashaboard')
@push('style-admin')

    <style>
        .modal-full {
                max-width: 98%;
            }
    </style>
    <style>
#myDIV {
  display:none;
 
}
</style>
    <style>
.show-multiple-image-preview img
{
padding: 6px;
max-width: 100px;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>

<style type="text/css">
		 .image_container {
		 	height: 160px;
		 	width: 200px;
		 	border-radius: 6px;
		 	overflow: hidden;
		 	margin: 10px;
		 }
		 .image_container img {
		 	height: 100%;
		 	width: auto;
		 	object-fit: cover;
		 }
		 .image_container span {
		 	top: -6px;
		 	right: 8px;
		 	color: red;
		 	font-size: 28px;
		 	font-weight: normal;
		 	cursor: pointer;
		 }
	</style>
      <link rel="stylesheet" href=https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css" id="theme-styles">
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Peluang</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Peluang</a></div>
              <div class="breadcrumb-item"><a href="#">Peluang</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Info Peluang</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="userPenggunaModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="user-management" style="width:100%;">
                                    <thead>
                                    <th>Aksi</th>
                                        <th >No</th>
                                        <th>Judul Peluang</th>
                                        <th>Daerah</th>
                                        <th>Sektor Peluang</th>
                                        <th>Prioritas</th>
                                        <th>Tahun</th>
                                        <th>Status</th>
                                       
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- all define modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="UserManagementModal">
         <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data" id=form>
                        <input type="hidden" class="form-control" name="IdUser" placeholder="Ketikkan Id User..." />
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Judul Peluang:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="judul_peluang" placeholder="Ketikkan Judul Peluang..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Keterangan:</label>
                            <div class="col-sm-9">
                            <textarea name="keterangan" cols="40" rows="10" id="keterangan" class="form-control input-sm " placeholder="Keterangan" ></textarea>
                            </div>
                        </div>
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Sektor Peluang:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " id="sektor" name="peluang">
                                 <option value=""> -- Pilih Salah Satu Sektor Peluang -- </option>
                            
                                 
                                 </select>
                                
                             </div>
                        </div>
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Sumber Data:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " id="sumber" name="sumber_data">
                                 <option value=""> -- Pilih Salah Satu sumber data -- </option>
                              
                            
                                 
                                 </select>
                                
                             </div>
                        </div>
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Daerah:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " id="daerah" name="daerah">
                                 <option value=""> -- Pilih Salah Satu Daerah -- </option>
                               
                            
                                 
                                 </select>
                                
                             </div>
                        </div>
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Tahun:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " name="tahun">
                                 <option value=""> -- Pilih Tahun  -- </option>
                                 <option value="2021"> 2021</option>
                             
                                 
                                 </select>
                                
                             </div>
                        </div>
                        
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Prioritas:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " name="prioritas">
                                 <option value=""> -- Pilih Salah Satu Prioritas -- </option>
                                 <option value="1"> Prioritas </option>
                                 <option value="2"> Daerah </option>
                         
                              
                              
                                 
                                 </select>
                                
                             </div>
                        </div>
                        
                        <div class="form-group form-row">
                       
                            <label class="col-form-label col-sm-3" style="text-align:left;">Image:</label>
                            <div class="col-sm-9">
                       
                            <input type="file" class="form-control" id="image-upload" placeholder="Post Image" name="image_upload[]"   multiple="" class="d-none"  onchange="image_select()" required multiple>
                            <div class="card-body d-flex flex-wrap justify-content-start" id="container">
   	        <!-- Image will be show here-->    	  
   	   </div>
                        </div>
                        </div>
                       
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">PDF:</label>
                            <div class="col-sm-9">
                            <input type="file" class="form-control" id="pdf-upload" placeholder="Post Image" name="pdf_upload"   multiple="" class="d-none"  onchange="image_select()" required multiple>
                       
                            <div class="card-body d-flex flex-wrap justify-content-start" id="pdf">
   	        <!-- Image will be show here-->    	  
   	   </div>
                        </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">VIDEO:</label>
                            <div class="col-sm-9">
                            <input type="file" class="form-control" id="video-upload" placeholder="Post Image" name="video_upload"   multiple="" class="d-none"  onchange="image_select()" required multiple>
                            <div class="card-body d-flex flex-wrap justify-content-start" id="video">
   	        <!-- Image will be show here-->    	  
   	   </div>
                        </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nilai IRR:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="irr" placeholder="Nilai IRR..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nilai NPV:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="npv" placeholder="Nilai NPV..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nilai PP:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="pp" placeholder="Nilai PP..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Nilai Investasi:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="nilai_investasi" placeholder="Nilai Investasi..." />
                            </div>
                        </div>
                      
                     
                       
                        <div class ="btn bg-primary" style="color:white" onclick="myFunction()">Translate EN</div>
                        <div id="myDIV">
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Judul Peluang:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="judul_peluang_en" placeholder="Ketikkan Judul Peluang EN..." />
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Keterangan:</label>
                            <div class="col-sm-9">
                            <textarea name="keterangan_en" cols="40" rows="10" id="keterangan_en" class="form-control input-sm " placeholder="Keterangan" ></textarea>
                            </div>
                        
                       
                                    </div>
                       
                                </div>
                        <div class="form-group row">
                                    <label for="provinsi" class="col-sm-2 mt-2 col-form-label">Latitude</label>
                                    <div class="col-sm-4 mt-2">
                                       <input type="text" name="Lat" id="y" class="form-control"  />
                                    </div>
                                    <label for="provinsi" class="col-sm-2 mt-2 col-form-label">Longitude</label>
                                    <div class="col-sm-4 mt-2">
                                        <input type="text" name="Lon" id="x" class="form-control" />
                                        <input type="hidden" name="total" id="total" class="form-control" />
                                    </div>
                                </div>
                               

                                <body onload="xz()">
                                <div class="form-group">
                                   <div class="col-12">
                                        <div class="frame_map" id="wrapper" style="margin:5px">
                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                            <div id="map"
                                                style="width: 100%; height: 500px; "></div>
                                        </div>
                                   </div>
                                </div>
        </body>

                    </td>
                    <td class="as" align="center">
                        <script type="text/javascript"><!--
                            google_ad_client = "pub-0418728868848218";
                            /* 160x600, created 1/21/10 */
                            google_ad_slot = "6707462996";
                            google_ad_width = 160;
                            google_ad_height = 600;//-->
                        </script>
                    </td>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
                    <button type="button" class="btn btn-primary" id="BtnSavePeluang"><i class="fa fa-save"></i> Simpan Data</button>
                </div>
            </div>
         </div>
    </div>
@endsection
@push('script-adm')
<script type="text/javascript">
 var testz = {{Auth::user()->id_role}};
 var userz = {{Auth::user()->id_user}};
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js" ></script>
    <script src={{ asset('/modulejs/admins/peluang/menu_setting.js') }}></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
      <script src=https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js></script>

  <style type="text/css">
    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container { z-index: 100000 !important; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>


<!--<link href="/d.css" type="text/css" rel="stylesheet">-->


	
    @endpush
