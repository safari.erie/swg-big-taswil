@extends('admin_layouts.main')
@section('title','Dashaboard')
@push('style-admin')
    <style>
        .modal-full {
                max-width: 98%;
            }
    </style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Master Komoditi</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Komoditi</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
             <h2 class="section-title">Data Master Komoditi</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="KomoditiModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover nowrap"  id="Komoditi" style="width:100%;">
                                    <thead>
                                        <th >No</th>
                                        <th>Komoditi</th>
                                        <th>Sektor komoditi</th>
                                        <th>Tanggal entry</th>
                                        <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- all define modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="KomoditiModal">
         <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                   <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data">
                     
                        <div class="form-group form-row">
                             <label class="col-form-label col-sm-3" style="text-align:left;">Sektor Komoditi:</label>
                             <div class="col-sm-9">

                                 <select class="form-control " name="peluang">
                                 <option value=""> -- Pilih Salah Satu Sektor -- </option>
                                 <option value="1"> Perkebunan</option>
                                 <option value="2"> Pertanian</option>
                                 
                                 </select>
                                
                             </div>
                        </div>
                        <input type="hidden" class="form-control" name="IdUser" placeholder="Ketikkan Id User..." />
                        <div class="form-group form-row">
                            <label class="col-form-label col-sm-3" style="text-align:left;">Komoditi:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Komoditi" placeholder="Ketikkan Judul Peluang..." />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
                    <button type="button" class="btn btn-primary" id="BtnSaveKomoditi"><i class="fa fa-save"></i> Simpan Data</button>
                </div>
            </div>
         </div>
    </div>
@endsection
@push('script-adm')
    <script src={{ asset('/module-js/admins/Master_komoditi/komoditi.js') }}></script>
@endpush
