@extends('admin_layouts.main')
@section('title','Kecamatan')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Kecamatan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Kecamatan</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Master Kecamatan</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="KecamatanModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="umkm-kecamatan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Kode Kecamatan</th>
                                            <th>Nama </th>
                                            <th>Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            1
                                            </td>
                                            <td>Kecamatan A</td>

                                            <td><div class="badge badge-success">Completed</div></td>
                                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade" tabindex="-1" role="dialog" id="kecamatanModal">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Kecamatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="kd_prov" class="col-sm-4 col-form-label">Pilih Provinsi</label>
                        <div class="col-sm-8">
                            <select class="form-control">
                                <option>Pilih Provinsi</option>
                                <option>Aceh</option>
                                <option>Jawa Barat</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label for="kd_prov" class="col-sm-4 col-form-label">Pilih Kabupaten</label>
                        <div class="col-sm-8">
                            <select class="form-control">
                                <option>Pilih Kabupaten</option>
                                <option>Kab 1</option>
                                <option>Kab 2</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kd_kecamatan" class="col-sm-4 col-form-label">Kode Kecamatan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="kd_kecamatan" name="kd_kecamatan" placeholder="Kode Kecamatan">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nm_kecamatan" class="col-sm-4 col-form-label">Nama Kecamatan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nm_kecamatan" id="nm_kecamatan" placeholder="Nama Kecamatan">
                        </div>
                    </div>




                    </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/kecamatan/kecamatan_list.js') }}></script>
@endpush
