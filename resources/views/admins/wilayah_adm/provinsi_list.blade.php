@extends('admin_layouts.main')
@section('title','Provinsi')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Provinsi</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Provinsi</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Master Provinsi</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" onclick="provinsiModalShow()" class="btn btn-primary" > <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="umkm-perusahaan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Kode Provinsi</th>
                                            <th>Nama </th>
                                            <th>Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                            1
                                            </td>
                                            <td>Provinsi</td>

                                            <td><div class="badge badge-success">Completed</div></td>
                                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade" tabindex="-1" role="dialog" id="provinsiModal">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Provinsi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="kd_prov" class="col-sm-4 col-form-label">Kode Provinsi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="kd_prov" name="kd_prov" placeholder="Kode Prov">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nm_provinsi" class="col-sm-4 col-form-label">Nama Provinsi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nm_provinsi" id="nm_provinsi" placeholder="Nama Prov">
                        </div>
                    </div>




                    </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/provinsi/provinsi_list.js') }}></script>
@endpush
