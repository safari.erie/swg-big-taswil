@extends('admin_layouts.main')
@section('title','SarPras Pelabuhan')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Sarana Dan Prasarana</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Pelabuhan</a></div>
              <div class="breadcrumb-item">Data Pelabuhan</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Pelabuhan</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="pelabuhanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                    </div>
                    <div class="card-body">


                      <div class="table-responsive">
                        <table class="table table-striped" id="pelabuhan">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Provinsi</th>
                              <th>Kabupaten</th>
                              <th>Sumber Data</th>
                              <th>Alamat</th>
                              <th>Kelas</th>
                              <th>Fungsi</th>
                              <th>Panjang Dermaga</th>
                              <th>Kedalaman</th>
                              <th>No Telepon</th>
                              <th>No Fax</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>



    <div class="modal fade"  role="dialog" id="Kabupaten">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Pelabuhan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">
                    <input type="hidden" name="IdProvinsi" id="IdProvinsi">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="provinsi" class="col-sm-4  col-form-label">Provinsi</label>
                                <div class="col-sm-8" id="cmbProv">
                                <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kabupaten" class="col-sm-4  col-form-label">Kabupaten</label>
                                <div class="col-sm-8" id="cmbProv">
                                <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameKabkota" class="col-sm col-form-label" id="setNameKabkota"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sumberdata" class="col-sm-4  col-form-label">Sumber Data</label>
                                <div class="col-sm-8" id="cmbProv">
                                <select class="form-control select2" name="IdSumberData" id="IdSumberData">
                                    <option value="0">-- Pilih Salah Satu --</option>
                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameSumberData" class="col-sm col-form-label" id="setNameSumberData"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sumberdata" class="col-sm-4  col-form-label">Kelas</label>
                                <div class="col-sm-8" id="cmbProv">
                                <select class="form-control select2" name="IdKelas" id="IdKelas">
                                    <option value="0">-- Pilih Salah Satu --</option>
                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameKelas" class="col-sm col-form-label" id="setNameKelas"></label>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="nib" class="col-sm-4 col-form-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Nama" name="Nama" placeholder="Input Nama">
                                    <label for="setNama" class="col-sm col-form-label" id="setNama" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="keterangan" class="col-sm-4 col-form-label">Keterangan</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="Keterangan" name="Keterangan" style="height: 80px" placeholder="Input Keterangan"></textarea>
                                    <label for="setKeterangan" class="col-sm col-form-label" id="setKeterangan" style="display: none"></label>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group row" >
                                <label for="keterangan" class="col-sm-4 col-form-label">Fungsi</label>
                                <div class="col-sm-8">
                                    <select name="Fungsi" id="Fungsi" class="form-control select2">
                                        <option value="0"> -- Pilih Salah Satu --</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="keterangan" class="col-sm-4 col-form-label">Panjang Dermaga</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="PanjangDermaga" name="PanjangDermaga" placeholder="Input Panjang Dermaga">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Kedalaman</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Kedalaman" name="Kedalaman" placeholder="Input Kedalaman">
                                    <label for="setTlp" class="col-sm col-form-label" id="setIata" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">No. Fax</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="NoFax" name="NoFax" placeholder="Input Fax">
                                    <label for="setFax" class="col-sm col-form-label" id="setFax" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Url Web</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="UrlWeb" name="UrlWeb" placeholder="Input Url Web">
                                    <label for="setUrlWeb" class="col-sm col-form-label" id="setUrlWeb" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="keterangan" class="col-sm-4 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="Alamat" name="Alamat" style="height: 80px" placeholder="Input Alamat"></textarea>
                                    <label for="setAlamat" class="col-sm col-form-label" id="setAlamat" style="display: none"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row" id="divBtnTranslate">
                        <label for="btnTranslate" class="col-sm-4  col-form-label">Translate</label>
                        <div class="col-sm-8">
                            <button type="button" class="btn btn-warning" onclick="ShowTranslate()" id="btnTrasnalte"> <i class="fa fa-language" aria-hidden="true"></i> Translate</button>
                        </div>
                    </div>


                    <div id="ColumnTransalte" class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon"><i class="fa fa-language"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kolom Translate</div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanEn" class="col-sm-4 col-form-label">Nama  En</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmPerusahaanEn" name="NmPerusahaanEn"  placeholder="Input Nama  [English]">
                                <label for="setNmPerusahaanEn" class="col-sm col-form-label" id="setNmPerusahaanEn" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanEn" class="col-sm-4 col-form-label">Keterangan En</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="KeteranganEn" id="KeteranganEn" placeholder="Input Keterangan [English]" style="height: 80px"></textarea>
                                    <label for="setNmPerusahaanEn" class="col-sm col-form-label" id="setNmPerusahaanEn" style="display: none"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Latitude</label>
                                <div class="col-sm-8">
                                    <input type="text" name="Lat" id="x" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLat" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Longitude</label>
                                <div class="col-sm-8">

                                    <input type="text" name="Lon" id="y" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLon" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                    </div>



                     <body onload="initialize(0,0)">
                        <div class="form-group">
                        <div class="col-12">
                                <div class="frame_map" id="wrapper" style="margin:5px">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                    <div id="map"
                                        style="width: 100%; height: 500px; "></div>
                                </div>
                        </div>
                        </div>
                    </body>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>





@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/cmb_common.js') }}></script>
    <script src={{ asset('/modulejs/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/modulejs/admins/sarana_prasarana/pelabuhan.js') }}></script>
    <script src={{ asset('/modulejs/admins/map_google.js') }}></script>

@endpush
