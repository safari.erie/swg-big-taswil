@extends('admin_layouts.main')
@section('title','Kbli')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Master Kbli</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Kbli</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Master Kbli</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="kbliModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped " id="master-kbli">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Kode Kategori</th>
                                            <th>Kode Gol Pokok</th>
                                            <th>Kode Gol</th>
                                            <th>Kode Sub Gol</th>
                                            <th>Kode Kbli</th>
                                            <th>Nama Kbli</th>
                                            <th>Deskripsi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade" tabindex="-1" role="dialog" id="masterKbliModal">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Kbli</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                    <label for="nib" class="col-sm-2 col-form-label">Kode Kategori</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="KdKateggori" name="KdKateggori" maxlength="2" placeholder="Input Kd Kategori">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Kode Gol Pokok</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="KdGolPokok" name="KdGolPokok" maxlength="3"  placeholder="Input Kd Gol Pokok">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Kode Gol </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="KdGol" name="KdGol" maxlength="4"  placeholder="Input Kd Gol ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Kode Sub Gol </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="KdSubGol" name="KdSubGol" maxlength="5"  placeholder="Input Sub Gol ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Kode Kbli </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="KdKbli" name="KdKbli" maxlength="6"  placeholder="Input Kode Kbli ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Nama Kbli </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="NmKbli" name="NmKbli"  placeholder="Input Nama Kbli ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Deskripsi Kbli </label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="DesKbli" id="DesKbli" placeholder="Input Deskripsi Kbli"> </textarea>
                    </div>
                  </div>


                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSaveKbli">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/master_kbli/kbli_list.js') }}></script>
@endpush
