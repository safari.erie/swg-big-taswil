@extends('admin_layouts.main')
@section('title','Umkm Perusahaan')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Umkm Perusahaan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item"><a href="#">Perusahaan</a></div>
              <div class="breadcrumb-item">Laporan Umkm</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Laporan Perusahaan dan Umkm</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    {{-- <div class="card-header">
                        <div class="row">
                            <div class="col-md-8">
                                 <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputKeteranganKebutuhanPerusahaanProdukEn" class="col-form-label">Keterangan Produk En</label>
                                    <select class="form-control">
                                        <option>Pilih salha satu</option>
                                    </select>
                                    </div>
                                 </div>
                            </div>
                            <div class="col-md-6">
                                <a href="javascript:void(0)" class="btn btn-warning" onclick="UmkmPerusahaanModalShow()"> <i class="fa fa-filter"></i> filter </a>
                            </div>
                        </div>

                    </div> --}}
                    <div class="card-body">
                        <div class="alert alert-primary alert-has-icon">
                            {{-- <div class="alert-icon"><i class="fa fa-language"></i></div> --}}
                            <div class="alert-body">
                                {{-- <div class="alert-title">Filter</div> --}}
                                <div class="alert-body">
                                    <form class="form-inline" id="FormFilter">


                                        <div class="col-md-4">
                                             <select class="form-control mb-2 mr-sm-2 select2" id="FilterProvinsi" name="FilterProvinsi">
                                                {{-- <option>tetet</option> --}}
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                             {{-- <label class="sr-only" for="">Kabupaten</label> --}}
                                                <select class="form-control mb-2 mr-sm-2 select2" id="FilterKabKot" name="FilterKabKot">

                                                </select>
                                        </div>

                                        <button type="button" class="btn btn-success mb-2 mr-sm-2" onclick="FilterLaporanUmkm();"><i class="fa fa-filter"></i> Filter</button>
                                </form>
                                </div>
                            </div>
                        </div>

                      <div class="table-responsive">
                        <table class="table table-striped" id="laporan-umkm-perusahaan">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Nama</th>
                              <th>Alamat</th>
                              <th>Jenis Perusahaan</th>
                              <th>Status</th>
                              <th>Verifikasi</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>





@endsection

@push('script-adm')
    <script src={{ asset('/module-js/admins/cmb_common.js') }}></script>
    <script src={{ asset('/module-js/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/module-js/admins/umkm/laporan_umkm.js') }}></script>
    <script src={{ asset('/module-js/admins/map_google.js') }}></script>

@endpush
