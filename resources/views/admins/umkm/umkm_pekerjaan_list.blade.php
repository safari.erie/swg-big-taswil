@extends('admin_layouts.main')
@section('title','Umkm Pekerjaan')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')
    {{-- <link href={{url('/admins/plugin/leaflet/leaflet.css')}} rel="stylesheet" />
    <link href={{url('/admins/plugin/leaflet/leaflet.fullscreen.css')}} rel="stylesheet" type="text/css" />
    <link href={{url('/admins/plugin/leaflet/L.Control.Zoomslider.css')}} rel="stylesheet" type="text/css" />
    <link href={{url('/admins/plugin/leaflet/L.Control.Basemaps.css')}} rel="stylesheet" />
    <style>
        .modal-full {
                max-width: 98%;
            }
         #map {
            margin: 0px;
            width: 100%;
            height: 350px;
            padding: 0px;
        }
    </style> --}}

<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Master Umkm</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Master</a></div>
              <div class="breadcrumb-item"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">UMKM <strong> Pekerjaan </strong></h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="umkmPekerjaanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped" id="master-umkm-pekerjaan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Nama Umkm</th>
                                            <th>Nama Pekerjaan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section" style="display: none" id="section-umkm-kbli" >

        <div class="section-body">
             <h2 class="section-title">Data Umkm <strong> KBLI </strong></h2>
             <div class="row">
                <div class="col-12">
                    <div class="card">
                         <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="umkmKbliModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                            <input type="hidden" name="id_umkm"  >
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped" id="master-umkm-klbi">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Kbli</th>
                                            <th>Umkm</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade" tabindex="-1" role="dialog" id="masterUmkmModal">
        <div class="modal-dialog modal-lg modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Umkm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                    <label for="nib" class="col-sm-2 col-form-label">Kode Nib</label>
                    <div class="col-sm-6">
                      <input type="number" class="form-control" id="KdNib" name="KdNib" placeholder="Input Kd Nib">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Nama Perusahaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="NmPerusahaan" name="NmPerusahaan"  placeholder="Input Nama Perusahaan">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNoTelepon" class="col-sm-2 col-form-label">No Telepon </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputNoTelepon" name="NoPhone"  placeholder="Input No Telepon ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNoTelepon" class="col-sm-2 col-form-label">Alamat Perusahaan </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="AlmtPerusahaan" name="AlmtPerusahaan" style="height: 80px" placeholder="Input Alamat Perusahaan"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="NmPic" class="col-sm-2 col-form-label">Pic</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="NmPic" name="NmPic"  placeholder="Input Pic ">
                    </div>
                  </div>
                  <div class="form-group row">
                            <label for="provinsi" class="col-sm-2 mt-2 col-form-label">Provinsi</label>
                            <div class="col-sm-4 mt-2">
                                <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                                </select>
                            </div>
                            <label for="provinsi" class="col-sm-2 mt-2 col-form-label" name="IdKabKot">Kabupaten</label>
                            <div class="col-sm-4 mt-2">
                                <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">
                                    <option value="0">Pilih Kabupaten</option>
                                    <option value="1">Kab 1</option>
                                    <option value="2">Kab 2</option>
                                </select>
                            </div>
                   </div>
                   <div class="form-group row">
                        <label for="latitude" class="col-sm-2 mt-2 col-form-label">Latitude</label>
                        <div class="col-sm-4 mt-2">
                            <input type="text" name="Lat" id="x" class="form-control" readonly />
                        </div>
                        <label for="longitude" class="col-sm-2 mt-2 col-form-label">Longitude</label>
                        <div class="col-sm-4 mt-2">
                            <input type="text" name="Lon" id="y" class="form-control" readonly />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-12">
                            <div class="frame_map" id="wrapper" style="margin:5px">
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                <div id="map"
                                    style="width: 100%; height: 500px; "></div>
                            </div>
                        </div>
                    </div>



                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSaveUmkm">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade"  role="dialog" id="masterUmkmPekerjaanModal">
        <div class="modal-dialog modal-lg modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Umkm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>

                  <div class="form-group row">
                    <label for="umkm" class="col-sm-2 col-form-label">Umkm</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="IdUmkm" id="IdUmkm">

                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="umkm" class="col-sm-2 col-form-label">Perusahaan</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="IdPrs" id="IdPrs">

                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="umkm" class="col-sm-2 col-form-label">Perusahaan Detail</label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="IdPrsDtl" id="IdPrsDtl">

                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="umkm" class="col-sm-2 col-form-label">Pekerjaa </label>
                    <div class="col-sm-6">
                        <select class="form-control select2" name="IdPekerjaan" id="IdPekerjaan">

                        </select>
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSaveUmkmKbli">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')

    <script src={{ asset('/module-js/admins/cmb_common.js') }}></script>
    <script src={{ asset('/module-js/admins/master_umkm/umkm_pekerjaan_list.js') }}></script>
@endpush
