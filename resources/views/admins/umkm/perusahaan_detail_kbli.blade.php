@extends('admin_layouts.main')
@section('title','Perusahaan')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Umkm Perusahaan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item"><a href="#">Perusahaan Detail</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Perusahaan Detail Kbli</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                         <input type="hidden" id="IdPrsDtl" value="{{$dt['idPrsDtl']}}" name="IdPrsDtl">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="perusahaanDetailKbliModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                            <a href='{{ $_ENV['APP_URL'] }}/umkm/perusahaan/detail?IdPrs={{$dt['DataPrsDtl']['id_prs']}}'  class="btn btn-dark ml-2"> <i class="fa fa-backward"></i> Kembali </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped " id="umkm-perusahaan-detil-kbli">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Detail Perusahaan</th>
                                            <th>Nama Kbli</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade" tabindex="-1" role="dialog" id="perusahaanDetailKbliModal">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Perusahaan Detil Kbli</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row" id="inputKbli">
                        <input type="hidden" id="IdPrsKbli" name="IdPrsKbli">
                        <label for="nib" class="col-sm-2 col-form-label">Kbli</label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="IdKbli" id="IdKbli">

                            </select>
                        </div>
                    </div>
                    <div id="showPrsDtlKbli" style="display: none">

                        <div class="form-group row">
                             <label for="perusahaanDetail" class="col-sm-3 col-form-label">Perusahaan Detail</label>
                            <div class="col-sm-6">
                                <label for="setNmPrsDtl" class="col-sm col-form-label" id="setNmPrsDtl"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                             <label for="namaKbli" class="col-sm-3 col-form-label">Nama Kbli</label>
                            <div class="col-sm-6">
                                <label for="setNmKbli" class="col-sm col-form-label" id="setNmKbli"></label>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSavePerusahaanDetailKbli">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    <script src={{ asset('/module-js/admins/cmb_common.js') }}></script>
    <script src={{ asset('/module-js/admins/perusahaan/perusahaan_detail_kbli_list.js') }}></script>
@endpush
