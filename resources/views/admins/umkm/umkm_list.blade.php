@extends('admin_layouts.main')
@section('title','Umkm')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Umkm </h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm dan Perusahaan</a></div>
              <div class="breadcrumb-item"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Umkm</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="UmkmPerusahaanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped" id="umkm-perusahaan">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Nama</th>
                              <th>Alamat</th>
                              <th>Jenis Perusahaan</th>
                              <th>Status</th>
                              <th>Verifikasi</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>

    <section class="section" id="SectionCabang" style="display: none">
        <div class="section-body">
              <h2 class="section-title" id="section-title-cabang">Daftar Cabang Perusahaan</h2>
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="UmkmCabangPerusahaanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                            <input type="hidden" name="IdPerusahaanClicked" id="IdPerusahaanClicked">
                        </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped" id="umkm-perusahaan-cabang">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Nama</th>
                              <th>Alamat</th>
                              <th>No Telp</th>
                              <th>No Fax</th>
                              <th>Pembuat</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>

    <section class="section" id="SectionPerusahaanKbli" style="display: none">
        <div class="section-body">
              <h2 class="section-title" id="section-title--perusahaan-kbli">Daftar Kbli Perusahaan</h2>
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="UmkmPerusahaanKbli()"> <i class="fa fa-plus"></i> Tambah </a>

                        </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped" id="umkm-perusahaan-kbli">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Tahun</th>
                              <th>Kode</th>
                              <th>Nama</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>

    <section class="section" id="SectionPerusahaanProduk" style="display: none">
        <div class="section-body">
              <h2 class="section-title" id="section-title-perusahaan-produk">Daftar Produk</h2>
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="UmkmPerusahaanProduk()"> <i class="fa fa-plus"></i> Tambah </a>

                        </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped" id="umkm-perusahaan-produk">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Nama</th>
                              <th>Keterangan</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>

   {{--  <section class="section" id="SectionPerusahaanKebutuhanProdukKbli" style="display: none">
        <div class="section-body">
              <h2 class="section-title" id="section-title-perusahaan-kebutuhan-produk-kbli">Daftar Produk</h2>
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="UmkmPerusahaanKebutuhanProdukKbli()"> <i class="fa fa-plus"></i> Tambah </a>

                        </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-striped" id="umkm-perusahaan-kebutuhan-produk-kbli">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Tahun</th>
                              <th>Kode KBLI</th>
                              <th>Nama KBLI</th>
                              <th>Nama Produk</th>
                              <th>Keterangan</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section> --}}



    <!-- Modal -->
     <div class="modal fade"  role="dialog" id="UmkmPerusahaanModal">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">
                    <input type="hidden" name="IdPerusahaan" id="IdPerusahaan">
                    <input type="hidden" name="IdPerusahaanCabang" id="IdPerusahaanCabang">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group row" id="DivNib">
                                <label for="nib" class="col-sm-4 col-form-label">NIB</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Nib" name="Nib" placeholder="Input NIB">
                                    <label for="setNib" class="col-sm col-form-label" id="setNib" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputNamaPerusahaan" class="col-sm-4 col-form-label">Nama Perusahaan</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmPerusahaan" name="NmPerusahaan"  placeholder="Nama [Bahasa Indonesia]">
                                <label for="setNmPerusahaan" class="col-sm col-form-label" id="setNmPerusahaan" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row" id="DivJenisPerusahaan">
                                <label for="jenisPerusahaan" class="col-sm-4 col-form-label">Jenis Perusahaan</label>

                                <div class="col-sm-8">
                                    <label for="jenisPerusahaan" class="col-sm-4 col-form-label">UMKM</label>
                                   {{--  <div class="form-check form-check-inline rdIdTipe">
                                        <input class="form-check-input" type="radio" name="IdTipe" id="inlineRadio1" value="1">
                                        <label class="form-check-label" for="inlineRadio1">PMA</label>
                                    </div>
                                        <div class="form-check form-check-inline rdIdTipe">
                                        <input class="form-check-input" type="radio" name="IdTipe" id="inlineRadio2" value="2">
                                        <label class="form-check-label" for="inlineRadio2">PMDN</label>
                                    </div> --}}
                                    <label for="setIdTipe" class="col-sm col-form-label" id="setIdTipe" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                 <label for="provinsi" class="col-sm-4  col-form-label">Provinsi</label>
                                 <div class="col-sm-8" id="cmbProv">
                                    <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                                    </select>
                                 </div>
                                 <div class="col-sm-8" id="divCmbProv" style="display: none">
                                        <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                 </div>
                            </div>
                            <div class="form-group row">
                                 <label for="kabupaten" class="col-sm-4  col-form-label">Kabupaten</label>
                                <div class="col-sm-8" id="cmbKabkot">
                                    <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">

                                    </select>
                                </div>
                                <div class="col-sm-8" id="divCmbKabkot" style="display: none">
                                    <label for="setNameKab" class="col-sm col-form-label" id="setNameKab"></label>
                                 </div>
                            </div>
                            <div class="form-group row">
                                 <label for="alamat" class="col-sm-4  col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <Textarea class="form-control" style="height: 88px" name="Alamat" id="Alamat" placeholder="Input Alamat"> </Textarea>
                                    <label for="setAlamat" class="col-sm col-form-label" id="setAlamat" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row" id="divBtnTranslate">
                                 <label for="labaelBtnTranslate" class="col-sm-4  col-form-label">Translate</label>
                                <div class="col-sm-8">
                                    <button type="button" class="btn btn-warning" onclick="ShowTranslate()" id="btnTrasnalte"> <i class="fa fa-language" aria-hidden="true"></i> Translate</button>
                                </div>
                            </div>


                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Nama Kontak</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Cp" name="Cp" placeholder="Input Nama Kontak">
                                    <label for="setCp" class="col-sm col-form-label" id="setCp" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">No. Tlp</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="NoTlp" name="NoTlp" placeholder="Input Tlp">
                                    <label for="setTlp" class="col-sm col-form-label" id="setTlp" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">No. Fax</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="NoFax" name="NoFax" placeholder="Input Fax">
                                    <label for="setFax" class="col-sm col-form-label" id="setFax" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Url Web</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="UrlWeb" name="UrlWeb" placeholder="Input Url Web">
                                    <label for="setUrlWeb" class="col-sm col-form-label" id="setUrlWeb" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Email" name="Email" placeholder="Input Email">
                                    <label for="setEmail" class="col-sm col-form-label" id="setEmail" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                 <label for="photoPerusahaanUmkm" class="col-sm-4  col-form-label">Lampiran</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" id="FileName" name="FileName" >
                                    <img id="setFileName" src={{asset('/admins/assets/img/bkpm.png')}} class="img-thumbnail">

                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="ColumnTransalte" class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon"><i class="fa fa-language"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kolom Translate</div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanEn" class="col-sm-4 col-form-label">Nama Perusahaan En</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmPerusahaanEn" name="NmPerusahaanEn"  placeholder="Input Name [English]">
                                <label for="setNmPerusahaanEn" class="col-sm col-form-label" id="setNmPerusahaanEn" style="display: none"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Latitude</label>
                                <div class="col-sm-8">
                                    <input type="text" name="Lat" id="x" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLat" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Longitude</label>
                                <div class="col-sm-8">

                                    <input type="text" name="Lon" id="y" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLon" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                    </div>





                    <div class="form-group row">
                        <div class="col-12">
                            <div class="frame_map" id="wrapper" style="margin:5px">
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                <div id="map"
                                    style="width: 100%; height: 500px; "></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    {{-- Modal Perusahaan Kbli --}}
    <div class="modal fade"  role="dialog" id="UmkmPerusahaanKbliModal">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal-title-perusahaan-kbli">Form Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">

                    <div class="form-group row">
                        <label for="inputPerusahaanKbliTahun" class="col-sm-3 col-form-label">Tahun </label>
                        <div class="col-sm-4">
                            <select class="form-control select2" name="Tahun" id="Tahun">
                            </select>
                        </div>
                    </div>
                   {{--  <div class="form-group row">
                        <label for="inputPerusahaanKategori" class="col-sm-3 col-form-label">Kategori </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="Kategori" id="Kategori">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanGolPokok" class="col-sm-3 col-form-label"> Gol Pokok </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="GolPokok" id="GolPokok">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanGolongan" class="col-sm-3 col-form-label"> Golongan </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="Golongan" id="Golongan">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanSubGolongan" class="col-sm-3 col-form-label"> Sub Golongan </label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="SubGolongan" id="SubGolongan">
                            </select>
                        </div>
                        <div class="col-sm-2" id="DivSubGolongan" style="display: none">
                            <button type="button" class="btn btn-primary" id="btnSaveSubGolongan"><i class="fa fa-check" aria-hidden="true"></i> Pilih </button>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="inputPerusahaanKelompok" class="col-sm-3 col-form-label"> Kelompok </label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="Kelompok" id="Kelompok">
                            </select>
                        </div>
                        <div class="col-sm-2" id="DivKelompok" style="display: none">
                            <button type="button" class="btn btn-primary" id="btnSaveKelompok"><i class="fa fa-check" aria-hidden="true"></i> Pilih </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>

    {{-- Modal Perusahaan Produk --}}
    <div class="modal fade"  role="dialog" id="UmkmPerusahaanProdukModal">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal-title-perusahaan-produks">Form Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">

                    <div class="form-group row">
                        <label for="inputPerusahaanKbliPerusahaanProduk" class="col-sm-3 col-form-label">KBLI </label>
                        <div class="col-sm-4">
                            <select class="form-control select2" name="KbliPerusahaanProduk" id="KbliPerusahaanProduk">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputNamaPerusahaanProduk" class="col-sm-3 col-form-label">Nama </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="NamaPerusahaanProduk" id="NamaPerusahaanProduk" placeholder="Input Nama Produk" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputKeteranganPerusahaanProduk" class="col-sm-3 col-form-label">Keterangan </label>
                        <div class="col-sm-6">
                            <textarea name="KeteranganPerusahaanProduk" id="KeteranganPerusahaanProduk" class="form-control" style="height: 80px" placeholder="Input Keterangan"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputFilePerusahaanProduk" class="col-sm-3 col-form-label">Gambar </label>
                        <div class="col-sm-8">
                            <input type="file" name="FileNamePerusahaanProduk" id="FileNamePerusahaanProduk" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row" id="DivBtnTranslatePerusahaanProduk">
                        <label for="btnTransaltePerusahaanProduk" class="col-sm-3 col-form-label">Translate </label>
                        <div class="col-sm-8">
                             <button type="button" class="btn btn-warning" onclick="ShowTranslatePerusahaanProduk()" id="btnTranslatePerusahaanProduk"> <i class="fa fa-language" aria-hidden="true"></i> Transalte</button>
                        </div>
                    </div>

                    <div id="ColumnTranslatePerusahaanProduk" class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon"><i class="fa fa-language"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kolom Translate Perusahaan Produk</div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanProdukEn" class="col-sm-4 col-form-label">Nama Produk En</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmPerusahaanProdukEn" name="NmPerusahaanProdukEn"  placeholder="Input Name Product [English]">
                                <label for="setNmPerusahaanProdukEn" class="col-sm col-form-label" id="setNmPerusahaanProdukEn" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanProdukEn" class="col-sm-4 col-form-label">Keterangan Produk En</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="KeteranganPerusahaanProdukEn" name="KeteranganPerusahaanProdukEn" style="height: 80px" placeholder="Input Keterangan [English]"></textarea>

                                    <label for="setKeteranganPerusahaanProdukEn" class="col-sm col-form-label" id="setKeteranganPerusahaanProdukEn" style="display: none"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
             <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaanProduk">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    {{-- modal Perusahaan Kebutuha Produk --}}
    <div class="modal fade"  role="dialog" id="UmkmPerusahaanKebutuhanProdukKbliModal">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="modal-title-perusahaan-kebutuhan-produk-kbli">Form Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">

                    <div class="form-group row">
                        <label for="inputPerusahaanKebutuhanTahun" class="col-sm-3 col-form-label">Tahun </label>
                        <div class="col-sm-4">
                            <select class="form-control select2" name="TahunProdukKebutuhan" id="TahunProdukKebutuhan">
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="inputPerusahaanKategoriPerusahaanKebutuhan" class="col-sm-3 col-form-label">Kategori </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="KategoriPerusahaanKebutuhan" id="KategoriPerusahaanKebutuhan">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanGolPokok" class="col-sm-3 col-form-label"> Gol Pokok </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="GolPokokPerusahaanKebutuhan" id="GolPokokPerusahaanKebutuhan">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanGolongan" class="col-sm-3 col-form-label"> Golongan </label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="GolonganPerusahaanKebutuhan" id="GolonganPerusahaanKebutuhan">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPerusahaanSubGolonganPerusahaanKebutuhan" class="col-sm-3 col-form-label"> Sub Golongan </label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="SubGolonganPerusahaanKebutuhan" id="SubGolonganPerusahaanKebutuhan">
                            </select>
                        </div>

                    </div> --}}
                    <div class="form-group row">
                        <label for="inputPerusahaanKelompokPerusahaanKebutuhan" class="col-sm-3 col-form-label"> Kelompok </label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="KelompokPerusahaanKebutuhan" id="KelompokPerusahaanKebutuhan">
                            </select>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label for="inputNamaPerusahaanKebutuhanProduk" class="col-sm-3 col-form-label"> Nama </label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="NamaPerusahaanKebutuhanProduk" name="NamaPerusahaanKebutuhanProduk">
                        </div>

                    </div>
                    <div class="form-group row">
                        <label for="inputKeteranganPerusahaanKebutuhanProduk" class="col-sm-3 col-form-label"> Keterangan </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="KeteranganPerusahaanKebutuhanProduk" name="KeteranganPerusahaanKebutuhanProduk" style="height: 80px"></textarea>
                        </div>
                    </div>

                    <div class="form-group row" id="DivBtnTranslateKebutuhanPerusahaanProduk">
                        <label for="btnTransalteKebutuhanPerusahaanProduk" class="col-sm-3 col-form-label">Translate </label>
                        <div class="col-sm-8">
                             <button type="button" class="btn btn-warning" onclick="ShowTranslateKebutuhanProdukPerusahaan()" id="btnTranslateKebutuhanProdukPerusahaan"> <i class="fa fa-language" aria-hidden="true"></i> Transalte</button>
                        </div>
                    </div>

                    <div id="ColumnTranslatePerusahaanKebutuhanProduk" class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon"><i class="fa fa-language"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kolom Translate Kebutuhan Produk Perusahaan</div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanKebutuhanProdukEn" class="col-sm-4 col-form-label">Nama Produk En</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmKebutuhanPerusahaanProdukEn" name="NmKebutuhanPerusahaanProdukEn"  placeholder="Input Name Product [English]">
                                <label for="setNmKebutuhanPerusahaanProdukEn" class="col-sm col-form-label" id="setNmKebutuhanPerusahaanProdukEn" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputKeteranganKebutuhanPerusahaanProdukEn" class="col-sm-4 col-form-label">Keterangan Produk En</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="KeteranganKebutuhanPerusahaanProdukEn" name="KeteranganKebutuhanPerusahaanProdukEn" style="height: 80px" placeholder="Input Keterangan [English]"></textarea>

                                    <label for="setKeteranganKebutuhanPerusahaanProdukEn" class="col-sm col-form-label" id="setKeteranganKebutuhanPerusahaanProdukEn" style="display: none"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
             <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSaveKebutuhanPerusahaanProduk">Save changes</button>
            </div>
        </div>
        </div>
    </div>



@endsection

@push('script-adm')
    <script src={{ asset('/module-js/admins/cmb_common.js') }}></script>
    <script src={{ asset('/module-js/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/module-js/admins/umkm/umkm_list.js') }}></script>
    <script src={{ asset('/module-js/admins/map_google.js') }}></script>

@endpush
