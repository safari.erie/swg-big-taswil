@extends('admin_layouts.main')
@section('title','Perusahaan')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Umkm Perusahaan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item"><a href="#">Perusahaan</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Umkm Perusahaan</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="perusahaanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped " id="umkm-perusahaan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>NIB</th>
                                            <th>Nama Perusahaan</th>
                                            <th>Jenis Perusahaan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade"  role="dialog" id="perusahaanModal">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="IdPrs" id="IdPrs">
                    <div class="form-group row">
                    <label for="nib" class="col-sm-2 col-form-label">NIB</label>
                    <div class="col-sm-6">
                      <input type="number" class="form-control" id="KdNib" name="KdNib" placeholder="Input NIB">
                      <label for="setNib" class="col-sm col-form-label" id="setNib" style="display: none"></label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputNamaPerusahaan" class="col-sm-2 col-form-label">Nama Perusahaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="NmPerusahaan" name="NmPerusahaan"  placeholder="Input Nama Perusahaan">
                      <label for="setNmPerusahaan" class="col-sm col-form-label" id="setNmPerusahaan" style="display: none"></label>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="jenisPerusahaan" class="col-sm-2 col-form-label">Jenis Perusahaan</label>
                    <div class="col-sm-10 mt-2">
                        <div class="form-check form-check-inline rdFlagJenis">
                            <input class="form-check-input" type="radio" name="FlagJenis" id="inlineRadio1" value="1">
                            <label class="form-check-label" for="inlineRadio1">PMA</label>
                        </div>
                            <div class="form-check form-check-inline rdFlagJenis">
                            <input class="form-check-input" type="radio" name="FlagJenis" id="inlineRadio2" value="2">
                            <label class="form-check-label" for="inlineRadio2">PMDN</label>
                        </div>
                        <label for="setFlagJenis" class="col-sm col-form-label" id="setFlagJenis" style="display: none"></label>
                    </div>
                  </div>

                  <div class="form-group row" id="tableShowPrsShowDetail" style="display: none">
                       <div class="col-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped table-responsive" id="umkm-perusahaan-detail">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Judul</th>
                                            <th>Alamat</th>
                                            <th>No. Telepon</th>
                                            <th>PIC</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten Kota</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>

                       </div>
                  </div>

                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade"  role="dialog" id="perusahaanConfrmModalDelete">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Delete Perusahaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>

@endsection

@push('script-adm')
    <script src={{ asset('/module-js/admins/perusahaan/perusahaan_list.js') }}></script>
@endpush
