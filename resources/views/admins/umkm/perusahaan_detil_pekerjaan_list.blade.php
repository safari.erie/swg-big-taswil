@extends('admin_layouts.main')
@section('title','Pekerjaan')
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Umkm Perusahaan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item"><a href="#">Perusahaan Detail Pekerjaan</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Data Perusahaan Detail Pekerjaan</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                         <input type="hidden" id="IdPrsDtl" value="{{$dt['idPrsDtl']}}" name="IdPrsDtl">
                        <div class="card-header">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="perusahaanDetailPekerjaanModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                            <a href='{{ $_ENV['APP_URL'] }}/umkm/perusahaan/detail?IdPrs={{$dt['DataPrsDtl']['id_prs']}}'  class="btn btn-dark ml-2"> <i class="fa fa-backward"></i> Kembali </a>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped " id="umkm-perusahaan-detil-pekerjaan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Detail Perusahaan</th>
                                            <th>Nama Kbli</th>
                                            <th>Deskripsi Produk</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade"  role="dialog" id="perusahaanDetailPekerjaanModal">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Perusahaan Detil Pekerjaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="IdPekerjaan" id="IdPekerjaan">
                    <div class="form-group row">
                        <label for="nib" class="col-sm-2 col-form-label">Kbli</label>
                        <div class="col-sm-6" id="DivSelectKbli">
                            <select class="form-control select2" name="IdKbli" id="IdKbli" style="width:100%"></select>

                        </div>
                        <div class="col-sm-6" style="display: none" id="DivNameKbli">
                             <label for="setNameKbli" class="col-sm col-form-label" id="setNameKbli" ></label>
                        </div>
                  </div>
                  <div class="form-group row">
                        <label for="desProduk" class="col-sm-2 col-form-label">Deskripsi Produk</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="DesProduk" id="DesProduk" style="height: 88px"></textarea>
                            <label for="setDesProd" class="col-sm col-form-label" id="setDesProd" style="display: none"></label>
                        </div>
                  </div>


                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnSavePerusahaanDetailPekerjaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    <script src={{ asset('/module-js/admins/cmb_common.js') }}></script>
    <script src={{ asset('/module-js/admins/perusahaan/perusahaan_detail_pekerjaan_list.js') }}></script>
@endpush
