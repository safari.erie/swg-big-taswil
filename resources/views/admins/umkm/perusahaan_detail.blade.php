@extends('admin_layouts.main')
@section('title','Perusahaan')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')
    {{-- <link href={{url('/admins/plugin/leaflet/leaflet.css')}} rel="stylesheet" />
    <link href={{url('/admins/plugin/leaflet/leaflet.fullscreen.css')}} rel="stylesheet" type="text/css" />
    <link href={{url('/admins/plugin/leaflet/L.Control.Zoomslider.css')}} rel="stylesheet" type="text/css" />
    <link href={{url('/admins/plugin/leaflet/L.Control.Basemaps.css')}} rel="stylesheet" />
    <style>
        .modal-full {
                max-width: 98%;
            }
         #map {
            margin: 0px;
            width: 100%;
            height: 350px;
            padding: 0px;
        }
    </style> --}}
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">
    .modal-full {
                max-width: 80%;
            }
    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            {{-- {{$dt['idPrs']}} --}}
            <h1>Umkm Perusahaan Detail</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Umkm</a></div>
              <div class="breadcrumb-item"><a href="#">Perusahaan</a></div>
              <div class="breadcrumb-item">Data</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title"> <small>Nama Perusahaan</small>  {{$dt['dataPerusahaan']}}</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header" id="card-header-detail">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="perusahaanDetailModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                            <a href='{{ $_ENV['APP_URL'] }}/umkm/perusahaan'  class="btn btn-dark ml-2"> <i class="fa fa-backward"></i> Kembali </a>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="IdPrs" value="{{$dt['idPrs']}}" name="IdPrs">
                            <div id="table">
                                 <table class="table table-bordered table table-striped " id="umkm-perusahaan-detail">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            No
                                        </th>
                                        <th>Judul</th>
                                        <th>Alamat</th>
                                        <th>No. Telepon</th>
                                        <th>PIC</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten Kota</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                            </div>

                            <div id="form" style="display: none" >
                                <form>
                                    <input type="hidden" id="IdPrsDtl" name="IdPrsDtl" >
                                    <div class="form-group row">
                                        <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                                        <div class="col-sm-6">
                                        <input type="text" class="form-control" id="NmDetil" name="NmDetil" placeholder="Input Judul">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat Perusahaan</label>
                                    <div class="col-sm-10">
                                    <textarea class="form-control" name="AlmtPerusahaan" id="alamat" style="height: 88px" placeholder="Input Alamat"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="input-tlpn" class="col-sm-2 col-form-label">No. Telepon</label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="NoPhone" name="NoPhone" placeholder="Input No Telepon">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="input-tlpn" class="col-sm-2 col-form-label">Nama PIC</label>
                                    <div class="col-sm-6">
                                    <input type="text" class="form-control" id="NmPic" name="NmPic"  placeholder="Input PIC">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="provinsi" class="col-sm-2 mt-2 col-form-label">Provinsi</label>
                                    <div class="col-sm-4 mt-2" id="cmbProv">
                                        <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                                        </select>
                                    </div>
                                    <label for="kabupaten" class="col-sm-2 mt-2 col-form-label " name="IdKabKot">Kabupaten</label>
                                    <div class="col-sm-4 mt-2">
                                        <select class="form-control select2" id="IdAdmKab" name="IdAdmKab">
                                            <option>Pilih Kabupaten</option>
                                            <option>Kab 1</option>
                                            <option>Kab 2</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label for="latitude" class="col-sm-2 mt-2 col-form-label">Latitude</label>
                                    <div class="col-sm-4 mt-2">
                                       <input type="text" name="Lat" id="x" class="form-control" readonly />
                                    </div>
                                    <label for="longitude" class="col-sm-2 mt-2 col-form-label">Longitude</label>
                                    <div class="col-sm-4 mt-2">
                                        <input type="text" name="Lon" id="y" class="form-control" readonly />
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="col-12">
                                        <div class="frame_map" id="wrapper" style="margin:5px">
                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                            <div id="map"
                                                style="width: 100%; height: 500px; "></div>
                                        </div>
                                   </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="alert alert-secondary">
                                            <div class="text-right">
                                                <button type="button" class="btn btn-primary" id="btnSavePerusahaanDetil"> Simpan</button>
                                            <button type="button" class="btn btn-default" onclick="cancelDetailPerusahaan()"> Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
     <div class="modal fade"  role="dialog" id="perusahaanDetailModal">
        <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Perusahaan Detil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="inputNamaPerusahaan" class="col-sm-3 col-form-label">Nama Perusahaan</label>
                        <div class="col-sm-9">
                        <label for="setNmPerusahaanDetail" class="col-sm col-form-label" id="setNmPerusahaanDetail" ></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputAlamatPerusahaan" class="col-sm-3 col-form-label">Alamat Perusahaan</label>
                        <div class="col-sm-9">
                        <label for="setAlmtPerusahaanDetail" class="col-sm col-form-label" id="setAlmtPerusahaanDetail" ></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPhonePerusahaan" class="col-sm-3 col-form-label">No Telepon</label>
                        <div class="col-sm-9">
                        <label for="setPhonePerusahaanDetail" class="col-sm col-form-label" id="setPhonePerusahaanDetail" ></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPicPerusahaan" class="col-sm-3 col-form-label">Pic</label>
                        <div class="col-sm-9">
                        <label for="setPicPerusahaanDetail" class="col-sm col-form-label" id="setPicPerusahaanDetail" ></label>
                        </div>
                    </div>
                    <div class="form-group row" id="tableShowPrsDetailKbli">
                       <div class="col-12">
                            <h2 class="title">Data  <strong>KBLI</strong></h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped table-responsive" id="umkm-perusahaan-detail-kbli">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Nama KBLI</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>

                       </div>
                    </div>
                    <div class="form-group row" id="tableShowPrsDetailPekerjaan">
                       <div class="col-12">
                            <h2 class="title">Data  <strong>Pekerjaan</strong></h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table table-striped table-responsive" id="umkm-perusahaan-detail-pekerjaan">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th>Deskripsi Pekerjaan</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>

                       </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('script-adm')
    {{-- <script src={{asset('/admins/plugin/leaflet/leaflet.js')}} type="text/javascript"></script>
    <script src={{asset('/admins/plugin/leaflet/L.Control.Basemaps.js')}} type="text/javascript"></script>
    <script src={{asset('/admins/plugin/leaflet/Leaflet.fullscreen.min.js')}}></script>
    <script src={{asset('/admins/plugin/leaflet/L.Control.Zoomslider.js')}} type="text/javascript"></script>
 --}}
    {{-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDbnJkuCxdMYNBi19TakuSsPBt5RCLke10" type="text/javascript"></script>
 --}}

    <script src={{ asset('/module-js/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/module-js/admins/perusahaan/perusahaan_detail.js') }}></script>
    <script src={{ asset('/module-js/admins/perusahaan/perusahaan_detail_map.js') }}></script>

@endpush
