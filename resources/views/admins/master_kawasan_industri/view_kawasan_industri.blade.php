@extends('admin_layouts.main')
@section('title','Master Kawasan Industri')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Master</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Kawsan Industri</a></div>
              <div class="breadcrumb-item">Kawasan Industri</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Kawasan Industri</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="KawasanIndustriModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                    </div>
                    <div class="card-body">


                      <div class="table-responsive">
                        <table class="table table-striped" id="kawasan-industris">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Sumber Data</th>
                              <th>Kategori</th>
                              <th>Nama</th>
                              <th>Keterangan</th>
                              <th>Alamat</th>
                              <th>Luas</th>
                              <th>Status</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>



    <div class="modal fade"  role="dialog" id="ModalKawasanIndustri">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Kawasan Industri</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">
                    <input type="hidden" name="IdProvinsi" id="IdProvinsi">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="provinsi" class="col-sm-4  col-form-label">Provinsi</label>
                                <div class="col-sm-8" id="cmbProv">
                                <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kab" class="col-sm-4  col-form-label">Kabupaten</label>
                                <div class="col-sm-8" id="cmbKab">
                                <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sumber-data" class="col-sm-4  col-form-label">Sumber Data</label>
                                <div class="col-sm-8" id="cmbKab">
                                <select class="form-control select2" name="IdSumberData" id="IdSumberData">
                                    <option value="0"> -- Pilih Salah Satu --</option>
                                    <option value="6854"> Kawasan Industri - Indonesia industrial estate directory 2015/2016</option>
                                    <option value="6864"> Kantor Pemerintahan - Internet data diolah</option>
                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sumber-data" class="col-sm-4  col-form-label">Kategori</label>
                                <div class="col-sm-8" id="cmbKab">
                                <select class="form-control select2" name="IdKategori" id="IdKategori">
                                    <option value="0"> -- Pilih Salah Satu --</option>
                                    <option value="6854"> Not Available</option>
                                    <option value="6864"> Kawasan Ekonomi Khusus</option>
                                    <option value="6864"> Kawasan Industri HKI</option>
                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Nama" name="Nama" placeholder="Input Nama">
                                    <label for="setNama" class="col-sm col-form-label" id="setNama" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Keterangan</label>
                                <div class="col-sm-8">
                                    <textarea style="height: 80px" class="form-control" placeholder="Input Keterangan"></textarea>
                                    <label for="setNama" class="col-sm col-form-label" id="setNama" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Luas</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="Luas" name="Luas" placeholder="Input Luas">
                                    <label for="setKabupaten" class="col-sm col-form-label" id="setKabupaten" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Luas Satuan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="NoTlp" name="NoTlp" placeholder="Luas Satuan">
                                    <label for="setTlp" class="col-sm col-form-label" id="setTlp" style="display: none"></label>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="sumber-data" class="col-sm-4  col-form-label">Bandara Terdekat</label>
                                <div class="col-sm-8" id="cmbKab">
                                <select class="form-control select2" name="IdBandara" id="IdBandara">
                                    <option value="0"> -- Pilih Salah Satu --</option>

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Jarak Bandara Terdekat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="JarakBandara" name="JarakBandara" placeholder="Input Jarak Bandara">
                                    <label for="setKabupaten" class="col-sm col-form-label" id="setKabupaten" style="display: none"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sumber-data" class="col-sm-4  col-form-label">Pelabuhan Terdekat</label>
                                <div class="col-sm-8" id="cmbKab">
                                <select class="form-control select2" name="IdPelabuhan" id="IdPelabuhan">
                                    <option value="0"> -- Pilih Salah Satu --</option>

                                </select>
                                </div>
                                <div class="col-sm-8" id="divCmbProv" style="display: none">
                                    <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Jarak Pelabuhan Terdekat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="JarakPelabuhan" name="JarakPelabuhan" placeholder="Input Jarak Pelabuhan">
                                    <label for="setKabupaten" class="col-sm col-form-label" id="setKabupaten" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="nama" class="col-sm-4 col-form-label">Jarak Ibukota</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="JarakIbukota" name="JarakIbukota" placeholder="Input Jarak Ibukota">
                                    <label for="setKabupaten" class="col-sm col-form-label" id="setKabupaten" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                            <label for="namaKontak" class="col-sm-4 col-form-label">No. Tlp</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="NoTlp" name="NoTlp" placeholder="Input Tlp">
                                <label for="setTlp" class="col-sm col-form-label" id="setTlp" style="display: none"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="namaKontak" class="col-sm-4 col-form-label">No. Fax</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="NoFax" name="NoFax" placeholder="Input Fax">
                                <label for="setFax" class="col-sm col-form-label" id="setFax" style="display: none"></label>
                            </div>
                        </div>
                         <div class="form-group row">
                                <label for="namaKontak" class="col-sm-4 col-form-label">Url Web</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="UrlWeb" name="UrlWeb" placeholder="Input Url Web">
                                    <label for="setUrlWeb" class="col-sm col-form-label" id="setUrlWeb" style="display: none"></label>
                                </div>
                            </div>
                        <div class="form-group row">
                                 <label for="photoPerusahaanUmkm" class="col-sm-4  col-form-label">Foto</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" id="FileName" name="FileName" >

                                </div>
                            </div>
                        </div>

                    </div>








                        <div class="form-group row" id="divBtnTranslate">
                            <label for="btnTranslate" class="col-sm-4  col-form-label">Translate</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-warning" onclick="ShowTranslate()" id="btnTrasnalte"> <i class="fa fa-language" aria-hidden="true"></i> Translate</button>
                            </div>
                        </div>


                    <div id="ColumnTransalte" class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-primary alert-has-icon">
                            <div class="alert-icon"><i class="fa fa-language"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Kolom Translate</div>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanEn" class="col-sm-4 col-form-label">Nama  En</label>
                                <div class="col-sm-8">
                                <input type="text" class="form-control" id="NmPerusahaanEn" name="NmPerusahaanEn"  placeholder="Input Nama  [English]">
                                <label for="setNmPerusahaanEn" class="col-sm col-form-label" id="setNmPerusahaanEn" style="display: none"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputNamaPerusahaanEn" class="col-sm-4 col-form-label">Keterangan En</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" style="height: 80px" id="KeteranganKawasanIndustriEn" name="KeteranganKawasanIndustriEn"  placeholder="Ketengan En"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Latitude</label>
                                <div class="col-sm-8">
                                    <input type="text" name="Lat" id="x" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLat" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="latitude" class="col-sm-4 mt-2 col-form-label">Longitude</label>
                                <div class="col-sm-8">

                                    <input type="text" name="Lon" id="y" class="form-control" readonly />
                                    <label for="setEmail" class="col-sm col-form-label" id="setLon" style="display: none"></label>
                                </div>
                            </div>

                        </div>
                    </div>



                     <body onload="initialize(0,0)">
                        <div class="form-group">
                        <div class="col-12">
                                <div class="frame_map" id="wrapper" style="margin:5px">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                    <div id="map"
                                        style="width: 100%; height: 500px; "></div>
                                </div>
                        </div>
                        </div>
                    </body>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>





@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/cmb_common.js') }}></script>
    <script src={{ asset('/modulejs/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/modulejs/admins/master_kawasan_industri/kawasan_industri.js') }}></script>
    <script src={{ asset('/modulejs/admins/map_google.js') }}></script>

@endpush
