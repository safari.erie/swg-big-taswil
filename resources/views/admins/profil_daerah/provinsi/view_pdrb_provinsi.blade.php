@extends('admin_layouts.main')
@section('title','Pdrb Provinsi')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profil Daerah</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Profil Daerah</a></div>
              <div class="breadcrumb-item">Provinsi</div>
              <div class="breadcrumb-item">Pdrb</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Pdrb Provinsi</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="ProfilDaerahProvPdrbModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                    </div>
                    <div class="card-body">


                      <div class="table-responsive">
                        <table class="table table-striped" id="pdrb-provinsi">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Provinsi</th>
                              <th>Tahun</th>
                              <th>Sumber Data</th>
                              <th>Sektor</th>
                              <th>Pct</th>
                              <th>Jumlah</th>
                              <th>Laju Pertumbuhan</th>
                              <th>Status</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>



    <div class="modal fade"  role="dialog" id="ModalPdrbProv">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Pdrb Provinsi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">
                    <input type="hidden" name="IdProvinsi" id="IdProvinsi">
                    <div class="form-group row">
                        <label for="provinsi" class="col-sm-4  col-form-label">Provinsi</label>
                        <div class="col-sm-8" id="cmbProv">
                        <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                        </select>
                        </div>
                        <div class="col-sm-8" id="divCmbProv" style="display: none">
                            <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                        </div>
                </div>
                    <div class="form-group row" >
                        <label for="nib" class="col-sm-4 col-form-label">Tahun</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="Tahun" name="Tahun" placeholder="Input  Tahun">
                            <label for="setTahun" class="col-sm col-form-label" id="setTahun" style="display: none"></label>
                        </div>
                    </div>



                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Sumber Data</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="SumberData" id="SumberData" >
                                <option value="0">-- Pilih Salah Satu --</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Sektor</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="Sektor" id="Sektor" >
                                <option value="0">-- Pilih Salah Satu --</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Pct</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="Pct" name="Pct" placeholder="Input Jumlah Pct">
                            <label for="setPria" class="col-sm col-form-label" id="setPct" style="display: none"></label>
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Jumlah</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="Jumlah" name="Jumlah" placeholder="Input Jumlah">
                            <label for="setPria" class="col-sm col-form-label" id="setPria" style="display: none"></label>
                        </div>
                    </div>

                     <div class="form-group row">
                            <label for="namaKontak" class="col-sm-4 col-form-label">Laju Pertumbuhan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="NoTlp" name="NoTlp" placeholder="Input Laju Pertumbuhan">
                                <label for="setLajuPertumbuhan" class="col-sm col-form-label" id="setLajuPertumbuhan" style="display: none"></label>
                            </div>
                        </div>



                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>





@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/cmb_common.js') }}></script>
    <script src={{ asset('/modulejs/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/modulejs/admins/profil_daerah//provinsi/pdrb.js') }}></script>

@endpush
