@extends('admin_layouts.main')
@section('title','Demografi Kabupaten')
@push('meta')
    <meta name="description" content="Find the latitude and longitude of a point using Google Maps.">
    <meta name="keywords" content="latitude, longitude, google maps, get latitude and longitude">
@endpush
@push('style-admin')

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyDphwoRHE7wxAJWDjBzPObLUgycF1aIEW4" type="text/javascript"></script>
<style type="text/css">

    .modal-mid-full {
                max-width: 78%;
            }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        z-index: 9999;
    }


    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999
    }

    #pac-input:focus {
        border-color: #4d90fe;
        z-index: 9999
    }


    .pac-container { z-index: 100000 !important;  font-family: Roboto; }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
<style>
    #target {
        width: 345px;
    }
</style>
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profil Daerah</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Profil Daerah</a></div>
              <div class="breadcrumb-item">Kabupaten</div>
              <div class="breadcrumb-item">Demografi</div>
            </div>
        </div>
        <div class="section-body">
              <h2 class="section-title">Demografi Kabupaten</h2>
              <p class="section-lead"></a>.
              </p>

              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                         <a href="javascript:void(0)" class="btn btn-primary" onclick="ProfilDaerahKabkotaDemografiModalShow()"> <i class="fa fa-plus"></i> Tambah </a>
                    </div>
                    <div class="card-body">


                      <div class="table-responsive">
                        <table class="table table-striped" id="demografi-provinsi">
                          <thead>
                            <tr>
                              <th>Aksi</th>
                              <th>Provinsi</th>
                              <th>Kabupaten</th>
                              <th>Tahun</th>
                              <th>Kategori</th>
                              <th>Sumber Data</th>
                              <th>Jumlah Pria</th>
                              <th>Jumlah Wanita</th>
                              <th>Kepadatan Penduduk</th>
                              <th>Laju Pertumbuhan</th>
                              <th>Status</th>
                            </tr>
                          </thead>

                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </section>



    <div class="modal fade"  role="dialog" id="ModalDemografiProv">
        <div class="modal-dialog modal-mid-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title">Form Demografi Provinsi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data"  class="form-horizontal">
                    <input type="hidden" name="IdProvinsi" id="IdProvinsi">
                    <div class="form-group row">
                        <label for="provinsi" class="col-sm-4  col-form-label">Provinsi</label>
                        <div class="col-sm-8" id="cmbProv">
                        <select class="form-control select2" name="IdAdmProv" id="IdAdmProv">

                        </select>
                        </div>
                        <div class="col-sm-8" id="divCmbProv" style="display: none">
                            <label for="setNameProv" class="col-sm col-form-label" id="setNameProv"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                                 <label for="kabupaten" class="col-sm-4  col-form-label">Kabupaten</label>
                                <div class="col-sm-8" id="cmbKabkot">
                                    <select class="form-control select2" name="IdAdmKabKot" id="IdAdmKabKot">

                                    </select>
                                </div>
                                <div class="col-sm-8" id="divCmbKabkot" style="display: none">
                                    <label for="setNameKab" class="col-sm col-form-label" id="setNameKab"></label>
                                 </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nib" class="col-sm-4 col-form-label">Tahun</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="Tahun" name="Tahun" placeholder="Input  Tahun">
                            <label for="setTahun" class="col-sm col-form-label" id="setTahun" style="display: none"></label>
                        </div>
                    </div>


                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Kategori</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="Kategori" id="Kategori" >
                                <option value="0">-- Pilih Salah Satu --</option>
                            </select>

                        </div>
                    </div>
                     <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Sumber Data</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="SumberData" id="SumberData" >
                                <option value="0">-- Pilih Salah Satu --</option>
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Jumlah Pria</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="JumlahPria" name="JumlahPria" placeholder="Input Jumlah Pria">
                            <label for="setPria" class="col-sm col-form-label" id="setPria" style="display: none"></label>
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Jumlah Wanita</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="JumlahPria" name="JumlahPria" placeholder="Input Jumlah Wanita">
                            <label for="setWanita" class="col-sm col-form-label" id="setWanita" style="display: none"></label>
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="nama" class="col-sm-4 col-form-label">Kepadatan Penduduk</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" id="Kepadatan Penduduk" name="Kepadatan Penduduk" placeholder="Input  Kepadatan Penduduk">
                            <label for="setKepadatanPenduduk" class="col-sm col-form-label" id="setKepadatanPenduduk" style="display: none"></label>
                        </div>
                    </div>
                     <div class="form-group row">
                            <label for="namaKontak" class="col-sm-4 col-form-label">Laju Pertumbuhan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="NoTlp" name="NoTlp" placeholder="Input Laju Pertumbuhan">
                                <label for="setLajuPertumbuhan" class="col-sm col-form-label" id="setLajuPertumbuhan" style="display: none"></label>
                            </div>
                        </div>



                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnSavePerusahaan">Save changes</button>
            </div>
        </div>
        </div>
    </div>





@endsection

@push('script-adm')
    <script src={{ asset('/modulejs/admins/cmb_common.js') }}></script>
    <script src={{ asset('/modulejs/admins/administrasi_wilayah/combo_wilayah.js') }}></script>
    <script src={{ asset('/modulejs/admins/profil_daerah//kabupaten/demografi.js') }}></script>

@endpush
