<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  @stack('meta')
  <title>Pir - @yield('title')</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href={{asset('/admins/modules/bootstrap/css/bootstrap.min.css')}} >
  <link rel="stylesheet" href={{asset('/admins/modules/fontawesome/css/all.min.css')}}>

  <!-- plugin datatables -->
  {{-- <link rel="stylesheet" href={{asset('/admins/plugin/datatable/1.10.19/css/dataTables.bootstrap4.min.css')}}> --}}
  <link rel="stylesheet" href={{asset('/admins/modules/datatables/datatables.min.css')}}>
  <link rel="stylesheet" href={{asset('/admins/modules/datatables/1.10.16/css/dataTables.bootstrap4.min.css')}}>
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">

  <!-- CSS Libraries -->
   <link rel="stylesheet" href={{asset('/admins/plugin/sweetalert/dist/sweetalert.css')}}>
   <link rel="stylesheet" href={{asset('/admins/modules/izitoast/css/iziToast.min.css')}}>
   <link rel="stylesheet" href={{asset('/admins/modules/select2/dist/css/select2.min.css')}}>
  <!-- Template CSS -->
  <link rel="stylesheet" href={{asset('/admins/assets/css/style.css')}}>
  <link rel="stylesheet" href={{asset('/admins/assets/css/components.css')}}>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <style>
      #loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        display: block;
        opacity: 0.9;
        background-color: #fff;
        z-index: 2000;
        text-align: center;
    }
  </style>
  <script >
    function LoadingBar(Status){
        if (Status == "wait") {
                $("body").css("cursor", "progress");
                $("#loading").removeAttr("style");
            } else if (Status == "success") {
                $("body").css("cursor", "default");
                $("#loading").css("display", "none");
            }
    }
  </script>
  @stack('style-admin')
</head>
