function ComboKbli(handleData, IdKbli) {
    var Url = base_url + "/api/Umkm/GetKblis";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdKbli == IdKbli) {
                        html_combo += `<option value=${value.IdKbli} selected> ${value.KdKbli} - ${value.NmKbli} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdKbli}> ${value.KdKbli} -  ${value.NmKbli} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboUmkm(handleData, IdUmkm) {
    var Url = base_url + "/api/Umkm/GetUmkms";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdUmkm == IdUmkm) {
                        html_combo += `<option value=${value.IdUmkm} selected> ${value.KdNib} - ${value.NmPerusahaan} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdUmkm}> ${value.KdNib} -  ${value.NmPerusahaan} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboPerusahaan(handleData, Id) {
    var Url = base_url + "/api/Umkm/GetPerusahaans";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdPrs == Id) {
                        html_combo += `<option value=${value.IdPrs} selected> ${value.KdNib} - ${value.NmPerusahaan} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdPrs}> ${value.KdNib} -  ${value.NmPerusahaan} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboPerusahaanDetail(handleData, IdPrs, IdPrsDtl) {
    var Url = base_url + "/api/Umkm/GetPerusahaanDetils?IdPrs=" + IdPrs;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdPrsDtl == IdPrsDtl) {
                        html_combo += `<option value=${value.IdPrsDtl} selected> ${value.Judul} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdPrsDtl}>  ${value.Judul} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboPekerjaan(handleData, IdPrsDtl, IdPekerjaan) {
    var Url = base_url + "/api/Umkm/GetPekerjaans?IdPrsDtl=" + IdPrsDtl;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdPekerjaan == IdPekerjaan) {
                        html_combo += `<option value=${value.IdPekerjaan} selected> ${value.DesProduk} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdPekerjaan}>  ${value.DesProduk} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboGetTahun(handleData, IdTahun) {
    var html_combo = "";
    html_combo = '<option value="">- Pilih Tahun -</option>';
    var d = new Date($.now());
    var TahunSekarang = d.getFullYear();
    var TahunMulai = d.getFullYear() - 2;
    for (TahunSekarang; TahunSekarang >= TahunMulai; TahunSekarang--) {
        if (IdTahun == null) {
            /* if (TahunSekarang == d.getFullYear()) {
                html_combo +=
                    '<option value="' +
                    TahunSekarang +
                    '" selected>' +
                    TahunSekarang +
                    "</option>";
            } else { */
            html_combo +=
                '<option value="' +
                TahunSekarang +
                '">' +
                TahunSekarang +
                "</option>";
            /* } */
        } else {
            if (TahunSekarang == IdTahun) {
                html_combo +=
                    '<option value="' +
                    TahunSekarang +
                    '" selected>' +
                    TahunSekarang +
                    "</option>";
            } else {
                html_combo +=
                    '<option value="' +
                    TahunSekarang +
                    '">' +
                    TahunSekarang +
                    "</option>";
            }
        }
    }
    handleData(html_combo);
}

function ComboGetKategoriKbli(handleData, Tahun, IdKbli) {
    var Url = base_url + "/api/Umkm/GetKategoriKblis?Tahun=" + Tahun;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.id_kbli == IdKbli) {
                        html_combo += `<option value=${value.id_kbli} selected> ${value.kode} - ${value.nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.id_kbli}> ${value.kode} -  ${value.nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';

                handleData(html_combo);
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}
function ComboGetKelompok(handleData, Tahun, IdKbli) {
    var Url = base_url + "/api/Umkm/GetKelompokKblis?Tahun=" + Tahun;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.id_kbli == IdKbli) {
                        html_combo += `<option value=${value.id_kbli} selected> ${value.kode} - ${value.nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.id_kbli}> ${value.kode} -  ${value.nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';

                handleData(html_combo);
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboGetGolPokok(handleData, Tahun, IdParent, IdGolPokok) {
    var Url =
        base_url +
        "/api/Umkm/GetKbliByIdParents?Tahun=" +
        Tahun +
        "&IdParent=" +
        IdParent;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.id_kbli == IdGolPokok) {
                        html_combo += `<option value=${value.id_kbli} selected> ${value.kode} - ${value.nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.id_kbli}> ${value.kode} -  ${value.nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                /* swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                }); */
                handleData(html_combo);
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboPerusahaanKbliIdPerusahaan(
    handleData,
    IdPerusahaan,
    IdPerusahaanKbli
) {
    var Url =
        base_url +
        "/api/Umkm/GetPerusahaanKbliByIdPerusahaans?IdPerusahaan=" +
        IdPerusahaan;

    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            console.log(res);
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdPerusahaanKbli == IdPerusahaanKbli) {
                        html_combo += `<option value=${value.IdPerusahaanKbli} selected> ${value.Kode} - ${value.Nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdPerusahaanKbli}> ${value.Kode} -  ${value.Nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                handleData(html_combo);
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}
