/* Global Var */
var NamaPerusahaanClicked = "";

$("#umkm-perusahaan").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Umkm/GetPerusahaans?Type=umkm",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "40%", visible: true },
        { targets: [1], width: "10%", visible: true },
        { targets: [2], width: "30%", visible: true },
        { targets: [3], width: "10%", visible: true },
        { targets: [4], width: "5%", visible: true },
        { targets: [5], width: "5%", visible: true },
    ],
    columns: [
        {
            className: "flex",
            data: "IdUmkm",
            render: function (data, type, full, meta) {
                var NamePrs = '"' + full.NamaPerusahaan + '"';
                var BtnEdit =
                    "<button class='btn btn-primary btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show Cabang | Kbli | Produk' onClick='showCbgKbliPrd(" +
                    full.IdPerusahaan +
                    "," +
                    NamePrs +
                    ")'> <i class='fa fa-eye' aria-hidden='true'></i>   </button>" +
                    "<button class='btn btn-info btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show Umkm Perusahaan' onClick='showDetail(" +
                    full.IdPerusahaan +
                    ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>" +
                    "<button class='btn btn-success btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Edit Perusahaan' onCLick='ShowEdit(" +
                    full.IdPerusahaan +
                    ")' > <i class='fa fa-edit' aria-hidden='true'></i>  </button>" +
                    "<button class='btn btn-secondary btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Approve' > <i class='fa fa-check' aria-hidden='true'></i>  </button>" +
                    "<button class='btn btn-danger btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus' onClick='DeletePerusahaan(" +
                    full.IdPerusahaan +
                    ")' > <i class='fa fa-trash' aria-hidden='true'></i> </button>";
                data = BtnEdit;
                return data;
            },
        },
        {
            data: "NamaPerusahaan",
        },
        { data: "Alamat" },
        { data: "Tipe" },
        {
            data: "Status",
            render: function (data, type, full, meta) {
                var Data = "";
                if (full.Status === 1) {
                    Data += `<span class="badge badge-secondary">Input Data</span>`;
                } else if (full.Status === 2) {
                    Data += `<span class="badge badge-success">Approve</span>`;
                } else {
                    Data += `<span class="badge badge-pill badge-warning">Reject</span>`;
                }

                return Data;
            },
        },
        {
            data: "Status",
            render: function (data, type, full, meta) {
                var Data = "";
                if (full.StatusVerifikasi === 1) {
                    Data += `<span class="badge badge-danger">Unpublish</span>`;
                } else if (full.Status === 2) {
                    Data += `<span class="badge badge-info">Publish</span>`;
                }

                return Data;
            },
        },
    ],
    bDestroy: true,
});

function clearPerusahaanModal() {
    $("input[name='Nib']").val("");
    $("input[name='NmPerusahaan']").val("");
    $("input[name='Cp']").val("");
    $("input[name='NoTlp']").val("");
    $("input[name='NoFax']").val("");
    $("input[name='UrlWeb']").val("");
    $("input[name='Email']").val("");
    $("input[name='Lat']").val("");
    $("input[name='Lon']").val("");
    $("input[name='NmPerusahaanEn']").val("");
    $("#FileName").val("");
    $("textarea[name='Alamat']").val("");
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        0,
        ""
    );
    /* $("input[name=IdTipe][value=" + 1 + "]").prop("checked", false);
    $("input[name=IdTipe][value=" + 2 + "]").prop("checked", false); */
}

/* onclick Add  */
const UmkmPerusahaanModalShow = () => {
    clearPerusahaanModal();

    $("#UmkmPerusahaanModal").modal({ backdrop: "static", keyboard: false });
    $(".modal-title").html("Form Add Perusahaan Umkm");
    showNoView();
    $("#DivNib").show();
    $("#DivJenisPerusahaan").show();
    $("#setFileName").hide();

    initialize(0, 0);
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    $("#btnSavePerusahaan").attr("onClick", "SaveUmkmPerusahaan('Tambah')");
};

const ShowEdit = (idPerusahaan) => {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPerusahaan=" + idPerusahaan,
        method: "GET",
        dataType: "JSON",
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                /*  let idTipeValue =
                    res.Data.IdTipe === 1
                        ? $(
                              "input[name=IdTipe][value=" +
                                  res.Data.IdTipe +
                                  "]"
                          ).prop("checked", true)
                        : $(
                              "input[name=IdTipe][value=" +
                                  res.Data.IdTipe +
                                  "]"
                          ).prop("checked", true); */

                showNoView();
                $("#DivNib").show();
                $("#DivJenisPerusahaan").show();
                $(".modal-title").html(
                    `Edit Perusahaan <strong> ${res.Data.NamaPerusahaan}</strong`
                );
                $("#UmkmPerusahaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });

                $("input[name='IdPerusahaan']").val(
                    parseInt(res.Data.IdPerusahaan)
                );
                $("input[name='Nib']").val(parseInt(res.Data.Nib));
                $("input[name='NmPerusahaan']").val(res.Data.NamaPerusahaan);

                $("input[name='NmPerusahaan']").val(res.Data.NamaPerusahaan);
                ComboGetWilayahProvinsi(function (obj) {
                    $("select#IdAdmProv").html(obj);
                }, res.Data.IdAdmProvinsi);
                ComboGetWilayahKabKota(
                    function (HtmlCombo) {
                        $("select[name='IdAdmKabKot']").empty();
                        $("select[name='IdAdmKabKot']").append(HtmlCombo);
                    },
                    res.Data.IdAdmProvinsi,
                    res.Data.IdAdmKabkot
                );
                $("textarea[name='Alamat']").val(res.Data.Alamat);
                $("input[name='Cp']").val(res.Data.Cp);
                $("input[name='NoTlp']").val(res.Data.NoTelp);
                $("input[name='NoFax']").val(res.Data.NoFax);
                $("input[name='UrlWeb']").val(res.Data.UrlWeb);
                $("input[name='Email']").val(res.Data.Email);
                $("input[name='Lat']").val(res.Data.Lat);
                $("input[name='Lon']").val(res.Data.Lon);
                $("input[name='NmPerusahaanEn']").val(res.Data.NameTranslate);
                $("#setFileName").attr("src", res.Data.FileName);
                $("#btnSavePerusahaan").attr(
                    "onClick",
                    "SaveUmkmPerusahaan('EditPerusahaan')"
                );
                initialize(res.Data.Lat, res.Data.Lon);
            }
        },
    });
};

$("select[name='IdAdmProv']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});

function DeletePerusahaan(IdPerusahaan) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaan",
                    data: { IdPerusahaan: IdPerusahaan },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                    $("#perusahaanModal").modal("hide");
                                }
                            );
                        } else {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "error",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        }
                    },
                });
            }
        }
    );
}

function SaveUmkmPerusahaan(type) {
    let emptyStr = "-";
    var data = new FormData();
    let Nib = $("input[name='Nib']").val();
    let NmPerusahaan = $("input[name='NmPerusahaan']").val();
    let Alamat = $("textarea[name='Alamat']").val();
    let NoTlp = $("input[name='NoTlp']").val();
    let IdTipe = 3;
    let NoFax = $("input[name='NoFax']").val();
    let UrlWeb = $("input[name='UrlWeb']").val();
    let Email = $("input[name='Email']").val();
    let Cp = $("input[name='Cp']").val();
    let NmPerusahaanEn = $("input[name='NmPerusahaanEn']").val();
    let FileName = $("#FileName")[0].files[0];
    let Lat = $("input[name='Lat']").val();
    let Lon = $("input[name='Lon']").val();

    let IdAdmKabKot = $("select[name='IdAdmKabKot'] option")
        .filter(":selected")
        .val();

    if (type === "Tambah") {
        url = base_url + "/Umkm/AddPerusahaanUmkm";
        methods = "POST";

        data.append("Nib", Nib);
        data.append("IdTipe", IdTipe);
        data.append("IdAdmKabKot", IdAdmKabKot);
        data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    } else if (type === "EditPerusahaan") {
        let IdPerusahaan = $("input[name='IdPerusahaan']").val();
        url = base_url + "/Umkm/EditPerusahaanUmkm";
        methods = "POST";
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("Nib", Nib);
        data.append("IdTipe", IdTipe);
        data.append("IdAdmKabKot", IdAdmKabKot);
        data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    } else if (type === "TambahPerusahaanCabang") {
        let IdPerusahaanClicked = $("input[name='IdPerusahaanClicked']").val();
        url = base_url + "/Umkm/AddPerusahaanCabangUmkm";
        methods = "POST";
        data.append("IdPerusahaan", IdPerusahaanClicked);
        data.append("IdAdmKabKot", IdAdmKabKot);
        data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        if (type === "Tambah" || type === "EditPerusahaan") {
                            $("#UmkmPerusahaanModal").modal("hide");
                            $("#umkm-perusahaan").DataTable().ajax.reload();
                        } else {
                            $("#UmkmPerusahaanModal").modal("hide");
                            $("#umkm-perusahaan-cabang")
                                .DataTable()
                                .ajax.reload();
                        }
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};

function showNoView() {
    $("#Nib").show();
    $("#NmPerusahaan").show();
    /*  $(".rdIdTipe").show(); */
    $("#cmbProv").show();
    $("#cmbKabkot").show();
    $("#Alamat").show();
    $("#Cp").show();
    $("#NoTlp").show();
    $("#NoFax").show();
    $("#UrlWeb").show();
    $("#Email").show();
    $("#divBtnTranslate").show();
    $("#FileName").show();
    $("#NmPerusahaanEn").show();
    $("#x").show();
    $("#y").show();

    $("#setNib").hide();
    $("#setNmPerusahaan").hide();
    /*  $("#setIdTipe").hide(); */
    $("#divCmbProv").hide();
    $("#divcmbKabkot").hide();
    $("#setAlamat").hide();
    $("#setCp").hide();
    $("#setTlp").hide();
    $("#setFax").hide();
    $("#setUrlWeb").hide();
    $("#setEmail").hide();
    $("#setLat").hide();
    $("#setLon").hide();
    $("#ColumnTransalte").hide();
    $("#setNmPerusahaanEn").hide();
}

function ShowForView() {
    $("#Nib").hide();
    $("#NmPerusahaan").hide();
    /* $(".rdIdTipe").hide(); */
    $("#cmbProv").hide();
    $("#cmbKabkot").hide();
    $("#Alamat").hide();
    $("#Cp").hide();
    $("#NoTlp").hide();
    $("#NoFax").hide();
    $("#UrlWeb").hide();
    $("#Email").hide();
    $("#divBtnTranslate").hide();
    $("#FileName").hide();
    $("#NmPerusahaanEn").hide();
    $("#x").hide();
    $("#y").hide();

    $("#setNib").show();
    $("#setNmPerusahaan").show();
    /* $("#setIdTipe").show(); */
    $("#divCmbProv").show();
    $("#divcmbKabkot").show();
    $("#setAlamat").show();
    $("#setCp").show();
    $("#setTlp").show();
    $("#setFax").show();
    $("#setUrlWeb").show();
    $("#setEmail").show();
    $("#setLat").show();
    $("#setLon").show();
    $("#ColumnTransalte").show();
    $("#setNmPerusahaanEn").show();
}

/* All action from datatables */
const showCbgKbliPrd = (idPerusaahaan, NamaPerusahaan) => {
    NamaPerusahaanClicked = NamaPerusahaan;
    $("#SectionPerusahaanProduk").hide();
    $("#SectionCabang").show();
    $("#SectionPerusahaanKbli").show();
    //$("#SectionPerusahaanKebutuhanProdukKbli").show();
    $("#section-title-cabang").html(
        `Daftar Lokasi Kerja <strong> ${NamaPerusahaan}</strong>`
    );
    $("#section-title--perusahaan-kbli").html(
        `Daftar KBLI <strong> ${NamaPerusahaan}</strong>`
    );
    /* $("#section-title-perusahaan-kebutuhan-produk-kbli").html(
        `Daftar Kebutuhan Produk KBLI <strong> ${NamaPerusahaan}</strong>`
    ); */
    $("#setFileName").show();
    GetCabangPerusahaan(idPerusaahaan);
    GetPerusahaanKbli(idPerusaahaan);
    //GetPerusahaanKebutuhanProdukKbli(idPerusaahaan);
    $("#IdPerusahaanClicked").val(idPerusaahaan);
};

function GetPerusahaanKebutuhanProdukKbli(IdPerusahaan) {
    $("#umkm-perusahaan-kebutuhan-produk-kbli").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanKebutuhanProdukKblis?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "30%", visible: true },
            { targets: [1], width: "5%", visible: true },
            { targets: [2], width: "10%", visible: true },
            { targets: [3], width: "20%", visible: true },
            { targets: [4], width: "20%", visible: true },
            { targets: [5], width: "25%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanKebutuhanKbli",
                data: "IdPerusahaan",
                data: "IdPerusahaanKebutuhanProduk",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-info mr-2' data-tooltip='tooltip' data-placement='top' title='Show Produk' onClick='ShowPerusahaanProduk(" +
                        full.IdPerusahaanKebutuhanKbli +
                        ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>";
                    BtnEdit +=
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Tahun" },
            { data: "Kode" },
            { data: "NamaKbli" },
            { data: "NamaKebutuhanProduk" },
            { data: "KeteranganKebutuhanProduk" },
        ],
        bDestroy: true,
    });
}

function GetPerusahaanKbli(IdPerusahaan) {
    $("#umkm-perusahaan-kbli").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanKblis?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "10%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
            { targets: [3], width: "15%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanKbli",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-info mr-2' data-tooltip='tooltip' data-placement='top' title='Show Produk' onClick='ShowPerusahaanProduk(" +
                        full.IdPerusahaanKbli +
                        ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>";
                    BtnEdit +=
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Tahun" },
            { data: "Kode" },
            { data: "Nama" },
        ],
        bDestroy: true,
    });
}

function GetCabangPerusahaan(IdPerusahaan) {
    $("#umkm-perusahaan-cabang").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanCabangs?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "35%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "20%", visible: true },
            { targets: [3], width: "10%", visible: true },
            { targets: [4], width: "10%", visible: true },
            { targets: [5], width: "10%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanCabang",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-success mr-2'> <i class='fa fa-eye' aria-hidden='true'></i> Show </button>" +
                        "<button class='btn btn-success mr-2'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                        "<button class='btn btn-danger mr-2'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Nama" },
            { data: "Alamat" },
            { data: "NoTelp" },
            { data: "NoFax" },
            { data: "CreatedBy" },
        ],
        bDestroy: true,
    });
}

const showDetail = (idPerusaahaan) => {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPerusahaan=" + idPerusaahaan,
        method: "GET",
        dataType: "JSON",
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                let idTipeValue = res.Data.IdTipe === 1 ? "PMA" : "PMDN";
                ShowForView();
                $("#UmkmPerusahaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                $(".modal-title").html("Form Show Detail Perusahaan Umkm");
                $("#setNib").html(`<strong> ${res.Data.Nib}</strong>`);
                $("#setNmPerusahaan").html(
                    `<strong> ${res.Data.NamaPerusahaan}</strong>`
                );
                $("#setIdTipe").html(`<strong> UMKM </strong>`);
                $("#setNameProv").html(
                    `<strong> ${res.Data.NamaProvinsi}</strong>`
                );
                $("#setNameKab").html(
                    `<strong> ${res.Data.NamaKabKot}</strong>`
                );
                $("#setAlamat").html(`<strong> ${res.Data.Alamat}</strong>`);
                $("#setCp").html(`<strong> ${res.Data.Cp}</strong>`);
                $("#setTlp").html(`<strong> ${res.Data.NoTelp}</strong>`);
                $("#setFax").html(`<strong> ${res.Data.NoFax}</strong>`);
                $("#setUrlWeb").html(`<strong> ${res.Data.UrlWeb}</strong>`);
                $("#setEmail").html(`<strong> ${res.Data.Email}</strong>`);
                $("#setNmPerusahaanEn").html(
                    `<strong> ${res.Data.NameTranslate}</strong>`
                );
                $("#setLat").html(`<strong> ${res.Data.Lat}</strong>`);
                $("#setLon").html(`<strong> ${res.Data.Lon}</strong>`);
                $("#setFileName").attr("src", res.Data.FileName);
                initialize(res.Data.Lat, res.Data.Lon);
            }
        },
    });
};

/* Perusahaan Cabang */
const UmkmCabangPerusahaanModalShow = () => {
    clearPerusahaanModal();
    $("#UmkmPerusahaanModal").modal({ backdrop: "static", keyboard: false });
    $(".modal-title").html("Form Add Perusahaan Cabang Umkm");
    showNoView();

    $("#setFileName").hide();

    initialize(0, 0);
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    $("#DivNib").hide();
    $("#DivJenisPerusahaan").hide();
    $("#btnSavePerusahaan").attr(
        "onClick",
        "SaveUmkmPerusahaan('TambahPerusahaanCabang')"
    );
};

/* Perusahaan KBLI */
const UmkmPerusahaanKbli = () => {
    $("#UmkmPerusahaanKbliModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetTahun(function (obj) {
        $("select#Tahun").html(obj);
    });

    $("#modal-title-perusahaan-kbli").html("Form Add Kbli Perusahaan");
};

/* Perusahaan Produk */
function ShowPerusahaanProduk(IdPerusahaanKbli) {
    $("#SectionPerusahaanProduk").show();
    $("#section-title-perusahaan-produk").html(
        `Daftar Produk <strong> ${NamaPerusahaanClicked}</strong>`
    );
    GetPerusahaanProduks(IdPerusahaanKbli);
}

function GetPerusahaanProduks(IdPerusahaanKbli) {
    $("#umkm-perusahaan-produk").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanProduk?IdPerusahaanKbli=" +
                IdPerusahaanKbli,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "10%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanProduk",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-info mr-2' data-tooltip='tooltip' data-placement='top' title='Show Produk' onClick='ShowPerusahaanProduk(" +
                        full.IdPerusahaanProduk +
                        ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>";
                    BtnEdit +=
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Nama" },
            { data: "Keterangan" },
        ],
        bDestroy: true,
    });
}

function UmkmPerusahaanProduk() {
    let IdPerusahaan = $("input[name='IdPerusahaanClicked']").val();
    $("#UmkmPerusahaanProdukModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    $("#modal-title-perusahaan-produks").html(`Add Perusahaan Produk`);
    ComboPerusahaanKbliIdPerusahaan(
        function (obj) {
            $("select#KbliPerusahaanProduk").html(obj);
        },
        IdPerusahaan,
        ""
    );
    $("#btnSavePerusahaanProduk").attr(
        "onClick",
        "SavePerusahaanProduk('Tambah');"
    );
}

function SavePerusahaanProduk(type) {
    let IdPerusahaanKbli = $("select[name='KbliPerusahaanProduk'] option")
        .filter(":selected")
        .val();
    let NamaPerusahaanProduk = $("input[name='NamaPerusahaanProduk']").val();
    let KeteranganPerusahaanProduk = $(
        "textarea[name='KeteranganPerusahaanProduk']"
    ).val();
    let FileNamePerusahaanProduk = $("#FileNamePerusahaanProduk")[0].files[0];
    let NmPerusahaanProdukEn = $("input[name='NmPerusahaanProdukEn']").val();
    let KeteranganPerusahaanProdukEn = $(
        "textarea[name='KeteranganPerusahaanProdukEn']"
    ).val();
    var data = new FormData();
    if (type === "Tambah") {
        url = base_url + "/Umkm/AddPerusahaanProduk";
        methods = "POST";

        data.append("IdPerusahaanKbli", IdPerusahaanKbli);
        data.append("NamaPerusahaanProduk", NamaPerusahaanProduk);
        data.append("KeteranganPerusahaanProduk", KeteranganPerusahaanProduk);
        data.append("FileNamePerusahaanProduk", FileNamePerusahaanProduk);
        data.append("NmPerusahaanProdukEn", NmPerusahaanProdukEn);
        data.append(
            "KeteranganPerusahaanProdukEn",
            KeteranganPerusahaanProdukEn
        );
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#UmkmPerusahaanProdukModal").modal("hide");
                        $("#umkm-perusahaan-produk").DataTable().ajax.reload();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan-produk").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

function ShowTranslatePerusahaanProduk() {
    $("#btnTranslatePerusahaanProduk").attr(
        "onClick",
        "HideTranslatePerusahaanProduk();"
    );
    $("#btnTranslatePerusahaanProduk").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Translate`
    );
    $("#ColumnTranslatePerusahaanProduk").show();
    $("#ColumnTranslatePerusahaanProduk").fadeIn(50000);
}

function HideTranslatePerusahaanProduk() {
    $("#btnTranslatePerusahaanProduk").attr(
        "onClick",
        "ShowTranslatePerusahaanProduk();"
    );
    $("#btnTranslatePerusahaanProduk").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Translate`
    );
    $("#ColumnTranslatePerusahaanProduk").hide();
}

$("select[name='Tahun']").change(function () {
    ComboGetKelompok(
        function (obj) {
            $("select#Kelompok").html(obj);
        },
        this.value,
        ""
    );
    /* ComboGetGolPokok(
        function (obj) {
            $("select#GolPokok").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#Golongan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolongan").html(obj);
        },
        this.value,
        0,
        ""
    );

    ComboGetGolPokok(
        function (obj) {
            $("select#Kelompok").html(obj);
        },
        this.value,
        0,
        ""
    ); */
});

/* $("select[name='Kategori']").change(function () {
    let Tahun = $("select[name='Tahun'] option").filter(":selected").val();
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokok").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
}); */

/* $("select[name='GolPokok']").change(function () {
    let Tahun = $("select[name='Tahun'] option").filter(":selected").val();
    ComboGetGolPokok(
        function (obj) {
            $("select#Golongan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
}); */

/* $("select[name='Golongan']").change(function () {
    let Tahun = $("select[name='Tahun'] option").filter(":selected").val();
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolongan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
}); */

/* $("select[name='SubGolongan']").change(function () {
    let Tahun = $("select[name='Tahun'] option").filter(":selected").val();
    ComboGetGolPokok(
        function (obj) {
            $("select#Kelompok").html(obj);
        },
        Tahun,
        this.value,
        ""
    );

    if (this.value != 0) {
        $("#DivSubGolongan").show();
        $("#btnSaveSubGolongan").attr(
            "onClick",
            "SavePerusahaanKbli('SubGolongan')"
        );
    } else {
        $("#DivSubGolongan").hide();
    }
}); */

$("select[name='Kelompok']").change(function () {
    if (this.value != 0) {
        $("#DivKelompok").show();
        $("#btnSaveKelompok").attr("onClick", "SavePerusahaanKbli('Kelompok')");
    } else {
        $("#DivKelompok").hide();
    }
});

function SavePerusahaanKbli(type) {
    let SubGolongan = $("select[name='SubGolongan'] option")
        .filter(":selected")
        .val();
    let Kelompok = $("select[name='Kelompok'] option")
        .filter(":selected")
        .val();
    var data = new FormData();
    let IdPerusahaan = $("input[name='IdPerusahaanClicked']").val();
    url = base_url + "/api/Umkm/AddPerusahaanKbli";
    methods = "POST";
    if (type === "SubGolongan") {
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("IdKbli", SubGolongan);
    } else {
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("IdKbli", Kelompok);
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#UmkmPerusahaanKbliModal").modal("hide");
                        $("#umkm-perusahaan-kbli").DataTable().ajax.reload();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan-kbli").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

function UmkmPerusahaanKebutuhanProdukKbli() {
    $("#UmkmPerusahaanKebutuhanProdukKbliModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetTahun(function (obj) {
        $("select#TahunProdukKebutuhan").html(obj);
    });

    $("#modal-title-perusahaan-kebutuhan-produk-kbli").html(
        "Form Add Kebutuhan Produk Perusahaan"
    );
    $("#btnSaveKebutuhanPerusahaanProduk").attr(
        "onClick",
        "SaveKebutuhanPerusaanProduk('Tambah');"
    );
}

function SaveKebutuhanPerusaanProduk(type) {
    let IdPerusahaan = $("input[name='IdPerusahaanClicked']").val();

    let SubGolongan = $("select[name='SubGolonganPerusahaanKebutuhan'] option")
        .filter(":selected")
        .val();
    let Kelompok = $("select[name='KelompokPerusahaanKebutuhan'] option")
        .filter(":selected")
        .val();

    let IdKbli;
    if (SubGolongan != 0 && Kelompok != 0) {
        IdKbli = Kelompok;
    } else if (SubGolongan != 0 && Kelompok == 0) {
        IdKbli = SubGolongan;
    }

    let NamaPerusahaanKebutuhanProduk = $(
        "input[name='NamaPerusahaanKebutuhanProduk']"
    ).val();
    let KeteranganPerusahaanKebutuhanProduk = $(
        "textarea[name='KeteranganPerusahaanKebutuhanProduk']"
    ).val();
    let NamaPerusahaanKebutuhanProdukEn = $(
        "input[name='NmKebutuhanPerusahaanProdukEn']"
    ).val();
    let KeteranganPerusahaanKebutuhanProdukEn = $(
        "textarea[name='KeteranganKebutuhanPerusahaanProdukEn']"
    ).val();
    var data = new FormData();
    if (IdKbli === undefined) {
        swal({
            title: "Konfirmasi",
            text: "Sub Golongan atau Kelompok belum dipilih",
            type: "error",
        });
    } else {
        if (type == "Tambah") {
            url = base_url + "/Umkm/AddPerusahaanKebutuhanProdukKbli";
            methods = "POST";
            data.append("IdPerusahaan", IdPerusahaan);
            data.append("IdKbli", IdKbli);
            data.append(
                "NamaPerusahaanKebutuhanProduk",
                NamaPerusahaanKebutuhanProduk
            );
            data.append(
                "KeteranganPerusahaanKebutuhanProduk",
                KeteranganPerusahaanKebutuhanProduk
            );
            data.append(
                "NamaPerusahaanKebutuhanProdukEn",
                NamaPerusahaanKebutuhanProdukEn
            );
            data.append(
                "KeteranganPerusahaanKebutuhanProdukEn",
                KeteranganPerusahaanKebutuhanProdukEn
            );
        }

        $.ajax({
            url: url,
            method: methods,
            dataType: "json",
            data: data,
            contentType: false,
            processData: false,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                LoadingBar("wait");
            },
            success: function (res) {
                LoadingBar("success");
                if (res.Status) {
                    swal(
                        {
                            title: "Konfirmasi",
                            text: res.Message,
                            type: "success",
                        },
                        function () {
                            $("#umkm-perusahaan-kebutuhan-produk-kbli")
                                .DataTable()
                                .ajax.reload();
                            $("#UmkmPerusahaanKebutuhanProdukKbliModal").modal(
                                "hide"
                            );
                        }
                    );
                } else {
                    swal(
                        {
                            title: "Konfirmasi",
                            text: res.Message,
                            type: "error",
                        },
                        function () {
                            $("#umkm-perusahaan-kebutuhan-produk-kbli")
                                .DataTable()
                                .ajax.reload();
                        }
                    );
                }
            },
        });
    }
}

$("select[name='TahunProdukKebutuhan']").change(function () {
    console.log(this.value);
    ComboGetKelompok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        ""
    );
    /* ComboGetKategoriKbli(
        function (obj) {
            $("select#KategoriPerusahaanKebutuhan").html(obj);
        },
        this.value,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );

    ComboGetGolPokok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    ); */
});

/* $("select[name='KategoriPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokokPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='GolPokokPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#GolonganPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='GolonganPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolonganPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='SubGolonganPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
}); */

function ShowTranslateKebutuhanProdukPerusahaan() {
    $("#btnTranslateKebutuhanProdukPerusahaan").attr(
        "onClick",
        "HideTranslateKebutuhanProdukPerusahaan();"
    );
    $("#btnTranslateKebutuhanProdukPerusahaan").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Translate`
    );
    $("#ColumnTranslatePerusahaanKebutuhanProduk").show();
    $("#ColumnTranslatePerusahaanKebutuhanProduk").fadeIn(50000);
}

function HideTranslateKebutuhanProdukPerusahaan() {
    $("#btnTranslateKebutuhanProdukPerusahaan").attr(
        "onClick",
        "ShowTranslateKebutuhanProdukPerusahaan();"
    );
    $("#btnTranslateKebutuhanProdukPerusahaan").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Translate`
    );
    $("#ColumnTranslatePerusahaanKebutuhanProduk").hide();
}
