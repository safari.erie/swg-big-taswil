/* Global Var */
var NamaPerusahaanClicked = "";

var addButtonUpload = ".btnAddFileUpload";

var fileWrapper = ".file_wrapper";
var incFileUpload = 1;
var aTable;
var tableTmpPerusahaanKBLI;

var arrSelectedIdKelompokKbliTmp = [];
var selectedYear = 0;
var tableUmkmPerusahaan;

$(document).ready(function () {
    if (permission !== null && permission["can_read"]) {
        tableUmkmPerusahaan = $("#umkm-perusahaan").DataTable({
            paging: true,
            searching: true,
            ordering: true,
            info: true,
            pageLength: 10,
            lengthChange: true,
            scrollX: true,
            processing: true,
            ajax: {
                url: base_url + "/api/Umkm/GetPerusahaans?Type=perusahaan",
                method: "GET",
                beforeSend: function (xhr) {},
                dataSrc: function (json) {
                    if (json.Data == null) {
                        swal({
                            title: "Gagal Menampilkan Data Perusahaan",
                            text: json.Message,
                            confirmButtonClass: "btn-danger text-white",
                            confirmButtonText: "Oke, Mengerti",
                            type: "error",
                        });
                        return json;
                    } else {
                        return json.Data;
                    }
                },
            },
            columnDefs: [
                { targets: [0], width: "25%", visible: true },
                { targets: [1], width: "10%", visible: true },
                { targets: [2], width: "30%", visible: true },
                { targets: [3], width: "10%", visible: true },
                { targets: [4], width: "5%", visible: true },
                { targets: [5], width: "5%", visible: true },
            ],
            columns: [
                {
                    className: "flex",
                    data: "IdUmkm",
                    render: function (data, type, full, meta) {
                        var NamePrs = '"' + full.NamaPerusahaan + '"';
                        var BtnEdit =
                            "<button class='btn btn-primary btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show Lokasi Kerja ' onClick='showCbgKbliPrd(" +
                            full.IdPerusahaan +
                            "," +
                            NamePrs +
                            ")'> <i class='fa fa-eye' aria-hidden='true'></i>   </button>" +
                            "<button class='btn btn-info btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show  Perusahaan' onClick='showDetail(" +
                            full.IdPerusahaan +
                            ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>";

                        if (permission["can_edit"]) {
                            BtnEdit +=
                                '<button class="btn btn-success btn-sm mr-2" data-tooltip="tooltip" data-placement="top" title="Edit Perusahaan"> <i class="fa fa-edit"></i>  </button>';
                        }
                        if (permission["can_approve"]) {
                            BtnEdit +=
                                '<button class="btn btn-warning btn-sm mr-2" data-tooltip="tooltip" data-placement="top" title="Approve Perusahaan"> <i class="fa fa-check"></i>  </button>';
                        }
                        if (permission["can_delete"]) {
                            BtnEdit +=
                                '<button class="btn btn-danger btn-sm" data-tooltip="tooltip" data-placement="top" title="Delete Perusahaan"> <i class="fa fa-trash"></i>  </button>';
                        }
                        data = BtnEdit;
                        return data;
                    },
                },
                {
                    data: "NamaPerusahaan",
                },
                { data: "Alamat" },
                { data: "Tipe" },
                {
                    data: "Status",
                    render: function (data, type, full, meta) {
                        var Data = "";
                        if (full.Status === DOCUMENT_CREATE) {
                            Data += `<span class="badge badge-secondary">Input Data</span>`;
                        } else if (full.Status === DOCUMENT_APPROVE) {
                            Data += `<span class="badge badge-success">Approve</span>`;
                        } else {
                            Data += `<span class="badge badge-pill badge-warning">Reject</span>`;
                        }

                        return Data;
                    },
                },
                {
                    data: "StatusVerifikasi",
                    render: function (data, type, full, meta) {
                        var Data = "";
                        if (full.StatusVerifikasi === DOCUMENT_NO_VERIFIKASI) {
                            Data += `<span class="badge badge-danger">Belum</span>`;
                        } else if (
                            full.StatusVerifikasi === DOCUMENT_VERIFIKASI
                        ) {
                            Data += `<span class="badge badge-info">Sudah</span>`;
                        }

                        return Data;
                    },
                },
            ],
            bDestroy: true,
        });
    } else {
        tableUmkmPerusahaan = $("#umkm-perusahaan").DataTable({
            language: {
                emptyTable: NO_READ_PERMISSION,
            },
        });
    }
});

const removeArrayItem = (arr, itemToRemove) => {
    return arr.filter((item) => item !== itemToRemove);
};

/* Reset Select2 after add row */
function initailizeSelect2() {
    $(".select2").select2({
        containerCssClass: "select2-container",
        width: "resolve",
        selectOnClose: true,
    });
}

/* Event Dynamic Multiple add Row File upload */
$(addButtonUpload).click(() => {
    console.log("");
    var dynamicFieldUpload = `<div class="row mb-2" >
                            <div class="col-md-3">
                                <select class="form-control select2" name="TipeUpload[]">
                                    <option value="0"> -- Pilih Salah Satu --</option>
                                    <option value="1"> Gambar </option>
                                    <option value="2"> Video </option>
                                    <option value="3"> Dokumen </option>
                                </select>
                            </div>
                             <div class="col-md-3">
                                <input type="text" placeholder="Input Judul Upload" class="form-control" name="Judul[]" />
                            </div>
                             <div class="col-md-3">
                                <input type="file" placeholder="Input Judul Upload" class="form-control" name="Judul[]" />

                            </div>
                            <button type="button" class="btn btn-danger btnRemoveFileUpload" > <i class="fa fa-minus" aria-hidden="true"></i> </button>
                        </div>`;
    incFileUpload++;
    console.log(incFileUpload);
    $(fileWrapper).append(dynamicFieldUpload);
    // Initialize select2
    initailizeSelect2();
});

function initializeModal() {
    $(".modal")
        .on("hidden.bs.modal", function (e) {
            if ($(".modal:visible").length) {
                $(".modal-backdrop")
                    .first()
                    .css(
                        "z-index",
                        parseInt($(".modal:visible").last().css("z-index")) - 10
                    );
                $("body").addClass("modal-open");
            }
        })
        .on("show.bs.modal", function (e) {
            if ($(".modal:visible").length) {
                $(".modal-backdrop.in")
                    .first()
                    .css(
                        "z-index",
                        parseInt($(".modal:visible").last().css("z-index")) + 10
                    );
                $(this).css(
                    "z-index",
                    parseInt($(".modal-backdrop.in").first().css("z-index")) +
                        10
                );
            }
        });
}

function ShowModalKbli() {
    $("#modal-title-perusahaan-kbli").html("Master KBLI");
    $("#PerusahaanModalAddKbli").modal({ backdrop: "static", keyboard: false });
    initializeModal();
    ComboGetTahun(function (obj) {
        $("select#TahunKbli").html(obj);
    }, selectedYear);
    tableTmpPerusahaanKBLI = $("#TableTmpAddPerusahaanKbli").DataTable({
        columns: [
            { data: 0 },
            { data: 1, visible: false },
            { data: 2 },
            { data: 3 },
        ],
        destroy: true,
    });
}

$("#table_master_kbli tbody").on("click", "a", function () {
    //var data = aTable.row($(this).parents("tr")).data();

    var data = aTable.row($(this).parents("tr")).data();
    var rows = aTable.row($(this).parents("tr"));
    //rows.remove().draw();

    let HtmlDelete = "delete";
    console.log(arrSelectedIdKelompokKbliTmp.includes(data.id_kbli_kelompok));
    if (arrSelectedIdKelompokKbliTmp.includes(data.id_kbli_kelompok)) {
        iziToast.error({
            title: "Kelompok KBLI",
            message: "Kelompok Sudah Dipilih",
            position: "topCenter",
        });
    } else {
        arrSelectedIdKelompokKbliTmp.push(data.id_kbli_kelompok);
        tableTmpPerusahaanKBLI.row
            .add([
                '<a href="javascript:void(0)" data-animation="superscaled" data-overlaySpeed="100" data-overlayColor="#36404a" data-plugin="custommodal" class="btn btn-danger btn-rounded w-md waves-effect waves-light m-b-5" > <i class="fas fa-trash"></i> Hapus</a>',
                data.id_kbli_kelompok,
                data.kode_kelompok,
                data.nama_kelompok,
            ])
            .nodes();
        tableTmpPerusahaanKBLI.draw(false);
    }
});

$("#TableTmpAddPerusahaanKbli tbody").on("click", "a", function () {
    var rows = tableTmpPerusahaanKBLI.row($(this).parents("tr"));
    var data = tableTmpPerusahaanKBLI.row($(this).parents("tr")).data();

    arrSelectedIdKelompokKbliTmp = removeArrayItem(
        arrSelectedIdKelompokKbliTmp,
        data[1]
    );

    rows.remove().draw();
    console.log(arrSelectedIdKelompokKbliTmp);
});

function PerusahaanModalHideKbli() {
    $("#PerusahaanModalAddKbli").modal("hide");
    $("#UmkmPerusahaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    initializeModal();
}

$("select[name='TahunKbli']").change(function () {
    selectedYear = this.value;
    GetMasterKblis(this.value);
});

function GetMasterKblis(tahun) {
    console.log(tahun);
    aTable = $("#table_master_kbli").DataTable({
        paging: true,
        pageLength: 5,
        searching: true,
        ordering: true,
        info: true,
        ajax: {
            url: base_url + "/api/Umkm/GetKelompokKbliParents?Tahun=" + tahun,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columns: [
            {
                render: function (data, type, full, meta) {
                    return (
                        '<a href="javascript:void(0)" class="btn btn-primary" > <i class="fa fa-plus" aria-hidden="true"> </i> ' +
                        full.id_kbli_kelompok +
                        "  </a>"
                    );
                },
            },
            { data: "kode_kategori", name: "kode_kategori", autoWidth: true },
            { data: "nama_kategori", name: "nama_kategori", autoWidth: true },
            { data: "kode_golpok", name: "kode_golpok", autoWidth: true },
            { data: "nama_golpok", name: "nama_golpok", autoWidth: true },
            { data: "kode_golongan", name: "kode_golongan", autoWidth: true },
            { data: "nama_golongan", name: "nama_golongan", autoWidth: true },
            {
                data: "kode_subgolongan",
                name: "kode_subgolongan",
                autoWidth: true,
            },
            {
                data: "nama_subgolongan",
                name: "nama_subgolongan",
                autoWidth: true,
            },
            {
                data: "nama_kelompok",
                name: "nama_kelompok",
                autoWidth: true,
            },
        ],
        lengthChange: false,
        dom: 'l<"toolbar">frtip',
        destroy: true,
    });
}

// Remove button upload is clicked
$(fileWrapper).on("click", ".btnRemoveFileUpload", function (e) {
    e.preventDefault();

    $(this).parent("div").remove(); //Remove field html
    incFileUpload--; //Decrement field counter
});

function addRowUpload(i) {}

function clearPerusahaanModal() {
    $("input[name='Nib']").val("");
    $("input[name='NmPerusahaan']").val("");
    $("input[name='Cp']").val("");
    $("input[name='NoTlp']").val("");
    $("input[name='NoFax']").val("");
    $("input[name='UrlWeb']").val("");
    $("input[name='Email']").val("");
    $("input[name='Lat']").val("");
    $("input[name='Lon']").val("");
    $("input[name='NmPerusahaanEn']").val("");
    $("#FileName").val("");
    $("textarea[name='Alamat']").val("");

    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        0,
        ""
    );
    $("input[name=IdTipe][value=" + 1 + "]").prop("checked", false);
    $("input[name=IdTipe][value=" + 2 + "]").prop("checked", false);
}

/* onclick Add  */
function UmkmPerusahaanModalShow() {
    clearPerusahaanModal();

    $("#UmkmPerusahaanModal").modal({ backdrop: "static", keyboard: false });
    $(".modal-title").html("Form Add Perusahaan ");
    showNoView();
    $("#DivNib").show();
    $("#DivNama").show();
    $("#DivJenisPerusahaan").show();
    $("#setFileName").hide();

    //initialize(0, 0);

    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    $("#btnSavePerusahaan").attr("onClick", "SaveUmkmPerusahaan('Tambah')");
}

const ShowEdit = (idPerusahaan) => {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPerusahaan=" + idPerusahaan,
        method: "GET",
        dataType: "JSON",
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                let idTipeValue =
                    res.Data.IdTipe === 1
                        ? $(
                              "input[name=IdTipe][value=" +
                                  res.Data.IdTipe +
                                  "]"
                          ).prop("checked", true)
                        : $(
                              "input[name=IdTipe][value=" +
                                  res.Data.IdTipe +
                                  "]"
                          ).prop("checked", true);

                showNoView();
                $("#DivNib").show();
                $("#DivJenisPerusahaan").show();
                $(".modal-title").html(
                    `Edit Perusahaan <strong> ${res.Data.NamaPerusahaan}</strong`
                );
                $("#UmkmPerusahaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });

                $("input[name='IdPerusahaan']").val(
                    parseInt(res.Data.IdPerusahaan)
                );
                $("input[name='Nib']").val(parseInt(res.Data.Nib));
                $("input[name='NmPerusahaan']").val(res.Data.NamaPerusahaan);

                $("input[name='NmPerusahaan']").val(res.Data.NamaPerusahaan);
                ComboGetWilayahProvinsi(function (obj) {
                    $("select#IdAdmProv").html(obj);
                }, res.Data.IdAdmProvinsi);
                ComboGetWilayahKabKota(
                    function (HtmlCombo) {
                        $("select[name='IdAdmKabKot']").empty();
                        $("select[name='IdAdmKabKot']").append(HtmlCombo);
                    },
                    res.Data.IdAdmProvinsi,
                    res.Data.IdAdmKabkot
                );
                $("textarea[name='Alamat']").val(res.Data.Alamat);
                $("input[name='Cp']").val(res.Data.Cp);
                $("input[name='NoTlp']").val(res.Data.NoTelp);
                $("input[name='NoFax']").val(res.Data.NoFax);
                $("input[name='UrlWeb']").val(res.Data.UrlWeb);
                $("input[name='Email']").val(res.Data.Email);
                $("input[name='Lat']").val(res.Data.Lat);
                $("input[name='Lon']").val(res.Data.Lon);
                $("input[name='NmPerusahaanEn']").val(res.Data.NameTranslate);
                $("#setFileName").attr("src", res.Data.FileName);
                $("#btnSavePerusahaan").attr(
                    "onClick",
                    "SaveUmkmPerusahaan('EditPerusahaan')"
                );
                initialize(res.Data.Lat, res.Data.Lon);
            }
        },
    });
};

$("select[name='IdAdmProv']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});

function DeletePerusahaan(IdPerusahaan) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaan",
                    data: { IdPerusahaan: IdPerusahaan },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                    $("#perusahaanModal").modal("hide");
                                }
                            );
                        } else {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "error",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        }
                    },
                });
            }
        }
    );
}

function SaveUmkmPerusahaan(type) {
    let emptyStr = "-";
    var data = new FormData();
    let Nib = $("input[name='Nib']").val();
    let NmPerusahaan = $("input[name='NmPerusahaan']").val();
    let Alamat = $("textarea[name='Alamat']").val();
    let NoTlp = $("input[name='NoTlp']").val();
    let IdTipe = $('input[name="IdTipe"]:checked').val();
    let NoFax = $("input[name='NoFax']").val();
    let UrlWeb = $("input[name='UrlWeb']").val();
    let Email = $("input[name='Email']").val();
    let Cp = $("input[name='Cp']").val();
    let NmPerusahaanEn = $("input[name='NmPerusahaanEn']").val();
    //let FileName = $("#FileName")[0].files[0];
    let Lat = $("input[name='Lat']").val();
    let Lon = $("input[name='Lon']").val();

    let IdAdmKabKot = $("select[name='IdAdmKabKot'] option")
        .filter(":selected")
        .val();

    if (type === "Tambah") {
        url = base_url + "/Umkm/AddPerusahaanUmkm";
        methods = "POST";

        data.append("Nib", Nib);
        data.append("IdTipe", IdTipe);
        data.append("IdAdmKabKot", IdAdmKabKot);
        data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("ArrKbliSelected", arrSelectedIdKelompokKbliTmp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        // data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    } else if (type === "EditPerusahaan") {
        let IdPerusahaan = $("input[name='IdPerusahaan']").val();
        url = base_url + "/Umkm/EditPerusahaanUmkm";
        methods = "POST";
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("Nib", Nib);
        data.append("IdTipe", IdTipe);
        data.append("IdAdmKabKot", IdAdmKabKot);
        data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    } else if (type === "TambahPerusahaanCabang") {
        let IdPerusahaanClicked = $("input[name='IdPerusahaanClicked']").val();
        url = base_url + "/Umkm/AddPerusahaanCabangUmkm";
        methods = "POST";
        data.append("IdPerusahaan", IdPerusahaanClicked);
        data.append("IdAdmKabKot", IdAdmKabKot);
        //data.append("NamaPerusahaan", NmPerusahaan);
        data.append("Alamat", Alamat);
        data.append("NoTlp", NoTlp);
        data.append("NoFax", NoFax);
        data.append("UrlWeb", UrlWeb);
        data.append("Email", Email);
        data.append("Cp", Cp);
        data.append("FileName", FileName);
        data.append("Lat", Lat);
        data.append("Lon", Lon);
        data.append("NmPerusahaanEn", NmPerusahaanEn);
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        if (type === "Tambah" || type === "EditPerusahaan") {
                            $("#UmkmPerusahaanModal").modal("hide");
                            $("#umkm-perusahaan").DataTable().ajax.reload();
                        } else {
                            $("#UmkmPerusahaanModal").modal("hide");
                            $("#umkm-perusahaan-cabang")
                                .DataTable()
                                .ajax.reload();
                        }
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

const ShowTranslate = () => {
    console.log("trans");
    $("#btnTranslate").attr("onClick", "HideTransalte();");
    $("#btnTranslate").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTranslate").attr("onClick", "ShowTranslate();");
    $("#btnTranslate").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};

function showNoView() {
    $("#Nib").show();
    $("#NmPerusahaan").show();
    $(".rdIdTipe").show();
    $("#cmbProv").show();
    $("#cmbKabkot").show();
    $("#Alamat").show();
    $("#Cp").show();
    $("#NoTlp").show();
    $("#NoFax").show();
    $("#UrlWeb").show();
    $("#Email").show();
    $("#divBtnTranslate").show();
    $("#FileName").show();
    $("#NmPerusahaanEn").show();
    $("#x").show();
    $("#y").show();

    $("#setNib").hide();
    $("#setNmPerusahaan").hide();
    $("#setIdTipe").hide();
    $("#divCmbProv").hide();
    $("#divcmbKabkot").hide();
    $("#setAlamat").hide();
    $("#setCp").hide();
    $("#setTlp").hide();
    $("#setFax").hide();
    $("#setUrlWeb").hide();
    $("#setEmail").hide();
    $("#setLat").hide();
    $("#setLon").hide();
    $("#ColumnTransalte").hide();
    $("#setNmPerusahaanEn").hide();
}

function ShowForView() {
    $("#Nib").hide();
    $("#NmPerusahaan").hide();
    $(".rdIdTipe").hide();
    $("#cmbProv").hide();
    $("#cmbKabkot").hide();
    $("#Alamat").hide();
    $("#Cp").hide();
    $("#NoTlp").hide();
    $("#NoFax").hide();
    $("#UrlWeb").hide();
    $("#Email").hide();
    $("#divBtnTranslate").hide();
    $("#FileName").hide();
    $("#NmPerusahaanEn").hide();
    $("#x").hide();
    $("#y").hide();

    $("#setNib").show();
    $("#setNmPerusahaan").show();
    $("#setIdTipe").show();
    $("#divCmbProv").show();
    $("#divcmbKabkot").show();
    $("#setAlamat").show();
    $("#setCp").show();
    $("#setTlp").show();
    $("#setFax").show();
    $("#setUrlWeb").show();
    $("#setEmail").show();
    $("#setLat").show();
    $("#setLon").show();
    $("#ColumnTransalte").show();
    $("#setNmPerusahaanEn").show();
}

/* All action from datatables */
const showCbgKbliPrd = (idPerusaahaan, NamaPerusahaan) => {
    NamaPerusahaanClicked = NamaPerusahaan;
    $("#SectionPerusahaanProduk").hide();
    $("#SectionCabang").show();
    //$("#SectionPerusahaanKbli").show();
    //$("#SectionPerusahaanKebutuhanProdukKbli").show();
    $("#section-title-cabang").html(
        `Daftar Lokasi Kerja <strong> ${NamaPerusahaan}</strong>`
    );
    $("#section-title--perusahaan-kbli").html(
        `Daftar KBLI <strong> ${NamaPerusahaan}</strong>`
    );
    $("#section-title-perusahaan-kebutuhan-produk-kbli").html(
        `Daftar Kebutuhan Produk KBLI <strong> ${NamaPerusahaan}</strong>`
    );
    $("#setFileName").show();
    GetCabangPerusahaan(idPerusaahaan);
    GetPerusahaanKbli(idPerusaahaan);
    // GetPerusahaanKebutuhanProdukKbli(idPerusaahaan);
    $("#IdPerusahaanClicked").val(idPerusaahaan);
};

const showLokasiKerjaKebutuhanPrdk = (idPerusahaanLokasiKerja) => {
    console.log(idPerusahaanLokasiKerja);
    $("#IdPerusahaanLokasiClicked").val(idPerusahaanLokasiKerja);
    $("#SectionPerusahaanKebutuhanProdukKbli").show();
    GetPerusahaanKebutuhanProdukKbli(idPerusahaanLokasiKerja);
};

function GetPerusahaanKebutuhanProdukKbli(IdPerusahaan) {
    $("#umkm-perusahaan-kebutuhan-produk-kbli").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanKebutuhanProdukKblis?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "15%", visible: true },
            { targets: [1], width: "20%", visible: true },
            { targets: [2], width: "40%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanKebutuhanKbli",
                data: "IdPerusahaan",
                data: "IdPerusahaanKebutuhanProduk",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-success mr-2' data-tooltip='tooltip' data-placement='top' title='Edit'> <i class='fa fa-edit' aria-hidden='true'></i>  </button>" +
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            /*  { data: "Tahun" },
            { data: "Kode" },
            { data: "NamaKbli" }, */
            { data: "NamaKebutuhanProduk" },
            { data: "KeteranganKebutuhanProduk" },
        ],
        bDestroy: true,
    });
}

function GetPerusahaanKbli(IdPerusahaan) {
    $("#umkm-perusahaan-kbli").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanKblis?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "10%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
            { targets: [3], width: "15%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanKbli",
                render: function (data, type, full, meta) {
                    /*  var BtnEdit =
                        "<button class='btn btn-info mr-2' data-tooltip='tooltip' data-placement='top' title='Show Produk' onClick='ShowPerusahaanProduk(" +
                        full.IdPerusahaanKbli +
                        ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>"; */
                    var BtnEdit =
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Tahun" },
            { data: "Kode" },
            { data: "Nama" },
        ],
        bDestroy: true,
    });
}

function GetCabangPerusahaan(IdPerusahaan) {
    $("#umkm-perusahaan-cabang").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanCabangs?IdPerusahaan=" +
                IdPerusahaan,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data Lokasi Kerja Perusahaan",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "20%", visible: true },
            { targets: [1], width: "30%", visible: true },
            { targets: [2], width: "10%", visible: true },
            { targets: [3], width: "10%", visible: true },
            { targets: [4], width: "10%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanCabang",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-primary btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show Kebutuhan Produk KBLI' onClick='showLokasiKerjaKebutuhanPrdk(" +
                        full.IdPerusahaanCabang +
                        ")'  > <i class='fa fa-eye' aria-hidden='true'></i>  </button>" +
                        "<button class='btn btn-info btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Show Perusahaan Lokasi Kerja' > <i class='fa fa-eye' aria-hidden='true'></i>  </button>" +
                        "<button class='btn btn-success btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Edit Perusahaan Lokasi Kerja'> <i class='fa fa-edit' aria-hidden='true'></i>  </button>" +
                        "<button class='btn btn-danger btn-sm mr-2' data-tooltip='tooltip' data-placement='top' title='Delete Perusahaan Lokasi Kerja'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Alamat" },
            { data: "NoTelp" },
            { data: "NoFax" },
            { data: "CreatedBy" },
        ],
        bDestroy: true,
    });
}

const showDetail = (idPerusaahaan) => {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPerusahaan=" + idPerusaahaan,
        method: "GET",
        dataType: "JSON",
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                let idTipeValue = res.Data.IdTipe === 1 ? "PMA" : "PMDN";
                ShowForView();
                $("#UmkmPerusahaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                $(".modal-title").html("Form Show Detail Perusahaan Umkm");
                $("#setNib").html(`<strong> ${res.Data.Nib}</strong>`);
                $("#setNmPerusahaan").html(
                    `<strong> ${res.Data.NamaPerusahaan}</strong>`
                );
                $("#setIdTipe").html(`<strong> ${idTipeValue}</strong>`);
                $("#setNameProv").html(
                    `<strong> ${res.Data.NamaProvinsi}</strong>`
                );
                $("#setNameKab").html(
                    `<strong> ${res.Data.NamaKabKot}</strong>`
                );
                $("#setAlamat").html(`<strong> ${res.Data.Alamat}</strong>`);
                $("#setCp").html(`<strong> ${res.Data.Cp}</strong>`);
                $("#setTlp").html(`<strong> ${res.Data.NoTelp}</strong>`);
                $("#setFax").html(`<strong> ${res.Data.NoFax}</strong>`);
                $("#setUrlWeb").html(`<strong> ${res.Data.UrlWeb}</strong>`);
                $("#setEmail").html(`<strong> ${res.Data.Email}</strong>`);
                $("#setNmPerusahaanEn").html(
                    `<strong> ${res.Data.NameTranslate}</strong>`
                );
                $("#setLat").html(`<strong> ${res.Data.Lat}</strong>`);
                $("#setLon").html(`<strong> ${res.Data.Lon}</strong>`);
                $("#setFileName").attr("src", res.Data.FileName);
                initialize(res.Data.Lat, res.Data.Lon);
            }
        },
    });
};

/* Perusahaan Cabang */
const UmkmCabangPerusahaanModalShow = () => {
    clearPerusahaanModal();

    $("#UmkmPerusahaanModal").modal({ backdrop: "static", keyboard: false });
    $(".modal-title").html("Form Add Lokasi Kerja Perusahaan");
    showNoView();

    $("#setFileName").hide();

    //initialize(0, 0);
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    $("#DivNib").hide();
    $("#DivNama").hide();
    $("#DivJenisPerusahaan").hide();
    $("#btnSavePerusahaan").attr(
        "onClick",
        "SaveUmkmPerusahaan('TambahPerusahaanCabang')"
    );
};

/* Perusahaan KBLI */
const UmkmPerusahaanKbli = () => {
    $("#UmkmPerusahaanKbliModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetTahun(function (obj) {
        $("select#Tahun").html(obj);
    });

    $("#modal-title-perusahaan-kbli").html("Form Add Kbli Perusahaan");
};

/* Perusahaan Produk */
function ShowPerusahaanProduk(IdPerusahaanKbli) {
    $("#SectionPerusahaanProduk").show();
    $("#section-title-perusahaan-produk").html(
        `Daftar Produk <strong> ${NamaPerusahaanClicked}</strong>`
    );
    GetPerusahaanProduks(IdPerusahaanKbli);
}

function GetPerusahaanProduks(IdPerusahaanKbli) {
    $("#umkm-perusahaan-produk").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url:
                base_url +
                "/api/Umkm/GetPerusahaanProduk?IdPerusahaanKbli=" +
                IdPerusahaanKbli,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "10%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
        ],
        columns: [
            {
                data: "IdPerusahaanProduk",
                render: function (data, type, full, meta) {
                    /* var BtnEdit =
                        "<button class='btn btn-info mr-2' data-tooltip='tooltip' data-placement='top' title='Show Produk' onClick='ShowPerusahaanProduk(" +
                        full.IdPerusahaanProduk +
                        ")'> <i class='fa fa-eye' aria-hidden='true'></i>  </button>"; */
                    var BtnEdit =
                        "<button class='btn btn-danger mr-2' data-tooltip='tooltip' data-placement='top' title='Hapus'> <i class='fa fa-trash' aria-hidden='true'></i>  </button>";
                    data = BtnEdit;
                    return data;
                },
            },
            { data: "Nama" },
            { data: "Keterangan" },
        ],
        bDestroy: true,
    });
}

function UmkmPerusahaanProduk() {
    let IdPerusahaan = $("input[name='IdPerusahaanClicked']").val();
    $("#UmkmPerusahaanProdukModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    $("#modal-title-perusahaan-produks").html(`Add Perusahaan Produk`);
    ComboPerusahaanKbliIdPerusahaan(
        function (obj) {
            $("select#KbliPerusahaanProduk").html(obj);
        },
        IdPerusahaan,
        ""
    );
    $("#btnSavePerusahaanProduk").attr(
        "onClick",
        "SavePerusahaanProduk('Tambah');"
    );
}

function SavePerusahaanProduk(type) {
    let IdPerusahaanKbli = $("select[name='KbliPerusahaanProduk'] option")
        .filter(":selected")
        .val();
    let NamaPerusahaanProduk = $("input[name='NamaPerusahaanProduk']").val();
    let KeteranganPerusahaanProduk = $(
        "textarea[name='KeteranganPerusahaanProduk']"
    ).val();
    let FileNamePerusahaanProduk = $("#FileNamePerusahaanProduk")[0].files[0];
    let NmPerusahaanProdukEn = $("input[name='NmPerusahaanProdukEn']").val();
    let KeteranganPerusahaanProdukEn = $(
        "textarea[name='KeteranganPerusahaanProdukEn']"
    ).val();
    var data = new FormData();
    if (type === "Tambah") {
        url = base_url + "/Umkm/AddPerusahaanProduk";
        methods = "POST";

        data.append("IdPerusahaanKbli", IdPerusahaanKbli);
        data.append("NamaPerusahaanProduk", NamaPerusahaanProduk);
        data.append("KeteranganPerusahaanProduk", KeteranganPerusahaanProduk);
        data.append("FileNamePerusahaanProduk", FileNamePerusahaanProduk);
        data.append("NmPerusahaanProdukEn", NmPerusahaanProdukEn);
        data.append(
            "KeteranganPerusahaanProdukEn",
            KeteranganPerusahaanProdukEn
        );
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#UmkmPerusahaanProdukModal").modal("hide");
                        $("#umkm-perusahaan-produk").DataTable().ajax.reload();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan-produk").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

function ShowTranslatePerusahaanProduk() {
    $("#btnTranslatePerusahaanProduk").attr(
        "onClick",
        "HideTranslatePerusahaanProduk();"
    );
    $("#btnTranslatePerusahaanProduk").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Translate`
    );
    $("#ColumnTranslatePerusahaanProduk").show();
    $("#ColumnTranslatePerusahaanProduk").fadeIn(50000);
}

function HideTranslatePerusahaanProduk() {
    $("#btnTranslatePerusahaanProduk").attr(
        "onClick",
        "ShowTranslatePerusahaanProduk();"
    );
    $("#btnTranslatePerusahaanProduk").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Translate`
    );
    $("#ColumnTranslatePerusahaanProduk").hide();
}

$("select[name='Tahun']").change(function () {
    /* ComboGetKategoriKbli(
        function (obj) {
            $("select#Kategori").html(obj);
        },
        this.value,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokok").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#Golongan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolongan").html(obj);
        },
        this.value,
        0,
        ""
    );

    ComboGetGolPokok(
        function (obj) {
            $("select#Kelompok").html(obj);
        },
        this.value,
        0,
        ""
    ); */
    ComboGetKelompok(
        function (obj) {
            $("select#Kelompok").html(obj);
        },
        this.value,
        ""
    );
});

$("select[name='Kelompok']").change(function () {
    if (this.value != 0) {
        $("#DivKelompok").show();
        $("#btnSaveKelompok").attr("onClick", "SavePerusahaanKbli('Kelompok')");
    } else {
        $("#DivKelompok").hide();
    }
});

function SavePerusahaanKbli(type) {
    let SubGolongan = $("select[name='SubGolongan'] option")
        .filter(":selected")
        .val();
    let Kelompok = $("select[name='Kelompok'] option")
        .filter(":selected")
        .val();
    var data = new FormData();
    let IdPerusahaan = $("input[name='IdPerusahaanClicked']").val();
    url = base_url + "/api/Umkm/AddPerusahaanKbli";
    methods = "POST";
    if (type === "SubGolongan") {
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("IdKbli", SubGolongan);
    } else {
        data.append("IdPerusahaan", IdPerusahaan);
        data.append("IdKbli", Kelompok);
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#UmkmPerusahaanKbliModal").modal("hide");
                        $("#umkm-perusahaan-kbli").DataTable().ajax.reload();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan-kbli").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}

function UmkmPerusahaanKebutuhanProdukKbli() {
    $("#UmkmPerusahaanKebutuhanProdukKbliModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetTahun(function (obj) {
        $("select#TahunProdukKebutuhan").html(obj);
    });

    $("#modal-title-perusahaan-kebutuhan-produk-kbli").html(
        "Form Add Kebutuhan Produk Perusahaan"
    );
    $("#btnSaveKebutuhanPerusahaanProduk").attr(
        "onClick",
        "SaveKebutuhanPerusaanProduk('Tambah');"
    );
}

function SaveKebutuhanPerusaanProduk(type) {
    let IdPerusahaanLokasi = $("input[name='IdPerusahaanLokasiClicked']").val();

    /* let SubGolongan = $("select[name='SubGolonganPerusahaanKebutuhan'] option")
        .filter(":selected")
        .val();
    let Kelompok = $("select[name='KelompokPerusahaanKebutuhan'] option")
        .filter(":selected")
        .val();

    let IdKbli;
    if (SubGolongan != 0 && Kelompok != 0) {
        IdKbli = Kelompok;
    } else if (SubGolongan != 0 && Kelompok == 0) {
        IdKbli = SubGolongan;
    } */

    let NamaPerusahaanKebutuhanProduk = $(
        "input[name='NamaPerusahaanKebutuhanProduk']"
    ).val();
    let KeteranganPerusahaanKebutuhanProduk = $(
        "textarea[name='KeteranganPerusahaanKebutuhanProduk']"
    ).val();
    let NamaPerusahaanKebutuhanProdukEn = $(
        "input[name='NmKebutuhanPerusahaanProdukEn']"
    ).val();
    let KeteranganPerusahaanKebutuhanProdukEn = $(
        "textarea[name='KeteranganKebutuhanPerusahaanProdukEn']"
    ).val();
    var data = new FormData();
    /* if (IdKbli === undefined) {
        swal({
            title: "Konfirmasi",
            text: "Sub Golongan atau Kelompok belum dipilih",
            type: "error",
        });
    } else { */
    if (type == "Tambah") {
        url = base_url + "/api/Umkm/AddPerusahaanKebutuhanProdukKbli";
        methods = "POST";
        data.append("IdPerusahaanLokasi", IdPerusahaanLokasi);
        data.append(
            "NamaPerusahaanKebutuhanProduk",
            NamaPerusahaanKebutuhanProduk
        );
        data.append(
            "KeteranganPerusahaanKebutuhanProduk",
            KeteranganPerusahaanKebutuhanProduk
        );
        data.append(
            "NamaPerusahaanKebutuhanProdukEn",
            NamaPerusahaanKebutuhanProdukEn
        );
        data.append(
            "KeteranganPerusahaanKebutuhanProdukEn",
            KeteranganPerusahaanKebutuhanProdukEn
        );
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#umkm-perusahaan-kebutuhan-produk-kbli")
                            .DataTable()
                            .ajax.reload();
                        $("#UmkmPerusahaanKebutuhanProdukKbliModal").modal(
                            "hide"
                        );
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#umkm-perusahaan-kebutuhan-produk-kbli")
                            .DataTable()
                            .ajax.reload();
                    }
                );
            }
        },
    });
}

/* $("select[name='TahunProdukKebutuhan']").change(function () {
    console.log(this.value);
    ComboGetKategoriKbli(
        function (obj) {
            $("select#KategoriPerusahaanKebutuhan").html(obj);
        },
        this.value,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );

    ComboGetGolPokok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
}); */

$("select[name='TahunProdukKebutuhan']").change(function () {
    console.log(this.value);
    ComboGetKelompok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        ""
    );
    /* ComboGetKategoriKbli(
        function (obj) {
            $("select#KategoriPerusahaanKebutuhan").html(obj);
        },
        this.value,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#GolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolonganPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    );

    ComboGetGolPokok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        this.value,
        0,
        ""
    ); */
});

$("select[name='KategoriPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#GolPokokPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='GolPokokPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#GolonganPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='GolonganPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#SubGolonganPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

$("select[name='SubGolonganPerusahaanKebutuhan']").change(function () {
    let Tahun = $("select[name='TahunProdukKebutuhan'] option")
        .filter(":selected")
        .val();
    ComboGetGolPokok(
        function (obj) {
            $("select#KelompokPerusahaanKebutuhan").html(obj);
        },
        Tahun,
        this.value,
        ""
    );
});

function ShowTranslateKebutuhanProdukPerusahaan() {
    $("#btnTranslateKebutuhanProdukPerusahaan").attr(
        "onClick",
        "HideTranslateKebutuhanProdukPerusahaan();"
    );
    $("#btnTranslateKebutuhanProdukPerusahaan").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Translate`
    );
    $("#ColumnTranslatePerusahaanKebutuhanProduk").show();
    $("#ColumnTranslatePerusahaanKebutuhanProduk").fadeIn(50000);
}

function HideTranslateKebutuhanProdukPerusahaan() {
    $("#btnTranslateKebutuhanProdukPerusahaan").attr(
        "onClick",
        "ShowTranslateKebutuhanProdukPerusahaan();"
    );
    $("#btnTranslateKebutuhanProdukPerusahaan").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Translate`
    );
    $("#ColumnTranslatePerusahaanKebutuhanProduk").hide();
}
