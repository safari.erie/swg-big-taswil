ComboGetWilayahProvinsi(function (obj) {
    $("select#FilterProvinsi").html(obj);
});

$("select[name='FilterProvinsi']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='FilterKabKot']").empty();
            $("select[name='FilterKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
