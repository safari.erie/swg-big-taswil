var dataSet = [
    {
        NamaProvinsi: "Aceh",
        Tahun: 2009,
        SumberData: "Aceh Besar Dalam Angka ",
        KepadatanPenduduk: 10,
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Medan Star Industrial Estate",
        Keterangan: "-",
        Alamat: "Jl. Raya Medan-Lubuk Pakam, Tj. Morawa B, Tj. Morawa, Kabupaten Deli Serdang, Sumatera Utara",
        JumlahPria: "2071",
        Pct: "30%",
        LajuPertumbuhan: 20,
        Sektor: "-",
        Status: "2",
    },
    {
        NamaProvinsi: "Sumatera Utara",
        Tahun: 2010,
        SumberData: "Aceh Besar Dalam Angka",
        KepadatanPenduduk: 23,
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Kawasan Industrial Medan",
        Keterangan: "-",
        Alamat: "Wisma Kawasan Industri Medan, Jalan Pulau Batam No.1 Kompleks KIM Tahap 2, Medan, Sumatera Utara",
        JumlahPria: "960",
        Pct: "42%",
        LajuPertumbuhan: 20,
        Sektor: "-",
        Status: "2",
    },
    {
        NamaProvinsi: "Sumatera Barat",
        Tahun: 2004,
        SumberData:
            "Kawasan Industri - Indonesia industrial estate directory 2015/2016",
        KepadatanPenduduk: 650,
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Padang Industrial Park",
        Keterangan: "-",
        Alamat: "Jl. H. Agus Salim No.17, Sawahan Tim., Padang Tim., Kota Padang, Sumatera Barat",
        JumlahPria: "616",
        Pct: "62%",
        LajuPertumbuhan: 20,
        Sektor: "-",
        Status: "1",
    },
];
var TableDemografi = $("#pdrb-provinsi").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "20%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "15%", visible: true },
        { targets: [6], width: "15%", visible: true },
        { targets: [7], width: "15%", visible: true },
        { targets: [7], width: "15%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus &nbsp;&nbsp;</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "NamaProvinsi" },
        { data: "Tahun" },
        { data: "SumberData" },
        { data: "Sektor" },
        { data: "Pct" },
        { data: "KepadatanPenduduk" },
        { data: "LajuPertumbuhan" },
        {
            data: "Status",
            render: function (data, type, full, meta) {
                let strStatus = "";
                if (full.Status === "2") {
                    strStatus += `<span class="badge badge-success"> Approved</span>`;
                } else {
                    strStatus += `<span class="badge badge-warning"> Pending</span>`;
                }

                return strStatus;
            },
        },
    ],
});

const ProfilDaerahProvPdrbModalShow = () => {
    $("#ModalPdrbProv").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};
