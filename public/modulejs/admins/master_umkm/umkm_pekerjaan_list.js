var TableUmkm = $("#master-umkm-pekerjaan").dataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Umkm/GetUmkmPekerjaans",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },

    columns: [
        {
            className: "desktop",
            data: "id_umkm_pkj",
        },
        {
            className: "desktop",
            data: "nm_perusahaan",
        },
        {
            className: "desktop",
            data: "des_produk",
        },
        {
            className: "desktop",
            data: "id_umkm_pkj",

            render: function (data, type, full, meta) {
                var BtnEdit =
                    "<button class='btn btn-success mr-2'> <i class='fa fa-eye' aria-hidden='true'></i> Show </button>" +
                    "<button class='btn btn-success mr-2'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                    "<button class='btn btn-danger mr-2'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

// show add umkm pekerjaan
function umkmPekerjaanModalShow() {
    $("#masterUmkmPekerjaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboUmkm(function (obj) {
        $("select#IdUmkm").html(obj);
    });
    ComboPerusahaan(function (obj) {
        $("select#IdPrs").html(obj);
    });
}

$("select[name='IdPrs']").change(function () {
    let IdPrs = $("select[name='IdPrs'] option").filter(":selected").val();
    console.log(this.value + " Test");
    ComboPerusahaanDetail(
        function (HtmlCombo) {
            $("select[name='IdPrsDtl']").empty();
            $("select[name='IdPrsDtl']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
$("select[name='IdPrsDtl']").change(function () {
    ComboPekerjaan(
        function (HtmlCombo) {
            $("select[name='IdPekerjaan']").empty();
            $("select[name='IdPekerjaan']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
