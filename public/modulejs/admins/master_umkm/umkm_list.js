var TableUmkm = $("#master-umkm").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Umkm/GetUmkms",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "5%", visible: true },
        { targets: [6], width: "25%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "KdNib" },
        { data: "NmPerusahaan" },
        { data: "AlmtPerusahaan" },
        { data: "NoPhone" },
        { data: "NmPic" },
        {
            data: "IdUmkm",
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="ShowUmkmKbli(' +
                    full.IdUmkm +
                    ')" class="btn btn-primary btn-sm"><i class="fa fa-bars"></i> Umkm Kbli</button> ' +
                    "<button class='btn btn-success mr-2'> <i class='fa fa-eye' aria-hidden='true'></i> Show </button>" +
                    "<button class='btn btn-success mr-2'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                    "<button class='btn btn-danger mr-2'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function GetUmkmKblisByIdUmkm(param) {
    $("#master-umkm-klbi").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url: "/api/Umkm/GetUmkmKblis?IdUmkm=" + param,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "5%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
            { targets: [3], width: "15%", visible: true },
        ],
        columns: [
            { data: "No" },
            { data: "NmKbli" },
            { data: "NmPerusahaan" },
            {
                data: "IdUmkmKbli",
                render: function (data, type, full, meta) {
                    var BtnEdit =
                        "<button class='btn btn-success mr-2'> <i class='fa fa-eye' aria-hidden='true'></i> Show </button>" +
                        "<button class='btn btn-success mr-2'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                        "<button class='btn btn-danger mr-2'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                    data = BtnEdit;
                    return data;
                },
            },
        ],
        bDestroy: true,
    });
}

function ShowUmkmKbli(idUmkmKbli) {
    document.getElementById("section-umkm-kbli").style.display = "block";
    $("input[name='id_umkm']").val(idUmkmKbli);
    GetUmkmKblisByIdUmkm(idUmkmKbli);
}

const umkmModalShow = () => {
    $("#masterUmkmModal").modal({ backdrop: "static", keyboard: false });
    $("#btnSaveUmkm").attr("onClick", "SaveUmkm('Tambah');");
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
};

const umkmKbliModalShow = () => {
    $("#btnSaveUmkmKbli").attr("onClick", "SaveUmkmKbli('Tambah');");
    $("#masterUmkmKbliModal").modal({ backdrop: "static", keyboard: false });
    ComboKbli(function (obj) {
        $("select#IdKbli").html(obj);
    });
};

function SaveUmkmKbli(type) {
    let IdUmkm = $("input[name='id_umkm']").val();
    let IdKbli = $("select[name='IdKbli'] option").filter(":selected").val();

    if (type === "Tambah") {
        url = "/api/Umkm/AddUmkmKbli";
        methods = "Post";
        data = {
            IdUmkm: IdUmkm,
            IdKbli: IdKbli,
        };
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#master-umkm-klbi").DataTable().ajax.reload();
                        $("#masterUmkmKbliModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {}
                );
            }
        },
    });
}

function SaveUmkm(type) {
    let KdNib = $("input[name='KdNib']").val();
    let NmPerusahaan = $("input[name='NmPerusahaan']").val();
    let NoPhone = $("input[name='NoPhone']").val();
    let AlmtPerusahaan = $("textarea[name='AlmtPerusahaan']").val();
    let NmPic = $("input[name='NmPic']").val();
    let IdKabKot = $("select[name='IdAdmKabKot'] option")
        .filter(":selected")
        .val();
    let Lat = $("input[name='Lat']").val();
    let Lon = $("input[name='Lon']").val();

    if (type === "Tambah") {
        url = "/api/Umkm/AddUmkm";
        methods = "POST";
        data = {
            KdNib: KdNib,
            NmPerusahaan: NmPerusahaan,
            AlmtPerusahaan: AlmtPerusahaan,
            NoPhone: NoPhone,
            NmPic: NmPic,
            IdKabKot: IdKabKot,
            Lat: Lat,
            Lon: Lon,
        };
    }
    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#master-umkm").DataTable().ajax.reload();
                        $("#masterUmkmModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {}
                );
            }
        },
    });
}
