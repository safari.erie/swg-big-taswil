let idPrsDtl = $("input[name='IdPrsDtl']").val();
var TablePerusahaanDetilKbli = $("#umkm-perusahaan-detil-pekerjaan").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: "/api/Umkm/GetPekerjaans?IdPrsDtl=" + idPrsDtl,
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "2%", visible: true },
        { targets: [1], width: "10%", visible: true },
        { targets: [2], width: "25%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "NmPerusahaanDetil" },
        { data: "NmKbli" },
        { data: "DesProduk" },
        {
            data: "IdPekerjaan",
            data: "IdKbli",
            render: function (data, type, full, meta) {
                var Btn =
                    "<button class='btn btn-info mr-2' onClick=ShowPrsPekerjaan(" +
                    full.IdPekerjaan +
                    ")> <i class='fa fa-eye' aria-hidden='true'></i> Show </button > " +
                    "<button class='btn btn-success mr-2' onClick=ShowPrsPekerjaanEdit(" +
                    full.IdPekerjaan +
                    "," +
                    full.IdKbli +
                    ") > <i class='fa fa-check' aria-hidden='true'></i> Edit </button > " +
                    "<button class='btn btn-danger mr-2' onClick=ShowPrsPekerjaanDelete(" +
                    full.IdPekerjaan +
                    ")> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = Btn;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function ShowPrsPekerjaan(IdPekerjaan) {
    $.ajax({
        url: base_url + "/api/Umkm/GetPekerjaan?IdPekerjaan=" + IdPekerjaan,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                $("#perusahaanDetailPekerjaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                $("input[name='IdPekerjaan']").val(res.Data.id_pekerjaan);

                $("#btnSavePerusahaanDetailPekerjaan").hide();
                $("#DivSelectKbli").hide();
                $("#DesProduk").hide();
                $("#setDesProd").show();
                $("#DivNameKbli").show();
                $("#setNameKbli").html(
                    `<strong>  ${res.Data.nm_kbli}</strong>`
                );
                $("#setDesProd").html(
                    `<strong>  ${res.Data.des_produk}</strong>`
                );
            }
        },
    });
}

function ShowPrsPekerjaanEdit(IdPekerjaan, IdKbli) {
    $.ajax({
        url: base_url + "/api/Umkm/GetPekerjaan?IdPekerjaan=" + IdPekerjaan,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                $("#DivSelectKbli").show();
                $("#DesProduk").show();
                $("#setDesProd").hide();
                $("#DivNameKbli").hide();
                $("#perusahaanDetailPekerjaanModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                $("input[name='IdPekerjaan']").val(res.Data.id_pekerjaan);
                ComboKbli(function (obj) {
                    $("select#IdKbli").html(obj);
                }, IdKbli);
                $("#btnSavePerusahaanDetailPekerjaan").attr(
                    "onClick",
                    "SavePerusahaanDetailPekerjaan('Edit');"
                );

                $("textarea[name='DesProduk']").val(res.Data.des_produk);
            }
        },
    });
}

function ShowPrsPekerjaanDelete(IdPekerjaan) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaanDetailPekerjaan",
                    data: { IdPekerjaan: IdPekerjaan },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan-detil-pekerjaan")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        } else {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "Danger",
                                },
                                function () {
                                    $("#umkm-perusahaan-detil-pekerjaan")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        }
                    },
                });
            }
        }
    );
}

function perusahaanDetailPekerjaanModalShow() {
    $("#perusahaanDetailPekerjaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    $("#DivSelectKbli").show();
    $("#DesProduk").show();
    $("#setDesProd").hide();
    $("#DivNameKbli").hide();
    $("input[name='IdPekerjaan']").val("");
    $("textarea[name='DesProduk']").val("");
    ComboKbli(function (obj) {
        $("select#IdKbli").html(obj);
    });
    $("#btnSavePerusahaanDetailPekerjaan").attr(
        "onClick",
        "SavePerusahaanDetailPekerjaan('Tambah');"
    );
}

function SavePerusahaanDetailPekerjaan(type) {
    let IdPekerjaan = $("input[name='IdPekerjaan']").val();
    let IdKbli = $("select[name='IdKbli'] option").filter(":selected").val();
    let IdPrsDtl = $("input[name='IdPrsDtl']").val();
    let DesProduk = $("textarea[name='DesProduk']").val();

    if (type === "Tambah") {
        url = base_url + "/api/Umkm/AddPerusahaanDetailPekerjaan";
        methods = "POST";
        data = {
            IdPrsDtl: IdPrsDtl,
            IdKbli: IdKbli,
            DesProduk: DesProduk,
        };
    } else {
        url = base_url + "/api/Umkm/EditPerusahaanDetailPekerjaan";
        methods = "POST";
        data = {
            IdPekerjaan: IdPekerjaan,
            IdKbli: IdKbli,
            DesProduk: DesProduk,
        };
    }
    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#umkm-perusahaan-detil-pekerjaan")
                            .DataTable()
                            .ajax.reload();
                        $("#perusahaanDetailPekerjaanModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {}
                );
            }
        },
    });
}
