/* load for the first */
$(document).ready(function () {
    //$("#umkm-perusahaan").DataTable();
});

var TablePerusahaan = $("#umkm-perusahaan").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Umkm/GetPerusahaans",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "KdNib" },
        { data: "NmPerusahaan" },
        { data: "FlagJenis" },
        {
            data: "IdPrs",
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="ShowDetail(' +
                    full.IdPrs +
                    ')" class="btn btn-primary btn-sm"><i class="fa fa-bars"></i> Detail Perusahaan</button>&nbsp;&nbsp; <button class="btn btn-warning mr-2" onCLick="Show(' +
                    full.IdPrs +
                    ')"' +
                    "> <i class='fa fa-eye' aria-hidden='true'></i> Show</button> <button class='btn btn-success mr-2' onClick='EditForm(" +
                    full.IdPrs +
                    ")'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                    "<button class='btn btn-danger mr-2' onClick='DeletePerusahaan(" +
                    full.IdPrs +
                    ")'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function DeletePerusahaan(IdPrs) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaan",
                    data: { IdPrs: IdPrs },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            $("#umkm-perusahaan").DataTable().ajax.reload();
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                    $("#perusahaanModal").modal("hide");
                                }
                            );
                        } else {
                            alert(res.Message);
                        }
                    },
                });
            }
        }
    );
}

function ShowDetail(idPrs) {
    window.location = base_url + "/umkm/perusahaan/detail?IdPrs=" + idPrs;
}

function Show(idPrs) {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPrs=" + idPrs,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                let flagJenis = res.Data.flag_jenis === 1 ? "PMA" : "PMDN";
                showHideForView();
                $("#setNib").html(`<strong> ${res.Data.kd_nib}</strong>`);
                $("#setNmPerusahaan").html(
                    `<strong> ${res.Data.nm_perusahaan}</strong>`
                );
                $("#setFlagJenis").html(`<strong> ${flagJenis}</strong>`);
                GetDetailPrs(idPrs);
            }
        },
    });
}

function showHideForView() {
    $("#perusahaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    $("#setNib").show();
    $("#KdNib").hide();
    $("#setNmPerusahaan").show();
    $("#NmPerusahaan").hide();
    $("#setFlagJenis").show();
    $(".rdFlagJenis").hide();
    $("#btnSavePerusahaan").hide();
    $("#tableShowPrsShowDetail").show();
}

function showHideForNoView() {
    $("#perusahaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    $("#setNib").hide();
    $("#KdNib").show();
    $("#setNmPerusahaan").hide();
    $("#NmPerusahaan").show();
    $("#setFlagJenis").hide();
    $(".rdFlagJenis").show();
    $("#btnSavePerusahaan").show();
    $("#tableShowPrsShowDetail").hide();
}

function EditForm(idPrs) {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaan?IdPrs=" + idPrs,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            console.log(res);
            if (res.Status) {
                let flagJenis =
                    res.Data.flag_jenis === 1
                        ? $(
                              "input[name=FlagJenis][value=" +
                                  res.Data.flag_jenis +
                                  "]"
                          ).prop("checked", true)
                        : $(
                              "input[name=FlagJenis][value=" +
                                  res.Data.flag_jenis +
                                  "]"
                          ).prop("checked", true);

                showHideForNoView();
                $("input[name='IdPrs']").val(parseInt(res.Data.id_prs));
                $("input[name='KdNib']").val(parseInt(res.Data.kd_nib));
                $("input[name='NmPerusahaan']").val(res.Data.nm_perusahaan);
            }
            $("#btnSavePerusahaan").attr("onClick", "SavePerusahaan('Edit');");
        },
    });
}

/* function for onclick show modal perusahaan */
const perusahaanModalShow = () => {
    $("#perusahaanModal").modal({
        backdrop: "static",
        keyboard: false,
    });

    $("#setNib").hide();
    $("#KdNib").show();
    $("#setNmPerusahaan").hide();
    $("#NmPerusahaan").show();
    $("#setFlagJenis").hide();
    $(".rdFlagJenis").show();
    $("#btnSavePerusahaan").show();
    $("#btnSavePerusahaan").attr("onClick", "SavePerusahaan('Tambah');");
    $("input[name='IdPrs']").val();
    $("#tableShowPrsShowDetail").hide();
};

function SavePerusahaan(type) {
    if (type === "Tambah") {
        url = base_url + "/api/Umkm/AddPerusahaan";
        methods = "POST";
        data = {
            KdNib: $("input[name='KdNib']").val(),
            NmPerusahaan: $("input[name='NmPerusahaan']").val(),
            FlagJenis: $('input[name="FlagJenis"]:checked').val(),
        };
    } else {
        url = base_url + "/api/Umkm/EditPerusahaan";
        methods = "POST";
        data = {
            IdPrs: $("input[name='IdPrs']").val(),
            KdNib: $("input[name='KdNib']").val(),
            NmPerusahaan: $("input[name='NmPerusahaan']").val(),
            FlagJenis: $('input[name="FlagJenis"]:checked').val(),
        };
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#umkm-perusahaan").DataTable().ajax.reload();
                        $("#perusahaanModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#perusahaanModal").modal("hide");
                    }
                );
            }
        },
    });
}

function GetDetailPrs(IdPrs) {
    var TablePerusahaan = $("#umkm-perusahaan-detail").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url: "/api/Umkm/GetPerusahaanDetils?IdPrs=" + IdPrs,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },

        columns: [
            { data: "No" },
            { data: "Judul" },
            { data: "Alamat" },
            { data: "Phone" },
            { data: "NmPic" },
            { data: "Provinsi" },
            { data: "KabKot" },
        ],
        bDestroy: true,
    });
}
