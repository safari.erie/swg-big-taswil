let idPrsDtl = $("input[name='IdPrsDtl']").val();
const perusahaanDetailKbliModalShow = () => {
    $("#inputKbli").show();
    $("#showPrsDtlKbli").hide();
    $("input[name='IdPrsKbli']").val("");
    $("#btnSavePerusahaanDetailKbli").show();
    $("#perusahaanDetailKbliModal").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboKbli(function (obj) {
        $("select#IdKbli").html(obj);
    });
    $("#btnSavePerusahaanDetailKbli").attr(
        "onClick",
        "SavePerusahaanDetilKbli('Tambah');"
    );
};

var TablePerusahaanDetilKbli = $("#umkm-perusahaan-detil-kbli").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url:
            base_url + "/api/Umkm/GetPerusahaanDetilKbli?IdPrsKbli=" + idPrsDtl,
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "2%", visible: true },
        { targets: [1], width: "10%", visible: true },
        { targets: [2], width: "25%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "NmPrsDetil" },
        { data: "NmKlbi" },
        {
            data: "IdPrsKbli",
            data: "IdKbli",
            render: function (data, type, full, meta) {
                var Btn =
                    "<button class='btn btn-info mr-2' onClick='ShowPrsKbli(" +
                    full.IdPrsKbli +
                    "," +
                    full.IdKbli +
                    ")' > <i class='fa fa-eye' aria-hidden='true'></i> Show </button > " +
                    "<button class='btn btn-success mr-2' onClick='ShowPrsKbliEdit(" +
                    full.IdPrsKbli +
                    "," +
                    full.IdKbli +
                    ")'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button > " +
                    "<button class='btn btn-danger mr-2' onClick=ShowKbliDelete(" +
                    full.IdPrsKbli +
                    ")> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = Btn;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function ShowPrsKbliEdit(IdPrsKbli, IdKbli) {
    $.ajax({
        url:
            base_url +
            "/api/Umkm/GetPerusahaanDetilKbliById?IdPrsKbli=" +
            IdPrsKbli,
        method: "GET",
        dataType: "json",
        beforeSend: function (res) {},
        success: function (res) {
            if (res.Status) {
                $("input[name='IdPrsKbli']").val("");
                $("#perusahaanDetailKbliModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                IdPrsKbli = $("input[name='IdPrsKbli']").val(IdPrsKbli);
                $("#inputKbli").show();
                $("#showPrsDtlKbli").hide();
                $("#btnSavePerusahaanDetailKbli").show();
                ComboKbli(function (obj) {
                    $("select#IdKbli").html(obj);
                }, IdKbli);

                $("#btnSavePerusahaanDetailKbli").attr(
                    "onClick",
                    "SavePerusahaanDetilKbli('Edit');"
                );
            }
        },
    });
}

function ShowPrsKbli(IdPrsKbli, IdKbli) {
    $.ajax({
        url:
            base_url +
            "/api/Umkm/GetPerusahaanDetilKbliById?IdPrsKbli=" +
            IdPrsKbli,
        method: "GET",
        dataType: "json",
        beforeSend: function (res) {},
        success: function (res) {
            if (res.Status) {
                console.log(res);
                $("#perusahaanDetailKbliModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });
                $("#inputKbli").hide();
                $("#showPrsDtlKbli").show();
                $("#btnSavePerusahaanDetailKbli").hide();
                $("#setNmPrsDtl").html(
                    `<strong> ${res.Data.nm_detil}</strong>`
                );
                $("#setNmKbli").html(`<strong> ${res.Data.nm_kbli}</strong>`);
            }
        },
    });
}

function SavePerusahaanDetilKbli(type) {
    let IdPrsDtl = $("input[name='IdPrsDtl']").val();
    let IdKbli = $("select[name='IdKbli'] option").filter(":selected").val();

    if (type === "Tambah") {
        url = base_url + "/api/Umkm/AddPerusahaanDetilKbli";
        methods = "POST";
        data = {
            IdPrsDtl: IdPrsDtl,
            IdKbli: IdKbli,
        };
    } else {
        let IdPrsKbli = $("input[name='IdPrsKbli']").val();
        url = base_url + "/api/Umkm/EditPerusahaanDetilKbli";
        methods = "POST";
        data = {
            IdPrsKbli: IdPrsKbli,
            IdKbli: IdKbli,
        };
    }

    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#umkm-perusahaan-detil-kbli")
                            .DataTable()
                            .ajax.reload();
                        $("#perusahaanDetailKbliModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {}
                );
            }
        },
    });
}

function ShowKbliDelete(IdPrsKbli) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaanDetilKbli",
                    data: { IdPrsKbli: IdPrsKbli },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            $("#umkm-perusahaan-detil-kbli")
                                .DataTable()
                                .ajax.reload();
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan")
                                        .DataTable()
                                        .ajax.reload();
                                    $("#perusahaanModal").modal("hide");
                                }
                            );
                        } else {
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "danger",
                                },
                                function () {
                                    $("#umkm-perusahaan-detil-kbli")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        }
                    },
                });
            }
        }
    );
}
