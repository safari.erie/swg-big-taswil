/* load for the first */
let idPrs = $("input[name='IdPrs']").val();
$(document).ready(function () {});
var marker;
/* function for onclick show modal perusahaan detail */
const perusahaanDetailModalShow = () => {
    document.getElementById("table").style.display = "none";
    document.getElementById("card-header-detail").style.display = "none";
    document.getElementById("form").style.display = "block";
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    $("#btnSavePerusahaanDetil").attr(
        "onClick",
        "SavePerusahaanDetil('Tambah');"
    );
    //map.remove();
    /*  map = L.map("map").setView([-1.4509444, 117.62527], 4);
    L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
        attribution:
            '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);
    map.on("click", function (e) {
        marker = L.marker(e.latlng, {
            draggable: true,
        }).addTo(map);
        var latlng = map.mouseEventToLatLng(e.originalEvent);
        console.log(latlng.lat + ", " + latlng.lng);
        document.getElementById("lat").value = latlng.lat;
        document.getElementById("lon").value = latlng.lng;
        marker.on("dragend", function (e) {
            document.getElementById("lat").value = marker.getLatLng().lat;
            document.getElementById("lon").value = marker.getLatLng().lng;
        });
    }); */
};

const cancelDetailPerusahaan = () => {
    /* map.remove();
    marker.remove(); */
    document.getElementById("table").style.display = "block";
    document.getElementById("card-header-detail").style.display = "block";
    document.getElementById("form").style.display = "none";
    document.getElementById("x").value = "";
    document.getElementById("y").value = "";
};

const ShowPrsDetailEdit = (idPrsDtl) => {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaanDetil?IdPrsDtl=" + idPrsDtl,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                document.getElementById("table").style.display = "none";
                document.getElementById("card-header-detail").style.display =
                    "none";
                document.getElementById("form").style.display = "block";
                ComboGetWilayahProvinsi(function (obj) {
                    $("select#IdAdmProv").html(obj);
                });
                $("#btnSavePerusahaanDetil").attr(
                    "onClick",
                    "SavePerusahaanDetil('Edit');"
                );
                $("input[name='IdPrsDtl']").val(res.Data.id_prs_dtl);
                $("input[name='NmDetil']").val(res.Data.nm_detil);
                $("textarea[name='AlmtPerusahaan']").val(res.Data.alamat);
                $("input[name='NoPhone']").val(res.Data.no_phone);
                $("input[name='NmPic']").val(res.Data.nm_pic);
                $("input[name='Lat']").val(res.Data.lat);
                $("input[name='Lon']").val(res.Data.lon);
            }
        },
    });
};

var TablePerusahaan = $("#umkm-perusahaan-detail").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Umkm/GetPerusahaanDetils?IdPrs=" + idPrs,
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },

    columns: [
        { data: "No" },
        { data: "Judul" },
        { data: "Alamat" },
        { data: "Phone" },
        { data: "NmPic" },
        { data: "Provinsi" },
        { data: "KabKot" },
        {
            data: "IdPrs",
            data: "Judul",
            data: "Alamat",
            data: "Phone",
            data: "NmPic",
            data: "Lat",
            render: function (data, type, full, meta) {
                var Btn =
                    '<button type="button" onClick="ShowDetailKbli(' +
                    full.IdPrsDtl +
                    ')" class="btn btn-primary btn-sm"><i class="fa fa-bars"></i> Detail KBLI</button>&nbsp;&nbsp; <button type="button" onClick="ShowDetailPekerjaan(' +
                    full.IdPrsDtl +
                    ')" class="btn btn-warning mr-2"><i class="fa fa-bars" aria-hidden="true"></i> Pekerjaaan</button> &nbsp;&nbsp;' +
                    "<button class='btn btn-info mr-2' onClick='ShowDetailPerusahaan(" +
                    full.IdPrsDtl +
                    ")'> <i class='fa fa-eye' aria-hidden='true'></i> Show </button > " +
                    "<button class='btn btn-success mr-2'onClick='ShowPrsDetailEdit(" +
                    full.IdPrsDtl +
                    ")' > <i class='fa fa-check' aria-hidden='true'></i> Edit </button > " +
                    "<button class='btn btn-danger mr-2' onClick='ShowPrsDetailDelete(" +
                    full.IdPrsDtl +
                    ")'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = Btn;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function ShowDetailKbli(idPrsDtl) {
    window.location = "/umkm/perusahaan/detail_kbli?IdPrsDtl=" + idPrsDtl;
}

function ShowDetailPerusahaan(idPrsDtl) {
    $.ajax({
        url: base_url + "/api/Umkm/GetPerusahaanDetil?IdPrsDtl=" + idPrsDtl,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                GetTablePerusahaanDetailKbli(idPrsDtl);
                GetTablePerusahaanPekerjaan(idPrsDtl);
                $("#perusahaanDetailModal").modal({
                    backdrop: "static",
                    keyboard: false,
                });

                $("#setNmPerusahaanDetail").html(
                    `<strong> ${res.Data.nm_detil}</strong>`
                );
                $("#setAlmtPerusahaanDetail").html(
                    `<strong> ${res.Data.alamat}</strong>`
                );
                $("#setPhonePerusahaanDetail").html(
                    `<strong> ${res.Data.no_phone}</strong>`
                );
                $("#setPicPerusahaanDetail").html(
                    `<strong> ${res.Data.nm_pic}</strong>`
                );
            }
        },
    });
}

function ShowPrsDetailDelete(IdPrsDtl) {
    swal(
        {
            title: "Apakah Anda Yakin ?",
            text: "Anda akan menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya, Saya Yakin!",
            cancelButtonClass: "btn-danger",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: true,
        },
        function (then) {
            if (then) {
                $.ajax({
                    url: base_url + "/api/Umkm/DeletePerusahaanDetail",
                    data: { IdPrsDtl: IdPrsDtl },
                    method: "POST",
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (res) {
                        if (res.Status) {
                            $("#umkm-perusahaan-detail")
                                .DataTable()
                                .ajax.reload();
                            swal(
                                {
                                    title: "Konfirmasi",
                                    text: res.Message,
                                    type: "success",
                                },
                                function () {
                                    $("#umkm-perusahaan-detail")
                                        .DataTable()
                                        .ajax.reload();
                                }
                            );
                        } else {
                            alert(res.Message);
                        }
                    },
                });
            }
        }
    );
}

function ShowDetailPekerjaan(idPrsDtl) {
    window.location = "/umkm/perusahaan/detail_pekerjaan?IdPrsDtl=" + idPrsDtl;
}

function GetTablePerusahaanDetailKbli(idPrsDtl) {
    var TablePerusahaanDetilKbli = $("#umkm-perusahaan-detail-kbli").DataTable({
        paging: false,
        searching: false,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url: "/api/Umkm/GetPerusahaanDetilKbli?IdPrsKbli=" + idPrsDtl,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "2%", visible: true },
            { targets: [1], width: "10%", visible: true },
        ],
        columns: [{ data: "No" }, { data: "NmKlbi" }],
        bDestroy: true,
    });
}

function GetTablePerusahaanPekerjaan(idPrsDtl) {
    var TablePerusahaanDetilKbli = $(
        "#umkm-perusahaan-detail-pekerjaan"
    ).DataTable({
        paging: false,
        searching: false,
        ordering: true,
        info: true,
        pageLength: 10,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url: "/api/Umkm/GetPekerjaans?IdPrsDtl=" + idPrsDtl,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "2%", visible: true },
            { targets: [1], width: "10%", visible: true },
        ],
        columns: [
            { data: "No" },
            /* { data: "NmPerusahaanDetil" },
            { data: "NmKbli" }, */
            { data: "DesProduk" },
        ],
        bDestroy: true,
    });
}

function SavePerusahaanDetil(type) {
    if (type === "Tambah") {
        url = base_url + "/api/Umkm/AddPerusahaanDetil";
        methods = "POST";
        data = {
            IdPrs: parseInt($("input[name='IdPrs']").val()),
            NmDetil: $("input[name='NmDetil']").val(),
            AlmtPerusahaan: $("textarea[name='AlmtPerusahaan']").val(),
            NoPhone: $("input[name='NoPhone']").val(),
            NmPic: $("input[name='NmPic']").val(),
            IdKabKota: /* $("select[name='IdKabKot'] option")
                .filter(":selected")
                .val(), */ 1,
            Lat: $("input[name='Lat']").val(),
            Lon: $("input[name='Lon']").val(),
        };
    } else {
        url = base_url + "/api/Umkm/EditPerusahaanDetil";
        methods = "POST";
        data = {
            IdPrsDtl: parseInt($("input[name='IdPrsDtl']").val()),
            IdPrs: parseInt($("input[name='IdPrs']").val()),
            NmDetil: $("input[name='NmDetil']").val(),
            AlmtPerusahaan: $("textarea[name='AlmtPerusahaan']").val(),
            NoPhone: $("input[name='NoPhone']").val(),
            NmPic: $("input[name='NmPic']").val(),
            IdKabKota: 1,
            Lat: $("input[name='Lat']").val(),
            Lon: $("input[name='Lon']").val(),
        };
    }
    $.ajax({
        url: url,
        method: methods,
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#umkm-perusahaan-detail").DataTable().ajax.reload();
                        cancelDetailPerusahaan();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        cancelDetailPerusahaan();
                    }
                );
            }
        },
    });
}
