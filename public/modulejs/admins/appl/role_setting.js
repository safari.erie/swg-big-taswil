var url;
var tableRoleSetting = $("#roles").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 5,
    lengthMenu: [
        [5, 10, 25, 50, 100],
        [5, 10, 25, 50, 100],
    ],
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: "/api/Roles/GetRoles",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
    ],
    columns: [
        { data: "id_role" },
        { data: "role_name" },
        {
            data: "id_role",
            data: "role_name",
            render: function (data, type, full, meta) {
                var RoleName = "'" + full.role_name + "'";
                var BtnEdit =
                    '<button type="button" onClick="EditRole(' +
                    full.id_role +
                    ')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button>&nbsp;&nbsp;';
                BtnEdit +=
                    '<button type="button" onClick="EditMenu(' +
                    full.id_role +
                    "," +
                    RoleName +
                    ')" class="btn btn-warning btn-sm"><i class="fas fa-check"></i> Set Menu </button>&nbsp;&nbsp;';
                /* BtnEdit +=
                    '<button type="button" onClick="HakAkses(' +
                    full.id_role +
                    ')" class="btn btn-success btn-sm"><i class="fa fa-bookmark"></i> Hak Akses </button>&nbsp;&nbsp;'; */
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

const roleFormShow = () => {
    $("#FormRole").css("display", "block");
    $("#FormSetData").css("display", "none");
    $("#img-add-role").css("display", "none");
    $("#HeaderTitle").html("Tambah Role");
    url = "add";
};

function EditMenu(IdRole, RoleName) {
    $("#img-add-role").css("display", "none");
    $("#FormSetData").css("display", "block");
    $("#FormRole").css("display", "none");

    $("#LabelNoAssign").html("No Assign Menu");
    $("#LabelAssign").html("Assign Menu");

    $("#HeaderBody").html("Set Menu - " + RoleName);
    $(".ComboSumber").attr("name", "NoAssignTasks[]");
    $(".ComboTerpilih").attr("name", "AssignTasks[]");
    $("#BtnSave").attr("onClick", "save_data('SetMenu')");

    $("input[name='IdRole']").val(IdRole);
    //ComboGetApplTaskByRole(IdRole);

    ComboGetApplTaskByRole(function (
        HtmlComboNoAssignTasks,
        HtmlComboAssignTasks
    ) {
        $("select[name='NoAssignTasks[]']").html(HtmlComboNoAssignTasks);
        $("select[name='AssignTasks[]']").html(HtmlComboAssignTasks);
    },
    IdRole);
    GetPermissions(IdRole, RoleName);
    $("#DivIdPermission").show();
}

function GetPermissions(IdRole, RoleName) {
    $("#title-permision")
        .empty()
        .append(`Data Role : <strong> ${RoleName}</strong>`);
    let url = base_url + "/api/ApplTask/GetPermission?IdRole=" + IdRole;
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {},
        success: function (res) {
            let html = "";
            if (res.Status) {
                let no = 1;
                res.Data.forEach((value, key) => {
                    let can_read = value.can_read === 1 ? "checked" : "";
                    let can_edit = value.can_edit === 1 ? "checked" : "";
                    let can_approve = value.can_approve === 1 ? "checked" : "";
                    let can_delete = value.can_delete === 1 ? "checked" : "";
                    let can_add = value.can_add === 1 ? "checked" : "";
                    space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    let moduleName =
                        space.repeat(value.level) + value.appl_task_name;

                    html += `<tr>
                        <td> ${no++}</td>
                        <td> ${moduleName} </td>
                        <td class="text-center">
                            ${
                                value.controller_name != null
                                    ? `<label class="fancy-checkbox">
                                <input type="checkbox" name="add_${value.id_appl_task}" id="add_${value.id_appl_task}" ${can_add} onClick="changeStatus(this);" />
                                <span></span>
                            </label>`
                                    : ``
                            }
                        </td>
                        <td class="text-center">
                            ${
                                value.controller_name != null
                                    ? `<label class="fancy-checkbox">
                                <input type="checkbox" name="read_${value.id_appl_task}" id="read_${value.id_appl_task}" ${can_read} onClick="changeStatus(this);" />
                                <span></span>
                            </label>`
                                    : ``
                            }
                        </td>
                        <td class="text-center">
                            ${
                                value.controller_name != null
                                    ? `<label class="fancy-checkbox">
                                <input type="checkbox" name="edit_${value.id_appl_task}" id="edit_${value.id_appl_task}" ${can_edit} onClick="changeStatus(this);" />
                                <span></span>
                            </label>`
                                    : ``
                            }
                        </td>
                        <td class="text-center">
                            ${
                                value.controller_name != null
                                    ? `<label class="fancy-checkbox">
                                <input type="checkbox" name="approve_${value.id_appl_task}" id="approve_${value.id_appl_task}" ${can_approve} onClick="changeStatus(this);" />
                                <span></span>
                            </label>`
                                    : ``
                            }
                        </td>
                        <td class="text-center">
                            ${
                                value.controller_name != null
                                    ? `<label class="fancy-checkbox">
                                <input type="checkbox" name="delete_${value.id_appl_task}" id="delete_${value.id_appl_task}" ${can_delete} onClick="changeStatus(this);" />
                                <span></span>
                            </label>`
                                    : ``
                            }
                        </td>
                    </tr>`;
                });
            } else {
                html += "";
            }
            $("#role-permission tbody").empty().append(html);
        },
    });
}

function changeStatus(elements) {
    let col_id = $(elements).prop("id");
    let str = $(elements).prop("name").split("_");
    let checked = $(elements).prop("checked");
    let value = 0;
    let IdRole = $("input[name='IdRole']").val();
    if (checked === true) value = 1;

    data = new FormData();
    data.append("IdRole", IdRole);
    data.append("ColumnName", str[0]);
    data.append("IdApplTask", str[1]);
    data.append("Value", value);

    $.ajax({
        url: base_url + "/api/appl/change_permission",
        method: "POST",
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status !== true) {
                swal({
                    title: "Konfirmasi",
                    text: res.Message,
                    type: "error",
                });
            }
        },
    });
}

function HakAkses(IdRole) {
    window.location = base_url + "/appl/hak_akses?IdRole=" + IdRole;
}

function ComboGetApplTaskByRole(handleData, IdRole) {
    var Url = "/api/ApplTask/GetApplTaskByRole?IdRole=" + parseInt(IdRole);
    $.ajax({
        url: Url,
        type: "GET",
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
        },
        beforeSend: function (beforesend) {
            /*  ProgressBar("wait"); */
        },
        success: function (res) {
            /* ProgressBar("success"); */
            var HtmlComboNoAssignTasks = "";
            var HtmlComboAssignTasks = "";
            if (res.Status == true) {
                console.log(res.Data);
                space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                var JmlNoAssignTasks = res.Data.NoAssignTasks.length;
                var JmlAssignTasks = res.Data.AssignTasks.length;

                for (var inat = 0; inat < JmlNoAssignTasks; inat++) {
                    let NameNoAssign =
                        space.repeat(res.Data.NoAssignTasks[inat].Level) +
                        res.Data.NoAssignTasks[inat].ApplTaskName;
                    HtmlComboNoAssignTasks +=
                        "<option value='" +
                        res.Data.NoAssignTasks[inat].IdApplTask +
                        "'>" +
                        NameNoAssign +
                        "</option>";
                }
                for (var iat = 0; iat < JmlAssignTasks; iat++) {
                    let NameAssign =
                        space.repeat(res.Data.AssignTasks[iat].Level) +
                        res.Data.AssignTasks[iat].ApplTaskName;
                    HtmlComboAssignTasks +=
                        "<option value='" +
                        res.Data.AssignTasks[iat].IdApplTask +
                        "' selected>" +
                        NameAssign +
                        "</option>";
                }
                handleData(HtmlComboNoAssignTasks, HtmlComboAssignTasks);
            }
        },
        error: function (responserror, a, e) {
            /* ProgressBar("success"); */
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

// modifikasi fungsi seperti dual list box
$(".ComboSumber").on("change", function () {
    var ValueSelected = this.value;
    var TextSelected = $("option:selected", this).text();

    var HtmlSelected =
        "<option value='" +
        ValueSelected +
        "' selected>" +
        TextSelected +
        "</option>";
    $(".ComboTerpilih").append(HtmlSelected);
    $("option:selected", this).remove();
});

$(".ComboTerpilih").on("change", function () {
    var ValueSelected = this.value;
    var TextSelected = $("option:selected", this).text();

    var HtmlSelected =
        "<option value='" + ValueSelected + "'>" + TextSelected + "</option>";
    $(".ComboSumber").append(HtmlSelected);
    $("option:selected", this).remove();
});

function PilihSemua() {
    $(".ComboSumber")
        .find("option")
        .each(function (index, item) {
            var ValueSelected = $(item).val();
            var TextSelected = $(item).text();

            var HtmlSelected =
                "<option value='" +
                ValueSelected +
                "' selected>" +
                TextSelected +
                "</option>";
            $(".ComboTerpilih").append(HtmlSelected);

            $(this).remove();
        });
}

function BatalPilihSemua() {
    $(".ComboTerpilih")
        .find("option")
        .each(function (index, item) {
            var ValueSelected = $(item).val();
            var TextSelected = $(item).text();

            var HtmlSelected =
                "<option value='" +
                ValueSelected +
                "'>" +
                TextSelected +
                "</option>";
            $(".ComboSumber").append(HtmlSelected);

            $(this).remove();
        });
}

const save_data = (type) => {
    let idRole = $("input[name='IdRole']").val();
    let datas;
    if (type === "SetMenu") {
        var IdApplTasks = "";
        $(".ComboTerpilih")
            .find("option")
            .each(function (index, item) {
                IdApplTasks += ";" + parseInt($(item).val());
            });
        IdApplTasks = IdApplTasks.substring(1);
        var Url = "/api/ApplTask/AddTasksRole";
        datas = {
            IdRole: idRole,
            IdApplTasks: IdApplTasks,
        };
    }
    $.ajax({
        url: Url,
        method: "POST",
        dataType: "json",
        data: datas,
        beforeSend: function (res) {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#roles").DataTable().ajax.reload();
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#roles").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
};

const save_role = () => {
    var RoleName = $("input[name='RoleName']").val();
    let urlAccess;
    let data;
    if (url == "add") {
        urlAccess = "/api/Roles/AddRole";
        data = {
            role_name: RoleName,
        };
    } else {
        urlAccess = "/api/Roles/UpdateRole";
        data = {
            IdRole: $("input[name='id_role']").val(),
            RoleName: RoleName,
        };
    }
    $.ajax({
        url: urlAccess,
        method: url == "add" ? "POST" : "PUT",
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#roles").DataTable().ajax.reload();
                        $("input[name='RoleName']").val("");
                        if (urlAccess == "edit") {
                            urlAccess = "add";
                        }
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#roles").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
};

const EditRole = (idRole) => {
    url = "edit";
    $.ajax({
        url: "/api/Roles/GetRole",
        dataType: "json",
        method: "GET",
        data: {
            idRole: idRole,
        },
        beforeSend: function () {},
        success: function (res) {
            $("#DivIdPermission").hide();
            if (res.Status) {
                $("#FormRole").css("display", "block");
                $("#FormSetData").css("display", "none");
                $("#img-add-role").css("display", "none");
                $("#HeaderTitle").html("Edit Role");
                $("input[name='RoleName']").val(res.Data.role_name);
                $("input[name='id_role']").val(res.Data.id_role);
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: "Data Tidak ada",
                        type: "danger",
                    },
                    function () {
                        $("#FormRole").css("display", "none");
                        $("#FormSetData").css("display", "block");
                        $("#img-add-role").css("display", "blocl");
                        $("#HeaderTitle").html("Edit Role");
                        $("input[name='RoleName']").val(res.Data.role_name);
                    }
                );
            }
        },
    });
};
