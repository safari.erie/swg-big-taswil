var TableApplSetting = $("#appl_setting").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 5,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: "/api/ApplSetting/GetApplSettings",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
    ],
    columns: [
        { data: "code" },
        { data: "name" },
        {
            data: "value_type",
            render: function (data, type, full, meta) {
                let condition =
                    full.value_type == 1
                        ? full.value_string
                        : full.value_type == 2
                        ? full.value_date
                        : full.value_number;
                data = condition;
                return data;
            },
        },
        {
            data: "code",
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditRole(' +
                    full.code +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;';
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});
