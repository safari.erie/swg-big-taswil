function changeStatus(elements) {
    let col_id = $(elements).prop("id");
    let str = $(elements).prop("name").split("_");
    let checked = $(elements).prop("checked");
    let value = 0;
    let IdRole = $("input[name='IdRole']").val();
    if (checked === true) value = 1;
    console.log(str[0]);
    console.log(str[1]);
    data = new FormData();
    data.append("IdRole", IdRole);
    data.append("ColumnName", str[0]);
    data.append("IdApplTask", str[1]);
    data.append("Value", value);

    $.ajax({
        url: base_url + "/api/appl/change_permission",
        method: "POST",
        dataType: "json",
        data: data,
        contentType: false,
        processData: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status !== true) {
                swal({
                    title: "Konfirmasi",
                    text: res.Message,
                    type: "error",
                });
            }
        },
    });
}
