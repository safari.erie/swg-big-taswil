function CmbTypeAppl(id) {
    let htmCmb = "";
    $.ajax({
        url: "/api/ApplTask/GetAppls",
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                htmCmb += `<option value=""> --Pilih Aplikasi </option>`;
                res.Data.forEach((value, key) => {
                    if (value.id_appl == id) {
                        htmCmb += `<option value=${value.id_appl} selected> ${value.appl_name} </option>`;
                    }
                    {
                        htmCmb += `<option value=${value.id_appl}> ${value.appl_name} </option>`;
                    }
                });
            } else {
                htmCmb += `<option value="0"> --Data Tidak ada-- </option>`;
            }
            $("select[name=IdAppl]").empty().append(htmCmb);
        },
    });
}

CmbTypeAppl();

$("select[name='IdAppl']").change(function () {
    $("input[name='IdAppl']", "#FormMenu").val(this.value);
    $("input[name='ApplName']", "#FormMenu").val(
        $(this).find("option:selected").text()
    );
    if (this.value != "" && this.value != null) {
        $("#DivBtnTambah").removeAttr("style");

        $("#ImgSearch").css("display", "none");

        $("#menus").removeAttr("style");
        $(".table-responsive").removeAttr("style");
        GetDataMenu(this.value);
    } else {
        $("#DivBtnTambah").css("display", "none");

        $("#TabelMenuSetting").css("display", "none");
        $(".table-responsive").css("display", "none");
        $("#ImgSearchTabel").removeAttr("style");
        $("#ImgSearch").removeAttr("style");
        $("#FormMenu").css("display", "none");
    }
});

function GetDataMenu(idAppl) {
    $("#menus").DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: true,
        pageLength: 5,
        lengthChange: true,
        scrollX: true,
        processing: true,
        ajax: {
            url: "/api/ApplTask/GetApplTasks?IdAppl=" + idAppl,
            method: "GET",
            beforeSend: function (xhr) {},
            dataSrc: function (json) {
                if (json.Data == null) {
                    swal({
                        title: "Gagal Menampilkan Data User",
                        text: json.Message,
                        confirmButtonClass: "btn-danger text-white",
                        confirmButtonText: "Oke, Mengerti",
                        type: "error",
                    });
                    return json;
                } else {
                    return json.Data;
                }
            },
        },
        columnDefs: [
            { targets: [0], width: "5%", visible: true },
            { targets: [1], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
            { targets: [2], width: "15%", visible: true },
        ],
        columns: [
            { data: "appl_name" },
            { data: "appl_task_name" },
            {
                data: "controller_name",
                data: "action_name",
                render: function (data, type, full, meta) {
                    return (
                        "<strong>" +
                        full.controller_name +
                        "</strong> / <strong>" +
                        full.action_name +
                        "</strong>"
                    );
                },
            },
            {
                data: "id_appl_task",
                data: "id_appl_task_parent",
                data: "id_appl",
                data: "description",
                data: "icon_name",
                render: function (data, type, full, meta) {
                    var ParamMenu =
                        "'" +
                        full.id_appl +
                        "','" +
                        full.id_appl_task +
                        "','" +
                        full.id_appl_task_parent +
                        "','" +
                        full.appl_task_name +
                        "','" +
                        full.id_appl +
                        "','" +
                        full.controller_name +
                        "','" +
                        full.action_name +
                        "','" +
                        full.description +
                        "','" +
                        full.icon_name +
                        "'";
                    var ParamHapusMenu =
                        "'" +
                        full.IdApplTask +
                        "','" +
                        full.appl_task_name +
                        "'";
                    var aksi_set_menu =
                        'onClick="EditMenu(' + ParamMenu + ');"';
                    var aksi_hapus_menu =
                        'onClick="HapusMenu(' + ParamHapusMenu + ');"';
                    data =
                        '<button type="button" class="btn btn-primary" ' +
                        aksi_set_menu +
                        '><i class="fa fa-pencil"></i> Edit</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" ' +
                        aksi_hapus_menu +
                        '><i class="fa fa-trash"></i> Hapus</button>';
                    return data;
                },
            },
        ],
        bDestroy: true,
    });
}

function CmbParentMenu(idAppl, idApplTask) {
    let htmCmb = "";
    $.ajax({
        url: "/api/ApplTask/GetApplTasks?IdAppl=" + idAppl,
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                htmCmb += `<option value=""> --Pilih Parent Menu </option>`;
                res.Data.forEach((value, index) => {
                    if (value.controller_name === null) {
                        if (value.id_appl_task == idApplTask) {
                            htmCmb += `<option value=${value.id_appl_task} selected> ${value.appl_task_name} </option>`;
                        } else {
                            htmCmb += `<option value=${value.id_appl_task}> ${value.appl_task_name} </option>`;
                        }
                    }
                });
            } else {
                htmCmb += `<option value="0"> --Data Tidak ada-- </option>`;
            }
            $("select[name=IdApplTaskParent]").empty().append(htmCmb);
        },
    });
}

function TambahMenu() {
    $("#FormMenu").trigger("reset").css("display", "block");
    $("#img-add-menu").css("display", "none");
    $("#HeaderTitle").html("Form Tambah Menu");
    let idAppl = $("select[name='IdAppl'] option").filter(":selected").val();
    let idApplToText = $("select[name='IdAppl'] option")
        .filter(":selected")
        .text();
    $("input[name='ApplName']").val(idApplToText);
    $("input[name='ApplName']").attr("readonly", true);
    CmbParentMenu(idAppl, "");
}

function EditMenu(
    id_appl,
    id_appl_task,
    id_appl_task_parent,
    appl_task_name,
    id_appl,
    controller_name,
    action_name,
    description,
    icon_name
) {
    $("#FormMenu").trigger("reset").css("display", "block");
    $("#img-add-menu").css("display", "none");
    $("#HeaderTitle").html("Edit Menu");
    let idAppl = $("select[name='IdAppl'] option").filter(":selected").val();
    let idApplToText = $("select[name='IdAppl'] option")
        .filter(":selected")
        .text();
    $("input[name='ApplName']").val(idApplToText);
    $("input[name='ApplName']").attr("readonly", true);
    CmbParentMenu(id_appl, id_appl_task_parent);
    $("input[name='ApplTaskName']").val(appl_task_name);
    $("input[name='ControllerName']").val(controller_name);
    $("input[name='ActionName']").val(action_name);

    $("textarea[name='Description']").val(description);
    $("input[name='ActionName']").val(icon_name);
}

function save_menu() {
    let idAppl = $("select[name='IdAppl'] option").filter(":selected").val();
    let idApplTaskParent = $("select[name='IdApplTaskParent'] option")
        .filter(":selected")
        .val();
    let applTaskName = $("input[name='ApplTaskName']").val();
    let controllerName = $("input[name='ControllerName']").val();
    let actionName = $("input[name='ActionName']").val();
    let iconName = $("input[name='IconName']").val();
    let description = $("textarea[name='Description']").val();
    let data = {
        IdAppl: idAppl,
        ApplTaskName: applTaskName,
        IdApplTaskParent: idApplTaskParent,
        ControllerName: controllerName,
        ActionName: actionName,
        Description: description,
        IconName: iconName,
    };

    $.ajax({
        url: "/api/ApplTask/AddApplTask",
        method: "POST",
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        let idAppl = $("select[name='IdAppl'] option")
                            .filter(":selected")
                            .val();
                        $("#menus").DataTable().ajax.reload();
                        CmbParentMenu(idAppl, "");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#menus").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}
