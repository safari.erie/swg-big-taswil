function SetFullName(FirstName, MiddleName, LastName) {
    var satu = FirstName == null ? "" : FirstName;
    var dua = MiddleName == null ? "" : MiddleName;
    var tiga = LastName == null ? "" : LastName;
    if (FirstName != null) satu += " ";
    if (MiddleName != null) dua += " ";
    if (LastName != null) tiga += " ";
    return satu + dua + tiga;
}

var TableUserManagement = $("#user-management").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: base_url + "/api/Users/GetUsers",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "15%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "Username" },
        { data: "Email" },
        {
            render: function (data, type, full, meta) {
                var data = SetFullName(
                    full.FirstName,
                    full.MiddleName,
                    full.LastName
                );
                return data;
            },
        },
        {
            data: "Roles[1].role_name",
        },
        { data: "Status" },
        {
            data: "IdUser",
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;';
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

function GetRoles(id_role) {
    let htmlCmb = "";
    $.ajax({
        url: base_url + "/api/Roles/GetRoles",
        method: "GET",
        dataType: "json",
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                htmlCmb += `<option value=""> -- Pilih Salah Satu Role -- </option>`;
                res.Data.forEach((value, index) => {
                    if (value.id_role == id_role) {
                        htmlCmb += `<option value=${value.id_role} selected> ${value.role_name} </option>`;
                    } else {
                        htmlCmb += `<option value=${value.id_role} > ${value.role_name} </option>`;
                    }
                });
            } else {
                htmlCmb += `<option value="0"> -- Data tidak ada -- </option>`;
            }

            $("select[name=Roles]").empty().append(htmlCmb);
        },
    });
}

const userPenggunaModalShow = () => {
    clearModalUser();
    $("#DivIdProv").hide();
    $("#DivIdKabkot").hide();
    $("#DivKawasanIndustri").hide();
    $("#UserManagementModal").modal({ backdrop: "static", keyboard: false });
    $("#TitleFormUser").html("Tambah Pengguna Aplikasi");
    $("#BtnSaveUser").attr("onClick", "SaveUser('Tambah');");
    $("#DivPassword", "#FormUser").show();
    GetRoles("");
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
};

function SaveUser(type) {
    let role = $("select[name='Roles'] option").filter(":selected").val();
    let Provinsi = $("select[name='IdAdmProv'] option")
        .filter(":selected")
        .val();
    let KabKot = $("select[name='IdAdmKabKot'] option")
        .filter(":selected")
        .val();
    let username = $("input[name='Username']").val();
    let password = $("input[name='Password']").val();
    let email = $("input[name='Email']").val();
    let firstname = $("input[name='FirstName']").val();
    let middleName = $("input[name='MiddleName']").val();
    let lastName = $("input[name='LastName']").val();
    let address = $("textarea[name='Address']").val();
    let phoneNumber = $("input[name='PhoneNumber']").val();
    let mobileNumber = $("input[name='MobileNumber']").val();
    let data = {
        Username: username,
        Password: password,
        Email: email,
        Firstname: firstname,
        MiddleName: middleName,
        LastName: lastName,
        Address: address,
        PhoneNumber: phoneNumber,
        MobileNumber: mobileNumber,
        Role: role,
        Provinsi: Provinsi,
        KabKot: KabKot,
    };

    $.ajax({
        url: "/api/Users/AddUser",
        method: "POST",
        dataType: "json",
        data: data,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        beforeSend: function () {
            LoadingBar("wait");
        },
        success: function (res) {
            LoadingBar("success");
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#user-management").DataTable().ajax.reload();
                        $("#UserManagementModal").modal("hide");
                    }
                );
            } else {
                console.log(res.Status);
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "error",
                    },
                    function () {
                        $("#user-management").DataTable().ajax.reload();
                        $("#UserManagementModal").modal("hide");
                    }
                );
            }
        },
    });
}

const clearModalUser = () => {
    $("input[name='Username']").val("");
    $("input[name='Password']").val("");
    $("input[name='Email']").val("");
    $("input[name='FirstName']").val("");
    $("input[name='MiddleName']").val("");
    $("input[name='LastName']").val("");
    $("input[name='PhoneNumber']").val("");
    $("input[name='MobileNumber']").val("");
    $("textarea[name='Address']").val("");
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        0,
        ""
    );
};

// onchange role
$("select[name='Roles']").change(function () {
    // if check prov show combo provinsi
    // if check kab show combo provinsi and kab
    // else hide combo prov or kab
    let role = this.value;
    if (role === "3") {
        $("#DivIdProv").show();
    } else if (role === "4") {
        $("#DivIdProv").show();
        $("#DivIdKabkot").show();
        $("#DivKawasanIndustri").hide();
    } else if (role === "5") {
        $("#DivKawasanIndustri").show();
        $("#DivIdProv").hide();
        $("#DivIdKabkot").hide();
    } else {
        $("#DivIdProv").hide();
        $("#DivIdKabkot").hide();
        $("#DivKawasanIndustri").hide();
    }
});

// onchange prov
$("select[name='IdAdmProv']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
