var dataSet = [
    {
        Provinsi: "Aceh",
        Kabupaten: "Banda Aceh",
        KodeAdm: 1118,
        NamaPendidikan: "Penddikan 1",
        Nama: "Pidie Jaya",
        Ibukota: "Meureudu",
        LuasWilayah: 57736.6,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "10Km",
    },
    {
        Provinsi: "Aceh",
        Kabupaten: "Aceh Besar",
        KodeAdm: 1175,
        NamaPendidikan: "Penddikan 2",
        Nama: "Subulussalam",
        Ibukota: "Subulussalam",
        LuasWilayah: 1175.71,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "8Km",
    },
    {
        Provinsi: "Aceh",
        Kabupaten: "Aceh",
        KodeAdm: 1101,
        NamaPendidikan: "Penddikan 3",
        Nama: "Subulussalam",
        Ibukota: "Sinabang",
        LuasWilayah: 1827.35,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "4Km",
    },
    {
        Provinsi: "Sumatera Utara",
        Kabupaten: "Aceh",
        KodeAdm: 1203,
        NamaPendidikan: "Penddikan 4",
        Nama: "Tapanuli Selatan",
        Ibukota: "Sipirok",
        LuasWilayah: 4352.86,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "15Km",
    },
    {
        Provinsi: "Sumatera Utara",
        Kabupaten: "Aceh",
        KodeAdm: 1271,
        NamaPendidikan: "Penddikan 4",
        Nama: "Sibolga",
        Ibukota: "Sibolga",
        LuasWilayah: 10.77,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "-",
    },
    {
        Provinsi: "Sumatera Barat",
        Kabupaten: "Padang",
        KodeAdm: 16,
        NamaPendidikan: "Penddikan 8",
        Nama: "Solok Selatan",
        Ibukota: "Padang Aro",
        LuasWilayah: 3346,
        NoTlp: "-",
        NoFax: "-",
        SumberData: "-",
        Kelas: "Ekonomi",
        Kategori: "-",
        TimeStart: "05:00",
        TimeEnd: "21:30",
        Fungsi: "-",
        PanjangDermaga: "-",
    },
];

var TableProvinsi = $("#pendidikan").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "15%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "10%", visible: true },
        { targets: [5], width: "10%", visible: true },
        { targets: [6], width: "10%", visible: true },
        { targets: [7], width: "10%", visible: true },
        { targets: [8], width: "10%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "Provinsi" },
        { data: "Kabupaten" },
        { data: "NamaPendidikan" },
        { data: "Kategori" },
        { data: "Kategori" },
        { data: "Fungsi" },
        { data: "Kategori" },
        { data: "Kategori" },
    ],
});

const pendidikanModalShow = () => {
    $("#pendidikanModal").modal({ backdrop: "static", keyboard: false });
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        0,
        ""
    );
    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};

$("select[name='IdAdmProv']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
