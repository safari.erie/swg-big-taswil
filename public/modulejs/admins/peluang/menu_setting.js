function SetFullName(FirstName, MiddleName, LastName) {
    var satu = FirstName == null ? "" : FirstName;
    var dua = MiddleName == null ? "" : MiddleName;
    var tiga = LastName == null ? "" : LastName;
    if (FirstName != null) satu += " ";
    if (MiddleName != null) dua += " ";
    if (LastName != null) tiga += " ";
    return satu + dua + tiga;
}

$(document).ready(function(){
    $('#keterangan').summernote({
        height: "300px",
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            },
            onMediaDelete : function(target) {
                deleteImage(target[0].src);
            }
        }
    });
    $('#keterangan_en').summernote({
        height: "300px",
        callbacks: {
            onImageUpload: function(image) {
                uploadImageEN(image[0]);
            },
            onMediaDelete : function(target) {
                deleteImageEN(target[0].src);
            }
        }
    });

    function uploadImage(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            url: "/api/Peluang/summernote",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(url) {
                $('#keterangan').summernote("insertImage", url);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
    function uploadImageEN(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            url: "/api/Peluang/summernote",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            success: function(url) {
                
                $('#keterangan_en').summernote("insertImage", url);
                $("#UserManagementModal").animate({ scrollTop: 1200 }, "slow");
            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    function deleteImage(src) {

        $.ajax({
            data: {'url' : src},
            type: "POST",
            url: "/api/Peluang/remove_summernote",
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }

    function deleteImageEN(src) {

        $.ajax({
            data: {'url' : src},
            type: "POST",
            url: "/api/Peluang/remove_summernote",
            cache: false,
            success: function(response) {
                console.log(response);
            }
        });
    }

});
 


var images = [];
var xxz = [];


function image_select() {
    var image = document.getElementById('image-upload').files;
    for (i = 0; i < image.length; i++) {
          if (check_duplicate(image[i].name)) {
     images.push({
                  "name" : 'sas',
                  "url" : URL.createObjectURL(image[i]),
                  "file" : image[i],
            })
          } else 
          {
               alert(image[i].name + " is already added to the list");
          }
    }

}
function image_show() {
    var image = "";
    images.forEach((i) => {
         image += `<div class="image_container d-flex justify-content-center position-relative">
                <img src="`+ i.url +`" alt="Image">
              
          </div>`;
    })
    return image;
}
$(document).ready(function() {
    $('#btn-file-reset-id').on('click', function() {
        $('#fileId').val('');
      });
  });
function check_duplicate(name) {
    var image = true;
    if (images.length > 0) {
        for (e = 0; e < images.length; e++) {
            if (images[e].name == name) {
                image = false;
                break;
            }
        }
    }
    return image;
}
var TablePeluang = $("#user-management").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
  
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: "/api/Peluang/GetPeluang",
        method: "GET",
        data: {
            id_peluang: 'all',
            role: testz
        },
        beforeSend: function (xhr) {},
        dataSrc: function (json,data,test) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "15%", visible: true },
        { targets: [6], width: "15%", visible: true },
     
      
    ],
    columns: [
        {
            data: "0.id_peluang",
            render: function (test, type, x, meta) {
                if ( testz == 1){
                   if(x.role_edit == 1 && x.role_delete == 1 ) {
                    var BtnEdit =
                    '<button type="button" onClick="Editmenu(' +
                    x.id_peluang +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="hapuspm(' + x.id_peluang + ',' +  x.tahun +',' +  x.id_daerah +',' +  x.id_prioritas +')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</button>';
                   } else if ( x.role_edit == 1 && x.role_delete == 0) {
                    var BtnEdit =
                    '<button type="button" onClick="Editmenu(' +
                    x.id_peluang +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;';
                } else if ( x.role_edit == 0 && x.role_delete == 1) {
                    var BtnEdit =
                    '<button type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</button>';
                } else if ( x.role_edit == 0 && x.role_delete == 0) {
                    var BtnEdit =
                    '';
   
              }
            
            } else {
               
                    if(x.role_edit == 1 && x.role_delete == 1 ) {
                        var BtnEdit =
                    
                    '<button type="button" onClick="Editmenu(' +
                    x.id_peluang +
                    ')"  class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="Hapus(' + x.id_peluang + ')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus</button>';
       
                        } else if ( x.role_edit == 1 && x.role_delete == 0) {
                            var BtnEdit =
                    
                            '<button type="button" onClick="Editmenu(' +
                            x.id_peluang +
                            ')"  class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;';
               
                        } else if ( x.role_edit == 0 && x.role_delete == 1) {
                    var BtnEdit =
                    
                    '&nbsp;&nbsp;<button type="button" onClick="Hapus(' + x.id_peluang + ')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Hapus</button>';
         } else if ( x.role_edit == 0 && x.role_delete == 0) {
            var BtnEdit =
                    
            '';

               }
              
                
            }
                
                    data = BtnEdit;
                return data;
            },
        },
        { data: "No" },
        { data: "judul" },
        { data: "daerah" },
        { data: "sektor" },
        { data: "prioritas" },
        { data: "tahun" },
        { data: "Status",
        render: function (test, type, full, meta) {
            if ( full.status == 1){
                if ( full.role_approve == 1) {
                    var BtnEdit =
                    '<button type="button" onClick="approve(' +
                    full.id_peluang +
                    ')" class="btn btn-warning btn-sm"><i class=""></i> Pending</button>&nbsp;&nbsp;';
                } else {
                    var BtnEdit =
                    '<button type="button" onClick="status(' +
                    full.id_peluang +
                    ')" class="btn btn-warning btn-sm"><i class=""></i> Pending</button>&nbsp;&nbsp;';
                }
          
            } else if (full.status == 2) {
                if ( full.role_approve == 1) {
                var BtnEdit =
                
                '<button type="button" onClick="reject(' +
                full.id_peluang +
                ')" class="btn btn-success btn-sm"><i class=""></i> Approve</button>&nbsp;&nbsp;';
                } else {

                    var BtnEdit =
                    '<button type="button" class="btn btn-success btn-sm"><i class=""></i> Approve</button>&nbsp;&nbsp;';

                }

            } else if (full.status == 3) {
                if ( full.role_approve == 1) {
                var BtnEdit =
                '<button type="button" onClick="approve(' +
                full.id_peluang +
                ')" class="btn btn-danger btn-sm"><i class=""></i> &nbsp;&nbsp;Reject&nbsp;</button>&nbsp;&nbsp;';
                } else {
                    var BtnEdit =
                    '<button type="button" onClick="status(' +
                    full.id_peluang +
                    ')"  class="btn btn-danger btn-sm"><i class=""></i> &nbsp;&nbsp;Reject&nbsp;</button>&nbsp;&nbsp;';

                }
            } else if (full.status == 4) {
                var BtnEdit =
                '<button type="button" class="btn btn-primary btn-sm"><i class=""></i> Terhapus</button>&nbsp;&nbsp;';


            }
            data = BtnEdit;
            return data;
        },
    
    },
       
    ],
    bDestroy: true,
});

function status(id) {
     
    $.ajax({
        url: "/api/Peluang/GetPeluang",
        method: "GET",
        dataType: "json",
        async: false,
        data: {
            id_peluang: id,
            role: testz
            
        },
        contentType: 'application/json; charset=utf-8',
        success: function (res) {
            TablePeluang.ajax.reload();
            res.Data.forEach((value, key) => {
            Swal.fire({
                title: 'Status Peluang',
                text: value.ketz,
              
                showCancelButton: true,
             
              })
            })
          
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("Error!", "Coba Lagi", "error");
        }
    });

      }

function approve(ids) {
    
    Swal.fire({
        title: "Siliahkan Pilih Approve/reject!",
        text: "Tulis keterangan untuk Approve dibawah ini :",
        input: 'textarea',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Approve`,
        denyButtonText: `Reject`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        var yay = result.value;
        if (result.isConfirmed) {
            $.ajax({
                url: "/api/Peluang/updatePeluang",
                type: "POST",
                data: {
                    id_peluang: ids,
                   status : 'approve',
                   keterangan : result.value,
                   user : userz
    
                },
                dataType: "html",
                success: function () {
                    TablePeluang.ajax.reload();
                    Swal.fire('File Ini telah di Approve!', '', 'success')
                  
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "Coba Lagi", "error");
                }
            });
        
       
        } else if (result.isDenied) {
         reject(ids);
   
         
        }
      })
    }
    function reject(ids) {
    
        Swal.fire({
            title: "Apa Anda yakin untuk Reject file ini?",
             text: "Tulis alasan dibawah ini :",
         input: 'textarea',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: `Reject`,
          
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    url: "/api/Peluang/updatePeluang",
                    type: "POST",
                    data: {
                        id_peluang: ids,
                       status : 'reject',
                       keterangan : result.value,
                       user : userz
        
                    },
                    dataType: "html",
                    success: function () {
                  
                        Swal.fire('File Ini telah di reject!', '', 'success')
                        TablePeluang.ajax.reload();
                      
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Coba Lagi", "error");
                    }
                });
            
           
            } 
          })
        }

    



        function Hapus(ids) {
    
            Swal.fire({
                title: "Apa anda yakin?",
                text: "Menghapus file ini!",
                showDenyButton: false,
                showCancelButton: true,
                confirmButtonText: `Hapus`,
              
              }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: "/api/Peluang/updatePeluang",
                        type: "POST",
                        data: {
                            id_peluang: ids,
                           status : 'hapus',
                           user : userz
            
                        },
                        dataType: "html",
                        success: function () {
                            TablePeluang.ajax.reload();
                            Swal.fire('File Ini telah di hapus!', '', 'success')
                          
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Coba Lagi", "error");
                        }
                    });
                
               
                } 
              })
            }

function hapuspm(ids,tahunz,daerahz,prioritasz) {
    Swal.fire({
        title: "Apa anda yakin?",
        text: "Menghapus file ini!",
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: `Hapus`,
      
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
        $.ajax({
            url: "/api/Peluang/DeletePeluang",
            type: "POST",
            data: {
                id_peluang: ids,
                tahun: tahunz,
                daerah: daerahz,
                prioritas: prioritasz

            },
            dataType: "html",
            success: function () {
                TablePeluang.ajax.reload();
               
              
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error!", "Coba Lagi", "error");
            }
        });
    } 
})
}
function myFunction() {
 
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    
    } else {
        x.style.display = "none";
    }
  }
$('#sumber').click(function(){ 
    var id=$(this).val();
    $('select[name="sumber_data"]').select2();
    $.ajax({
        url: "/api/Peluang/Getsumber",
        method : "GET",
        dataType : 'json',
        success: function(res){
          
            res.Data.forEach((value, key) => {
                var html ="";
                html += '<option value='+value.id_sumber+'>'+value.nama+'</option>';
              
               
                $('#sumber').append(html)
        

        })
    }
    });
    return false;
}); 

$('#sektor').click(function(){ 
    $('select[name="peluang"]').select2();
    $.ajax({
        url: "/api/Peluang/Getsektor",
        method : "GET",
        dataType : 'json',
        success: function(res){
          
            res.Data.forEach((value, key) => {
                var html ="";
                html += '<option value='+value.id_sektor+'>'+value.nama+'</option>';
              
               
                $('#sektor').append(html)
        

        })
    }
    });
    return false;
}); 
$('#daerah').click(function(){ 
    var id=$(this).val();
    $('select[name="daerah"]').select2();
    $.ajax({
        url: "/api/Peluang/Getdaerah",
        method : "GET",
        dataType : 'json',
        success: function(res){
          
            res.Data.forEach((value, key) => {
                var html ="";
                html += '<option value='+value.id_adm+'>'+value.nama+'</option>';
              
               
                $('#daerah').append(html)
        

        })
    }
    });
    return false;
});     
 
const userPenggunaModalShow = () => {
   
    $("#UserManagementModal").modal({ backdrop: "static", keyboard: false });
    $("#TitleFormUser").html("Tambah Peluang");
    $("#BtnSavePeluang").attr("onClick", "SavePeluang('Tambah');");
    $("#DivPassword", "#FormUser").show();
        //  $('input').val('');
        // $("select").each(function() { this.selectedIndex = 0 });
        ;
};
$("#UserManagementModal").on("hidden.bs.modal", function () {
   

  });
  function get_daerah() {
  $('select[name="daerah"]').select2();
    $.ajax({
        url: "/api/Peluang/Getdaerah",
        method : "GET",
        dataType : 'json',
        success: function(res){
          
            res.Data.forEach((value, key) => {
                var html ="";
                html += '<option value='+value.id_adm+'>'+value.nama+'</option>';
              
               
                $('#daerah').append(html)
        
            
        })
        }
    })
    }
    function get_sumber() {
        $('select[name="sumber_data"]').select2();
        $.ajax({
            url: "/api/Peluang/Getsumber",
            method : "GET",
            dataType : 'json',
            success: function(res){
              
                res.Data.forEach((value, key) => {
                    var html ="";
                    html += '<option value='+value.id_sumber+'>'+value.nama+'</option>';
                  
                   
                    $('#sumber').append(html)
            
    
            })
        }
        });
          }

          function get_sektor() {
            $('select[name="peluang"]').select2();
            $.ajax({
                url: "/api/Peluang/Getsektor",
                method : "GET",
                dataType : 'json',
                success: function(res){
                  
                    res.Data.forEach((value, key) => {
                        var html ="";
                        html += '<option value='+value.id_sektor+'>'+value.nama+'</option>';
                      
                       
                        $('#sektor').append(html)
                
        
                })
            }
            });
        }
function Editmenu(id) {
  
  
   
    $.ajax({
        url: "/api/Peluang/GetPeluang",
        method: "GET",
        dataType: "json",
        async: false,
        data: {
            id_peluang: id,
            role: testz
        },
        contentType: 'application/json; charset=utf-8',
        success: function (res) {
           

            res.Data.forEach((value, key) => {
                let text = "";
                for(let i= 0; i< value.jumlah_img; i++) {
                    var propName = 'file_' + i;
                    var yz = propName + '_tipe_1';
                  
                    text +=  `<div class="image_container d-flex justify-content-center position-relative">
                  <img src="/upload/` + value.id_daerah+ `/Peluang/`+value.tahun+`/`+value.prioritas+`/`+value[yz]+`" alt="Image">
                
            </div>`;
           
            }
            let video = "";
            for(let i= 0; i< value.jumlah_video; i++) {
                var propName = 'file_' + i;
                var yz = propName + '_tipe_2';
              
                video +=  `<div>
                <video width="500" height="240" controls> <source  src="/upload/` + value.id_daerah+ `/Peluang/`+value.tahun+`/`+value.prioritas+`/`+value[yz]+`" >
                </video>
            
        </div>`;
       
        }
        
        let pdf = "";
        for(let i= 0; i< value.jumlah_pdf; i++) {
            var propName = 'file_' + i;
            var yz = propName + '_tipe_3';
          
            pdf +=  `<div >
            <a href="/upload/` + value.id_daerah+ `/Peluang/`+value.tahun+`/`+value.prioritas+`/`+value[yz]+`" >Open `+value[yz]+`</a>
          
      </div>`;
     
        
 
   
    }
    
        
             
                document.getElementById('container').innerHTML = text;
                document.getElementById('video').innerHTML = video;
                document.getElementById('pdf').innerHTML = pdf;
          
                $("#UserManagementModal").modal({ backdrop: "static", keyboard: false });
                $("#TitleFormUser").html("Edit Peluang");
                $("#BtnSavePeluang").attr("onClick", "EditPeluang("+ value.id_peluang + ");");
                $("input[name='judul_peluang']").val(value.judul);
                $("input[name='judul_peluang_en']").val(value.judul_en);
                xxz.push({
                    "x" : -7.455070724750454,
                    "y" : 109.24804656249997,
                    
                  
              })
              $("input[name='total']").val(value.jumlah);
                $("input[name='Lon']").val(value.lon);
                $("input[name='Lat']").val(value.lat);
                $("input[name='irr']").val(value.irr);
                $("input[name='npv']").val(value.npv);
                $("input[name='pp']").val(value.pp);
                $('#keterangan').summernote('code', value.keterangan);
                $('#keterangan_en').summernote('code', value.keterangan_en);
                $('select[name="peluang"]').select2().select2('val',value.id_sektor);
                $('select[name="sumber_data"]').select2().select2('val',value.id_sumber_data);
                get_daerah();
                get_sektor();
                get_sumber();
                $('select[name="daerah"]').append('<option value='+value.id_daerah+'>'+value.daerah+'</option>');
                $('select[name="daerah"]').select2().select2('val',value.id_daerah);
                $('select[name="peluang"]').append('<option value='+value.id_sektor+'>'+value.sektor+'</option>');
                $('select[name="peluang"]').select2().select2('val',value.id_sektor);
                $('select[name="sumber_data"]').append('<option value='+value.id_sumber_data+'>'+value.sumber_data+'</option>');
                $('select[name="sumber_data"]').select2().select2('val',value.id_sumber_data);
            
              
                $('select[name="tahun"]').select2().select2('val',value.tahun);
                $('select[name="prioritas"]').select2().select2('val',value.prioritas);
                $("input[name='nilai_investasi']").val(value.nilai_investasi);
                xz();
                });
            
        },
    });

}



function EditPeluang(id) {
  
    let judul_peluang = $("input[name='judul_peluang']").val();
    
    let judul_peluang_en = $("input[name='judul_peluang_en']").val();
    let sumber_data1= $("select[name='sumber_data']").val();;
    let daerah1= $("select[name='daerah']").val();
    let peluang1= $("select[name='peluang']").val();
    let tahun1= $("select[name='tahun']").val();
    let prioritas1= $("select[name='prioritas']").val();
    let nilai_investasi1= $("input[name='nilai_investasi']").val();
    let keterangan1= $("#keterangan").val();
    let keterangan1_en= $("#keterangan_en").val();
    let total1= $("input[name='total']").val();
    let irr1= $("input[name='irr']").val();
    let npv1= $("input[name='npv']").val();
    let pp1= $("input[name='pp']").val();
    let latz= $("input[name='Lat']").val();
    let lonz= $("input[name='Lon']").val();

    var formData = new FormData(); 
    let TotalImages = $('#image-upload')[0].files.length; //Total Images
    let images = $('#image-upload')[0];
    let pdf = $('#pdf-upload')[0].files;
    let totalpdf = $('#pdf-upload')[0].files.length;
    let video = $('#video-upload')[0].files;
    let totalvideo = $('#video-upload')[0].files.length;
    for (let i = 0; i < TotalImages; i++) {
        formData.append('images' + i, images.files[i]);
    }
    formData.append('file', pdf[0]);
    formData.append('video', video[0]);
    formData.append('id_peluang', id);
    formData.append('user', userz);
    formData.append('lon', lonz);
    formData.append('lat',latz);
    formData.append('irr', irr1);
    formData.append('npv', npv1);
    formData.append('pp', pp1);
    formData.append('TotalImages', TotalImages);
    formData.append('totalpdf', totalpdf);
    formData.append('totalvideo', totalvideo);
    formData.append('id_prioritas', prioritas1);
    formData.append('total', total1);
    formData.append('judul', judul_peluang);
    formData.append('judul_en', judul_peluang_en);
    formData.append('keterangan', keterangan1);
    formData.append('keterangan_en', keterangan1_en);
    formData.append('sumber_data', sumber_data1);
    formData.append('daerah',  daerah1);
    formData.append('sektor_peluang', peluang1);
    formData.append('tahun', tahun1);
  
    formData.append('nilai_investasi',  nilai_investasi1);
  
   

    $.ajax({
        url: "/api/Peluang/updatePeluang",
        method: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                Swal.fire('File telah di update!', '', 'success')
                        $("#user-management").DataTable().ajax.reload();
                        // $('input').val('');
                        // $("select").each(function() { this.selectedIndex = 0 });
                        $("#UserManagementModal").modal("hide");
            
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#user-management").DataTable().ajax.reload();
                        $("#UserManagementModal").modal("hide");
                    }
                );
            }
        },
    });

}

    // Multiple images preview with JavaScript

    
function SavePeluang(type) {
  
    let judul_peluang = $("input[name='judul_peluang']").val();
    let judul_peluang_en = $("input[name='judul_peluang_en']").val();
    let sumber_data1= 9593;
    let daerah1= $("select[name='daerah']").val();
    let peluang1= $("select[name='peluang']").val();
    let tahun1= $("select[name='tahun']").val();
    let prioritas1= $("select[name='prioritas']").val();
    let irr1= $("input[name='irr']").val();
    let npv1= $("input[name='npv']").val();
    let pp1= $("input[name='pp']").val();
    let keterangan1= $("#keterangan").val();
    let keterangan1_en= $("#keterangan_en").val();
    let nilai_investasi1= $("input[name='nilai_investasi']").val();
    let latz= $("input[name='Lat']").val();
    let lonz= $("input[name='Lon']").val();
  

    var formData = new FormData(); 
    let TotalImages = $('#image-upload')[0].files.length; //Total Images
    let images = $('#image-upload')[0];
    let pdf = $('#pdf-upload')[0].files;;
    let video = $('#video-upload')[0].files;;
    for (let i = 0; i < TotalImages; i++) {
        formData.append('images' + i, images.files[i]);
    }
    formData.append('file', pdf[0]);
    formData.append('video', video[0]);
    formData.append('user', userz);
    formData.append('lon', lonz);
    formData.append('lat',latz);
    formData.append('irr', irr1);
    formData.append('npv', npv1);
    formData.append('pp', pp1);
    formData.append('TotalImages', TotalImages);
    formData.append('judul', judul_peluang);
    formData.append('judul_en', judul_peluang_en);
    formData.append('keterangan', keterangan1);
    formData.append('keterangan_en', keterangan1_en);
    formData.append('sumber_data', sumber_data1);
    formData.append('daerah',  daerah1);
    formData.append('sektor_peluang', peluang1);
    formData.append('tahun', tahun1);
    formData.append('id_prioritas', prioritas1);
    formData.append('nilai_investasi',  nilai_investasi1);
  
   

    $.ajax({
        url: "/api/Peluang/AddPeluang",
        method: "POST",
        dataType: "json",
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                
                Swal.fire('File telah di save!', '', 'success')
                  
                        
                        $("#user-management").DataTable().ajax.reload();
                        // $('input').val('');
                        // $("select").each(function() { this.selectedIndex = 0 });
                        $("#UserManagementModal").modal("hide");
                    
                
            } else {
                Swal.fire(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#user-management").DataTable().ajax.reload();
                        $("#UserManagementModal").modal("hide");
                    }
                );
            }
        },
    });

}
   //<![CDATA[
    // Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

    var map = null;
    var latsgn = 1;
    var lgsgn = 1;
    //var marker = null;
    var posset = 0;
    var ls = '';
    var lm = '';
    var ld = '';
    var lgs = '';
    var lgm = '';
    var lgd = '';
    var mrks = {mvcMarkers: new google.maps.MVCArray()};
    var iw;
    var drag = false;
    function xz() {
        var map;
        //var marker;
        var x_value = $("#x").val();
        var y_value = $("#y").val();
        //ll = new google.maps.LatLng(20.0, -10.0);
        if (xxz == '') {
            ll = new google.maps.LatLng(-3.299566, 118.168945);
            
        } else {
          
            ll = new google.maps.LatLng(x_value,y_value);
        }
        zoom = 5;
        var mO = {
            scaleControl: true,
            zoom: zoom,
            zoomControl: true,
            zoomControlOptions: {style: google.maps.ZoomControlStyle.LARGE},
            center: ll,
            disableDoubleClickZoom: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), mO);
        map.setTilt(0);
        map.panTo(ll);
        marker = new google.maps.Marker({
            position: ll,
            map: map,
            draggable: true,
            title: 'Marker is Draggable',
            animation: google.maps.Animation.DROP
        });
        google.maps.event.addListener(marker, 'click', function (mll) {
            gC(mll.latLng);
            var html = "<div style='color:#000;background-color:#fff;padding:5px;width:150px;'><p>Latitude - Longitude:<br />" + String(mll.latLng.toUrlValue()) + "<br /><br />Lat: " + ls + "&#176; " + lm + "&#39; " + ld + "&#34;<br />Long: " + lgs + "&#176; " + lgm + "&#39; " + lgd + "&#34;</p></div>";
            iw = new google.maps.InfoWindow({content: html});
            iw.open(map, marker);
        });
        google.maps.event.addListener(marker, 'dragstart', function () {
            if (iw) {
                iw.close();
            }
        });
        google.maps.event.addListener(marker, 'dragend', function (event) {
            posset = 1;
            if (map.getZoom() < 10) {
                map.setZoom(10);
            }
            map.setCenter(event.latLng);
            computepos(event.latLng);
            drag = true;
            setTimeout(function () {
                drag = false;
            }, 250);
        });
        google.maps.event.addListener(map, 'click', function (event) {
            if (drag) {
                return;
            }
            posset = 1;
            fc(event.latLng);
            if (map.getZoom() < 10) {
                map.setZoom(10);
            }
            map.panTo(event.latLng);
            computepos(event.latLng);
            placeMarker(event.latLng);
        });
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
     

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        // [END region_getplaces]
    }

    function placeMarker(location) {
        console.log(marker);
        if (marker) {
            //if marker already was created change positon
            marker.setPosition(location);
        } else {
            //create a marker
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });
        }
    }

    function computepos(point) {


        var ls = point.lat();
        var ls2 = point.lng();
        // var latA = Math.abs(Math.round(point.lat() * 1000000.));
        // var lonA = Math.abs(Math.round(point.lng() * 1000000.));
        // if (point.lat() < 0)
        // {
        //     var ls = '-' + Math.floor((latA / 1000000)).toString();
        // }
        // else
        // {
        //     var ls = Math.floor((latA / 1000000)).toString();
        // }
        // var lm = Math.floor(((latA / 1000000) - Math.floor(latA / 1000000)) * 60).toString();
        // var ld = (Math.floor(((((latA / 1000000) - Math.floor(latA / 1000000)) * 60) - Math.floor(((latA / 1000000) - Math.floor(latA / 1000000)) * 60)) * 100000) * 60 / 100000).toString();
        // if (point.lng() < 0)
        // {
        //     var lgs = '-' + Math.floor((lonA / 1000000)).toString();
        // }
        // else
        // {
        //     var lgs = Math.floor((lonA / 1000000)).toString();
        // }
        // var lgm = Math.floor(((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60).toString();
        // var lgd = (Math.floor(((((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60) - Math.floor(((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60)) * 100000) * 60 / 100000).toString();

        /*document.getElementById("latbox").value=point.lat().toFixed(6);
         document.getElementById("latboxm").value=ls;
         document.getElementById("latboxmd").value=lm;
         document.getElementById("latboxms").value=ld;*/

        document.getElementById("x").value = ls;
        document.getElementById("y").value = ls2;
        //document.getElementById("derajat").value=ld;


        // document.getElementById("derajat2").value = lgs;
        // document.getElementById("menit2").value = lgm;
        /*document.getElementById("lonbox").value=point.lng().toFixed(6);
         document.getElementById("lonboxm").value=lgs;
         document.getElementById("lonboxmd").value=lgm;
         document.getElementById("lonboxms").value=lgd;*/
    }

    function showAddress(address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setMapTypeId(google.maps.MapTypeId.HYBRID);
                if (map.getZoom() < 16) {
                    map.setZoom(16);
                } else {
                }
                marker.setPosition(results[0].geometry.location);
                posset = 1;
                computepos(results[0].geometry.location);
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }

    function showLatLong(latitude, longitude) {
        if (isNaN(latitude)) {
            alert(' Latitude must be a number. e.g. Use +/- instead of N/S');
            return false;
        }
        if (isNaN(longitude)) {
            alert(' Longitude must be a number.  e.g. Use +/- instead of E/W');
            return false;
        }

        latitude1 = Math.abs(Math.round(latitude * 1000000.));
        if (latitude1 > (90 * 1000000)) {
            alert(' Latitude must be between -90 to 90. ');
            document.getElementById("latbox1").value = '';
            return;
        }
        longitude1 = Math.abs(Math.round(longitude * 1000000.));
        if (longitude1 > (180 * 1000000)) {
            alert(' Longitude must be between -180 to 180. ');
            document.getElementById("lonbox1").value = '';
            return;
        }

        var point = new google.maps.LatLng(latitude, longitude);
        posset = 1;
        if (map.getZoom() < 7) {
            map.setZoom(7);
        } else {
        }
        map.setMapTypeId(google.maps.MapTypeId.HYBRID);
        map.setCenter(point);
        fc(point);
        computepos(point);
    }

    function showLatLong1(latitude, latitudem, latitudes, longitude, longitudem, longitudes) {
        if (isNaN(latitude)) {
            alert(' Latitude must be a number. e.g. Use +/- instead of N/S');
            return false;
        }
        if (isNaN(latitudem)) {
            alert(' Latitude must be a number. e.g. Use +/- instead of N/S');
            return false;
        }
        if (isNaN(latitudes)) {
            alert(' Latitude must be a number. e.g. Use +/- instead of N/S');
            return false;
        }
        if (isNaN(longitude)) {
            alert(' Longitude must be a number.  e.g. Use +/- instead of E/W');
            return false;
        }
        if (isNaN(longitudem)) {
            alert(' Longitude must be a number.  e.g. Use +/- instead of E/W');
            return false;
        }
        if (isNaN(longitudes)) {
            alert(' Longitude must be a number.  e.g. Use +/- instead of E/W');
            return false;
        }

        if (latitude.indexOf('-') < 0) {
            latsgn = 1;
        } else {
            latsgn = -1;
        }
        alat = Math.abs(Math.round(latitude * 1000000.));
        if (alat > (90 * 1000000)) {
            alert(' Degrees Latitude must be between -90 to 90. ');
            document.getElementById("latbox1m").value = '';
            document.getElementById("latbox1md").value = '';
            document.getElementById("latbox1ms").value = '';
            return;
        }
        latitudem = Math.abs(Math.round(latitudem * 1000000.) / 1000000); //integer
        absmlat = Math.abs(Math.round(latitudem * 1000000.)); //integer
        if (absmlat >= (60 * 1000000)) {
            alert(' Minutes Latitude must be between 0 to 59. ');
            document.getElementById("latbox1md").value = '';
            document.getElementById("latbox1ms").value = '';
            return;
        }
        latitudes = Math.abs(Math.round(latitudes * 1000000.) / 1000000);
        absslat = Math.abs(Math.round(latitudes * 1000000.));
        if (absslat > (59.99999999 * 1000000)) {
            alert(' Seconds Latitude must be between 0 and 59.99. ');
            document.getElementById("latbox1ms").value = '';
            return;
        }

        if (longitude.indexOf('-') < 0) {
            lgsgn = 1;
        } else {
            lgsgn = -1;
        }
        alon = Math.abs(Math.round(longitude * 1000000.));
        if (alon > (180 * 1000000)) {
            alert(' Degrees Longitude must be between -180 to 180. ');
            document.getElementById("lonbox1m").value = '';
            document.getElementById("lonbox1md").value = '';
            document.getElementById("lonbox1ms").value = '';
            return;
        }
        longitudem = Math.abs(Math.round(longitudem * 1000000.) / 1000000);
        absmlon = Math.abs(Math.round(longitudem * 1000000));
        if (absmlon >= (60 * 1000000)) {
            alert(' Minutes Longitude must be between 0 to 59. ');
            document.getElementById("lonbox1md").value = '';
            document.getElementById("lonbox1ms").value = '';
            return;
        }
        longitudes = Math.abs(Math.round(longitudes * 1000000.) / 1000000);
        absslon = Math.abs(Math.round(longitudes * 1000000.));
        if (absslon > (59.99999999 * 1000000)) {
            alert(' Seconds Longitude must be between 0 and 59.99. ');
            document.getElementById("lonbox1ms").value = '';
            return;
        }

        latitude = Math.round(alat + (absmlat / 60.) + (absslat / 3600.)) * latsgn / 1000000;
        longitude = Math.round(alon + (absmlon / 60) + (absslon / 3600)) * lgsgn / 1000000;
        var point = new google.maps.LatLng(latitude, longitude);
        posset = 1;
        if (map.getZoom() < 7) {
            map.setZoom(7);
        } else {
        }
        map.setMapTypeId(google.maps.MapTypeId.HYBRID);
        map.setCenter(point);
        fc(point);
        computepos(point);
    }

    function streetview() {
        if (posset === 0) {
            alert("Position Not Set.  Please click on the spot on the map to set the street view point.");
            return;
        }

        var point = map.getCenter();
        var t1 = String(point);
        t1 = t1.replace(/[() ]+/g, "");
        var str = "http://www.satelliteview.co/?e=" + t1 + ":0:Latitude-Longitude Point:sv:0";
        var popup = window.open(str, "llwindow");
        popup.focus();
    }

    function googleearth() {
        if (posset === 0) {
            alert("Position Not Set.  Please click on the spot on the map to set the Google Earth map point.");
            return;
        }
        var point = map.getCenter();
        var gearth_str = "/?r=googleearth&mt=Latitude-Longitude Point&ml=" + point.lat() + "&mg=" + point.lng();
        var popup = window.open(gearth_str, "llwindow");
        popup.focus();
    }

    function pictures() {
        if (posset === 0) {
            alert("Position Not Set.  Please click on the spot on the map to set the photograph point.");
            return;
        }
        var point = map.getCenter();
        var pictures_str = "http://www.picturepastime.com?r=pictures&mt=Latitude-Longitude Point&ml=" + point.lat() + "&mg=" + point.lng();
        var popup = window.open(pictures_str, "llwindow");
        popup.focus();
    }

    function lotsize() {
        if (posset === 0) {
            alert("Position Not Set.  Please click on the spot on the map to set the lot size map point.");
            return;
        }
        var point = map.getCenter();
        var t1 = String(point);
        t1 = t1.replace(/[() ]+/g, "");
        var lotsize_str = "http://www.satelliteview.co/?e=" + t1 + ":0:Latitude-Longitude Point:measure:0";
        var popup = window.open(lotsize_str, "llwindow");
        popup.focus();
    }

    function getaddress() {
        if (posset === 0) {
            alert("Position Not Set.  Please click on the spot on the map to set the get address map point.");
            return;
        }
        var point = map.getCenter();
        var t1 = String(point);
        t1 = t1.replace(/[() ]+/g, "");
        var getaddr_str = "http://www.satelliteview.co/?e=" + t1 + ":0:Latitude-Longitude Point:map:0";
        var popup = window.open(getaddr_str, "llwindow");
        popup.focus();
    }

    function fc(point) {
        gC(point);
        var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(point.toUrlValue()) + "<br /><br />Lat: " + ls + "&#176; " + lm + "&#39; " + ld + "&#34;<br />Long: " + lgs + "&#176; " + lgm + "&#39; " + lgd + "&#34;</p></div>";
        var iw = new google.maps.InfoWindow({content: html});
        var marker = new google.maps.Marker({position: point, map: map, draggable: true});
        mrks.mvcMarkers.push(marker);
        google.maps.event.addListener(marker, 'click', function (event) {
            gC(event.latLng);
            var html = "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" + String(event.latLng.toUrlValue()) + "<br /><br />Lat: " + ls + "&#176; " + lm + "&#39; " + ld + "&#34;<br />Long: " + lgs + "&#176; " + lgm + "&#39; " + lgd + "&#34;</p></div>";
            var iw = new google.maps.InfoWindow({content: html});
            iw.open(map, marker);
            computepos(event.latLng);
        });
    }

    function rL() {
        var marker = mrks.mvcMarkers.pop();
        if (marker) {
            marker.setMap(null);
            document.getElementById("latbox").value = '';
            document.getElementById("latboxm").value = '';
            document.getElementById("latboxmd").value = '';
            document.getElementById("latboxms").value = '';
            document.getElementById("lonbox").value = '';
            document.getElementById("lonboxm").value = '';
            document.getElementById("lonboxmd").value = '';
            document.getElementById("lonboxms").value = '';
        }
    }

    function gC(ll) {
        var latA = Math.abs(Math.round(ll.lat() * 1000000.));
        var lonA = Math.abs(Math.round(ll.lng() * 1000000.));
        if (ll.lat() < 0) {
            var tls = '-' + Math.floor((latA / 1000000)).toString();
        }
        else {
            var tls = Math.floor((latA / 1000000)).toString();
        }
        var tlm = Math.floor(((latA / 1000000) - Math.floor(latA / 1000000)) * 60).toString();
        var tld = (Math.floor(((((latA / 1000000) - Math.floor(latA / 1000000)) * 60) - Math.floor(((latA / 1000000) - Math.floor(latA / 1000000)) * 60)) * 100000) * 60 / 100000).toString();
        ls = tls.toString();
        lm = tlm.toString();
        ld = tld.toString();
        if (ll.lng() < 0) {
            var tlgs = '-' + Math.floor((lonA / 1000000)).toString();
        }
        else {
            var tlgs = Math.floor((lonA / 1000000)).toString();
        }
        var tlgm = Math.floor(((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60).toString();
        var tlgd = (Math.floor(((((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60) - Math.floor(((lonA / 1000000) - Math.floor(lonA / 1000000)) * 60)) * 100000) * 60 / 100000).toString();
        lgs = tlgs.toString();
        lgm = tlgm.toString();
        lgd = tlgd.toString();
    }

    function reset() {
        mrks.mvcMarkers.forEach(function (elem, index) {
            elem.setMap(null);
        });
        mrks.mvcMarkers.clear();
        document.getElementById("latbox").value = '';
        document.getElementById("latboxm").value = '';
        document.getElementById("latboxmd").value = '';
        document.getElementById("latboxms").value = '';
        document.getElementById("lonbox").value = '';
        document.getElementById("lonboxm").value = '';
        document.getElementById("lonboxmd").value = '';
        document.getElementById("lonboxms").value = '';
        marker.setPosition(map.getCenter());
    }

    function reset1() {
        marker.setPosition(map.getCenter());
        computepos(map.getCenter());
    }

    function hide_map() {
        $('#wrapper').toggle('fade');
        var elem = document.getElementById("sembunyi");
        if (elem.value === "Sembunyikan Peta")
            elem.value = "Tampilkan Peta";
        else
            elem.value = "Sembunyikan Peta";
    }


    //]]>