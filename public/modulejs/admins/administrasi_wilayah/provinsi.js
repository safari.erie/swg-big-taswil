var dataSet = [
    {
        KodeAdm: 11,
        Nama: "Aceh",
        Ibukota: "Banda Aceh",
        LuasWilayah: 57736.6,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        KodeAdm: 12,
        Nama: "Sumatera Utara",
        Ibukota: "Medan",
        LuasWilayah: 71680.7,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        KodeAdm: 13,
        Nama: "Sumatera Barat",
        Ibukota: "Padang",
        LuasWilayah: 42297.3,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        KodeAdm: 14,
        Nama: "Riau",
        Ibukota: "Pekan Baru",
        LuasWilayah: 329868,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        KodeAdm: 15,
        Nama: "Jambi",
        Ibukota: "Jambi",
        LuasWilayah: 53435,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        KodeAdm: 16,
        Nama: "Sumatera Selatan",
        Ibukota: "Palembang",
        LuasWilayah: 87017.4,
        NoTlp: "-",
        NoFax: "-",
    },
];

var TableProvinsi = $("#provinsis").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "15%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "10%", visible: true },
        { targets: [5], width: "10%", visible: true },
        { targets: [6], width: "10%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "KodeAdm" },
        { data: "Nama" },
        { data: "Ibukota" },
        { data: "LuasWilayah" },
        { data: "NoTlp" },
        { data: "NoFax" },
    ],
});

const ProvinsiModalShow = () => {
    $("#Provinsi").modal({ backdrop: "static", keyboard: false });
    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};
