var dataSet = [
    {
        Provinsi: "Aceh",
        KodeAdm: 1118,
        Nama: "Pidie Jaya",
        Ibukota: "Meureudu",
        LuasWilayah: 57736.6,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        Provinsi: "Aceh",
        KodeAdm: 1175,
        Nama: "Subulussalam",
        Ibukota: "Subulussalam",
        LuasWilayah: 1175.71,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        Provinsi: "Aceh",
        KodeAdm: 1101,
        Nama: "Simeulue",
        Ibukota: "Sinabang",
        LuasWilayah: 1827.35,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        Provinsi: "Sumatera Utara",
        KodeAdm: 1203,
        Nama: "Tapanuli Selatan",
        Ibukota: "Sipirok",
        LuasWilayah: 4352.86,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        Provinsi: "Sumatera Utara",
        KodeAdm: 1271,
        Nama: "Sibolga",
        Ibukota: "Sibolga",
        LuasWilayah: 10.77,
        NoTlp: "-",
        NoFax: "-",
    },
    {
        Provinsi: "Sumatera Barat",
        KodeAdm: 16,
        Nama: "Solok Selatan",
        Ibukota: "Padang Aro",
        LuasWilayah: 3346,
        NoTlp: "-",
        NoFax: "-",
    },
];

var TableProvinsi = $("#kabupatens").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "15%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "10%", visible: true },
        { targets: [5], width: "10%", visible: true },
        { targets: [6], width: "10%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "Provinsi" },
        { data: "KodeAdm" },
        { data: "Nama" },
        { data: "Ibukota" },
        { data: "LuasWilayah" },
        { data: "NoTlp" },
        { data: "NoFax" },
    ],
});

const KabupatenModalShow = () => {
    $("#Kabupaten").modal({ backdrop: "static", keyboard: false });
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};
