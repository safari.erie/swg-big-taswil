function ComboGetWilayahProvinsi(handleData, IdAdmProvinsi) {
    var Url = "/api/WilayahAdm/provinsis";
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.id_adm_provinsi == IdAdmProvinsi) {
                        html_combo += `<option value=${value.id_adm_provinsi} selected> ${value.nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.id_adm_provinsi}> ${value.nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}

function ComboGetWilayahKabKota(handleData, IdAdmProvinsi, IdAdmKabKota) {
    var Url = "/api/WilayahAdm/KabKota?IdAdmProvinsi=" + IdAdmProvinsi;
    $.ajax({
        url: Url,
        method: "GET",
        beforeSend: function (before) {},
        success: function (res) {
            if (res.Status == true) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                res.Data.forEach((value, key) => {
                    if (value.IdAdmKabKot == IdAdmKabKota) {
                        html_combo += `<option value=${value.IdAdmKabKot} selected> ${value.Nama} </option>`;
                    }
                    {
                        html_combo += `<option value=${value.IdAdmKabKot}> ${value.Nama} </option>`;
                    }
                });
                handleData(html_combo);
            } else if (res.Status == false) {
                var html_combo = "";
                html_combo = '<option value="0">- Pilih Salah Satu -</option>';
                /* swal({
                    title: "Gagal",
                    text: res.ReturnMessage,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                }); */
                handleData(html_combo);
            }
        },
        error: function (responserror, a, e) {
            swal({
                title: "Error :(",
                text: JSON.stringify(responserror) + " : " + e,
                confirmButtonClass: "btn-danger text-white",
                confirmButtonText: "Oke, Mengerti",
                type: "error",
            });
        },
    });
}
