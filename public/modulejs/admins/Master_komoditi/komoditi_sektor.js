
var dataSet = [
    [ "1", "Perkebunan"],
    [ "2", "Perternakan"],
    [ "3", "Perikanan"],
    [ "4", "Pertanian"]

];
$(document).ready(function() {
    $('#Komoditi').DataTable( {
        data: dataSet,
        columnDefs: [
                    { targets: [0], width: "5%", visible: true },
                    { targets: [1], width: "55%", visible: true },
                    { targets: [2], width: "25%", visible: true },
                  
                ],
        columns: [
            { title: "No" },
            { title: "Komoditi" },
          
            {
                            title: "Aksi",
                            render: function (data, type, full, meta) {
                                var BtnEdit =
                                    '<button type="button" onClick="EditUser(' +
                                    full.IdUser +
                                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                                    full.IdUser +
                                    ')" class="btn btn-danger btn-sm"><i class="fa fa-Trash"></i> Hapus</button>';
                                data = BtnEdit;
                                return data;
                            },
                        },
          
        ]
    } );
} );
// var Komoditi = $("#Komoditi").DataTable({
//     paging: true,
//     searching: true,
//     ordering: true,
//     info: true,
//     pageLength: 10,
//     serverSide: true,
//     lengthChange: true,
//     scrollX: true,
//     processing: true,
//     data : dataSet,
//     // ajax: {
//     //     url: "/api/Peluang/GetPeluang",
//     //     method: "GET",
//     //     beforeSend: function (xhr) {},
//     //     dataSrc: function (json) {
//     //         if (json.Data == null) {
//     //             swal({
//     //                 title: "Gagal Menampilkan Data User",
//     //                 text: json.Message,
//     //                 confirmButtonClass: "btn-danger text-white",
//     //                 confirmButtonText: "Oke, Mengerti",
//     //                 type: "error",
//     //             });
//     //             return json;
//     //         } else {
//     //             return json.Data;
//     //         }
//     //     },
//     // },
//     columnDefs: [
//         { targets: [0], width: "5%", visible: true },
//         { targets: [1], width: "25%", visible: true },
//         { targets: [2], width: "25%", visible: true },
//         { targets: [3], width: "40%", visible: true },
    
//     ],
//     columns: [
//         { data: "No" },
//         { data: "Address" },
//         { data: "Address" },
//         {
//             data: "IdUser",
//             render: function (data, type, full, meta) {
//                 var BtnEdit =
//                     '<button type="button" onClick="EditUser(' +
//                     full.IdUser +
//                     ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;';
//                 data = BtnEdit;
//                 return data;
//             },
//         },
//     ],
//     bDestroy: true,
// });



const KomoditiModalShow = () => {
    $("#KomoditiModal").modal({ backdrop: "static", keyboard: false });
    $("#TitleFormUser").html("Sektor Komoditi");
    $("#BtnSavePeluang").attr("onClick", "SavePeluang('Tambah');");
    $("#DivPassword", "#FormUser").show();
    
};

// function SaveKomoditi(type) {
  
//     let judul_peluang = $("input[name='judul_peluang']").val();
//     let daerah1= $("select[name='daerah']").val();

  
//     let data = {
//         judul: judul_peluang,
//         daerah: daerah1,
      
//     };

//     // $.ajax({
//     //     url: "/api/Peluang/AddPeluang",
//     //     method: "POST",
//     //     dataType: "json",
//     //     data: data,
//     //     beforeSend: function () {},
//     //     success: function (res) {
//     //         if (res.Status) {
//     //             swal(
//     //                 {
//     //                     title: "Konfirmasi",
//     //                     text: res.Message,
//     //                     type: "success",
//     //                 },
//     //                 function () {
//     //                     $("#Komoditi").DataTable().ajax.reload();
//     //                     $("#KomoitiModal").modal("hide");
//     //                 }
//     //             );
//     //         } else {
//     //             swal(
//     //                 {
//     //                     title: "Konfirmasi",
//     //                     text: res.Message,
//     //                     type: "danger",
//     //                 },
//     //                 function () {
//     //                     $("#Komoditi").DataTable().ajax.reload();
//     //                     $("#KomoditiModal").modal("hide");
//     //                 }
//     //             );
//     //         }
//     //     },
//     // });
// }
