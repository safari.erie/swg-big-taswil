let TableMasterKbli = $("#master-kbli").DataTable({
    paging: true,
    searching: true,
    ordering: true,
    info: true,
    pageLength: 10,
    lengthChange: true,
    scrollX: true,
    processing: true,
    ajax: {
        url: "/api/Umkm/GetKblis",
        method: "GET",
        beforeSend: function (xhr) {},
        dataSrc: function (json) {
            if (json.Data == null) {
                swal({
                    title: "Gagal Menampilkan Data User",
                    text: json.Message,
                    confirmButtonClass: "btn-danger text-white",
                    confirmButtonText: "Oke, Mengerti",
                    type: "error",
                });
                return json;
            } else {
                return json.Data;
            }
        },
    },
    columnDefs: [
        { targets: [0], width: "5%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "15%", visible: true },
        { targets: [6], width: "15%", visible: true },
        { targets: [7], width: "15%", visible: true },
        { targets: [8], width: "15%", visible: true },
    ],
    columns: [
        { data: "No" },
        { data: "KdKategori" },
        { data: "KdGolPokok" },
        { data: "KdGol" },
        { data: "KdSubGol" },
        { data: "KdKbli" },
        { data: "NmKbli" },
        { data: "DesKbli" },
        {
            data: "IdPrs",
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="ShowDetail(' +
                    full.IdPrs +
                    ')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Show</button> ' +
                    "<button class='btn btn-success mr-2'> <i class='fa fa-check' aria-hidden='true'></i> Edit </button>" +
                    "<button class='btn btn-danger mr-2'> <i class='fa fa-trash' aria-hidden='true'></i>  Hapus</button>";
                data = BtnEdit;
                return data;
            },
        },
    ],
    bDestroy: true,
});

const kbliModalShow = () => {
    $("#masterKbliModal").modal();
    $("#btnSaveKbli").attr("onClick", "SaveKbli('Tambah');");
};

function SaveKbli(type) {
    let KdKateggori = $("input[name='KdKateggori']").val();
    let KdGolPokok = $("input[name='KdGolPokok']").val();
    let KdGol = $("input[name='KdGol']").val();
    let KdSubGol = $("input[name='KdSubGol']").val();
    let KdKbli = $("input[name='KdKbli']").val();
    let NmKbli = $("input[name='NmKbli']").val();
    let DesKbli = $("textarea[name='DesKbli']").val();
    if (type === "Tambah") {
        url = "/api/Umkm/AddKbli";
        data = {
            KdKategori: KdKateggori,
            KdGolPokok: KdGolPokok,
            KdGol: KdGol,
            KdSubGol: KdSubGol,
            KdKbli: KdKbli,
            NmKbli: NmKbli,
            DesKbli: DesKbli,
        };
    }

    $.ajax({
        url: url,
        method: "POST",
        dataType: "json",
        data: data,
        beforeSend: function () {},
        success: function (res) {
            if (res.Status) {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "success",
                    },
                    function () {
                        $("#master-kbli").DataTable().ajax.reload();
                        $("#masterKbliModal").modal("hide");
                    }
                );
            } else {
                swal(
                    {
                        title: "Konfirmasi",
                        text: res.Message,
                        type: "danger",
                    },
                    function () {
                        $("#master-kbli").DataTable().ajax.reload();
                    }
                );
            }
        },
    });
}
