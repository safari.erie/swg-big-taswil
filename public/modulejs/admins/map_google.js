var map = null;
var latsgn = 1;
var lgsgn = 1;
//var marker = null;
var posset = 0;
var ls = "";
var lm = "";
var ld = "";
var lgs = "";
var lgm = "";
var lgd = "";
var mrks = { mvcMarkers: new google.maps.MVCArray() };
var iw;
var drag = false;
function initialize(x_value, y_value) {
    console.log(x_value.type);
    var map;
    //var marker;
    //ll = new google.maps.LatLng(20.0, -10.0);
    if (x_value.type === "load" && y_value.type === "load") {
        ll = new google.maps.LatLng(-3.299566, 118.168945);
    } else if (x_value !== 0 && y_value !== 0) {
        ll = new google.maps.LatLng(x_value, y_value);
    } else {
        ll = new google.maps.LatLng(-3.299566, 118.168945);
    }
    zoom = 5;
    var mO = {
        scaleControl: true,
        zoom: zoom,
        zoomControl: true,
        zoomControlOptions: { style: google.maps.ZoomControlStyle.LARGE },
        center: ll,
        disableDoubleClickZoom: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    map = new google.maps.Map(document.getElementById("map"), mO);
    map.setTilt(0);
    map.panTo(ll);
    marker = new google.maps.Marker({
        position: ll,
        map: map,
        draggable: true,
        title: "Marker is Draggable",
        animation: google.maps.Animation.DROP,
    });
    google.maps.event.addListener(marker, "click", function (mll) {
        gC(mll.latLng);
        var html =
            "<div style='color:#000;background-color:#fff;padding:5px;width:150px;'><p>Latitude - Longitude:<br />" +
            String(mll.latLng.toUrlValue()) +
            "<br /><br />Lat: " +
            ls +
            "&#176; " +
            lm +
            "&#39; " +
            ld +
            "&#34;<br />Long: " +
            lgs +
            "&#176; " +
            lgm +
            "&#39; " +
            lgd +
            "&#34;</p></div>";
        iw = new google.maps.InfoWindow({ content: html });
        iw.open(map, marker);
    });
    google.maps.event.addListener(marker, "dragstart", function () {
        if (iw) {
            iw.close();
        }
    });
    google.maps.event.addListener(marker, "dragend", function (event) {
        posset = 1;
        if (map.getZoom() < 10) {
            map.setZoom(10);
        }
        map.setCenter(event.latLng);
        computepos(event.latLng);
        drag = true;
        setTimeout(function () {
            drag = false;
        }, 250);
    });
    google.maps.event.addListener(map, "click", function (event) {
        if (drag) {
            return;
        }
        posset = 1;
        fc(event.latLng);
        if (map.getZoom() < 10) {
            map.setZoom(10);
        }
        map.panTo(event.latLng);
        computepos(event.latLng);
        placeMarker(event.latLng);
    });
    // Create the search box and link it to the UI element.
    var input = document.getElementById("pac-input");
    setTimeout(function () {
        console.log(input);
    }, 3000);
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", function () {
        searchBox.set("map", null);

        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25),
            };

            // Create a marker for each place.
            markers.push(
                new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location,
                })
            );

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
    // [END region_getplaces]
}

function placeMarker(location) {
    console.log(marker);
    if (marker) {
        //if marker already was created change positon
        marker.setPosition(location);
    } else {
        //create a marker
        marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true,
        });
    }
}

function computepos(point) {
    var ls = point.lat();
    var ls2 = point.lng();
    console.log(ls);
    document.getElementById("x").value = ls;
    document.getElementById("y").value = ls2;
}

function showAddress(address) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setMapTypeId(google.maps.MapTypeId.HYBRID);
            if (map.getZoom() < 16) {
                map.setZoom(16);
            } else {
            }
            marker.setPosition(results[0].geometry.location);
            posset = 1;
            computepos(results[0].geometry.location);
        } else {
            alert(
                "Geocode was not successful for the following reason: " + status
            );
        }
    });
}

function fc(point) {
    gC(point);
    var html =
        "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" +
        String(point.toUrlValue()) +
        "<br /><br />Lat: " +
        ls +
        "&#176; " +
        lm +
        "&#39; " +
        ld +
        "&#34;<br />Long: " +
        lgs +
        "&#176; " +
        lgm +
        "&#39; " +
        lgd +
        "&#34;</p></div>";
    var iw = new google.maps.InfoWindow({ content: html });
    var marker = new google.maps.Marker({
        position: point,
        map: map,
        draggable: true,
    });
    mrks.mvcMarkers.push(marker);
    google.maps.event.addListener(marker, "click", function (event) {
        gC(event.latLng);
        var html =
            "<div style='color:#000;background-color:#fff;padding:3px;width:150px;'><p>Latitude - Longitude:<br />" +
            String(event.latLng.toUrlValue()) +
            "<br /><br />Lat: " +
            ls +
            "&#176; " +
            lm +
            "&#39; " +
            ld +
            "&#34;<br />Long: " +
            lgs +
            "&#176; " +
            lgm +
            "&#39; " +
            lgd +
            "&#34;</p></div>";
        var iw = new google.maps.InfoWindow({ content: html });
        iw.open(map, marker);
        computepos(event.latLng);
    });
}

function rL() {
    var marker = mrks.mvcMarkers.pop();
    if (marker) {
        marker.setMap(null);
        document.getElementById("latbox").value = "";
        document.getElementById("latboxm").value = "";
        document.getElementById("latboxmd").value = "";
        document.getElementById("latboxms").value = "";
        document.getElementById("lonbox").value = "";
        document.getElementById("lonboxm").value = "";
        document.getElementById("lonboxmd").value = "";
        document.getElementById("lonboxms").value = "";
    }
}

function gC(ll) {
    var latA = Math.abs(Math.round(ll.lat() * 1000000));
    var lonA = Math.abs(Math.round(ll.lng() * 1000000));
    if (ll.lat() < 0) {
        var tls = "-" + Math.floor(latA / 1000000).toString();
    } else {
        var tls = Math.floor(latA / 1000000).toString();
    }
    var tlm = Math.floor(
        (latA / 1000000 - Math.floor(latA / 1000000)) * 60
    ).toString();
    var tld = (
        (Math.floor(
            ((latA / 1000000 - Math.floor(latA / 1000000)) * 60 -
                Math.floor(
                    (latA / 1000000 - Math.floor(latA / 1000000)) * 60
                )) *
                100000
        ) *
            60) /
        100000
    ).toString();
    ls = tls.toString();
    lm = tlm.toString();
    ld = tld.toString();
    if (ll.lng() < 0) {
        var tlgs = "-" + Math.floor(lonA / 1000000).toString();
    } else {
        var tlgs = Math.floor(lonA / 1000000).toString();
    }
    var tlgm = Math.floor(
        (lonA / 1000000 - Math.floor(lonA / 1000000)) * 60
    ).toString();
    var tlgd = (
        (Math.floor(
            ((lonA / 1000000 - Math.floor(lonA / 1000000)) * 60 -
                Math.floor(
                    (lonA / 1000000 - Math.floor(lonA / 1000000)) * 60
                )) *
                100000
        ) *
            60) /
        100000
    ).toString();
    lgs = tlgs.toString();
    lgm = tlgm.toString();
    lgd = tlgd.toString();
}

// event jendela di-load
//google.maps.event.addDomListener(window, "load", initialize);
