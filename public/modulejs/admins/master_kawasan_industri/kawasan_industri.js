var dataSet = [
    {
        SumberData:
            "Kawasan Industri - Indonesia industrial estate directory 2015/2016",
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Medan Star Industrial Estate",
        Keterangan: "-",
        Alamat: "Jl. Raya Medan-Lubuk Pakam, Tj. Morawa B, Tj. Morawa, Kabupaten Deli Serdang, Sumatera Utara",
        Luas: "103",
        Status: "2",
    },
    {
        SumberData:
            "Kawasan Industri - Indonesia industrial estate directory 2015/2016",
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Kawasan Industrial Medan",
        Keterangan: "-",
        Alamat: "Wisma Kawasan Industri Medan, Jalan Pulau Batam No.1 Kompleks KIM Tahap 2, Medan, Sumatera Utara",
        Luas: "960",
        Status: "2",
    },
    {
        SumberData:
            "Kawasan Industri - Indonesia industrial estate directory 2015/2016",
        Kategori: "Kawasan Ekonomi Khusus",
        Nama: "Padang Industrial Park",
        Keterangan: "-",
        Alamat: "Jl. H. Agus Salim No.17, Sawahan Tim., Padang Tim., Kota Padang, Sumatera Barat",
        Luas: "616",
        Status: "2",
    },
];

var TableKategori = $("#kawasan-industris").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "20%", visible: true },
        { targets: [1], width: "15%", visible: true },
        { targets: [2], width: "15%", visible: true },
        { targets: [3], width: "15%", visible: true },
        { targets: [4], width: "15%", visible: true },
        { targets: [5], width: "15%", visible: true },
        { targets: [6], width: "15%", visible: true },
        { targets: [7], width: "15%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus &nbsp;&nbsp;</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "SumberData" },
        { data: "Kategori" },
        { data: "Nama" },
        { data: "Keterangan" },
        { data: "Alamat" },
        { data: "Luas" },
        {
            data: "Status",
            render: function (data, type, full, meta) {
                let strStatus = "";
                if (full.Status === "1") {
                    strStatus += `<span class="badge badge-secondary"> Inisialisasi</span>`;
                } else {
                    strStatus += `<span class="badge badge-primary"> Approve</span>`;
                }

                return strStatus;
            },
        },
    ],
});
const KawasanIndustriModalShow = () => {
    $("#ModalKawasanIndustri").modal({
        backdrop: "static",
        keyboard: false,
    });
    ComboGetWilayahProvinsi(function (obj) {
        $("select#IdAdmProv").html(obj);
    });
    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};

$("select[name='IdAdmProv']").change(function () {
    ComboGetWilayahKabKota(
        function (HtmlCombo) {
            $("select[name='IdAdmKabKot']").empty();
            $("select[name='IdAdmKabKot']").append(HtmlCombo);
        },
        this.value,
        ""
    );
});
