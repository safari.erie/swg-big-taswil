var dataSet = [
    {
        Nama: "Not Available",
    },
    {
        Nama: "Kawasan Ekonomi Khusus",
    },
    {
        Nama: "Kawasan Industri HKL",
    },
];

var TableProvinsi = $("#kategori-kawasan-industris").DataTable({
    data: dataSet,
    columnDefs: [
        { targets: [0], width: "15%", visible: true },
        { targets: [1], width: "15%", visible: true },
    ],
    columns: [
        {
            render: function (data, type, full, meta) {
                var BtnEdit =
                    '<button type="button" onClick="EditProvinsi(' +
                    full.IdUser +
                    ')" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit</button>&nbsp;&nbsp;<button type="button" onClick="EditUser(' +
                    full.IdUser +
                    ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>';
                data = BtnEdit;
                return data;
            },
        },
        { data: "Nama" },
    ],
});

const KategoriKawasanModalShow = () => {
    $("#kategoriKawasanIndustri").modal({
        backdrop: "static",
        keyboard: false,
    });

    HideTransalte();
};

const ShowTranslate = () => {
    $("#btnTrasnalte").attr("onClick", "HideTransalte();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Hide Transalte`
    );
    $("#ColumnTransalte").show();
    $("#ColumnTransalte").fadeIn(50000);
};

const HideTransalte = () => {
    $("#btnTrasnalte").attr("onClick", "ShowTranslate();");
    $("#btnTrasnalte").html(
        `<i class="fa fa-language" aria-hidden="true"></i> Transalte`
    );
    $("#ColumnTransalte").hide();
};
