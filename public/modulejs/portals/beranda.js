var map;
require([
    "esri/map",
    "esri/dijit/PopupTemplate",
    "esri/layers/FeatureLayer",
    "dojo/_base/array",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/geometry/Geometry",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/PictureMarkerSymbol",
    "esri/Color",
    "esri/config",
    "esri/urlUtils",
    "esri/InfoTemplate",
    "esri/dijit/BasemapGallery",
    "esri/arcgis/utils",
    "dojo/parser",

    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dijit/TitlePane",
    "dojo/domReady!",
], function (
    Map,
    PopupTemplate,
    FeatureLayer,
    arrayUtils,
    ArcGISDynamicMapServiceLayer,
    Geometry,
    Point,
    webMercatorUtils,
    Graphic,
    SimpleMarkerSymbol,
    SimpleLineSymbol,
    SimpleFillSymbol,
    PictureMarkerSymbol,
    Color,
    esriConfig,
    urlUtils,
    InfoTemplate,
    BasemapGallery,
    arcgisUtils,
    parser
) {
    map = new Map("map", {
        basemap: "dark-grey",
        center: [117.62527, -1.4509444],
        zoom: 5,
    });

    //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
    var basemapGallery = new BasemapGallery(
        {
            showArcGISBasemaps: true,
            map: map,
        },
        "basemapGallery"
    );
    basemapGallery.startup();

    basemapGallery.on("error", function (msg) {
        console.log("basemap gallery error:  ", msg);
    });

    //add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
    var basemapGallery = new BasemapGallery(
        {
            showArcGISBasemaps: true,
            map: map,
        },
        "basemapGallery"
    );
    basemapGallery.startup();

    basemapGallery.on("error", function (msg) {
        console.log("basemap gallery error:  ", msg);
    });
    var title1 =
        '<div align="center">Batas Wilayah Provinsi <br><a id="remove" style="" /profil/detail/{kw}/id" target="_self" ><span>Click Detail</span></a> </div>';
    var isi1 =
        '<div style="float:left;"  >Provinsi :  </div><dd style="margin-left:60px;  ; "class="border-bottom pb-2"> {wa}</dd';
    var popupprov = new PopupTemplate({
        title: title1,
        description: isi1,
    });

    var featureLayerprovinsi = new FeatureLayer(
        "https://regionalinvestment.bkpm.go.id/gis/rest/services/Administrasi/batas_wilayah_provinsi/MapServer/0",
        {
            outFields: ["*"],

            infoTemplate: popupprov,
        }
    );
    map.addLayer(featureLayerprovinsi);
});
