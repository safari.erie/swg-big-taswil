<?php

use App\Http\Controllers\AdminApplController;
use App\Http\Controllers\PeluangController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PortalController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AdminMasterKbliController;
use App\Http\Controllers\MasterKomoditiController;
use App\Http\Controllers\MasterPeluangController;
use App\Http\Controllers\MasterSumberdataController;
use App\Http\Controllers\AdminUmkmController;
use App\Http\Controllers\ApplTasksController;
use App\Http\Controllers\KawasanIndustriController;
use App\Http\Controllers\ProfileDaerahController;
use App\Http\Controllers\ReferensiController;
use App\Http\Controllers\SarprasController;
use App\Http\Controllers\UmkmController;
use App\Http\Controllers\UmkmsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WilayahAdministrasiController;
use App\Http\Controllers\WilayahAdmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/',[AuthController::class,'showFormLogin'])->name('login');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin',[AdminDashboardController::class,'index'])->name('admin');


    Route::get('/api/Umkm/GetPerusahaans',[UmkmsController::class,'GetPerusahaans']);
    Route::get('/api/Umkm/GetPerusahaanCabangs',[UmkmsController::class,'GetPerusahaanCabangs']);
    Route::post('/Umkm/AddPerusahaanUmkm',[UmkmsController::class,'AddPerusahaanUmkm']);
    Route::post('/Umkm/AddPerusahaanCabangUmkm',[UmkmsController::class,'AddPerusahaanCabangUmkm']);
    Route::post('/Umkm/AddPerusahaanProduk',[UmkmsController::class,'AddPerusahaanProduk']);
    Route::post('/api/Umkm/AddPerusahaanKebutuhanProdukKbli',[UmkmsController::class,'AddPerusahaanKebutuhanProdukKbli']);
    Route::post('/Umkm/EditPerusahaanUmkm',[UmkmsController::class,'EditPerusahaanUmkm']);
    Route::get('/umkm/perusahaan',[UmkmController::class,'UmkmPerusahaan'])->name('UmkmPerusahaan');
    Route::get('/umkm/umkm',[UmkmController::class,'Umkm'])->name('Umkm');
    Route::get('/umkm/laporan-umkm',[UmkmController::class,'LaporanUmkm'])->name('LaporanUmkm');
    Route::get('/umkm/perusahaan/detail',[AdminUmkmController::class,'perusahaanDetail'])->name('perusahaanDetail');
    Route::get('/umkm/perusahaan/detail_kbli',[AdminUmkmController::class,'perusahaanDetailKbli'])->name('perusahaanDetailKbli');
    Route::get('/umkm/perusahaan/detail_pekerjaan',[AdminUmkmController::class,'perusahaanDetailPekerjaan'])->name('perusahaanDetailPekerjaan');
    Route::get('/umkm/pekerjaan',[AdminUmkmController::class,'formUmkmPekerjaan'])->name('umkmPekerjaan');
    Route::get('/master/provinsi',[WilayahAdmController::class,'formListProvinsi'])->name('provinsi');
    Route::get('/master/kabupaten',[WilayahAdmController::class,'formListKabupaten'])->name('kabupaten');
    Route::get('/master/kecamatan',[WilayahAdmController::class,'formListKecamatan'])->name('kecamatan');
    Route::get('/master/umkm',[AdminUmkmController::class,'formMasterUmkm'])->name('umkm');

    /*
        Route User Pengguna

    */

    Route::get('/User/UserPengguna',[UserController::class,'userPengguna'])->name('pengguna');
    Route::get('/api/Users/GetUsers',[UserController::class,'GetUsers']);
    Route::post('/api/Users/AddUser',[UserController::class,'AddUser']);


    /* permission */
    Route::get('/api/ApplTask/GetPermission',[AdminApplController::class,'GetPermission']);
    Route::get('/appl/hak_akses',[AdminApplController::class,'formPermission']);

    /* Referensi  */
    Route::get('/Referensi/Provinsi',[ReferensiController::class,'Provinsi']);
    Route::get('/Referensi/Kabupaten',[ReferensiController::class,'Kabupaten']);
    Route::get('/Referensi/KategoriKawasan',[ReferensiController::class,'KategoriKawasan']);

    /* Profile Daerah */
    Route::get('/ProfilDaerah/DemografiProvinsi',[ProfileDaerahController::class,'DemografiProvinsi']);
    Route::get('/ProfilDaerah/UmrProvinsi',[ProfileDaerahController::class,'UmrProvinsi']);
    Route::get('/ProfilDaerah/PdrbProvinsi',[ProfileDaerahController::class,'PdrbProvinsi']);
    Route::get('/ProfilDaerah/DemografiKabKot',[ProfileDaerahController::class,'DemografiKabKot']);
    Route::get('/ProfilDaerah/UmrKabkot',[ProfileDaerahController::class,'UmrKabkot']);
    Route::get('/ProfilDaerah/PdrbKabkot',[ProfileDaerahController::class,'PdrbKabkot']);

    /* Sar Pras */
    Route::get('/SarPras/Bandara',[SarprasController::class,'Bandara']);
    Route::get('/SarPras/Pelabuhan',[SarprasController::class,'Pelabuhan']);
    Route::get('/SarPras/Pendidikan',[SarprasController::class,'Pendidikan']);
    Route::get('/SarPras/RumahSakit',[SarprasController::class,'RumahSakit']);
    Route::get('/SarPras/Hotel',[SarprasController::class,'Hotel']);
    Route::get('/SarPras/Blk',[SarprasController::class,'Blk']);

    /* Master */
    Route::get('/master/kawasan_industri',[KawasanIndustriController::class,'index'])->name('MasterKawasanIndustri');
    Route::get('/master/inisial_kawasan_industri',[KawasanIndustriController::class,'InisialKawasanIndustri'])->name('MasterInisialKawasanIndustri');

    Route::post('/api/appl/change_permission',[AdminApplController::class,'ChangePermission']);


    Route::get('/appl/role_setting',[AdminApplController::class,'formListRole'])->name('role');
    Route::get('/appl/menu_setting',[AdminApplController::class,'formListMenu'])->name('menu');
    Route::get('/peluang/peluang',[PeluangController::class,'peluang'])->name('peluang');
    Route::get('/master/kbli',[AdminMasterKbliController::class,'formListKbli'])->name('masterKbli');
    Route::get('/master/komoditi',[MasterKomoditiController::class,'Master_Komoditi'])->name('komoditi');
    Route::get('/master/sektor_komoditi',[MasterKomoditiController::class,'Master_Sektor_Komoditi'])->name('sektor_komoditi');
    Route::get('/master/sektor_peluang',[MasterPeluangController::class,'Master_sektor_peluang'])->name('Peluang');
    Route::get('/master/peluang',[MasterPeluangController::class,'Master_peluang'])->name('Peluang');
    Route::get('/master/sumber_data',[MasterSumberdataController::class,'Master_sumber_data'])->name('sumber_data');
    Route::get('/appl/appl_setting',[AdminApplController::class,'formApplSetting'])->name('aplikasiSetting');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

});

