<?php
use App\Http\Controllers\AdminPeluangController;
use App\Http\Controllers\AdminUmkmsController;
use App\Http\Controllers\ApplTasksController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PeluangController;
use App\Http\Controllers\UmkmController;
use App\Http\Controllers\UmkmsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WilayahAdministrasiController;
use App\Repository\ApplTaskRepository;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/WilayahAdm/provinsis',[WilayahAdministrasiController::class,'provinsis']);
Route::get('/WilayahAdm/KabKota',[WilayahAdministrasiController::class,'GetKabKots']);
//Route::get('/Users/GetUsers',[UsersController::class,'GetUsers']);
Route::get('/Roles/GetRoles',[RolesController::class,'GetRoles']);
Route::post('/Roles/AddRole',[RolesController::class,'AddRole']);
Route::get('/Roles/GetRole',[RolesController::class,'GetRole']);
Route::delete('/Roles/DeleteRole',[RolesController::class,'DeleteRole']);
Route::put('/Roles/UpdateRole',[RolesController::class,'UpdateRole']);
Route::get('/ApplTask/GetApplTasks',[ApplTasksController::class,'GetApplTasks']);
Route::get('/ApplTask/GetApplTaskById',[ApplTasksController::class,'GetApplTaskById']);
Route::get('/ApplTask/GetApplTaskByRole',[ApplTasksController::class,'GetApplTaskByRole']);
Route::post('/ApplTask/AddTasksRole',[ApplTasksController::class,'AddTasksRole']);
Route::get('/ApplTask/GetAppls',[ApplTasksController::class,'GetAppls']);
Route::post('/ApplTask/AddApplTask',[ApplTasksController::class,'AddApplTask']);
Route::get('/ApplSetting/GetApplSettings',[ApplTasksController::class,'GetApplSettings']);


/*
Umkm Module
*/
Route::get('/Umkm/GetPerusahaan',[UmkmsController::class,'GetPerusahaan']);
Route::post('/Umkm/DeletePerusahaan',[UmkmsController::class,'DeletePerusahaan']);
Route::get('/Umkm/GetPerusahaanKblis',[UmkmsController::class,'GetPerusahaanKblis']);
Route::get('/Umkm/GetKategoriKblis',[UmkmsController::class,'GetKategoriKblis']);
Route::get('/Umkm/GetKelompokKblis',[UmkmsController::class,'GetKelompokKblis']);
Route::get('/Umkm/GetKelompokKbliParents',[UmkmsController::class,'GetKelompokKbliParents']);
Route::get('/Umkm/GetKbliByIdParents',[UmkmsController::class,'GetKbliByIdParents']);
Route::post('/Umkm/AddPerusahaanKbli',[UmkmsController::class,'AddPerusahaanKbli']);
Route::get('/Umkm/GetPerusahaanProduk',[UmkmsController::class,'GetPerusahaanProduk']);
Route::get('/Umkm/GetPerusahaanKbliByIdPerusahaans',[UmkmsController::class,'GetPerusahaanKbliByIdPerusahaans']);
Route::get('/Umkm/GetPerusahaanKebutuhanProdukKblis',[UmkmsController::class,'GetPerusahaanKebutuhanProdukKblis']);
/*
Route::post('/Umkm/EditPerusahaan',[AdminUmkmsController::class,'EditPerusahaan']);
Route::post('/Umkm/DeletePerusahaan',[AdminUmkmsController::class,'DeletePerusahaan']);
Route::resource('UmkmResource', AdminUmkmsController::class);
Route::get('/Umkm/GetPerusahaanDetils',[AdminUmkmsController::class,'GetPerusahaanDetils']);
Route::get('/Umkm/GetPerusahaanDetil',[AdminUmkmsController::class,'GetPerusahaanDetil']);
Route::post('/Umkm/AddPerusahaanDetil',[AdminUmkmsController::class,'AddPerusahaanDetil']);
Route::post('/Umkm/EditPerusahaanDetil',[AdminUmkmsController::class,'EditPerusahaanDetil']);
Route::post('/Umkm/DeletePerusahaanDetail',[AdminUmkmsController::class,'DeletePerusahaanDetail']);
Route::post('/Umkm/AddPerusahaanDetilKbli',[AdminUmkmsController::class,'AddPerusahaanDetilKbli']);
Route::post('/Umkm/EditPerusahaanDetilKbli',[AdminUmkmsController::class,'EditPerusahaanDetilKbli']);
Route::post('/Umkm/DeletePerusahaanDetilKbli',[AdminUmkmsController::class,'DeletePerusahaanDetilKbli']);
Route::get('/Umkm/GetPerusahaanDetilKbli',[AdminUmkmsController::class,'GetPerusahaanDetilKbli']);
Route::get('/Umkm/GetPerusahaanDetilKbliById',[AdminUmkmsController::class,'GetPerusahaanDetilKbliById']);
Route::get('/Umkm/GetUmkms',[AdminUmkmsController::class,'GetUmkms']);
Route::get('/Umkm/GetUmkmKblis',[AdminUmkmsController::class,'GetUmkmKblis']);
Route::post('/Umkm/AddUmkm',[AdminUmkmsController::class,'AddUmkm']);
Route::post('/Umkm/AddUmkmKbli',[AdminUmkmsController::class,'AddUmkmKbli']); */

/* Pekerjaan */
Route::get('/Umkm/GetPekerjaans',[AdminUmkmsController::class,'GetPekerjaans']);
Route::get('/Umkm/GetPekerjaan',[AdminUmkmsController::class,'GetPekerjaan']);
Route::post('/Umkm/AddPerusahaanDetailPekerjaan',[AdminUmkmsController::class,'AddPerusahaanDetailPekerjaan']);
Route::post('/Umkm/EditPerusahaanDetailPekerjaan',[AdminUmkmsController::class,'EditPerusahaanDetailPekerjaan']);
Route::post('/Umkm/DeletePerusahaanDetailPekerjaan',[AdminUmkmsController::class,'DeletePerusahaanDetailPekerjaan']);

/* Master KBLI Module */
Route::get('/Umkm/GetKblis',[AdminUmkmsController::class,'GetKblis']);
Route::post('/Umkm/AddKbli',[AdminUmkmsController::class,'AddKbli']);

/* Umkm Pekerjaan*/
Route::get('/Umkm/GetUmkmPekerjaans',[AdminUmkmsController::class,'GetUmkmPekerjaans']);


/*
Peluang Module
*/
Route::post('/Peluang/AddPeluang',[PeluangController::class,'AddPeluang']);
Route::post('/Peluang/DeletePeluang',[PeluangController::class,'DeletePeluang']);
Route::get('/Peluang/GetPeluang',[PeluangController::class,'GetPeluang']);
Route::get('/Peluang/Getdaerah',[PeluangController::class,'Getdaerah']);
Route::get('/Peluang/Getsektor',[PeluangController::class,'Getsektor']);
Route::get('/Peluang/Getsumber',[PeluangController::class,'Getsumber']);
Route::post('/Peluang/updatePeluang',[PeluangController::class,'updatePeluang']);
Route::post('/Peluang/summernote',[PeluangController::class,'upload_summernote']);
Route::post('/Peluang/remove_summernote',[PeluangController::class,'remove_summernote']);
