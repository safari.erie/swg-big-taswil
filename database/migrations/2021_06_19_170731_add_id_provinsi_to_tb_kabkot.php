<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdProvinsiToTbKabkot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('umkm.tb_kabkot', function (Blueprint $table) {
            //
              $table->unsignedBigInteger('id_provinsi');
            $table->foreign('id_provinsi')->references('id_provinsi')->on('umkm.tb_provinsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* Schema::table('umkm.tb_kabkot', function (Blueprint $table) {
            //
             $table->dropForeign(['id_provinsi']);
            $table->dropColumn(['id_provinsi']);
        }); */
    }
}
