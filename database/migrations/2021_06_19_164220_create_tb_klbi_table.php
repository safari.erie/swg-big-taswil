<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKlbiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umkm.tb_klbi', function (Blueprint $table) {
            $table->increments('id_klbi',11);
            $table->char('kd_kategori',2);
            $table->char('kd_gol_pokok',3);
            $table->char('kd_gol',4);
            $table->char('kd_sub_gol',5);
            $table->char('kd_klbi',5);
            $table->string('nm_klbi',255);
            $table->string('des_klbi',255);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umkm.tb_klbi');
    }
}
